$(document).ready(function(){
    $(".phone-mask").mask("+38?(999) 999-99-99");
    $('#gallery .owl-carousel').owlCarousel({
    loop:true,
    margin:30,
    singleItem:true,
    onInitialized: addButtonAll,
    navText: ["<span class='hubicon-arrow'></span>","<span class='hubicon-arrow'></span>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            margin:0,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
});

    function addButtonAll() {
        $('#gallery .owl-nav .owl-prev').after("<button id='showallgallery'>См. все</button>");
    }

    $("#showallgallery").click(function() {
        $("#gallery .owl-carousel").toggleClass("show_all");

        if($("#gallery .owl-carousel").hasClass( "show_all" )) {
            $("#showallgallery").text("Свернуть");
        } else {
            $("#showallgallery").text("См. все");
        }
    });

    $('#about-us .content').each(function() {
        var h = $(this).height();
        $(this).height(h).addClass('no-more');
    });

    $("#show-more-text").click(function() {
        $("#about-us .content").toggleClass('no-more');
        if($("#about-us .content").hasClass( "no-more" )) {
            $("#show-more-text").text("Показать больше");
        } else {
            $("#show-more-text").text("Скрыть");
        }
    });
});

$(window).on('load', function () {
    var $preloader = $('.new_preloader');
    $preloader.delay(500).fadeOut('slow');
});

$("a[href^='#']").on("click", function (event) {
    if($(this).attr('data-toggle') == 'pill')
        return;
    event.preventDefault();
    var id = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});    

$("button[data-scroll^='#']").on("click", function (event) {
    event.preventDefault();
    var id = $(this).attr('data-scroll'),
        top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});

$("button[data-location^='#']").on("click", function (event) {
    event.preventDefault();
    var location = $(this).attr('data-location'),
        top = $($(this).attr('data-scroll')).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
    console.log('.nav-tabs a[href="' + location + '"]');
    $('#locations .tab-buttons a[href="' + location + '"]').tab('show');
});

$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 100040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});