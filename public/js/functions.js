$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*
    mask
     */
    // if ($('.js-phone').length > 0) {
    //     $('.js-phone').inputmask({"mask": "+99(999)999-9999"})
    // }



    // наполняю модалку
    $('.victory').on('click', function () {
        var popup = $('.window-popup');
        $(popup).find('.js-regard-title').text($(this).data('title'));
        $(popup).find('.js-regard-description').html($(this).data('description'));
        $(popup).find('img').attr('src', $(this).data('image'));
        $(popup).find('.js-fancybox').attr('href', $(this).data('image'));
        $(popup).addClass('active');
    });

    $('.window-popup-close, #hide-popup').on('click', function (e) {
        $('.window-popup').removeClass('active');
    });

    //  смена города
    $('.js_change-city').closest('.select').find('select').on("click", function () {
        var _this = $(this);
        $.ajax({
            url: $('.js_change-city').data('route'),
            type: "POST",
            data: {
                'city': _this.val(),
            },
            success: function (data) {
                window.location.reload();
            }
        })
    });


    /*
     форма отзыва
     */
    $('.js-review-form').on('submit', function () {
        $('.js_error_area').addClass('hidden').empty();
        var _this = $(this),
            formData = new FormData($(this)[0]);
        $.ajax({
            contentType: false,
            url: $(_this).attr('action'),
            data: formData,
            type: 'POST',
            cache: false,
            processData: false,
            error: function (responce) {
                showErrors(_this, responce);
            },
            success: function (response) {
                $(_this)[0].reset();
                popup(response.title, response.message)
            }
        });
        return false;
    })

    /*
     все формы
     */
    $('form.js-form').on('submit', function () {
        var _this = $(this);
        $(_this).find('.js-error-area').hide().empty();
        $.ajax({
            url: $(_this).attr('action'),
            data: $(_this).serialize(),
            type: 'POST',
            error: function (responce) {
                showErrors(_this, responce);
            },
            success: function (response) {
                var emailInput = $(_this).find("input[name='email']");
                if ($(emailInput).length > 0) {
                    ga('send', 'event', {
                        'eventCategory': 'category',
                        'eventAction': 'action',
                        'eventLabel': $(emailInput).val(),
                    });
                }
                if ($(_this).closest('.modal').length > 0) {
                    $(_this).closest('.modal').hide();
                }
                if ($(_this).is('#feedbackForm') || $(_this).is('#courseForm') || $(_this).is('#form')) {
                    fbq('track', 'CompleteRegistration');
                }
                $(_this)[0].reset();
                if ($('#modalAnswer').length > 0) {
                    popup(response.title, response.message);
                } else {
                    $(_this).find('.js-error-area').empty().show();
                    $('.js-error-area').append('<p class="alert alert-success">' + response.title + '</p>');
                }
            }
        });

        return false;
    })

    /*
     * fb-event - Lead
     */
    $(document).on('click', '.js-create-lead', function () {
        fbq('track', 'Lead');
    })

    /*
     * fb-event - Lead
     */
    $(document).on('click', '.js-complete-lead', function () {
        fbq('track', 'CompleteRegistration');
    })

    $(document).on('click', '.blog-info__like, .blog-info__dislike', function () {
        var _this  = $(this),
            parent = $(_this).closest('[data-id]');
        $.ajax({
            url    : window.custom_var.routeLikes,
            data   : {
                id   : $(parent).data('id'),
                field: $(_this).hasClass('blog-info__like') ? 'likes' : 'dislikes'
            },
            type   : 'POST',
            success: function (response) {
                $(parent).find('.blog-info__like, .blog-info__dislike').each(function (i, el) {
                    $(el).find('.js-result').html($(el).hasClass('blog-info__like') ? response.likes : response.dislikes);
                });
            }
        });
    })

})

/*
 инфо-модлка
 */
function popup(_title, _message) {
    $('#modalAnswer .js-title').text(_title);
    $('#modalAnswer .js-message').text(_message);
    $('#modalAnswer').show().css("display", "flex");
    $('.navigate').addClass('navigate--row');
    $('body.course').addClass('no-scroll')
}

/*
ошибки
 */
function showErrors(_this, responce) {
    if (responce && typeof responce != 'undefined') {
        var errors = responce.responseJSON;
        if (typeof errors != 'undefined' && typeof errors.errors != 'undefined') {
            $(_this).find('.js-error-area').empty().show();
            Object.keys(errors.errors).forEach(function (key) {
                var val = errors.errors[key];
                $('.js-error-area').append('<p class="alert alert-danger">' + val + '</p>');
            });
        }
    }
}
