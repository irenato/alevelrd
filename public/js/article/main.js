(function () {
    "use strict";
    var swiper5;
    var swiper4;
    var body = document.querySelector('body'),
        isMobile = false,
        scrollTopPosition,
        browserYou,
        _winWidth = $(window).outerWidth();

    var genFunc = {

        initialized: false,
        initialize: function () {

            if (this.initialized) return;

            this.initialized = true;

            this.build();
        },

        build: function () {
            // browser
            browserYou = this.getBrowser();
            if (browserYou.platform == 'mobile') {
                isMobile = true;
                document.documentElement.classList.add('mobile');
            } else {
                document.documentElement.classList.add('desktop');
            }
            if ((browserYou.browser == 'ie')) {
                document.documentElement.classList.add('ie');
            }
            if (navigator.userAgent.indexOf("Edge") > -1) {
                document.documentElement.classList.add('edge');
            }
            if (navigator.userAgent.search(/Macintosh/) > -1) {
                document.documentElement.classList.add('macintosh');
            }
            if ((browserYou.browser == 'ie' && browserYou.versionShort < 9) || ((browserYou.browser == 'opera' || browserYou.browser == 'operaWebkit') && browserYou.versionShort < 18) || (browserYou.browser == 'firefox' && browserYou.versionShort < 30)) {
                alert('Обновите браузер');
            }
            if (document.querySelector('.yearN') !== null) {
                this.copyright();
            }
        },
        copyright: function () {
            var yearBlock = document.querySelector('.yearN'),
                yearNow = new Date().getFullYear().toString();
            if (yearNow.length) {
                yearBlock.innerText = yearNow;
            }
        },
        getBrowser: function () {
            var ua = navigator.userAgent;
            var bName = function () {
                if (ua.search(/Edge/) > -1) return "edge";
                if (ua.search(/MSIE/) > -1) return "ie";
                if (ua.search(/Trident/) > -1) return "ie11";
                if (ua.search(/Firefox/) > -1) return "firefox";
                if (ua.search(/Opera/) > -1) return "opera";
                if (ua.search(/OPR/) > -1) return "operaWebkit";
                if (ua.search(/YaBrowser/) > -1) return "yabrowser";
                if (ua.search(/Chrome/) > -1) return "chrome";
                if (ua.search(/Safari/) > -1) return "safari";
                if (ua.search(/maxHhon/) > -1) return "maxHhon";
            }();

            var version;
            switch (bName) {
                case "edge":
                    version = (ua.split("Edge")[1]).split("/")[1];
                    break;
                case "ie":
                    version = (ua.split("MSIE ")[1]).split(";")[0];
                    break;
                case "ie11":
                    bName = "ie";
                    version = (ua.split("; rv:")[1]).split(")")[0];
                    break;
                case "firefox":
                    version = ua.split("Firefox/")[1];
                    break;
                case "opera":
                    version = ua.split("Version/")[1];
                    break;
                case "operaWebkit":
                    bName = "opera";
                    version = ua.split("OPR/")[1];
                    break;
                case "yabrowser":
                    version = (ua.split("YaBrowser/")[1]).split(" ")[0];
                    break;
                case "chrome":
                    version = (ua.split("Chrome/")[1]).split(" ")[0];
                    break;
                case "safari":
                    version = ua.split("Safari/")[1].split("")[0];
                    break;
                case "maxHhon":
                    version = ua.split("maxHhon/")[1];
                    break;
            }
            var platform = 'desktop';
            if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())) platform = 'mobile';
            var browsrObj;
            try {
                browsrObj = {
                    platform: platform,
                    browser: bName,
                    versionFull: version,
                    versionShort: version.split(".")[0]
                };
            } catch (err) {
                browsrObj = {
                    platform: platform,
                    browser: 'unknown',
                    versionFull: 'unknown',
                    versionShort: 'unknown'
                };
            }
            return browsrObj;
        },
    };
    genFunc.initialize();

    /*sticky sidebar start */
    var _wW = $(window).width();
    var stickySidebar = $('.sticky');

    if (stickySidebar.length > 0 && _wW > 1030) {
        var stickyHeight = stickySidebar.height(),
            headerHeight = $('header').outerHeight() + 10,
            sidebarTop = stickySidebar.offset().top;
    }

    $(window).scroll(function () {
        if (stickySidebar.length > 0 && _wW > 1030) {
            var scrollTop = $(window).scrollTop() + headerHeight;

            if (sidebarTop < scrollTop) {
                stickySidebar.css('top', scrollTop - sidebarTop);

                var sidebarBottom = stickySidebar.offset().top + stickyHeight,
                    stickyStop = $('.sticky-container').offset().top + $('.sticky-container').height();
                if (stickyStop < sidebarBottom) {
                    var stopPosition = $('.sticky-container').height() - stickyHeight;
                    stickySidebar.css('top', stopPosition - 10);
                }
            } else {
                stickySidebar.css('top', '0');
            }
        }
    });

    $(window).resize(function () {
        if (stickySidebar.length > 0 && _wW > 1030) {
            stickyHeight = stickySidebar.height();
        }
    });
    /*sticky sidebar end */

    $(document).on("click", ".js_validate button[type=submit], .js_validate input[type=submit]", function () {
        var valid = validate($(this).parents(".js_validate"));
        if (valid == false) {
            return false;
        }
    });

    /*Function for same height*/

    window.addEventListener('load', function () {
        heightBlock();
    });
    window.addEventListener('orientationchange', function () {
        heightBlock();
    });
    $(window).on('resize', function () {
        heightBlock();
    });

    function heightBlock() {
        $('.js_height-block').each(function (i, e) {
                var elH = e.getElementsByClassName("height");
                var maxHeight = 0;
                for (var i = 0; i < elH.length; ++i) {
                    elH[i].style.height = "";
                    if (maxHeight < elH[i].clientHeight) {
                        maxHeight = elH[i].clientHeight;
                    }
                }
                for (var i = 0; i < elH.length; ++i) {
                    elH[i].style.height = maxHeight + "px";
                }
            }
        )
    }

    /*Function for same height end*/

    /*Function validate*/
    function validate(form) {
        var error_class = "error";
        var norma_class = "pass";
        var item = form.find("[required]");
        var e = 0;
        var reg = undefined;
        var pass = form.find('.password').val();
        var pass_1 = form.find('.password_1').val();
        var email = false;
        var password = false;
        var phone = false;

        function mark(object, expression) {
            if (expression) {
                object.parents('.input-field').addClass(error_class).removeClass(norma_class).find('.error').show();
                e++;
            } else
                object.parents('.input-field').addClass(norma_class).removeClass(error_class).find('.error').hide();
        }

        form.find("[required]").each(function () {
            switch ($(this).attr("data-validate")) {
                case undefined:
                    mark($(this), $.trim($(this).val()).length == 0);
                    break;
                case "email":
                    reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
                case "phone":
                    reg = /[0-9 -()+]{10}$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
                case "pass":
                    password = true;
                    reg = /^[a-zA-Z0-9_-]{6,}$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    password = false;
                    break;
                case "pass1":
                    mark($(this), pass_1 !== pass || $.trim($(this).val()).length === 0);
                    break;
                default:
                    reg = new RegExp($(this).attr("data-validate"), "g");
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
            }
        });
        e += form.find("." + error_class).length;
        if (e == 0)
            return true;
        else {
            $('.js_alert_error').show();
            setTimeout(function () {
                $('.js_alert_error').hide();
            }, 4000);
            form.find('.error input:first').focus();
            return false;
        }
    }

    /*Function validate end*/


    var video = {
        init: function () {
            video.play();
        },
        play: function () {
            $(document).on('click', '.video-poster', function (e) {
                video.setSrc(this);
            });
        },
        setSrc: function (el) {
            if (el) {
                var video_id = $(el).data('id');
                var url = 'https://www.youtube.com/embed/' + video_id + '?enablejsapi=1&version=3&showinfo=0&playerapiid=ytplayer&autoplay=1';
                $('.video-poster').hide();
                $('.icon-play').hide();
                $('iframe').attr('src', url).fadeIn();
            }
        }
    };
    video.init();
})();
