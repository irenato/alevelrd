/**
 * Created by macbook on 25.02.18.
 */
$(document).ready(function () {
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#photo_img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.js-action-enabled', function () {
        var _this = $(this);
        $.ajax({
            url     : disableActionRoute,
            type    : 'post',
            dataType: 'json',
            data    : {
                id               : $(_this).closest('.js-sortable-item').data('id'),
                _token           : $('meta[name="csrf-token"]').attr('content'),
                is_action_enabled: $(_this).prop('checked'),
            },
            success : function (res) {
                toastr.success("Изменения применены успешно");
            }
        })
    })

    $('.js-parent-form textarea').on('change, keypress, focusout', function () {
        var _this  = $(this),
            parent = $(this).closest('.js-parent-form');
        $.ajax({
            url     : $(parent).data('route'),
            type    : 'PUT',
            dataType: 'json',
            data    : {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "name"  : (_this).attr('name'),
                "value" : $(_this).val(),
            },
            success : function (res) {
                toastr.success("Редирект обновлен успешно.");
            }
        })
    })


    // выбранный элемент
    $('select[data-active]').each(function (i, el) {
        if ($(el).data('active') != '') {
            $(el).find('option[value="' + $(el).data('active') + '"]').attr('selected', 'selected');
        }
    })

    if ($('input[name=phone]').length > 0) {
        $('input[name=phone]').inputmask({"mask": "+99(999)999-9999"});
    }

    if ($('input.js-phone').length > 0) {
        $('input.js-phone').inputmask({"mask": "+99(999)999-9999"});
    }

    // preview images
    $("#photo").change(function () {
        readFile(this);
    });

    $('.js_form-link').on('click', function (e) {
        e.preventDefault();
        $('#photo').trigger('click');
    })


    $('.js_remove').on('click', function () {
        $('.js_destroy').trigger('click');
        return false;
    })

    // chosen
    if ($('.js_ch_multiple').length > 0) {
        $('.js_ch_multiple').chosen();
    }

    // adding new program to course
    $(document).on('click', '.js_add_item', function (e) {
        e.preventDefault();
        var parent = $(this).closest('.js_section').find('.js_parent'),
            item   = $(parent).find('.js_section.hidden').clone().removeClass('hidden');
        // $(item).find('textarea').addClass('ckeditor').each(function (i, el) {
        //     CKEDITOR.replace(el);
        // });
        if ($(item).find('.js-phone').length > 0) {
            $(item).find('.js-phone').inputmask({"mask": "+99(999)999-9999"})
        }
        $(parent).append(item);
    })

    // removing program from course
    $(document).on('click', '.js_remove_section', function (e) {
        e.preventDefault();
        var parent  = $(this).closest('.js_parent'),
            section = $(this).closest('.js_section');
        if ($(parent).find('.js_section').length > 2) {
            $(section).detach();
        } else {
            $(section).find('input:text').val('');
            $(section).find('textarea').text('');
        }

    })

    // preview thumbnail
    function readImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('img[data-img=' + $(input).data('img') + ']').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("input[data-img]").change(function () {
        readImg(this);
    });

    $('.js_image_link').on('click', function (e) {
        e.preventDefault();
        $('input[data-img=' + $(this).data('img') + ']').trigger('click');
    })

    // // add item
    $(document).on('click', '.js_add_item', function (e) {
        e.preventDefault();
        var item = $(this).closest('.js_section').clone();
        $(item).find('input').val('');
        $(this).closest('.js_parent').append(item);
    })

    // remove item
    $(document).on('click', '.js_remove_item', function (e) {
        e.preventDefault();
        if ($('.js_section').length >= 2) {
            $(this).closest('.js_section').detach();
        } else {
            $(this).closest('.js_section').find('input').val('');
        }
    })

    // upload images
    $("#upload-image").dropzone({
        url      : $('#upload-image').data('url'),
        headers  : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        addedfile: function (file) {
            console.log(file)
        },
        success  : function (file) {
            toastr.success("Изображение успешно загружено.");
            getImages();
        },
        error    : function (file) {
        }
    });

    // активность
    $(document).on('click', '[data-image-active]', function () {
        $.ajax({
            url     : $('#upload-image').data('url-active'),
            type    : 'POST',
            dataType: 'json',
            data    : {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                'active': $(this).prop('checked'),
                'id'    : $(this).attr('data-image-active')
            },
            success : function (res) {
                toastr.success("Активность изображения изменена.");
            }
        })
    })

    // Cover
    $(document).on('click', '[data-image-cover]', function () {
        $.ajax({
            url     : $('#upload-image').data('url-cover'),
            type    : 'POST',
            dataType: 'json',
            data    : {
                "_token"    : $('meta[name="csrf-token"]').attr('content'),
                'image_id'  : $('#gallery-image-modal input[name="image[id]"]').val(),
                'product_id': $('#upload-image').data('id'),
            },
            success : function (res) {
                toastr.success("Изображение стало превью товара.");
                $("#gallery-image-modal").modal('hide');
                getImages();
            }
        })
    });

    // Edit Modal
    $(document).on('click', '.gallery-env a[data-action="edit"]', function (ev) {
        ev.preventDefault();
        $("#gallery-image-modal").modal('show');
        $.ajax({
            url     : $('#upload-image').data('url-get-simple'),
            type    : 'POST',
            dataType: 'json',
            data    : {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                'id'    : $(this).parents('.col-md-3').find('input[type=hidden]').val()
            },
            success : function (res) {
                $('#gallery-image-modal input[name="image[title]"], #gallery-image-modal [name="image[alt]"]').val("");
                $('#gallery-image-modal input[name="image[title]"]').val(res.title);
                $('#gallery-image-modal [name="image[alt]"]').val(res.alt);
                $('#gallery-image-modal input[name="image[id]"]').val(res.id);
                $('#gallery-image-modal img').attr('src', res.src);
            }
        })
    });

    // save image
    $(document).on('click', '[data-image-save]', function () {
        $.ajax({
            url     : $('#upload-image').data('url-set'),
            type    : 'POST',
            dataType: 'json',
            data    : $('#gallery-image-modal form').serialize(),
            success : function (res) {
                toastr.success("Информация о изображении успешно обновлена.");
                $("#gallery-image-modal").modal('hide');
            }
        })
    });

    // delete image (modal)
    $(document).on('click', '.gallery-env a[data-action="trash"]', function (ev) {
        ev.preventDefault();
        $("#gallery-image-delete-modal").modal('show');
        _id = $(this).parents('.col-md-3').find('input[type=hidden]').val();
        $('#gallery-image-delete-modal [data-image-delete]').attr('data-image-delete', _id);
    });

    // delete image
    $(document).on('click', '[data-image-delete]', function () {
        $.ajax({
            url     : $('#upload-image').data('url-del'),
            type    : 'POST',
            dataType: 'json',
            data    : {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                'id'    : $(this).attr('data-image-delete')
            },
            success : function (res) {
                $("#gallery-image-delete-modal").modal('hide');
                toastr.success("Изображение удалено.");
                getImages();
            }
        })
        return false;
    });

    // sortable images

    if ($(".album-images").length > 0) {
        $('.album-images').sortable({
            update: function (event, ui) {
                var images = [];
                $('.js-image-item input[type=hidden]').each(function (i, el) {
                    images[i] = parseInt($(el).val());
                })
                if (images.length > 0)
                    $.ajax({
                        type : "POST",
                        url  : $('#upload-image').data('url-sort'),
                        cache: false,
                        data : {
                            "_token": $('meta[name="csrf-token"]').attr('content'),
                            "images": images
                        },
                    })
            }
        }).disableSelection();
    }
    if ($('.datetimepicker').length > 0) {
        $('.datetimepicker').datetimepicker({
            format: 'yyyy-mm-dd hh:ii:ss',
        });
    }
    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        });
    }


    // sortable for other
    if ($('.js-sortable').length > 0) {
        $('.js-sortable').sortable({
            update: function (event, ui) {
                var items = [];
                $('.js-sortable-item').each(function (i, el) {
                    items[i] = parseInt($(el).data('id'));
                })
                if (items.length > 0)
                    $.ajax({
                        type : "POST",
                        url  : $('.js-sortable').data('url'),
                        cache: false,
                        data : {
                            "_token": $('meta[name="csrf-token"]').attr('content'),
                            "items" : items
                        },
                    })
            }
        }).disableSelection();
    }

    // get images
    function getImages() {
        $.ajax({
            url     : $('#upload-image').data('url-get'),
            type    : 'POST',
            dataType: 'json',
            data    : {
                "_token": $('meta[name="csrf-token"]').attr('content')
            },
            success : function (res) {
                $(".album-images").html(res.images);
            }
        })
    }

    if ($("#upload-image").length > 0) {
        getImages();
    }
})