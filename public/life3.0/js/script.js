$(document).ready(function() {

	/* navWithoutFullPage */
	var navWithoutFullPageStart,
		navWithoutFullPageDestroy;

	navWithoutFullPageStart = function(){
		$('a[href^="#"]').click(function() {
			var objectTo = $(this).attr("href").slice(1);
			$("html, body").animate({
				scrollTop: $('[data-anchor='+ objectTo +']').offset().top + "px"
			}, {
				duration: 500,
				easing: "swing"
			});
			return false;
	    });
	}
	navWithoutFullPageDestroy = function(){
		$('a[href^="#"]').unbind("click");
	}

	/*fullpage*/
	if ($('#fullpage').length) {
		var fullPageCreated = false;
		createFullpage();

		function createFullpage() {
			if(fullPageCreated === false) {
				fullPageCreated = true;
				navWithoutFullPageDestroy();
				$('#fullpage').fullpage({
					autoScrolling: true,
					anchors: ['main', 'about', 'spikers', 'programm', 'timeline', 'partners', 'registration'],
					menu: '#navigation'
				});
			}
		}

		if(document.documentElement.clientWidth < 992 || document.documentElement.clientHeight < 641) {
			$.fn.fullpage.destroy('all');
			navWithoutFullPageStart();
		}

		$(window).resize(function() {
			if ( $(window).width() > 991 && $(window).height() > 640 ) {
				createFullpage();
			} else {
				if(fullPageCreated == true) {
					fullPageCreated = false;
					$.fn.fullpage.destroy('all');
					navWithoutFullPageStart();
				}
			}
		});
	}

	/**/
	$(".timeline [data-room]").hover(function(){
		var typeRoom =  $(this).data("room");
		$("#building ." + typeRoom + "-room").toggleClass('active');
 	});

	/**/
	function workshopBlockСontrol(){
		var flag = false;
		$("#workshopBlock input").each(function(){
		    if ($(this).is(':checked')){
		    	flag = true;
		    	return false;
		    }
		});
		if(flag){
			$("#registrationSubmit").prop('disabled',false);
		}else{
			$("#registrationSubmit").prop('disabled',true);
		}
	}

	$("#workshop").on('change', function(){
		$("#workshopBlock").toggleClass('active');
		if($("#workshop").is(':checked')){
			$("#registrationSubmit").prop('disabled',true);
			workshopBlockСontrol();
		}else{
			$("#registrationSubmit").prop('disabled',false);
		}
	});
	$("#workshopBlock input").on('change', function(){
		workshopBlockСontrol();
	});

	/* tooltip */
	var tooltipsterStartFlag = false;
	function tooltipsterStart(){
		if(tooltipsterStartFlag === false) {
			tooltipsterStartFlag = true;
			$('.tooltip').tooltipster({
			    contentCloning: true,
			    animation: 'fade',
		   		delay: 0,
		   		repositionOnScroll: true,
		   		trigger: 'hover',
		   		maxWidth: 400,
		   		minWidth: 300,
		   		interactive: true,
		   		distance: 10,
		   		delayTouch: [0, 0],
			});
		}
	}

	function tooltipsterInstedOf(){
		$('.box-programm .item').on('click', function(){
			$(this).children('.tooltip_templates').slideToggle().toggleClass('active');
		})
	}
	function tooltipsterInstedOfDestroy(){
		$('.box-programm .item').unbind("click");
	}

	if(document.documentElement.clientWidth > 991) {
		tooltipsterStart();
	}else{
		tooltipsterInstedOf();
	}

	$(window).resize(function() {
		if ( $(window).width() > 991 ) {
			tooltipsterStart();
			tooltipsterInstedOfDestroy();
		} else {
			if(tooltipsterStartFlag == true) {
				tooltipsterStartFlag = false;
				$('.tooltip').tooltipster('destroy');
				tooltipsterInstedOf();
			}
		}
	});

	/*  */
	$('.gallery-about').slick({
		infinite: true,
		slidesToShow: 3,
		autoplay: true,
		arrows: true,
		dots: false,
		draggable: true,
		speed: 500,
		vertical: true,
		verticalSwiping: true,
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
		      breakpoint: 1550,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2,
		        vertical: false,
				verticalSwiping: false,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        vertical: true,
				verticalSwiping: true,
		      }
		    },
		]
	});

	$('.carousel-speakers').slick({
		infinite: true,
		slidesToShow: 4,
		autoplay: false,
		arrows: true,
		dots: false,
		draggable: true,
		speed: 500,
		appendArrows: '.carousel-speakers-nav',
		prevArrow: $('.carousel-speakers-nav .slick-prev'),
		nextArrow: $('.carousel-speakers-nav .slick-next'),
		responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 575,
		      settings: {
		        slidesToShow: 1,
		      }
		    },
		]
	});

	$('.carousel-partners-general').slick({
		infinite: true,
		slidesToShow: 5,
		autoplay: false,
		arrows: true,
		dots: false,
		draggable: true,
		speed: 500,
		responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 575,
		      settings: {
		        slidesToShow: 1,
		      }
		    },
		]
	});

	$('.carousel-partners-premium').slick({
		infinite: true,
		autoplay: false,
		arrows: true,
		dots: false,
		draggable: true,
		speed: 500,
		slidesToShow: 5,
		rows: 2,
		responsive: [
		 	{
		      breakpoint: 1480,
		      settings: {
				rows: 1,
		      }
		    },
		    {
		      breakpoint: 1200,
		      settings: {
		      	rows: 1,
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		      	rows: 1,
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		      	rows: 1,
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 575,
		      settings: {
		      	rows: 1,
		        slidesToShow: 1,
		      }
		    },
		]
	});

	$('.carousel-partners-media').slick({
		infinite: true,
		slidesToShow: 8,
		autoplay: false,
		arrows: true,
		dots: false,
		draggable: true,
		speed: 500,
		responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 6,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 5,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 575,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		]
	});

	function tooltipster2Start(){
		$('.tooltip2').tooltipster({
		    contentCloning: true,
		    animation: 'fade',
	   		delay: 0,
	   		repositionOnScroll: true,
	   		trigger: 'hover',
	   		maxWidth: 250,
	   		minWidth: 250,
	   		interactive: true,
	   		distance: 0,
	   		delayTouch: [0, 0],
	   		theme: 'my-tooltipster'
		});
	}
	tooltipster2Start();

	$('.carousel-partners-general, .carousel-partners-premium').on('breakpoint', function(event, slick, breakpoint){
		tooltipster2Start();
	});

	$(".scroll").mCustomScrollbar({
		theme:"minimal"
	});

    $(".phone-mask").mask("+38?(999) 999-99-99");

    // $('#register').on('submit', function(e) {
    document.getElementById('registerclient').addEventListener('submit',function(e){
	e.preventDefault()

        var data = new FormData();

        data.append('name',$("input[name='name']").val());
        data.append('surname', $("input[name='surname']").val());
        data.append('phone', $("input[name='phone']").val());
        data.append('email', $("input[name='email']").val());

        if($("#workshop").is(':checked')) {
            if ($(".workshop1").prop("checked")) {
                data.append('workshop1', $('.workshop1').val());
            }
            if ($(".workshop2").prop("checked")) {
                data.append('workshop2', $('.workshop2').val());
            }
            if ($(".workshop3").prop("checked")) {
                data.append('workshop3', $('.workshop3').val());
            }
            if ($(".workshop4").prop("checked")) {
                data.append('workshop4', $('.workshop4').val());
            }
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            url: "project_register",
            data: data,
            contentType: false,
            processData: false,
            success: function (data) {

            	if(data == 'succes') {
                    $('#thankModal').modal('show')
                    if ("ga" in window) {
                        tracker = ga.getAll()[0];
                        if (tracker)
                            tracker.send("event", "project", "registration","projectlife");
                    }
                }
                	else {
                    $('#errorModal').modal('show')

                }
            }
        });

        return false;
    },false);

    // var deadline = 'October 13 2018 12:00: GMT+03:00';
    var deadline = '2018/10/13 12:00:00';

    function getTimeRemaining(endtime){
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor( (t/1000) % 60 );
        var minutes = Math.floor( (t/1000/60) % 60 );
        var hours = Math.floor( (t/(1000*60*60)) % 24 );
        var days = Math.floor( t/(1000*60*60*24) );
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }
    function initializeClock(id, endtime){
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        // var secondsSpan = clock.querySelector('.seconds');

        function updateClock(){
            var t = getTimeRemaining(endtime);
            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = t.hours;
            minutesSpan.innerHTML = t.minutes;
            // secondsSpan.innerHTML = t.seconds;
        }
        updateClock(); // запустите функцию один раз, чтобы избежать задержки
        var timeinterval = setInterval(updateClock,1000);
    }

    initializeClock('clockdiv', deadline);
    initializeClock('clockdiv2', deadline);

});