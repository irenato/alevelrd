const dirname = './';

module.exports = {
  src: {
    folder: `${dirname}src/`,
    favicon: {
      allFiles: `${dirname}/src/favicon/**.*`
    },
    assets: {
      allFiles: `${dirname}src/assets/**/*`
    },
    files: [
      `${dirname}src/fonts/**/*`
    ],
    manifest: {
      folder: `${dirname}src/manifest/`,
      allFiles: `${dirname}src/manifest/*.json`
    },
    img: {
      allFiles: `${dirname}src/img/**/*`,
      svg: `${dirname}src/sprite/**/*svg`
    },
    pug: {
      allFiles: [
        `${dirname}src/pug/modules/*.{pug,jade}`,
        `${dirname}src/pug/modules/common/*.{pug,jade}`,
        `${dirname}src/pug/modules/course/*.{pug,jade}`,
        `${dirname}src/pug/modules/course/inside/*.{pug,jade}`,
        `${dirname}src/pug/modules/home/blocks/*.{pug,jade}`,
        `${dirname}src/pug/modules/home/scenes/*.{pug,jade}`,
        `${dirname}src/pug/modules/home/scenes/about/*.{pug,jade}`,
        `${dirname}src/pug/modules/home/*.{pug,jade}`,
        `${dirname}src/pug/modules/testing/*.{pug,jade}`,
        `${dirname}src/pug/pages/*.{pug,jade}`,
        `${dirname}src/pug/*.{pug,jade}`
      ],
      pages: `${dirname}src/pug/pages/*.{pug,jade}`,
      data: `${dirname}src/pug/data.json`
    },
    sass: {
      allFiles: [
        `${dirname}src/sass/modules/*.{css,scss}`,
        `${dirname}src/sass/modules/course/*.{css,scss}`,
        `${dirname}src/sass/modules/home/*.{css,scss}`,
        `${dirname}src/sass/modules/testing/*.{css,scss}`,
        `${dirname}src/sass/modules/common/*.{css,scss}`,
        `${dirname}src/sass/modules/blog/*.{css,scss}`,
        `${dirname}src/sass/modules/common/window/*.{css,scss}`,
        `${dirname}src/sass/pages/*.{css,scss}`,
        `${dirname}src/sass/*.{css,scss}`
      ],
      files: {
        bootstrap: `${dirname}src/sass/bootstrap.{css,scss}`,
        bootstrapFiles: `${dirname}src/sass/libs/*/*.{css,scss}`,
        libs: `${dirname}src/sass/libs.{css,scss}`
      },
      pages: {
        files: `${dirname}src/sass/pages/*.{css,scss}`
      }
    },
    js: {
      allFiles: `${dirname}src/js/*.*`,
      allallFiles: `${dirname}src/js/**/*`
    },
    fonts: {
      allFiles: `${dirname}src/fonts/**/*`,
      woff: `${dirname}src/fonts/**/*.woff`,
      woff2: `${dirname}src/fonts/**/*.woff2`
    }
  },
  build: {
    folder: `../`,
    img: {
      folder: `../img/`
    },
    css: {
      folder: `../css`
    },
    js: {
      folder: `../js`,
      allFiles: `../js/*.*`
    },
    html: {
      allFiles: `../*.html`
    },
    fonts: {
      folder: `../fonts/`
    }
  }
};
