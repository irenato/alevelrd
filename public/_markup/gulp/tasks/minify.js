const gulp          = require('gulp');
const terser = require('gulp-terser');
const rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');


function es(){
    return gulp.src(['../js/index.js', '../js/landing.js', '../js/course.js', '../js/commons.js',  '!../js/*.min.js'])
        .pipe(terser())
        .pipe(rename(function (path) {
            path.extname = '.min.js';
        }))
        .pipe(gulp.dest('../js'));
}

gulp.task('minify', es);


gulp.task('minify-css', () => {
    // Folder with files to minify
    return gulp.src(['../css/*.css', '!../css/*.min.css'])
    //The method pipe() allow you to chain multiple tasks together
    //I execute the task to minify the files
        .pipe(cleanCSS())
        .pipe(rename(function (path) {
            path.extname = '.min.css';
        }))
        //I define the destination of the minified files with the method dest
        .pipe(gulp.dest('../css'));
});
