import {TimelineLite, TweenMax} from 'gsap'
import '../libs/datepicker';
import PubSub from 'pubsub-js'
import moment from 'moment'
import 'moment/locale/ru';

const $ = window.$;
$(function () {
  let dates = [];
  let events;

  $.fn.datepicker.dates['ru'] = {
    days: [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота'
    ],
    daysShort: ['Вск', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Суб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь'
    ],
    monthsShort: [
      'Янв',
      'Фев',
      'Мар',
      'Апр',
      'Май',
      'Июн',
      'Июл',
      'Авг',
      'Сен',
      'Окт',
      'Ноя',
      'Дек'
    ],
    today: 'Сегодня',
    clear: 'Очистить',
    weekStart: 1,
    monthsTitle: 'Месяцы'
  };

  let $events = $('#events');

  $('#dp')
    .datepicker({
      todayHighlight: true,
      language: 'ru',
      // multidate: true,
      minDate: new Date(),
      format: 'DD MMMM YYYY'
      // beforeShowDay: function (e) {
      //   return false
      // }
    }).on('changeDate', function (event) {
      var selected_date = event.date;

      var options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
      var weekDayOptions = {
        weekday: 'long'
      };
      $('.calendar__date').text(selected_date.toLocaleString('ru', options));
      $('.calendar__day').text(selected_date.toLocaleString('ru', weekDayOptions));
    });

  $('.datepicker-switch').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  if ($('.js-events').length > 0) {
    // test
    $.ajax({
      url: $('.js-events').data('route'),
      type: 'POST',
      success: function (data) {

        if (data.data) {
          events = data.data
          drawList(events)
          dates.push(new Date())
          $('#dp').datepicker('setDates', dates);
          $('.calendar__date').text(moment().format('DD MMMM YYYY'));
          $('.calendar__day').text(moment().format('dddd'));
        }
      }
    })
  }

  function drawList (events) {
    $events.empty()
    $.each(events, (index, event) => {
      let li = `<li data-date="${+new Date(event.begin)}" data-event="${index}" class="window-info__item" data-noscroll="true">
                <a class="one-block" data-noscroll="true" href="#">
                  <div class="one-block__left" data-noscroll="true">
                    <img data-noscroll="true" src=${event.smallImage} alt="event">
                  </div>
                  <div class="one-block__right" data-noscroll="true">
                    <div class="one-block__h3" data-noscroll="true">${event.title}</div>
                    <div class="one-block__date" data-noscroll="true">${moment(event.begin).format('DD MMMM YYYY')}</div>
                    <div class="one-block__p" data-noscroll="true">${event.description}</div>
                  </div>
                </a>
              </li>`;

      $events.append(li)
      dates.push(new Date(event.begin))
    });
    $('[data-event]').eq(0).addClass('active')
  }

  // Клик по превью события в списке справа
  $('#events').on('click', '.one-block', function () {
    if ($(this).hasClass('no-event')) return

    $('#events .window-info__item').removeClass('active');
    let number = $(this).parent().addClass('active').data('event')
    PubSub.publish('event', number)

    return false
  });

  // Обработка события event
  // Пример генерации события: PubSub.publish('event', 0)
  PubSub.subscribe('event', function (msg, number) {
    let event = events.filter((item, index) => index === number)[0];
    let $calendarBigImg = $('#calendar-big-img');

    TweenMax.to('.calendar', 1, {rotationY: '180'});
    TweenMax.to('.event', 1, {rotationY: '180'});

    $('.event__back').attr('href', event.fullLink);

    $('.js-go-to-event').attr('href', event.fullLink);
    if (event.is_external == 1) {
      $('.js-go-to-event').attr('target', '_blank');
    }
    $('.calendar__name').text(event.title);
    $('.calendar__description').text(event.filteredDescription);
    $('.calendar__back-day').text(moment(event.begin).format('dddd'));
    $('.calendar__back-date').text(moment(event.begin).format('DD MMMM YYYY'));

    $('.event').addClass('active');
    $('.calendar__front').hide();
    $('.calendar__back').fadeIn(800);
    $('.event__front').hide();
    $('.event__back').fadeIn(800).css({
      display: 'flex'
    });

    let tl = new TimelineLite();

    tl
      .to($calendarBigImg, 0.5, {
        filter: 'blur(20px)',
        transform: 'scale(1.1)'
      })
      .set($calendarBigImg, {
        attr: {
          src: event && event.bigImage
        }
      }).to($calendarBigImg, 0.5, {
        filter: 'blur(0)',
        transform: 'scale(1)'
      })
  });

  $('.calendar__goback').on('click', () => {
    TweenMax.to('.calendar', 1, {rotationY: '360'});
    TweenMax.to('.event', 1, {rotationY: '360'});
    $('.event').removeClass('active');
    $('.calendar__back').hide();
    $('.calendar__front').fadeIn(800);
    $('.event__back').hide();
    $('.event__front').fadeIn(800);
  });

  $('.datepicker').on('click', '.day', e => {
    const elem = e.target;
    const date = elem.getAttribute('data-date')

    let newList = events.filter(function (event) {
      return +new Date(event.begin) >= +date
    });

    if (newList.length) {
      drawList(newList)
    } else {
      let li = `<li class="window-info__item" data-noscroll="true">
                <span class="one-block no-event" data-noscroll="true" href="#">
                  Попробуйте поискать мероприятия в другие дни или просто приходите к нам в гости!
                </span>
              </li>`;

      $events.empty().append(li)
    }
  });
  if (!$('#events').children().length) {
    $('#events').append(`<li class="window-info__item" data-noscroll="true">
                <span class="one-block no-event" data-noscroll="true" href="#">
                  Попробуйте поискать мероприятия в другие дни или просто приходите к нам в гости!
                </span>
              </li>`);
    console.log('-------------', $('#events').children().length, $('#events').children());
  }
});
