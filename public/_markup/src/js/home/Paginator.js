import PubSub from 'pubsub-js';
import './PubSub/partner'
import './PubSub/tab'
import './PubSub/teacher'
import './start'
import './PubSub/hideAndShowScene'
import './PubSub/showScene'
import config from './config'

const $ = window.$;
class Paginator {
  constructor () {
    $('[data-window]').each((index, el) => {
      $(el).data('window', index)
    })

    $('.trigger').each((index, el) => {
      $(el).data('goto', index + 1)
    })

    this.height = $(document).height(); // Высота страницы
    this.countScenes = $('.layer').length; // Количество сцен
    this.numberOfScenes = this.countScenes;
    this.depthOfScene = this.height / this.numberOfScenes; // Глубина сцены
    this.activeScene = 0; // Активная сцена
    // При инициализации класса сразу вызываем следующие методы
    this.scrollEvents();
    this.pressEvents();
    // Обработка переходов сцены по кликам в меню
    this.menu();
    this.init();
    this.activeScenes();
    this.onHashChange();
  }

  init () {
    config.canGo = false;

    if (window.innerWidth < 1279) return
    $('.layer').hide()

    let sale = !!document.querySelector('.layer#sell');
    if (sale) {
      $('body').addClass('with-sale')
    }
    let scene = $(`[data-hash=${window.location.hash.substr(1) || 'home'}]`).data('window') || 0;

    if (scene !== 0) {
      $('.sale').hide()
    }

    // Переключение слайдов, генерация события
    PubSub.publish('hideAndShowScene', {
      from: this.activeScene,
      to: scene
    })

    this.activeScene = scene;
    this.activeScenes();
  }

  onHashChange () {
    let self = this;
    $(window).on('hashchange', (e) => {
      e.preventDefault()
      self.init()
    })
  }

  activeScenes () {
    if (this.activeScene === 2) {
      $('.goto__prev').fadeIn()
      $('.goto__next').fadeIn()
    } else {
      $('.goto__prev').fadeOut()
      $('.goto__next').fadeOut()
    }

    if (this.activeScene === 0) {
      $('.gotop').hide()
    } else if (this.activeScene === this.countScenes) {
      $('.gotop').show()
    } else {
      $('.gotop').hide()
    }

    if (window.innerWidth > 1279) {
      if (this.activeScene === 0 || this.activeScene === this.countScenes) {
        $('#menu').addClass('burger__black')
      } else {
        $('#menu').removeClass('burger__black')
      }
    }
  }

  menu () {
    const self = this;

    let $menuLink = $('.trigger, .start__btn, #stock, .sale__btn');

    // Клик по пункту меню
    $menuLink.on('click', function (e) {
      if (window.innerWidth > 1279) {
        e.preventDefault();
        let scene = $(this).data('goto');

        if (scene !== 0) {
          if (!scene) return;
        }

        if (self.activeScene === scene) return;
        if (!config.canGo) return;

        self.activeScene = scene;

        self.addHash(scene)
        self.activeScenes()
      }
    });

    $('.start__btn').on('click', function () {
      if (window.innerWidth <= 1279) {
        $('html, body').animate({
          scrollTop: $('.courses').offset().top
        }, 2000);
      }
    });
    $('.sale__btn').on('click', function () {
      if (window.innerWidth <= 1279) {
        $('html, body').animate({
          scrollTop: $('.js-sell').offset().top
        }, 2000);
      }
    })
  }

  addHash (scene) {
    let hash = $(`[data-window]`).eq(scene).data('hash') || 'home';
    window.location.hash = hash;
  }

  goTo (direction) {
    const self = this;

    $('.sale').hide()
    // Следующий слайд в зависимости от направления скрола
    let newScene = self.activeScene + direction;
    // Отключаем переключение дальше последнего слайда
    if (newScene > self.countScenes) {
      newScene = self.countScenes;
      if ($(`[data-window]`).eq(newScene).is(':visible')) return
    } else if (newScene < 0) {
      newScene = 0;
    }

    this.addHash(newScene)
    this.activeScenes()
  }

  event (e, direct) {
    let self = this;

    // End Prevent Scroll
    if (window.innerWidth <= 1279) return;
    // Prevent Scroll
    let area = e.target;
    if (area.dataset.noscroll) return;
    if ($(area).parents('#editor').data('noscroll')) return;
    if ($(area).parents('.noscroll').data('noscroll')) return
    if ($(area).parents('.menu__content').data('noscroll')) return
    if ($(area).parents('.menu-inside').data('noscroll')) return

    let delta = e.deltaY || e.detail || e.wheelDelta;

    if (delta < 0 && area.scrollTop === 0) window.addEventListener('touchstart', e => e.preventDefault(), { passive: false });

    if (delta > 0 && area.scrollHeight - area.clientHeight - area.scrollTop <= 1) window.addEventListener('touchstart', e => e.preventDefault(), { passive: false });

    e = e.originalEvent || e;
    let direction = direct || (e.deltaY > 0 ? 1 : -1); // Узнаем направление скрола вниз или вверх

    if ((self.activeScene + direction) > self.countScenes || (self.activeScene + direction) < 0) return;

    // Разрешен ли переход
    if (!config.canGo) return;
    self.goTo(direction)
  }

  scrollEvents () {
    let self = this;

    // Обработка скрола
    window.addEventListener('wheel', function (e) {
      self.event(e)
    })
  }

  pressEvents () {
    const self = this;

    $(document).on('keyup', function (e) {
      switch (e.keyCode) {
        case 40: // Если нажата стрелка вниз
          self.event(e, 1);
          break;
        case 38: // Если нажата стрелка вверх
          self.event(e, -1);
          break;
        case 39: // Если нажата стрелка вправо
          self.event(e, 1);
          break;
        case 37: // Если нажата стрелка влево
          self.event(e, -1);
          break;
        default:
          break;
      }
    })
  }
}

export default new Paginator()
