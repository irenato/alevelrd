import { TimelineLite } from 'gsap';

const $ = window.$;

$(function () {
  let scrolled = 0,
    isVisible = scrolled === 0,
    $start = $('.start');

  function start() {
    scrolled = $(window).scrollTop();
    if (scrolled > 0 && isVisible) {
      $('.triggers').fadeIn()
      isVisible = false;

      let tl = new TimelineLite();

      tl.fromTo($start, 0, {
        display: 'flex'
      }, {
        display: 'none'
      }, 0);
    } else if (!scrolled) {
      $('.triggers').fadeOut()
      $('.sale').show()
      $('body').addClass('with-sale')

      isVisible = true;

      let tl = new TimelineLite();

      tl.fromTo($start, 1, {
        display: 'none',
        y: -100
      }, {
        display: 'flex',
        y: 0
      }, 0);
    }
  }

  start();

  $(window).on('scroll', () => {
    if (window.innerWidth > 768) {
      start()
    } else {
      $start.show()
    }
  })
});
