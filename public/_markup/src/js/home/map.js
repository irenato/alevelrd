import GoogleMapsLoader from 'google-maps';
import PubSub from 'pubsub-js'

const $ = window.$;
let map;
// Местоположение: долгота, широта и коэффициент увеличения
let latitude = $('.map').data('lat'),
  longitude = $('.map').data('lng'),
  mapZoom = 17;

PubSub.subscribe('map', function () {
  GoogleMapsLoader.KEY = 'AIzaSyBk0TJN24WH7J7xRwq3fSFKprM8H68LyYU';
  GoogleMapsLoader.load(function (google) {
    // Адрес до иконки с маркером
    let markerUrl = '../img/markerMap.png';

    let mapOptions = {
      // Координаты
      center: new google.maps.LatLng(latitude, longitude),
      panControl: false,
      // draggable: false,
      zoomControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false,
      // styles: style,
      zoom: mapZoom,
      disableDefaultUI: true,
      gestureHandling: 'cooperative'
    };

    // Инициализация карты
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Маркер на карте
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude, longitude),
      map: map,
      visible: true,
      icon: markerUrl
    });
  });
})
