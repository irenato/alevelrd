import { TweenMax } from 'gsap';
const $ = window.$;
$(function () {
  let selectItem = $('.select.cities-id ');
  selectItem.each(function (i, e) {
    const _$elem = $(e);
    let $this = $(e).find('select'),
      numberOfOptions = $this.children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    let $styledSelect = $this.next('div.select-styled');

    let $list = $('<ul />', {
      'class': 'select-options'
    }).insertAfter($styledSelect);

    $styledSelect.text($this.children('option').eq(0).text());

    for (let i = 0; i < numberOfOptions; i++) {
      let val = $this.children('option').eq(i).val();
      if ($this.children('option').eq(i).attr('selected')) {
        $styledSelect.text($this.children('option').eq(i).text());
      }
      $('<li />', {
        rel: val,
        selected: $this.children('option').eq(i).attr('selected')
      }).prepend(`<i class="icon-${val}"/>`).append($('<span />', {
        text: $this.children('option').eq(i).text()
      })).appendTo($list);
    }

    let $listItems = $list.children('li');
    
    // setTimeout(() => {
    //
    // }, 1000);

    $styledSelect.click(function (e) {
      e.stopPropagation();
      let $this = $(this);
      $(e).find('.icon-after').toggleClass('icon-after--rotate');
      if ($this.hasClass('active')) {
        $this.toggleClass('active').next('ul.select-options').hide();
      } else {
        $this.toggleClass('active').next('ul.select-options').show(0);
        let _height = 0;
        _$elem.find('.select-options').css('height', _height);
        TweenMax.staggerFrom($('ul.select-options li'), 1, {
          scale: 0.5,
          opacity: 0,
          ease: Elastic.easeOut,
          force3D: true,
          // onComplete() {
          //   console.log('---1-onComplete---', this);
          //   _height += $(this.target).innerHeight() + 5;
          //   _$elem.find('.select-options').css('height', _height);
          //   // let thisList = _$elem.find('.select-options');
          //   // let _height = 0;
          //   // thisList.find('li').each((i, item) => {
          //   //   _height += $(item).innerHeight();
          //   // });
          //   // thisList.css('height', _height);
          // }
        }, 0.1)
      }
    });

    $listItems.click((e) => {
      const _$this = $(e.currentTarget);
      e.stopPropagation();
      $(e).find('.icon-after').toggleClass('icon-after--rotate');
      $styledSelect.text(_$this.text()).removeClass('active');
      $this.val(_$this.attr('rel'));
      $this.trigger('click');
      $list.hide();
      // console.log($listItems, 'center');
    });

    $(document).click(function () {
      $(e).find('.icon-after').removeClass('icon-after--rotate');
      $styledSelect.removeClass('active');
      $list.hide();
    });
  });

});
