import PubSub from 'pubsub-js'
const $ = window.$;

$(function () {
    let $posts = $('#our-posts');
    let posts = [];

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: $('.js-blog').data('route'),
        type: 'POST',
        success: function (data) {
            posts = data ? data.data : [];

            $.each(posts, (index, post) => {
                let li = `<li data-post="${index}" class="window-info__item" data-noscroll="true">
                    <a class="one-block" data-noscroll="true" href="#">
                      <div class="one-block__left" data-noscroll="true">
                        <img data-noscroll="true" src=${post.smallImage} alt="event">
                      </div>
                      <div class="one-block__right" data-noscroll="true">
                        <div class="one-block__h3" data-noscroll="true">${post.title}</div>
                        <div class="one-block__p" data-noscroll="true">${post.introtext}</div>
                      </div>
                    </a>
                  </li>`;

            $posts.append(li)
        });

            $('[data-post]').eq(0).addClass('active');
            PubSub.publish('post', posts[0]);
        }
    })

    // Клик по превью поста в списке справа
    $('#blog').on('click', '.one-block', function () {
        $('#blog .window-info__item').removeClass('active');
        let number = $(this).parent().addClass('active').data('post')
        PubSub.publish('post', posts[number])

        return false
    });
})
