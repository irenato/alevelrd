import PubSub from 'pubsub-js';
import '../home/PubSub/hideScene';
import '../libs/jquery.mask';
import Paginator from './Paginator'
import config from './config';

const $ = window.$;
$(function () {
  $('.js-mask').mask('+38 (000) 000 00 00');

  $('.js-open-modal-request').on('click', function () {
    $('.sale').hide()
    $('.start__btn ').hide()
    $('body').addClass('modal-opened').removeClass('with-sale')

    PubSub.publishSync('hideScene', {
      param: 'menu-in-row',
      cb: () => {
        $('.navigate').addClass('navigate--row');
        $('#modalFeedback').fadeIn(500).css('display', 'flex');
        $('.scene-bg').addClass('scene-bg--blur')
      }
    });
    return false
  });

  $('.js-open-modal-teacher').on('click', function () {
    $('.sale').hide()
    $('.start__btn ').hide()
    $('body').addClass('modal-opened').removeClass('with-sale')
    PubSub.publishSync('hideScene', {
      param: 'menu-in-row',
      cb: () => {
        $('.navigate').addClass('navigate--row');
        $('#modalTeacher').fadeIn(500).css('display', 'flex');
        $('.scene-bg').addClass('scene-bg--blur')
      }
    });
    return false
  });

  $('.js-open-modal-partner').on('click', function () {
    $('.sale').hide()
    $('.start__btn ').hide()
    $('body').addClass('modal-opened').removeClass('with-sale')
    PubSub.publishSync('hideScene', {
      param: 'menu-in-row',
      cb: () => {
        $('.navigate').addClass('navigate--row');
        $('#modalPartner').fadeIn(500).css('display', 'flex');
        $('.scene-bg').addClass('scene-bg--blur')
      }
    });

    return false
  });

  $('.modal-feedback').on('click', function (e) {
    e.stopPropagation();
  });

  $('#modalAnswer .btn').on('click', function (e) {
    if (window.innerWidth <= 768) {
      $('#modalAnswer').hide()
    }
  })

  $('#modalFeedback, #modalAnswer, #modalTeacher, #modalPartner, .modal-close').on('click', function (e) {
    $('#modalFeedback, #modalPartner, #modalTeacher, #modalAnswer').hide();
    $('body').removeClass('modal-opened');
    if (window.innerWidth > 768) {
      e.preventDefault();
      let scene = Paginator.activeScene;

      $('.scene-bg').removeClass('scene-bg--blur');
      $('.navigate').removeClass('navigate--row');
      $('.start__btn ').show();
      if (Paginator.activeScene === 0) {
        $('.sale').show();
      }
      if (!config.canGo) return;
      config.canGo = false;
      Paginator.activeScene = scene;

      // Переключение слайдов, генерация события
      PubSub.publish('showScene', {
        from: scene,
        to: scene
      });
    }

    const _$this = $(this);
    const _$cont = _$this.hasClass('modal')
      ? _$this
      : _$this.closest('modal');

    _$cont.find('input').val('');
    _$cont.find('select').each((i, e) => {
      const _$this = $(e);
      const val = _$this.find('option').eq(0).val();

      _$this.val(val).trigger("change");
    });
    
    _$cont.find('.js-error-area p').remove();
    _$cont.find('.js-error-area').css('display', 'none');
  });
});



