import {TimelineLite} from 'gsap';
import 'gsap/TextPlugin'
import PubSub from 'pubsub-js';

const $ = window.$;

PubSub.subscribe('post', function (msg, data) {
    $('#blog .window-info-photo__socials').html('');

    let tl = new TimelineLite(),
        $companyName = $('#blog .window-info-photo__h2'),
        $companyLink = $('#blog .js-partner-link'),
        $companyType = $('#blog .window-info-photo__p'),
        $companyDescription = $('#blog .window-info-photo__descr'),
        $blurImg = $('#blog .blur-img, #blog .window-info-photo__content');

    let socials = data ? data.links : [];

    if (socials) {
        socials.forEach(function (item, i, arr) {
            $('#blog .window-info-photo__socials').append('<li class="window-info-photo__social"><a class="${item.html_class}" href="${item.link}" target="_blank"> </a></li>')
        });
    }

    if ($(window).width() <= '568') {
        if ($('body').hasClass('safari')) {
            tl
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })

            $companyLink.attr("href", data && data.link),
                $companyName.text(data && data.title),
                $companyDescription.empty(),
                $companyDescription.append(data && data.description),
                $companyType.text(data && data.specialization)
        }
        else {

            tl
                .to($blurImg, 0.5, {
                    filter: 'blur(20px)',
                    transform: 'scale(1.1)'
                }, 'blur')
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })
                .to($blurImg, 0.5, {
                    filter: 'blur(0)',
                    transform: 'scale(1)'
                })


            $companyLink.attr("href", data && data.link),
                $companyName.text(data && data.title),
                $companyDescription.empty(),
                $companyDescription.append(data && data.description),
                $companyType.text(data && data.specialization)
        }
    }
    else {
        if ($('body').hasClass('safari')) {
            tl
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })
                .to($companyName, 1, {
                    text: {
                        value: data && data.title
                    }
                }, 'blur')
                /*.to($companyLink, 1, {
                    text: {
                        value: data && data.link
                    }
                }, 'blur')*/
                .to($companyType, 1, {
                    text: {
                        value: data && data.specialization
                    }
                }, 'blur')
                .to($companyDescription, 2, {
                    text: {
                        value: data && data.description
                    }
                }, 'blur')
            $("#blog .js-partner-link").attr("href", data && data.link)

        }
        else {
            tl
                .to($blurImg, 0.5, {
                    filter: 'blur(20px)',
                    transform: 'scale(1.1)'
                }, 'blur')
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                }).to($blurImg, 0.5, {
                filter: 'blur(0)',
                transform: 'scale(1)'
            })

                .to($companyName, 1, {
                    text: {
                        value: data && data.title,
                        ease: Linear.easeNone
                    }
                }, 'blur')

                .to($companyType, 1, {
                    text: {
                        value: data && data.specialization,
                        ease: Linear.easeNone
                    }
                }, 'blur')
                .to($companyDescription, 2, {
                    text: {
                        value: data && data.description,
                        ease: Linear.easeNone
                    }
                }, 'blur')
            $("#blog .js-partner-link").attr("href", data && data.link)

        }
    }
})

