import {TimelineMax} from 'gsap';
import PubSub from 'pubsub-js';

const $ = window.$;

PubSub.subscribe('hideScene', function (msg, data) {
  if (data === 0) {
    return
  }
  let $navigate = $('.navigate');

  $('.courses-coverflow').fadeOut();

  let tl = new TimelineMax({
    onComplete: () => {
      if (data.param !== 'menu-in-row') {
        $navigate.removeClass('navigate--row');
      }
    }
  });

  tl
    .call(() => {
      $(`[data-window]`).hide()
    })
    .to($('.scene-bg'), 0.2, {
      filter: 'none'
    })
    .call(() => {
      $(`.layer[data-window]`).hide()
      if(data && data.cb) {
        data.cb()
      }
    })
});


