import {TimelineMax, TweenMax} from 'gsap';
import TextPlugin from 'gsap/TextPlugin'
import PubSub from 'pubsub-js';
const $ = window.$;
PubSub.subscribe('tab', function (msg, data) {
  let tl = new TimelineMax(),
    $to = $(`.tabs__item--${data.to}`),
    $tabsItem = $(`.tabs__item`),
    $blurImg = $(`#about-blur-img`);

  tl
    .to($blurImg, 0.5, {
      filter: 'blur(20px)',
      transform: 'scale(1.1)'
    }, 'tabs')
    .set($blurImg, {
      css: {
        backgroundImage: `url(${data.img})`
      }
    }).to($blurImg, 0.5, {
      filter: 'blur(0)',
      transform: 'scale(1)'
    })

    .to($tabsItem, 0.3, {
      opacity: 0
    }, 'tabs')
    .set($tabsItem, {
      css: {
        display: 'none'
      }
    }).set($to, {
      css: {
        display: 'block'
      }
    }).to($to, 0.3, {
      opacity: 1
    })
});
