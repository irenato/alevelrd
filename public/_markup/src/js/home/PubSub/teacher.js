import {TimelineMax} from 'gsap';
import 'gsap/TextPlugin'
import PubSub from 'pubsub-js';

const $ = window.$;

PubSub.subscribe('teacher', function (msg, data) {
    $('#teachers .window-info-photo__socials').html('');
    let tl = new TimelineMax({
            onComplete() {
                $('#teachers .window-info-photo').css({
                    background: 'transparent'
                })
            }
        }),
        $teacherName = $('#teachers .window-info-photo__h2'),
        $teacherType = $('#teachers .window-info-photo__p'),
        $teacherDescription = $('#teachers .window-info-photo__descr'),
        $blurImg = $(`#teachers .blur-img, #teachers .window-info-photo__content`);

    let socials = data ? data.links : [];

    if (socials) {
        socials.forEach(function (item, i, arr) {
            $('#teachers .window-info-photo__socials').append('<li class="window-info-photo__social"><a class="${item.html_class}" href="${item.link}" target="_blank"></a></li>')
        });
    }

    $('#teachers .window-info-photo').css({
        background: '#fff'
    })



    if ($(window).width() <= '568') {

        if ($('body').hasClass('safari')) {
            tl
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })




            $teacherName.text(data.last_name + " " + data.first_name),
                $teacherDescription.empty(),
                $teacherDescription.append(data.description),
                $teacherType.text(data.specialization)
        } else {
            tl
                .to($blurImg, 1, {
                    filter: 'blur(20px)',
                    transform: 'scale(1.1)'
                }, 'blur')
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })
                .to($blurImg, 1, {
                    filter: 'blur(0)',
                    transform: 'scale(1)'
                })



            $teacherName.text(data.last_name + " " + data.first_name),
                $teacherDescription.empty(),
                $teacherDescription.append(data.description),
                $teacherType.text(data.specialization)

        }
    } else {
        if ($('body').hasClass('safari')) {
            tl
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })
                .to($teacherName, 1, {
                    text: {
                        value: data && `${data.last_name} ${data.first_name}`
                    }
                }, 'blur')
                .to($teacherType, 1, {
                    text: {
                        value: data && data.specialization
                    }
                }, 'blur')
                .to($teacherDescription, 2, {
                    text: {
                        value: data && data.description
                    }
                }, 'blur')
        } else {
            tl
                .to($blurImg, 1, {
                    filter: 'blur(20px)',
                    transform: 'scale(1.1)'
                }, 'blur')
                .set($blurImg, {
                    css: {
                        backgroundImage: `url(${data && data.bigImage})`
                    }
                })
                .to($blurImg, 1, {
                    filter: 'blur(0)',
                    transform: 'scale(1)'
                })
                .to($teacherName, 1, {
                    text: {
                        value: data && `${data.last_name} ${data.first_name}`
                    }
                }, 'blur')
                .to($teacherType, 1, {
                    text: {
                        value: data && data.specialization
                    }
                }, 'blur')
                .to($teacherDescription, 2, {
                    text: {
                        value: data && data.description
                    }
                }, 'blur')
        }
    }

})
