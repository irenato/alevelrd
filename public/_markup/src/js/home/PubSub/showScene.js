import { TimelineLite } from 'gsap';
import PubSub from 'pubsub-js';
import config from '../config';
const $ = window.$;

PubSub.subscribe('showScene', function (msg, data) {
  $('body').addClass('modal-active');

  let height = $(document).height(),
    numberOfScenes = $('.layer').length + 1,
    depthOfScene = height / numberOfScenes;

  let tl = new TimelineLite({
    onComplete: () => {
      config.canGo = true;
    }
  });

  tl
    .to(window, 0.5, {
      scrollTo: {
        y: data.to * depthOfScene
      }
    })
    .to($('.scene-bg'), 0.5, {
      filter: data.to ? 'blur(10PX) brightness(50%)' : 'none'
    })
    .fromTo($(`[data-window]`).eq(data.to), 0.5, {
      y: -50
    }, {
      y: 0,
      display: 'flex'
    }).call(() => {
      $(`.trigger[data-goto]`).eq(data.to - 1).addClass('trigger--active')

      if (data.to === $('.trigger').length) {
        PubSub.publish('map')
      }
    })
});
