import PubSub from 'pubsub-js'
const $ = window.$;

$(function () {
  let $partners = $('#our-partners');
  let partners = [];

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: $('.js-partners').data('route'),
    type: 'POST',
    success: function (data) {
      partners = data ? data.data : [];

      $.each(partners, (index, partner) => {
        let li = `<li data-partner="${index}" class="window-info__item" data-noscroll="true">
                    <a class="one-block" data-noscroll="true" href="#">
                      <div class="one-block__left" data-noscroll="true">
                        <img data-noscroll="true" src=${partner.smallImage} alt="event">
                      </div>
                      <div class="one-block__right" data-noscroll="true">
                        <div class="one-block__h3" data-noscroll="true">${partner.title}</div>
                        <div class="one-block__p" data-noscroll="true">${partner.introtext}</div>
                      </div>
                    </a>
                  </li>`;

        $partners.append(li)
      });

      $('[data-partner]').eq(0).addClass('active');
      PubSub.publish('partner', partners[0]);
    }
  })

  // Клик по превью партнера в списке справа
  $('#partners').on('click', '.one-block', function () {
    $('#partners .window-info__item').removeClass('active');
    let number = $(this).parent().addClass('active').data('partner')
    PubSub.publish('partner', partners[number])

    return false
  });
})
