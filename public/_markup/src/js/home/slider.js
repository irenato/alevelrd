import 'jquery.flipster';
const $ = window.$;
window.$ = $;
let coverflow;
let sliderNav;

function init() {
  coverflow = $("#coverflow").flipster({
    start: 'center',
    spacing: -0.6,
    buttons: true,
    loop: true,
    touch: true,
    onItemSwitch: function(currentItem, previousItem) {
      sliderNav.flipster('jump', $(currentItem).data('course')); 
    }
  });
  
  sliderNav = $('#navigation').flipster({
    style: 'flat',
    spacing: 0,
    scrollwheel: false,
    loop: true,
    start: "center",
    onItemSwitch: function(currentItem, previousItem) {
      coverflow.flipster('jump', $(currentItem).data('course'));  
    }
  });
}

if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()) || window.innerWidth <= 1279)  {
  $('body').addClass('safari')
} else {
  init()
}

$(window).on("resize", function() {
  if (window.innerWidth <= 1279) {
    if (coverflow) {
      coverflow.hide()
      sliderNav.hide()
    }
  } else {
    if (coverflow) {
      coverflow.show()
      sliderNav.show()
    } else {
      init()
    }
  }
})
