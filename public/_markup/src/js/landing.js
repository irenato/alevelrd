import PubSub from 'pubsub-js';
import './home/map';
import 'owl.carousel';
import './libs/jquery.mask';


const $ = window.$;

$(function () {
  $('.js-phone').mask('+38 (000) 000 00 00');

  $('#form').submit(function (e) {
    e.preventDefault()
    let th = $(this);
    $.ajax({
      type: 'POST',
      url: 'mail.php', // Change
      data: th.serialize()
    }).done(function () {
      alert('Thank you!');
      setTimeout(function () {
        // Done Functions
        th.trigger('reset');
      }, 1000);
    });
    return false;
  })

  let responsive = {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    1000: {
      items: 4
    }
  };

  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    nav: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 1000,
    autoplayTimeout: 2000,
    startPosition: $('.owl-carousel img').length / 2,
    responsive: responsive
  })

  PubSub.publish('map')
});
