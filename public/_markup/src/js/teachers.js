import PubSub from 'pubsub-js'

const $ = window.$;

$(function () {
  let $teachers = $('#our-teachers');

  let teachers = []

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: $('.js-teachers').data('route'),
    type: 'POST',
    success: function (data) {
      teachers = data ? data.data : [];

      $.each(teachers, (index, teacher) => {
        let li = `<li data-teacher="${index}" class="window-info__item" data-noscroll="true">
                    <a class="one-block" data-noscroll="true" href="#">
                      <div class="one-block__left" data-noscroll="true">
                        <img data-noscroll="true" src=${teacher.smallImage} alt="event">
                      </div>
                      <div class="one-block__right" data-noscroll="true">
                        <div class="one-block__h3" data-noscroll="true">${teacher.last_name} ${teacher.first_name}</div>
                        <div class="one-block__p" data-noscroll="true">${teacher.introtext}</div>
                      </div>
                    </a>
                  </li>`;

        $teachers.append(li);
        $('.one-block__p p').attr('data-noscroll', 'true');
      });

      $('[data-teacher]').eq(0).addClass('active');
      PubSub.publish('teacher', teachers[0]);
    }
  })

  // Клик по превью препода в списке справа
  $('#teachers').on('click', '.one-block', function () {
    $('#teachers .window-info__item').removeClass('active');
    let number = $(this).parent().addClass('active').data('teacher')
    PubSub.publish('teacher', teachers[number])

    return false
  });
});
