const $ = window.$;
let users = []

function setData (id) {
  let data = users.filter((user) => id == user.id)[0]

  if (data) {
    $('.students-main').hide().removeClass('opened')
    $('.students-detail').show().addClass('show')

    $('.js-photo').css({'background-image': 'url(' + data.bigImage + ')'})
    $('.students-detail__name').text(data.username)
    $('.students-detail__description').text(data.description)
    $('.students-detail__role').text(data.specialization)
    $('.students-detail__vk').attr('href', data.socials && data.socials.vk)

    if (data.social_links) {
      $('.students-detail__socials').html('')
      data.social_links.forEach(function (item, i, arr) {
        $('.students-detail__socials').append(`<a class="mx-5 ${item.html_class} text-dark display-24" href="#" target="_blank" href="${item.link}"></a>`)
      });
    }

    $('.students-detail__next').attr('href', '#' + (id + 1 > 7 ? 1 : id + 1))
    $('.students-detail__prev').attr('href', '#' + (id - 1 < 1 ? 7 : id - 1))
  }
}

$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: $('.students-main').data('route'),
    type: 'POST',
    success: function (data) {
      users = data.data

      $.each(users, (index, user) => {
        user.id = index + 1;
        $(`.students-grid__item[data-about=${user.id}], .students-detail__item[data-about=${user.id}]`).css({'background-image': 'url(' + user.bigImage + ')'})
        $(`.students-main__btn[data-about=${user.id}]`).text(user.specialization)
      })
    }
  })

  if (window.location.hash) {
    var id = window.location.hash.split('#')[1];
    setData(parseInt(id))
  }
})

window.addEventListener('hashchange', function (e) {
  if (window.location.hash) {
    var id = window.location.hash.split('#')[1];

    if (id) {
      setData(parseInt(id))
    } else {
      $('.students-main').show().addClass('opened')
      $('.students-detail').hide().removeClass('show')
    }
  } else {
    $('.students-main').show().addClass('opened')
    $('.students-detail').hide().removeClass('show')
  }
}, false);

$('[data-about]').on('click', function (e) {
  let id = $(this).data('about');

  if (id) {
    setData(parseInt(id))
  } else {
    $('.students-main').show().addClass('opened')
    $('.students-detail').hide().removeClass('show')
  }
});
const launch = function () {
  "use strict";

  const body = document.querySelector('body');
  let isMobile = false;
  let scrollTopPosition;
  let browserYou;
  let _winWidth = $(window).outerWidth();
  const genFunc = {
    initialized: false,
    initialize() {
      if (this.initialized) return;

      this.initialized = true;
      this.build();
    },
    build: function () {
      function getBrowser() {
        let ua = navigator.userAgent;
        let bName = (function () {
          if (ua.search(/Edge/) > -1) return "edge";
          if (ua.search(/MSIE/) > -1) return "ie";
          if (ua.search(/Trident/) > -1) return "ie11";
          if (ua.search(/Firefox/) > -1) return "firefox";
          if (ua.search(/Opera/) > -1) return "opera";
          if (ua.search(/OPR/) > -1) return "operaWebkit";
          if (ua.search(/YaBrowser/) > -1) return "yabrowser";
          if (ua.search(/Chrome/) > -1) return "chrome";
          if (ua.search(/Safari/) > -1) return "safari";
          if (ua.search(/maxHhon/) > -1) return "maxHhon";
        })();

        let version;

        switch (bName) {
          case "edge":
            version = (ua.split("Edge")[1]).split("/")[1];
            break;
          case "ie":
            version = (ua.split("MSIE ")[1]).split(";")[0];
            break;
          case "ie11":
            bName = "ie";
            version = (ua.split("; rv:")[1]).split(")")[0];
            break;
          case "firefox":
            version = ua.split("Firefox/")[1];
            break;
          case "opera":
            version = ua.split("Version/")[1];
            break;
          case "operaWebkit":
            bName = "opera";
            version = ua.split("OPR/")[1];
            break;
          case "yabrowser":
            version = (ua.split("YaBrowser/")[1]).split(" ")[0];
            break;
          case "chrome":
            version = (ua.split("Chrome/")[1]).split(" ")[0];
            break;
          case "safari":
            version = ua.split("Safari/")[1].split("")[0];
            break;
          case "maxHhon":
            version = ua.split("maxHhon/")[1];
            break;
        }

        let platform = 'desktop';

        if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())) {
          platform = 'mobile';
        }

        let browsrObj;

        try {
          browsrObj = {
            platform: platform,
            browser: bName,
            versionFull: version,
            versionShort: version.split(".")[0]
          };
        } catch (err) {
          browsrObj = {
            platform: platform,
            browser: 'unknown',
            versionFull: 'unknown',
            versionShort: 'unknown'
          };
        }

        return browsrObj;
      }

      browserYou = getBrowser();

      if (browserYou.platform == 'mobile') {
        isMobile = true;
        document.documentElement.classList.add('mobile');
      } else {
        document.documentElement.classList.add('desktop');
      }
      if ((browserYou.browser == 'ie')) {
        document.documentElement.classList.add('ie');
      }
      if (navigator.userAgent.indexOf("Edge") > -1) {
        document.documentElement.classList.add('edge');
      }
      if (navigator.userAgent.search(/Macintosh/) > -1) {
        document.documentElement.classList.add('macintosh');
      }
      if (
        (browserYou.browser == 'ie' && browserYou.versionShort < 9) ||
        ((browserYou.browser == 'opera' || browserYou.browser == 'operaWebkit') && browserYou.versionShort < 18) ||
        (browserYou.browser == 'firefox' && browserYou.versionShort < 30)
      ) {
        alert('Обновите браузер');
      }
    }
  };

  genFunc.initialize();
};

launch();
