
import Paginator from './course/Paginator';
import './course/Animator';
import './course/nav';
import './course/program';
import './common/preloader'
import './course/request'
import doubleBackground from './common/doubleBackground';
import slider from './course/slider'
import './libs/select2.full'
import './common/mainMenu'
import './home/selectCities'
import './common/timer'
import './home/simplebar'

// $(function () {
//   let modalSelect = $('.blue-select select');
//   modalSelect.each((i, e) = > {
//     let _$this = $(e);
//   _$this.select2({
//     minimumResultsForSearch: -1
//   });
//   _$this.on('select2:open', (e) = > {
//     let addNoScroll = $(document).find('.select2-results');
//   addNoScroll.addClass('noscroll');
//   addNoScroll.attr({'data-simplebar': 'init', "data-noscroll": "true"});
// });
// });
// });



const $ = window.$;

$(function () {
  if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()) || window.innerWidth <= 768) {
    $('body').addClass('safari')
  }

  $('.js-mask').mask('+38 (000) 000 00 00');
  let p = new Paginator();
  doubleBackground()
  slider();

  $('.course-teachers__item').on('click', function () {
    if ($(this).find('.course-teachers__details').hasClass('active')) {
      $('.course-teachers__item').removeClass('active')
      $(this).find('.course-teachers__details').slideToggle()
    } else {
      $('.course-teachers__item').removeClass('active')
      $('.course-teachers__details.active').removeClass('active').slideUp()
      $(this).addClass('active').find('.course-teachers__details').addClass('active').slideDown()
    }
  })

  $('.course-teachers__item').eq(0).addClass('active').find('.course-teachers__details').addClass('active').slideToggle()

  //file upload
  if (window.File && window.FileList && window.FileReader) {
    $("#choose-photo").on("change", function (e) {
      var $this = $(this);
      var value = $this.val();
      var maxSize = $this.data('max-size');
      var fileSize = $this.get(0).files[0].size;
      if (value) {
        console.log(fileSize);
        if (fileSize < maxSize) {
          var filename = $this.val().replace(/C:\\fakepath\\/i, '');
          var FR = new FileReader();
          FR.onload = function (e) {
            $this.parents('.reviews__photo').find('img').attr('src', e.target.result).addClass("choose-photo__img--show");
          };
          FR.readAsDataURL(e.target.files[0]);
        }
        else {
          alert('Размер файла не должен превышать 3Мб');
          return false;
        }
      }
    });
  }
  else {
    alert('File API не поддерживается данным браузером');
  }
})

$(window).on('resize', function () {
  doubleBackground()
})

$(window).on('scroll', function () {
  let logoWhite = false;
  let logoColored = false;
  let scrolled = $(window).scrollTop();
  if (scrolled && !logoColored) {
    logoColored = true;
    logoWhite = false
    $('.burger__logo img').attr('src', '/img/logo.png').css('display', 'none')
  } else if (!scrolled && !logoWhite) {
    logoColored = false;
    logoWhite = true
    $('.burger__logo img').attr('src', '/img/alevelua.svg').css('display', 'block')
  }
})

$(function () {
  // tabbed content
  // http://www.entheosweb.com/tutorials/css/tabs.asp
  $(".tab_content").hide();
  $(".tab_content:first").show();

  /* if in tab mode */
  $("ul.tabs li").click(function () {

    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

  });
  /* if in drawer mode */
  $(".tab_drawer_heading").click(function () {

    $(".tab_content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
  });

  $('ul.tabs li').last().addClass("tab_last");


  $('.course-reviews-popup__btn').on('click', function () {
    $('.course-reviews-popup').hide()
  })

  $('#feedback').on('click', function () {
    $('.course-reviews-popup').show()
  })
  $('.js-symbols-left').keypress(function () {
    var maxLength = 1000;
    if (this.value.length > maxLength) {
      return false;
    }
    $('.js-remainingSymbol').find('em').html(maxLength - this.value.length);
  });
  $('.js-form').on('submit', function () {
    setTimeout(function () {
      $('.js-remainingSymbol').find('em').html("0");
    }, 500)
  })
});
document.addEventListener('keyup', function (e) {
  if (e.keyCode === 27) {
    $('.modal-close').trigger('click')
  }
});

/*js for sale timer*/
function addDays(days) {
    var result = new Date();
    result.setTime(result.getTime() + (days * 24 * 60 * 60 * 1000));
    return result;
}
$(document).ready(function () {
    var daysleft = $('#timer').attr('data-daysLeft');

    var temp = addDays(daysleft);

    var deadline = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate(), 0, 0, 0);

    $('#timer').attr('data-deadline', deadline);

    initializeClock('clockdiv', deadline);



});

function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}


function windowSize(){
    if ($(window).width() <= '1200'){
        var isMuted =
            $('#video-mute')
                .toggleClass("button_voice__mute button_voice__noisy")
                .hasClass("button_voice__mute");

        $('#course-home__video').prop("muted", isMuted);
    }
}
$(document).ready(function () {

    $('#video-mute').on("click", function () {

        var isMuted =
            $(this)
                .toggleClass("button_voice__mute button_voice__noisy")
                .hasClass("button_voice__mute");

        $('#course-home__video').prop("muted", isMuted);

    });



    $(window).on('load resize',windowSize);
});


