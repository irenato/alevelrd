const $ = window.$;
export default function doubleBackground () {
  let widthOfLeftSide = (window.innerWidth - $('.course-main .window').width()) / 2 + $('.window-left').width();
  let widthWindow = window.innerWidth;
  let widthOfRightSide = widthWindow - widthOfLeftSide;

  $('.double-bg__left').width(widthOfLeftSide);
  $('.double-bg__right').width(widthOfRightSide);
}
