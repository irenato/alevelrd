import {TimelineLite} from 'gsap';
const $ = window.$;

$(window).on('scroll', canvas);

$(function () {
  canvas()
})

function canvas () {
  let scenes = {
    sceneLogo: $('.bg-logo'),
    sceneText: $('.bg-text'),
    scenePc: $('.bg-pc'),
    scene1: $('.bg-1'),
    scene2: $('.bg-2'),
    scene3: $('.bg-3'),
    scene4: $('.bg-4'),
    scene5: $('.bg-5'),
    scene6: $('.bg-6'),
    scene7: $('.bg-7'),
    sceneBg: $('.bg-bg')
  };

  let scrolled = $(document).scrollTop();

  let tl = new TimelineLite();
  let s = scrolled / 5000;

  let initialScale = 1;

  let scale1 = initialScale,
    scale2 = initialScale,
    scale3 = initialScale,
    scale4 = initialScale,
    scale5 = initialScale,
    scale6 = initialScale,
    scale7 = initialScale,
    scalePc = initialScale,
    scaleBg = initialScale;

  if (scrolled) {
    scenes.sceneText.hide();
  } else {
    scenes.sceneText.show();
  }

  if (s < 0.2) {
    scale1 = initialScale;
    scale2 = initialScale;
    scale3 = initialScale;
    scale4 = initialScale;
    scale5 = initialScale;
    scale6 = initialScale;
    scale7 = initialScale;
    scaleBg = initialScale;
    scalePc = Math.round((initialScale + (s * 10)) * 1000) / 1000;
    scenes.scenePc.show();
    scenes.sceneLogo.show();
  } else {
    scale1 = Math.round((initialScale + ((s - 0.2) * 1.5)) * 1000) / 1000;
    scale2 = Math.round((initialScale + ((s - 0.2) * 4.5)) * 1000) / 1000;
    scale3 = Math.round((initialScale + ((s - 0.2) * 4)) * 1000) / 1000;
    scale4 = Math.round((initialScale + ((s - 0.2) * 3.5)) * 1000) / 1000;
    scale5 = Math.round((initialScale + ((s - 0.2) * 3)) * 1000) / 1000;
    scale6 = Math.round((initialScale + ((s - 0.2) * 2.5)) * 1000) / 1000;
    scale7 = Math.round((initialScale + ((s - 0.2) * 2.5)) * 1000) / 1000;
    scaleBg = Math.round((initialScale + ((s - 0.2) * 1.5)) * 1000) / 1000;
    scenes.scenePc.hide();
    scenes.sceneLogo.hide();
  }

  tl
    .to(scenes.sceneBg, 0, {scaleX: scaleBg, scaleY: scaleBg}, 0)
    .to(scenes.scene1, 0, {scaleX: scale1, scaleY: scale1}, 0)
    .to(scenes.scene2, 0, {scaleX: scale2, scaleY: scale2}, 0)
    .to(scenes.scene3, 0, {scaleX: scale3, scaleY: scale3}, 0)
    .to(scenes.scene4, 0, {scaleX: scale4, scaleY: scale4}, 0)
    .to(scenes.scene5, 0, {scaleX: scale5, scaleY: scale5}, 0)
    .to(scenes.scene6, 0, {scaleX: scale6, scaleY: scale6}, 0)
    .to(scenes.scene7, 0, {scaleX: scale7, scaleY: scale7}, 0)
    .to(scenes.scenePc, 0, {scaleX: scalePc, scaleY: scalePc}, 0)
    .to(scenes.sceneLogo, 0, {scaleX: scalePc, scaleY: scalePc}, 0)
    .to(scenes.sceneText, 0, {scaleX: scalePc, scaleY: scalePc}, 0)
}
