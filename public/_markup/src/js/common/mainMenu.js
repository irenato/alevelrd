import { TweenMax } from 'gsap';
const $ = window.$;

$(function () {
  let
    $menuLB = $('.menu__left-blue'),
    $menuLO = $('.menu__left-orange'),
    $menuRB = $('.menu__right-blue'),
    $menuRO = $('.menu__right-orange'),
    $menu = $('.menu'),
    $courses = $('.nav-courses'),
    $coursesBtn = $('#courses-btn'),
    $burger = $('#menu'),
    $menu_inside_hide = $('.menu-inside__hide');

  $burger.on('click', () => {
    $menu.css({
      display: $('body').hasClass('safari') ? 'block' : 'flex'
    });

    if (window.innerWidth <= 768) {
      $('.menu').animate({
        scrollTop: 0
      }, 5);
    }

    TweenMax.to($menuLB, 0.3, {
      left: 0
    });
    TweenMax.to($menuLO, 0.3, {
      left: 0
    });
    TweenMax.to($menuRB, 0.3, {
      right: 0
    });
    TweenMax.to($menuRO, 0.3, {
      right: 0
    });
  });

  $menu.on('click', function () {
    if ($coursesBtn.hasClass('active')) {
      $coursesBtn.toggleClass('active');
      TweenMax.to($menuLO, 0.5, {
        left: 0
      });
      $coursesBtn.find('i').removeClass('active')
    } else {
      TweenMax.to($menuLB, 0.5, {
        left: '-100%'
      });
      TweenMax.to($menuLO, 0.5, {
        left: '-100%'
      });
      TweenMax.to($menuRB, 0.5, {
        right: '-100%'
      });
      TweenMax.to($menuRO, 0.5, {
        right: '-100%'
      });
      $menu.hide()
    }
  });

  $coursesBtn.on('click', function (e) {
    if (window.innerWidth > 768) {
      e.preventDefault();
      e.stopPropagation();
      $(this).toggleClass('active');
      if ($(this).hasClass('active')) {
        TweenMax.to($menuLO, 0.5, {
          left: '50rem'
        });
        TweenMax.to($courses, 0.5, {
          opacity: 1
        });
        $coursesBtn.find('i').addClass('active')
      } else {
        TweenMax.to($courses, 0.5, {
          opacity: 0
        });
        TweenMax.to($menuLO, 0.5, {
          left: '0px'
        });
        $coursesBtn.find('i').removeClass('active')
      }
    }
  });

  $menu_inside_hide.on('click', function (e) {
        if (window.innerWidth > 768) {
            e.preventDefault();
            e.stopPropagation();
            $($coursesBtn).toggleClass('active');
            if ($($coursesBtn).hasClass('active')) {
                TweenMax.to($menuLO, 0.5, {
                    left: '50rem'
                });
                TweenMax.to($courses, 0.5, {
                    opacity: 1
                });
                $coursesBtn.find('i').addClass('active')
            } else {
                TweenMax.to($courses, 0.5, {
                    opacity: 0
                });
                TweenMax.to($menuLO, 0.5, {
                    left: '0px'
                });
                $coursesBtn.find('i').removeClass('active')
            }
        }
    });

});
