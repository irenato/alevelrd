import Paginator from './testing/Paginator';
import './testing/Animator';
import doubleBackground from './common/doubleBackground';
import './common/preloader'
import './common/mainMenu'
import './home/selectCities'
import './testing/noselect'

const $ = window.$;

$(function () {
  let p = new Paginator();
  doubleBackground()
})

$(window).on('resize', function () {
  doubleBackground()
})
