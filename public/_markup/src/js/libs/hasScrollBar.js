(function ($) {
  $.fn.hasScrollBar = function () {
    var hasScrollBar = {},
      e = this.get(0);
    hasScrollBar.vertical = e.scrollHeight > e.clientHeight;
    hasScrollBar.horizontal = e.scrollWidth > e.clientWidth;
    return hasScrollBar;
  }
})(jQuery);
