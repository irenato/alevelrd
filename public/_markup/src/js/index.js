const $ = window.$;

import './home/map';
import './home/tabs';
import './home/calendar';
import './home/teachers';
import './home/partners';
import './common/mainMenu';
import './common/canvas';
import './common/preloader';
import './home/selectCities'
import Paginator from './home/Paginator';
import './home/request';
import './common/timer';
import PubSub from 'pubsub-js';
import './home/slider';
import './libs/select2.full';
import './home/simplebar';
import './common/jquery.fancybox.min';

$(() => {
  let modalSelect = $('.blue-select select');
  modalSelect.each((i, e) => {
    let _$this = $(e);
    _$this.select2({
      minimumResultsForSearch: -1
    });
    _$this.on('select2:open', (e) => {
      let addNoScroll = $(document).find('.select2-results');
      let searchScrollContainer = addNoScroll.find('.select2-dropdown');
      addNoScroll.addClass('noscroll');
      // addNoScroll.attr({'data-simplebar':'init', "data-noscroll":"true"});
      addNoScroll.attr({'data-noscroll':'true'});
    });
  });
});

const launch = function () {
  "use strict";

  const body = document.querySelector('body');
  let isMobile = false;
  let scrollTopPosition;
  let browserYou;
  let _winWidth = $(window).outerWidth();
  const genFunc = {
    initialized: false,
    initialize() {
      if (this.initialized) return;

      this.initialized = true;
      this.build();
    },
    build: function () {
      function getBrowser() {
        let ua = navigator.userAgent;
        let bName = (function () {
          if (ua.search(/Edge/) > -1) return "edge";
          if (ua.search(/MSIE/) > -1) return "ie";
          if (ua.search(/Trident/) > -1) return "ie11";
          if (ua.search(/Firefox/) > -1) return "firefox";
          if (ua.search(/Opera/) > -1) return "opera";
          if (ua.search(/OPR/) > -1) return "operaWebkit";
          if (ua.search(/YaBrowser/) > -1) return "yabrowser";
          if (ua.search(/Chrome/) > -1) return "chrome";
          if (ua.search(/Safari/) > -1) return "safari";
          if (ua.search(/maxHhon/) > -1) return "maxHhon";
        })();

        let version;

        switch (bName) {
          case "edge":
            version = (ua.split("Edge")[1]).split("/")[1];
            break;
          case "ie":
            version = (ua.split("MSIE ")[1]).split(";")[0];
            break;
          case "ie11":
            bName = "ie";
            version = (ua.split("; rv:")[1]).split(")")[0];
            break;
          case "firefox":
            version = ua.split("Firefox/")[1];
            break;
          case "opera":
            version = ua.split("Version/")[1];
            break;
          case "operaWebkit":
            bName = "opera";
            version = ua.split("OPR/")[1];
            break;
          case "yabrowser":
            version = (ua.split("YaBrowser/")[1]).split(" ")[0];
            break;
          case "chrome":
            version = (ua.split("Chrome/")[1]).split(" ")[0];
            break;
          case "safari":
            version = ua.split("Safari/")[1].split("")[0];
            break;
          case "maxHhon":
            version = ua.split("maxHhon/")[1];
            break;
        }

        let platform = 'desktop';

        if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())) {
          platform = 'mobile';
        }

        let browsrObj;

        try {
          browsrObj = {
            platform: platform,
            browser: bName,
            versionFull: version,
            versionShort: version.split(".")[0]
          };
        } catch (err) {
          browsrObj = {
            platform: platform,
            browser: 'unknown',
            versionFull: 'unknown',
            versionShort: 'unknown'
          };
        }

        return browsrObj;
      }

      browserYou = getBrowser();

      if (browserYou.platform == 'mobile') {
        isMobile = true;
        document.documentElement.classList.add('mobile');
      } else {
        document.documentElement.classList.add('desktop');
      }
      if ((browserYou.browser == 'ie')) {
        document.documentElement.classList.add('ie');
      }
      if (navigator.userAgent.indexOf("Edge") > -1) {
        document.documentElement.classList.add('edge');
      }
      if (navigator.userAgent.search(/Macintosh/) > -1) {
        document.documentElement.classList.add('macintosh');
      }
      if (
        (browserYou.browser == 'ie' && browserYou.versionShort < 9) ||
        ((browserYou.browser == 'opera' || browserYou.browser == 'operaWebkit') && browserYou.versionShort < 18) ||
        (browserYou.browser == 'firefox' && browserYou.versionShort < 30)
      ) {
        alert('Обновите браузер');
      }
    }
  };
  
  genFunc.initialize();
};

launch();

$(function () {
  $('.window-popup-close, #hide-popup').on('click', function (e) {
    $('.window-popup').removeClass('active');
  });
  // $('.js-fancybox').on('click', function(event) {
  //   event.preventDefault();
  //   $.fancybox.open()
  // });
  $().fancybox({
    selector: '.js-fancybox'
  });
  $(window).on('resize', resizeFunction);
  resizeFunction();

  function resizeFunction () {
    const width = window.innerWidth;
    if (width <= 768) {
      // $('body').removeClass('modal-active');
      if (Paginator.activeScene === 2) $('.courses-coverflow').hide();
      PubSub.publish('map')
    } else {
      // $('body').addClass('modal-active');
      if (Paginator.activeScene === 2) $('.courses-coverflow').show();
    }
  }

  if (Math.round(window.devicePixelRatio * 100) > 100 && window.devicePixelRatio < 200) {
    $('body').addClass('zoom150')
  } else {
    $('body').removeClass('zoom150')
  }
  $('.js-symbols-left').keypress(function () {
    var maxLength = 1000;
    if (this.value.length > maxLength) {
      return false;
    }
    $('.js-remainingSymbol').find('em').html(maxLength - this.value.length);
  });
  $('.js-form').on('submit', function () {
    setTimeout(function () {
      $('.js-remainingSymbol').find('em').html("0");
    },500)
  });
  // $('.js-gotop__prev').on('click', function (event) {
  //   event.preventDefault();
  //   console.log(11);
  //   $('html,body').animate({
  //     scrollTop: 0
  //   }, 700);
  // });
});

document.addEventListener('keyup', function (e) {
  if(e.keyCode === 27) {
    $('.modal-close').trigger('click')
  }
});


