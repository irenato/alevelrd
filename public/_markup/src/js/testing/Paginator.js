import PubSub from 'pubsub-js';
const $ = window.$;

export default class Paginator {
  constructor () {
    // При инициализации класа сразу вызываем методы scrollEvents, pressEvents и clickOnMouseIcon
    this.clickEvents();
    this.pressEvents();
    this.activeSlide = 1;
    this.countSlide = $('[data-slide-testing]').length;
    this.canGo = true; // Может ли проигрываться анимация?
    this.delay = 500; // Задержка на переключение слайдов
  }

  goTo (direction, e) {
    const self = this;

    // Prevent Scroll
    let area = e.target;
    if (area.dataset.noscroll) return;

    let delta = e.deltaY || e.detail || e.wheelDelta;

    if (delta < 0 && area.scrollTop === 0) e.preventDefault();

    if (delta > 0 && area.scrollHeight - area.clientHeight - area.scrollTop <= 1) e.preventDefault();
    // End Prevent Scroll

    // Следующий слайд в зависимости от направления скрола
    let newSlide = self.activeSlide + direction;

    // Скрыть кнопку предыдущий вопрос на 1 вопросе
    if (self.activeSlide === 1 && newSlide === 2) {
      $('.prev-question-btn').hide()
    } else {
      $('.prev-question-btn').show()
    }

    if (newSlide > self.countSlide) { // Отключаем переключение дальше последнего слайда
      return
    } else if (newSlide < 1) { // Отключаем переключение дальше первого слайда
      return
    } else if (newSlide === 1) {
      $('.testing__btns').show()
      $('.testing__steps').hide()
      $('.testing__finish').hide()
    } else if (newSlide === self.countSlide) {
      $('.testing__btns').hide()
      $('.testing__steps').hide()
      $('.testing__finish').show()
    } else {
      $('.testing__btns').hide()
      $('.testing__steps').show()
      $('.testing__finish').hide()
    }

    let perc = (newSlide / self.countSlide) * 100 + '%'
    $('.progress__percentages').text(perc)
    $('.progress__progress').css('width', perc)

    if (!self.canGo) return;
    self.canGo = false;

    // Переключение слайдов, генерация события
    PubSub.publishSync('goToTest', {
      from: self.activeSlide,
      to: newSlide,
      direction: direction || 0
    });

    // Переназначение текущего слайда
    this.activeSlide = newSlide;
    // Сбросить запрет на скролл
    setTimeout(function () {
      self.canGo = true;
    }, self.delay);
  }

  // Обработка скрола
  clickEvents () {
    let self = this;

    $('.js-test').on('click', function (e) {
      self.goTo($(this).data('direction'), e)
    });
  }

  // Обработка нажатий стрелочек на клавиатуре
  pressEvents () {
    const self = this;
    let direction = 0;

    $(document).on('keyup', function (e) {
      switch (e.keyCode) {
        case 40: // Если нажата стрелка вниз
          direction = 1;
          self.goTo(direction, e)
          break;
        case 38: // Если нажата стрелка вверх
          direction = -1;
          self.goTo(direction, e)
          break;
        case 39: // Если нажата стрелка вправо
          direction = 1;
          self.goTo(direction, e)
          break;
        case 37: // Если нажата стрелка влево
          direction = -1;
          self.goTo(direction, e)
          break;
        default:
          break;
      }
    })
  }
}
