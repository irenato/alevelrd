import PubSub from 'pubsub-js';
import { TimelineMax } from 'gsap';
import 'jquery.nicescroll';

import 'owl.carousel';
const $ = window.$;
// Обработчик события goToSlide
PubSub.subscribe('goToTest', function (msg, data) {
  // Ссылка на дом узлы слайдов
  let $currentSlide = $(`[data-slide-testing='${data.from}']`);
  let $newSlide = $(`[data-slide-testing='${data.to}']`);

  let tl = new TimelineMax({
    onComplete: function () {
      $currentSlide.css('visibility', 'hidden')
    }
  });

  tl
    .fromTo($currentSlide, 1, {
      y: 0
    }, {
      opacity: 0,
      y: data.direction > 0 ? '-100%' : '100%'
    }, 'testSlide')
    .fromTo($newSlide, 1, {
      y: data.direction > 0 ? '100%' : '-100%'
    }, {
      opacity: 1,
      y: '0%',
      visibility: 'visible'
    }, 'testSlide')
});
