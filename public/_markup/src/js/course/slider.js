// import 'slick-carousel';
import 'owl.carousel'

const $ = window.$;

export default function slider() {
  $('.reviews-slider').owlCarousel({
    nav: true,
    loop: true,
    items: 1,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    autoHeight: true,
    navText: [
      '<i class="icon-arrow-left"></i>',
      '<i class="icon-arrow-right"></i>'
    ],
    responsive: {
      570: {
        loop: false
      }
    }
  });
}
