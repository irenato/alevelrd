import PubSub from 'pubsub-js';
const $ = window.$;
export default class Paginator {
  constructor () {
    // При инициализации класа сразу вызываем эти методы
    this.scrollEvent();
    this.pressEvents();
    this.clickEvent();
    this.activeSlide = 1;
    this.slideInside = 1;
    this.countSlideInside = $('[data-slide-course]').length;
    this.canGo = true; // Может ли проигрываться анимация?
    this.canGoSlide = true; // Может ли проигрываться анимация главных переходов?
    this.canGoSlideInside = true; // Может ли проигрываться анимация внутренних слайдов?
    this.max = $('.section').length; // Количество слайдов
    this.delay = 1000; // Задержка на переключение слайдов
    this.init()
    this.onHashChange()
  }

  onHashChange () {
    let self = this;
    $(window).on('hashchange', () => {
      self.init()
    })
  }

  init () {
    let nextSlide = 1;
    let nextSlideInside = null;
    switch (window.location.hash.substr(1)) {
      case 'course': {
        nextSlide = 1;
        break;
      }
      case 'details': {
        nextSlide = 2;
        nextSlideInside = 1;
        break;
      }
      case 'info': {
        nextSlide = 2;
        nextSlideInside = 2;
        break;
      }
      case 'program': {
        nextSlide = 2;
        nextSlideInside = 3;
        break;
      }
      case 'places': {
        nextSlide = 2;
        nextSlideInside = 4;
        break;
      }
      case 'price': {
        nextSlide = 2;
        nextSlideInside = 5;
        break;
      }
      case 'teachers': {
        nextSlide = 2;
        nextSlideInside = 6;
        break;
      }
      case 'reviews': {
        nextSlide = 2;
        nextSlideInside = 7;
        break;
      }
      case 'request': {
        nextSlide = 3;
        break;
      }
      default: {
        nextSlide = 1;

        break;
      }
    }

    this.goTo({nextSlide, nextSlideInside})
  }

  /**
   * Метод переключения основных слайдов
   * @param click - данные по клику
   * @param direction - направление переключения основных слайдов
   * @param nextSlide - на какой основной слайд переключать
   * @param nextSlideInside - на какой слайд переключать
   */
  goToSlide ({click, direction, nextSlide = null, nextSlideInside}) {
    const self = this;

    if (self.activeSlide + direction === 4) return;
    // Следующий слайд в зависимости от направления скрола
    let newSlide = nextSlide || self.activeSlide + direction;

    if (!nextSlideInside) {
      let hash;
      switch (newSlide) {
        case 1:
          hash = 'course';
          break;
        case 2:
          hash = 'details';
          break;
        case 3:
          hash = 'request';
          break;
        default:
          hash = '';
          break;
      }

      window.location.hash = hash;
    }

    // Проверка на максимальный слайд
    // Отключаем переключение дальше последнего слайда (при прокрутке вперед)
    if (newSlide > self.max) return;
    // Проверка на минимальный слайд
    // Отключаем переключение дальше первого слайда (при прокрутке назад)
    if (newSlide < 1) return;

    if (!self.canGoSlide) return;
    self.canGoSlide = false;

    // Переключение на 2 слайд с первого
    // На внутреннем слайде показать первый слайд
    if (newSlide === 2 && self.activeSlide < newSlide) {
      self.goToSlideInside({
        direction: self.slideInside < 1 ? 1 : -1,
        nextSlideInside: click ? click.inside : (nextSlideInside || 1)
      })
    } else if (newSlide === 2 && self.activeSlide > newSlide) { // Переключение на второй слайд с последнего, на внутреннем слайде показать последний слайд
      self.goToSlideInside({
        direction: self.slideInside < 1 ? 1 : -1,
        nextSlideInside: click ? click.inside : self.countSlideInside
      })
    } else {
      $(`.trigger`).removeClass('trigger--active');
      $(`.trigger[data-page=${newSlide}]`).addClass('trigger--active');
    }

    // Переключение слайдов, генерация события
    PubSub.publish('goToSlide', {
      from: self.activeSlide,
      to: newSlide,
      direction: direction,
      delay: self.delay / 1000
    });

    // Переназначение текущего слайда
    self.activeSlide = newSlide;

    // Сбросить запрет на скролл
    setTimeout(function () {
      self.canGoSlide = true;
    }, self.delay);
  }

  /**
   * Метод переключения внутренних слайдов
   * @param direction - направление переключения
   * @param click - данные по клику
   * @param nextSlideInside - на какой внутренний слайд переключить
   */
  goToSlideInside ({click, direction, nextSlideInside}) {
    const self = this;
    if (!self.canGoSlideInside) return;
    self.canGoSlideInside = false;

    // Следующий внутренний слайд в зависимости от направления скрола
    let newSlideInside = nextSlideInside || self.slideInside + direction;

    // Переключение слайдов, генерация события
    PubSub.publish('goToSlideInside', {
      from: self.slideInside,
      to: nextSlideInside || newSlideInside,
      direction: direction,
      delay: self.delay / 1000
    });

    let hash;
    switch (newSlideInside) {
      case 1:
        hash = 'details';
        break;
      case 2:
        hash = 'info';
        break;
      case 3:
        hash = 'program';
        break;
      case 4:
        hash = 'places';
        break;
      case 5:
        hash = 'price';
        break;
      case 6:
        hash = 'teachers';
        break;
      case 7:
        hash = 'reviews';
        break;
      default:
        hash = '';
        break;
    }
    window.location.hash = hash;

    // Переназначение текущего слайда
    self.slideInside = newSlideInside;

    if (self.slideInside === 4) {
      setTimeout(() => $('.auditory').addClass('active'), 2000)
    } else {
      $('.auditory').removeClass('active')
    }

    $(`.trigger`).removeClass('trigger--active');
    $(`.trigger[data-inside=${self.slideInside}]`).addClass('trigger--active');

    // Сбросить запрет на скролл
    setTimeout(function () {
      self.canGoSlideInside = true;
    }, self.delay);
  }

  goTo ({e, direction = 1, nextSlide = null, nextSlideInside = null, click}) {
    if (window.innerWidth < 1279) return

    const self = this;

    if (e) {
      // Prevent Scroll
      let area = e.target;
      if ($(area).parents('.noscroll').data('noscroll')) return;
      if (area.dataset.noscroll) return true;

      let delta = e.deltaY || e.detail || e.wheelDelta;

      if (delta < 0 && area.scrollTop === 0) window.addEventListener('touchstart', e => e.preventDefault(), { passive: false });

      if (delta > 0 && area.scrollHeight - area.clientHeight - area.scrollTop <= 1) window.addEventListener('touchstart', e => e.preventDefault(), { passive: false });

        // End Prevent Scroll
    }

    if (!self.canGo) return;
    self.canGo = false;

    // Скорее всего переключение по клику
    // Если на 2 страницу и на N-слайд внутри
    if (nextSlide && nextSlideInside) {
      // Если нужно переключится на вторую страницу - переключаемся
      if (self.activeSlide !== nextSlide) {
        // Направление для переключения основных слайдов
        direction = self.activeSlide < nextSlide ? 1 : -1;
        // Метод переключения основного слайда
        self.goToSlide({
          click,
          e,
          direction,
          nextSlide,
          nextSlideInside
        })
      }
      // Направление для переключения внутренних слайдов
      direction = self.slideInside < nextSlideInside ? 1 : -1;
      // Метод переключения внутренних слайдов
      self.goToSlideInside({
        click,
        direction,
        nextSlideInside
      })
    } else if (nextSlide) { // Если переключение не на 2 страницу
      // Направление для переключения основных слайдов
      direction = self.activeSlide < nextSlide ? 1 : -1;
      // Метод переключения основного слайда
      self.goToSlide({
        click,
        e,
        direction,
        nextSlide
      })
    } else if (self.activeSlide === 2) { // Если переключение на 2 страницу
      // Скорее всего по скролу
      // Переключение с 1 внутреннего слайда на предыдущий основной слайд
      if (self.slideInside === 1 && direction === -1) {
        // Метод переключения основного слайда
        self.goToSlide({
          click,
          e,
          direction,
          nextSlide
        })
      } else if (self.slideInside === self.countSlideInside && direction === 1) {
        // Переключение с последнего внутреннего слайда на следующий основной слайд'
        self.goToSlide({
          click,
          e,
          direction,
          nextSlide
        })
      } else {
        // Скорее всего скрол внутри слайда с внутренними слайдами
        // Метод переключения внутренних слайдов
        self.goToSlideInside({
          click,
          e,
          direction,
          nextSlideInside
        })
      }
    } else {
      // Метод переключения основного слайда
      self.goToSlide({
        click,
        e,
        direction,
        nextSlide
      })
    }

    // Сбросить запрет на скролл
    setTimeout(function () {
      self.canGo = true;
    }, self.delay);
  }

  /**
   * Переключение слайдов по прокрутке колесика мышки
   */
  scrollEvent () {
    let self = this;

    $(window).on('wheel', function (e) {
      if ($('body').hasClass('no-scroll')) return;
      e = e.originalEvent;
      let direction = e.deltaY > 0 ? 1 : -1; // Узнаем направление скрола вниз или верх
      // Направление для переключения основных слайдов
      self.goTo({
        e,
        direction
      })
    });
  }

  /**
   * Переключение слайдов по нажатию стрелочек на клавиатуре
   */
  pressEvents () {
    const self = this;
    let direction = 0;

    $(document).on('keyup', function (e) {
      switch (e.keyCode) {
        case 40: // Если нажата стрелка вниз
          direction = 1;
          self.goTo({
            e,
            direction
          });
          break;
        case 38: // Если нажата стрелка вверх
          direction = -1;
          self.goTo({
            e,
            direction
          });
          break;
        case 39: // Если нажата стрелка вправо
          direction = 1;
          self.goTo({
            e,
            direction
          });
          break;
        case 37: // Если нажата стрелка влево
          direction = -1;
          self.goTo({
            e,
            direction
          });
          break;
        default:
          break;
      }
    })
  }

  /**
   * Переключение слайдов по нажатию на боковое меню
   */
  clickEvent () {
    let self = this;
    $('.course-home__start, [data-page]').on('click', function (e) {
      let data = $(this).data();
      // Направление для переключения основных слайдов
      self.goTo({
        e,
        nextSlide: data.page,
        nextSlideInside: data.inside,
        click: data
      })
    });
  }
}
