import PubSub from 'pubsub-js';

const $ = window.$;

PubSub.subscribe('goToSlide', function (msg, data) {
  $(`.window-nav__item`).removeClass('window-nav__item--active'); // Удаляем активный класс в меню
  $(`[data-menu=${data.slideInCourse}]`).addClass('window-nav__item--active'); // Добавляем активный класс в меню
});
