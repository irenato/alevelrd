<i>{{ $title }}</i>
@foreach($data->all() as $key => $value)
    @if($key == 'course_id')
        <b>Курс: {{ \App\Models\Course\Course::find($value)->title }}</b>
    @elseif($key == 'city_id')
        <b>Город: {{ \App\Models\Locations\City::find($value)->title }}</b>
    @elseif($key == 'event_id')
        <b>Ивент: {{ \App\Models\Event\Event::find($value)->title }}</b>
    @elseif($key == 'workshop1' or $key == 'workshop2' or $key == 'workshop3' or $key == 'workshop4' )
        <b>Workshop: {{ \App\Models\Project_life\Project_programm::find($value)->title }}</b>
    @else
        <b>{{ $value }}</b>
    @endif
@endforeach