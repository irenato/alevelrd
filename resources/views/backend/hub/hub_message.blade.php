@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки хаба</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('hub_message_show.update-all') }}" class="btn btn-blue"><span class="fa-plus"></span> Применить все</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="5%">id</th>
                            <th width="10%">Имя</th>
                            <th width="15%">Email</th>
                            <th width="20%">Телефон</th>
                            <th width="30%">Текст</th>
                            <th width="10%">Создана</th>
                            <th width="10%">Статус</th>
                            <th width="10%"> </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($messages as $message)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->tel !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->text !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ \Carbon\Carbon::parse($message->created_at)->format('d-m-Y H:i:s') }}</span>
                                </td>

                                <td class="user">
                                    @if($message->viewed == 1)
                                        <span class="badge badge-success badge-roundless upper">Просмотрено</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Новое</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! route('hub_message_show', ['id' => $message->id]) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $messages->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
