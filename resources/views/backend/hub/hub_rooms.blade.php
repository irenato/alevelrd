@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Аудитории Хаба</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{route('add_room_index')}}" class="btn btn-success"><span class="fa-plus"></span> Добавить аудиторию</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">room-key</th>
                            <th width="10%">Название</th>
                            <th width="20%">Фото</th>
                            <th width="20%">Фото на плане</th>
                            <th width="10%">Вместимость</th>
                            <th width="20%">Информация</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($rooms as $room)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $room->room_key !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $room->room_name }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    {{--<span class="email">{{ $room->room_image_location }}</span>--}}
                                    <img src="{{$room->room_image_location}}" class="img-responsive" alt="{{$room->room_image_location}}" />
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
{{--                                    <span class="email">{{ $room->room_image }}</span>--}}
                                    <img src="{{ $room->room_image }}" class="img-responsive" alt="{{ $room->room_image }}" />

                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $room->capacity_text }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email"> {!!  $room->room_information !!}</span>
                                </td>

                                <td>
                                    <a href="{{route('hub_room_edit',['id' => $room->id])}}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection