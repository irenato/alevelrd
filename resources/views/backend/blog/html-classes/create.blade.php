@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Создание HTML-class</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('blog.html-classes.index') }}">HTML-classes</a>
                </li>
                <li class="active">
                    <strong>Создание </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    {{ Form::open(['url' => route('blog.html-classes.store'), 'method' => 'POST', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off']) }}

    @include('backend.blog.html-classes.fields')

    {{ Form::close() }}

@endsection
