@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Статьи</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('blog.articles.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' => route('blog.articles.index'), 'method' => 'GET']) !!}
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" name="search" value="{{Request::get('search')}}" class="form-control"
                               placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">ID</th>
                            <th width="20%">Thumbnail</th>
                            <th width="20%">Title</th>
                            <th width="20%">Author</th>
                            <th width="20%">Tags</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ route('blog.articles.sort') }}">
                        @forelse($articles as $article)
                            <tr class="js-sortable-item" data-id="{{ $article->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $article->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <img src="{{ $article->thumbnailMini }}"/>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $article->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ $article->authors_articles->pluck('fullName')->implode(',') }}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ $article->tags_articles->pluck('name')->implode(',') }}</span>
                                </td>
                                <td>
                                    <a href="{!! route('blog.articles.edit', $article->id) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $articles->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
