<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Основная информация</span>
                </a>
            </li>
            <li>
                <a href="#content" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Контент</span>
                </a>
            </li>
            <li>
                <a href="#seo" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">SEO</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">
                <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <div class="col-md-10 col-sm-8">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="thumbnail" src="{{ isset($article) ? $article->thumbnailMini : url('assets/images/user-4.png') }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a> Миниатюра
                                    <div class="invisible">
                                        {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="user-img">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <a href="#">
                                        <img data-img="background" src="{{ isset($article) ? $article->backgroundMini : url('assets/images/user-4.png') }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a> Фон
                                    <div class="invisible">
                                        {{Form::file('background', ['accept' => 'image/*', 'data-img' => 'background'])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('alias', 'Алиас *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('alias', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Алиас'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('authors_articles', 'Авторы *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('authors_articles[]', $authors, null,['class'=>'form-control js_ch_multiple', 'multiple' => 'multiple', 'id' => 'authors_articles', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('tags_articles', 'Теги *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('tags_articles[]', $tags, null,['class'=>'form-control js_ch_multiple', 'multiple' => 'multiple', 'id' => 'tags_articles', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('related', 'Похожие статьи *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('related[]', $articles, null,['class'=>'form-control js_ch_multiple', 'multiple' => 'multiple', 'id' => 'tags_articles', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('likes', 'Лайков', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('likes', null, ['class' => 'form-control', 'id' => 'likes', 'data-validate' => 'required', "placeholder" => '0'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('dislikes', 'Дизлайков', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('dislikes', null, ['class' => 'form-control', 'id' => 'dislikes', 'data-validate' => 'required', "placeholder" => '0'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('views', 'Просмотров', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('views', null, ['class' => 'form-control', 'id' => 'views', 'data-validate' => 'required', "placeholder" => '0'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('position', 'Позиция', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('position', null, ['class' => 'form-control', 'id' => 'position', 'data-validate' => 'required', "placeholder" => '0'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            {{ Form::hidden('active', 0) }}
                            {{ Form::checkbox('active', 1, null, ['class' => 'iswitch iswitch-secondary', 'id' => 'active']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('is_outer', 'Со внешнего источника', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            {{ Form::hidden('is_outer', 0) }}
                            {{ Form::checkbox('is_outer', 1, null, ['class' => 'iswitch iswitch-secondary', 'id' => 'is_outer']) }}
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane" id="content"  role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        @foreach($locales as $lang)
                            <li class="@if ($loop->first) active @endif"><a data-toggle="tab"
                                                                            href="#v-pills-{{ $lang }}">{{ $lang }}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($locales as $lang)
                            <div class="tab-pane  @if ($loop->first) active @endif" id="v-pills-{{ $lang }}">
                                <div class="form-group">
                                    {{ Form::label('title' . '-' . $lang, 'Тайтл (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::text('' . $lang . '[title]', $article->{'title:'.$lang}??'',['class'=>'form-control', 'id' => 'title' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('content' . '-' . $lang, 'Контент (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::textarea('' . $lang . '[content]', $article->{'content:'.$lang}??'', ['class'=>'form-control ckeditor', 'id' => 'content' . '-' . $lang]) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="seo"  role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        @foreach($locales as $lang)
                            <li class="@if ($loop->first) active @endif"><a data-toggle="tab"
                                                                            href="#v-pills-{{ $lang }}">{{ $lang }}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($locales as $lang)
                            <div class="tab-pane  @if ($loop->first) active @endif" id="v-pills-{{ $lang }}">
                                <div class="form-group">
                                    {{ Form::label('seo_title' . '-' . $lang, 'СЕО-тайтл (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::text('' . $lang . '[seo_title]', $article->{'seo_title:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_title' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_canonical' . '-' . $lang, 'СЕО-canonical (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::text('' . $lang . '[seo_canonical]', $article->{'seo_canonical:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_canonical' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_robots' . '-' . $lang, 'СЕО-robots (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::select('seo_robots', $seo_robots, null,['class'=>'form-control', 'id' => 'seo_robots' . '-' . $lang, 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_keywords' . '-' . $lang, 'СЕО-keywords (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::textarea('' . $lang . '[seo_keywords]', $article->{'seo_keywords:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_keywords' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_description' . '-' . $lang, 'СЕО-description (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::textarea('' . $lang . '[seo_description]', $article->{'seo_description:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_description' . '-' . $lang]) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ route('blog.authors.index') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            @isset($article)
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            @endisset
        </div>
    </div>
</div>

@section('scripts')
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Blog\ArticleRequest')->ignore('') !!}
@endsection