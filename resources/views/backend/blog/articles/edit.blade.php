@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('blog.articles.index') }}">Статьи</a>
                </li>
                <li class="active">
                    <strong>Редактирование</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    {{ Form::model($article, ['url' => route('blog.articles.update', ['article' => $article->id]), 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off','files' => true]) }}

    @include('backend.blog.articles.fields')

    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['blog.articles.destroy', $article->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

