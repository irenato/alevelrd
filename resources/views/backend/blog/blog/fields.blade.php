<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#seo" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">SEO</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="seo"  role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        @foreach($locales as $lang)
                            <li class="@if ($loop->first) active @endif"><a data-toggle="tab"
                                                                            href="#v-pills-{{ $lang }}">{{ $lang }}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($locales as $lang)
                            <div class="tab-pane  @if ($loop->first) active @endif" id="v-pills-{{ $lang }}">
                                <div class="form-group">
                                    {{ Form::label('seo_title' . '-' . $lang, 'СЕО-тайтл (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::text('' . $lang . '[seo_title]', $blog->{'seo_title:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_title' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_canonical' . '-' . $lang, 'СЕО-canonical (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::text('' . $lang . '[seo_canonical]', $blog->{'seo_canonical:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_canonical' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_robots' . '-' . $lang, 'СЕО-robots (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::select('seo_robots', $seo_robots, null,['class'=>'form-control', 'id' => 'seo_robots' . '-' . $lang, 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_keywords' . '-' . $lang, 'СЕО-keywords (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::textarea('' . $lang . '[seo_keywords]', $blog->{'seo_keywords:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_keywords' . '-' . $lang]) !!}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('seo_description' . '-' . $lang, 'СЕО-description (' . $lang . ')', ['class' => 'control-label']) }}
                                    {!! Form::textarea('' . $lang . '[seo_description]', $blog->{'seo_description:'.$lang}??'',['class'=>'form-control', 'id' => 'seo_description' . '-' . $lang]) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ route('blog.authors.index') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
        </div>
    </div>
</div>

@section('scripts')
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Blog\ArticleRequest')->ignore('') !!}
@endsection