@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Создание</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('blog.authors.index') }}">Authors</a>
                </li>
                <li class="active">
                    <strong>Создание </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    {{ Form::open(['url' => route('blog.authors.store'), 'method' => 'POST', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'files' => true]) }}

    @include('backend.blog.authors.fields')

    {{ Form::close() }}

@endsection
