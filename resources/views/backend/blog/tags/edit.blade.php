@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('blog.tags.index') }}">Tags</a>
                </li>
                <li class="active">
                    <strong>Редактирование</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    {{ Form::model($tag, ['url' => route('blog.tags.update', ['tag' => $tag->id]), 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off']) }}

    @include('backend.blog.tags.fields')

    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['blog.tags.destroy', $tag->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

