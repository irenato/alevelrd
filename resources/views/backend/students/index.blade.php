@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Студенты</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/students/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить студента</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="10%">Фото</th>
                            <th width="15%">Фамилия</th>
                            <th width="15%">Имя</th>
                            <th width="20%">Город</th>
                            <th width="20%">Курс</th>
                            <th width="15%">Статус</th>
                            <th width="15%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/students', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>{{ Form::text('first_name', request()->first_name, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Фамилия"]) }}</th>
                            <th>{{ Form::text('last_name', request()->last_name, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Имя"]) }}</th>
                            <th>
                                <select name="city_id" class="form-control">
                                    <option value="">Выберите</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}"
                                                @if(request()->city_id == $city->id) selected="selected" @endif>{{ $city->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select name="course_id" class="form-control">
                                    <option value="">Выберите</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}"
                                                @if(request()->course_id == $course->id) selected="selected" @endif>{{ $course->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите</option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif>Активен
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Заблокирован
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($students as $student)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $student->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('students')->url(  $student->id . '/' . $student->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $student->first_name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $student->last_name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $student->city->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $student->courses_students()->pluck('title')->implode(',') !!}</span>
                                </td>
                                <td class="user">
                                    @if($student->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/students/' . $student->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $students->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection