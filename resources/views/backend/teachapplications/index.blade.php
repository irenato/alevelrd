@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки (учителя)</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('teachapplications.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('teachapplications.update-all') }}" class="btn btn-blue"><span class="fa-plus"></span> Применить все</a>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('teachapplications.export', request()->all()) }}" class="btn btn-primary"><span class="fa-download"></span> export</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="15%">Имя</th>
                            <th width="20%">Email</th>
                            <th width="15%">Телефон</th>
                            <th width="20%">Курс</th>
                            <th width="10%">Город</th>
                            <th width="10%">Статус</th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/teachapplications', 'method' => 'GET']) }}
                            <th></th>
                            <th>{{ Form::text('username', request()->username, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Автор"]) }}</th>
                            <th>{{ Form::text('email', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Email"]) }}</th>
                            <th>{{ Form::text('phone', request()->phone, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Телефон"]) }}</th>
                            <th>
                                <select name="course_id" class="form-control" data-active="{{ request()->course_id }}">
                                    <option value="">Выберите</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select name="city_id" class="form-control" data-active="{{ request()->city_id }}">
                                    <option value="">Выберите</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите
                                    </option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif>Активен
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Заблокирован
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($applications as $application)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->username !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->phone !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->course->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! isset($application->city) ? $application->city->title : '' !!}</span>
                                </td>
                                <td class="user">
                                    @if($application->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/teachapplications/' . $application->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $applications->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection