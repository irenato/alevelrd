@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование пользователя</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/users') }}">Пользователи</a>
                </li>
                <li class="active">
                    <strong>Редактирование пользователя</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/users/' . $user->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product']) }}
    {{ Form::hidden('id', $user->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('name', 'Имя *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::text('name', $user->name, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Имя'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::text('email', $user->email, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Email'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('password', 'Пароль *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::password('password', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Телефон'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($roles) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="role_id">Роль</label>
                                <div class="col-sm-9">
                                    <select name="role_id" id="role_id" class="form-control" data-active="{{ $user->roles_users()->pluck('id')->first()}}">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/users') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

