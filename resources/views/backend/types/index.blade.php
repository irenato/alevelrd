@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Типы курсов</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('types.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="10%">Фото</th>
                            <th width="35%">Название</th>
                            <th width="35%">Курсы</th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/types', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>{{ Form::text('title', request()->username, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Название"]) }}</th>
                            <th>
                                <select name="course_id" class="form-control" data-active="{{ request()->course_id }}">
                                    <option value="">Выберите</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($types as $type)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $type->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('types')->url(  $type->id . '/' . $type->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $type->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $type->courses->pluck('title')->implode(', ') !!}</span>
                                </td>
                                <td>
                                    <a href="{!! url('backend/types/' . $type->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $types->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
