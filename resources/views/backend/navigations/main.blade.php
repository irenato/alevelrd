<ul id="main-menu" class="main-menu">
    <li>
        <a href="#">
            <i class="fa-pagelines"></i>
            <span class="title">Страницы</span>
        </a>
        <ul>
            <li>
                <a href="#">
                    <i class="fa-home"></i>
                    <span class="title">Главная</span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('homepage.index') }}">
                            <i class="fa-home"></i>
                            <span class="title">Главная</span>
                        </a>
                    </li>
                <!-- <li>
                        <a href="{{ route('opinions.index') }}">
                            <i class="fa-paragraph"></i>
                            <span class="title">О нас говорят</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            <li>
                <a href="{{ route('creatorpage.index') }}">
                    <i class="fa-puzzle-piece"></i>
                    <span class="title">Сделано выпускниками</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-book"></i>
            <span class="title">Курсы</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('courses.index') }}">
                    <i class="fa-book"></i>
                    <span class="title">Курсы</span>
                </a>
            </li>
            <li>
                <a href="{{ route('types.index') }}">
                    <i class="fa-paragraph"></i>
                    <span class="title">Типы</span>
                </a>
            </li>
            <li>
                <a href="{{ route('actions.index') }}">
                    <i class="fa-gift"></i>
                    <span class="title">Акция</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-newspaper-o"></i>
            <span class="title">Блог</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('blog.blogs-content.index') }}">
                    <i class="fa-bookmark-o"></i>
                    <span class="title">Блог</span>
                </a>
            </li>
            <li>
                <a href="{{ route('blog.articles.index') }}">
                    <i class="fa-newspaper-o"></i>
                    <span class="title">Статьи</span>
                </a>
            </li>
            <li>
                <a href="{{ route('blog.authors.index') }}">
                    <i class="fa-pencil"></i>
                    <span class="title">Автора</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa-tags"></i>
                    <span class="title">Теги</span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('blog.tags.index') }}">
                            <i class="fa-tags"></i>
                            <span class="title">Теги</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('blog.html-classes.index') }}">
                            <i class="fa-html5"></i>
                            <span class="title">HTML-classes</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('blog.subscribers.index') }}">
                    <i class="fa-pencil"></i>
                    <span class="title">Подписка</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ url('backend/events') }}">
            <i class="fa-birthday-cake"></i>
            <span class="title">Ивенты</span>
        </a>
    </li>
    <li>
        <a href="{{ url('backend/discounts') }}">
            <i class="fa-gift"></i>
            <span class="title">Скидки</span>
        </a>
    </li>
    <li>
        <a href="{{ url('backend/teachers') }}">
            <i class="fa-child"></i>
            <span class="title">Учителя</span>
        </a>
    </li>
    <li>
        <a href="{{ url('backend/students') }}">
            <i class="fa-university"></i>
            <span class="title">Студенты</span>
        </a>
    </li>
    <li>
        <a href="{{ route('partners.index') }}">
            <i class="fa-empire"></i>
            <span class="title">Партнеры</span>
        </a>
    </li>
    <li>
        <a href="{{ route('promocodes.index') }}">
            <i class="fa-pied-piper"></i>
            <span class="title">Промокоды</span>
        </a>
    </li>
    <li>
        <a href="{{ route('rewards.index') }}">
            <i class="fa-gift"></i>
            <span class="title">Награды</span>
        </a>
    </li>
    <li>
        <a href="#">
            <i class="fa-leaf"></i>
            <span class="title">Заявки <span class="text-danger">{{ $total_amount }}</span></span>
        </a>
        <ul>
            <li>
                <a href="{{ route('reviews.index') }}">
                    <i class="fa-smile-o"></i>
                    <span class="title">Отзывы <span class="text-danger">{{ $newReviewsApplications }}</span></span>
                </a>
            </li>
            <li>
                <a href="{{ route('stapplications.index') }}">
                    <i class="fa-money"></i>
                    <span class="title">Студенты <span class="text-danger">{{ $newStudentsApplications }}</span></span>
                </a>
            </li>
            <li>
                <a href="{{ route('teachapplications.index') }}">
                    <i class="fa-magic"></i>
                    <span class="title">Учителя <span class="text-danger">{{ $newTeachersApplications }}</span></span>
                </a>
            </li>
            <li>
                <a href="{{ route('partnerapplications.index') }}">
                    <i class="fa-paperclip"></i>
                    <span class="title">Партнеры <span class="text-danger">{{ $newPartnersApplications }}</span></span>
                </a>
            </li>
            <li>
                <a href="{{ route('callbackapplications.index') }}">
                    <i class="fa-user"></i>
                    <span class="title">Сообщения <span class="text-danger">{{ $newCallbackApplications }}</span></span>
                </a>
            </li>
            <li>
                <a href="{{ route('eventapplications.index') }}">
                    <i class="fa-user"></i>
                    <span class="title">Мероприятия <span class="text-danger">{{ $newEventApplications }}</span></span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-question"></i>
            <span class="title">Тесты</span>
        </a>
        <ul>
            <li>
                <a href="{!! url('/backend/tests') !!}">
                    <i class="fa-paint-brush"></i>
                    <span class="title">Профориентационные</span>
                </a>
            </li>
            <li>
                <a href="{!! route('course-tests.index') !!}">
                    <i class="fa-paint-brush"></i>
                    <span class="title">Курсы</span>
                </a>
            </li>
            <li>
                <a href="{!! route('ttypes.index') !!}">
                    <i class="fa-paint-brush"></i>
                    <span class="title">Типы</span>
                </a>
            </li>
            <li>
                <a href="{!! route('test-content.index') !!}">
                    <i class="fa-paint-brush"></i>
                    <span class="title">Контент</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-google"></i>
            <span class="title">Сео</span>
        </a>
        <ul>
            <li>
                <a href="{!! url('/backend/seoscripts') !!}">
                    <i class="fa-cog"></i>
                    <span class="title">Скрипты</span>
                </a>
            </li>
            <li>
                <a href="{!! route('redirects.index') !!}">
                    <i class="fa-cog"></i>
                    <span class="title">Редиректы</span>
                </a>
            </li>
            <li>
                <a href="{!! route('sitemap.index') !!}">
                    <i class="fa-link"></i>
                    <span class="title">Карта сайта</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-newspaper-o"></i>
            <span class="title">Контакты</span>
        </a>
        <ul>
            <li>
                <a href="{!! url('/backend/cities') !!}">
                    <i class="fa-map-marker"></i>
                    <span class="title">Города</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="fa-cog"></i>
            <span class="title">Настройки</span>
        </a>
        <ul>
            <li>
                <a href="{!! url('/backend/settings') !!}">
                    <i class="fa-cog"></i>
                    <span class="title">Настройки</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/telegram-configs') !!}">
                    <i class="fa-paper-plane"></i>
                    <span class="title">Телеграм (каналы)</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/users') !!}">
                    <i class="fa-beer"></i>
                    <span class="title">Пользователи</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/roles') !!}">
                    <i class="fa-male"></i>
                    <span class="title">Роли</span>
                </a>
            </li>
            <li>
                <a href="{!! url('backend/permissions') !!}">
                    <i class="fa-unlock"></i>
                    <span class="title">Права доступа</span>
                </a>
            </li>
            <li>
                <a href="{{ route('creators.index') }}">
                    <i class="fa-puzzle-piece"></i>
                    <span class="title">Создатели сайта</span>
                </a>
            </li>
        </ul>
    </li>
    @permission('hub')
    <li>
        <a href="#">
            <i class="fa-cog"></i>
            <span class="title">Хаб {{$total_hub_amount}} </span>
        </a>
        <ul>
            <li>
                <a href="{{route('hub_message')}}">
                    <i class="fa-cog"></i>
                    <span class="title">Заявки {{$total_hub_amount}}</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin_hub')}}">
                    <i class="fa-male"></i>
                    <span class="title">Настройки</span>
                </a>
            </li>
            <li>
                <a href="{{route('hub_rooms')}}">
                    <i class="fa-male"></i>
                    <span class="title">Аудитории Хаба</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission
    @permission('projectlife')
    <li>
        <a href="#">
            <i class="fa-cog"></i>
            <span class="title">Жизнь проекта {{$total_project_amount}}</span>
        </a>
        <ul>

            <li>
                <a href="{{route('project_callbacks')}}">
                    <i class="fa-male"></i>
                    <span class="title">Заявки {{$total_project_amount}}</span>
                </a>
            </li>

            <li>
                <a href="{{route('project_admin')}}">
                    <i class="fa-male"></i>
                    <span class="title">Настройки</span>
                </a>
            </li>
            <li>
                <a href="{{route('project_speakers')}}">
                    <i class="fa-male"></i>
                    <span class="title">Спикеры</span>
                </a>
            </li>
            <li>
                <a href="{{route('project_programms')}}">
                    <i class="fa-male"></i>
                    <span class="title">Программы</span>
                </a>
            </li>
            <li>
                <a href="{{route('project_timeline')}}">
                    <i class="fa-male"></i>
                    <span class="title">Timeline ( таблица )</span>
                </a>
            </li>

            <li>
                <a href="{{route('partners')}}">
                    <i class="fa-male"></i>
                    <span class="title">Партнёры </span>
                </a>
            </li>


        </ul>
    </li>
    @endpermission
</ul>