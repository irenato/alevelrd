@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Учителя</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/teachers/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить учителя</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="10%">Фото</th>
                            <th width="15%">Фамилия</th>
                            <th width="15%">Имя</th>
                            <th width="20%">Специализация</th>
                            <th width="15%">Статус</th>
                            <th width="15%">Действие</th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/teachers', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>{{ Form::text('first_name', request()->first_name, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Фамилия"]) }}</th>
                            <th>{{ Form::text('last_name', request()->last_name, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Имя"]) }}</th>
                            <th>{{ Form::text('specialization', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Специализация"]) }}</th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите
                                    </option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif>Активен
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Заблокирован
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/teachers/sort/') }}">
                        @forelse($teachers as $teacher)
                            <tr class="js-sortable-item" data-id="{{ $teacher->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $teacher->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('teachers')->url(  $teacher->id . '/' . $teacher->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $teacher->first_name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $teacher->last_name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $teacher->specialization !!}</span>
                                </td>
                                <td class="user">
                                    @if($teacher->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/teachers/' . $teacher->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $teachers->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection