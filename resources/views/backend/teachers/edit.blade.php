@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование учителя</h1>
            <p class="description">Редактирование учителя</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/teachers') }}">Учителя</a>
                </li>
                <li class="active">
                    <strong>Редактирование учителя</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/teachers/' . $teacher->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $teacher->id) }}
    {{ Form::hidden('old_thumbnail', $teacher->thumbnail) }}
    {{ Form::hidden('old_logo', $teacher->logo) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#text" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Дополнительная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="logo" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('teachers')->url(  $teacher->id . '/' . $teacher->logo  ), 100, 100) }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> logo
                                        <div>
                                            {{Form::file('logo', ['accept' => 'image/*', 'data-img' => 'logo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#" class="js_form-link">
                                            <img id="photo_img" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('teachers')->url(  $teacher->id . '/' . $teacher->thumbnail  ), 100, 100) }}"
                                                 class="img-circle"
                                                 alt="user-pic"/>
                                        </a>
                                        <div>
                                            {{Form::file('thumbnail', ['accept' => 'image/*', 'id' => 'photo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('first_name', 'Фамилия *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('first_name', $teacher->first_name, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Фамилия'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('last_name', 'Имя *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('last_name', $teacher->last_name, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Имя'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('specialization', 'Специализация *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-check"></i></a>
                                    </div>
                                    {{  Form::text('specialization', $teacher->specialization, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Специализация'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($courses) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3 control-label" for="organization_id">Курсы</label>
                                <div class="col-sm-9">
                                    <select name="course_id[]" id="course_id" multiple class="form-control js_ch_multiple">
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}" @if(in_array($course->id, $teacher_courses)) selected="selected" @endif>{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, $teacher->active, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('is_teacher', 'Учитель / Лектор', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('is_teacher', 1, $teacher->is_teacher, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 control-label" for="js_clients_phones">Ссылка на профиль</label>
                            <div class="col-sm-10 js_parent">
                                @if(is_array($teacher->links))
                                    @foreach ($teacher->links as $key => $value)
                                        @if(isset($value['html_class']) && isset($value['link']))
                                            <div class="input-group js_section">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="fa-link"></i></a>
                                                </div>
                                                {{  Form::text('links[html_class][]', $value['html_class'], ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                                {{  Form::text('links[link][]', $value['link'], ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                                <div class="input-group-addon js_remove_item">
                                                    <a href="#">-</a>
                                                </div>
                                                <div class="input-group-addon js_add_item">
                                                    <a href="#">+</a>
                                                </div>
                                            </div>
                                            @else
                                            <div class="input-group js_section">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="fa-link"></i></a>
                                                </div>
                                                {{  Form::text('links[html_class][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                                {{  Form::text('links[link][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                                <div class="input-group-addon js_remove_item">
                                                    <a href="#">-</a>
                                                </div>
                                                <div class="input-group-addon js_add_item">
                                                    <a href="#">+</a>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @else
                                    <div class="input-group js_section">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa-link"></i></a>
                                        </div>
                                        {{  Form::text('links[html_class][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                        {{  Form::text('links[link][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                        <div class="input-group-addon js_remove_item">
                                            <a href="#">-</a>
                                        </div>
                                        <div class="input-group-addon js_add_item">
                                            <a href="#">+</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="text">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Краткое описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('introtext', $teacher->introtext, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('description', $teacher->description, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', $teacher->seo_title, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', $teacher->seo_keywords, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', $teacher->seo_robots, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', $teacher->seo_canonical, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', $teacher->seo_description, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group-separator __web-inspector-hide-shortcut__"></div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ url('backend/teachers') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['teachers.destroy', $teacher->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
