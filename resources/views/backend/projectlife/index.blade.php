@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование страницы </h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    {{ Form::open(['route' => 'project_life_update', 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true, 'multiple'=>'multiple']) }}
    {{ Form::hidden('id', $data->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Первый экран</span>
                    </a>
                </li>
                <li>
                    <a href="#two" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Второй экран</span>
                    </a>
                </li>
                <li>
                    <a href="#four" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Четвертый экран</span>
                    </a>
                </li>
                <li>
                    <a href="#six" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Шестой экран</span>
                    </a>
                </li>
                <li>
                    <a href="#seven" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Седьмой экран</span>
                    </a>
                </li>

                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Сео</span>
                    </a>
                </li>


            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('date', 'Дата *', ['class' => 'control-label','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('date', $data->date, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Дата'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('place', 'Место проведения *', ['class' => 'control-label','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('place', $data->place, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Место проведения'])}}
                                </div>
                            </div>
                        </div>


                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('amount', 'Количество заявок = реальных '.$count_callback.' + ', ['class' => 'control-label','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('amount', $data->amount, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Дата'])}}
                                </div>
                            </div>
                        </div>


                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('link', 'Ссылка на видео *', ['class' => 'control-label','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('link', $data->link, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Сыылка на видео'])}}
                                </div>
                            </div>
                        </div>


                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_1_h1', 'Заголовок *', ['class' => 'control-label','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_1_h1', $data->section_1_h1, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_1_h2', 'Текст class h2', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_1_h2', $data->section_1_h2, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_1_p', 'Текст', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::textarea('section_1_p', $data->section_1_p, ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_1_p_cl_title', 'Текст в блоке с обратным отчётом', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_1_p_cl_title', $data->section_1_p_cl_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>




                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_1_p_img', 'Фото', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="section_1_p_img" src="{{ $data->section_1_p_img, 100, 100 }}"
                                        class="img-rounded js_image_link"
                                        alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('section_1_p_img', ['accept' => 'image/*', 'data-img' => 'section_1_p_img'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('section_1_p_img_old', $data->section_1_p_img) }}
                            </div>
                        </div>

                    </div>

                </div>
                <div class="tab-pane" id="two">
                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Заголовок</label>
                        <div class="col-sm-9">
                            {{  Form::text('section_2h1_cl', $data->section_2h1_cl, ['class' => 'form-control','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('section_2p', 'Текст class h2', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('section_2p', $data->section_2p, ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                            </div>
                        </div>
                    </div>

                    {{-- 3 Слайдера --}}
                    @foreach($sliders as $slider)
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('slider_img'.$slider['id'], 'Фото на '. $slider['id']. ' слайдере', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="slider_img{{$slider['id']}}" src="{{$slider->img ,150,100}}"
                                         class="img-rounded js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('slider_img'.$slider['id'], ['accept' => 'image/*', 'data-img' => 'slider_img'.$slider['id']])}}
                                </div>
                            </div>
                            {{ Form::hidden('slider_img_old'.$slider['id'], $slider->img ) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Текст на {{$slider['id']}} слайдере </label>
                        <div class="col-sm-9">
                            {{  Form::text('slider_text'.$slider['id'], $slider->text , ['class' => 'form-control']) }}
                        </div>
                    </div>
                    @endforeach

                    </div>
                </div>

                <div class="tab-pane" id="four">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Заголовок </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_4_h1', $data->section_4_h1, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>


                <div class="tab-pane" id="six">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Название первой группы партнеров </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_6_h4_1', $data->section_6_h4_1, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Название второй группы партнеров </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_6_h4_2', $data->section_6_h4_2, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Название третьей группы партнеров </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_6_h4_3', $data->section_6_h4_3, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="seven">

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1"> Заголовок модалки ( после оставления заявки) </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_7_m_h2_cl', $data->section_7_m_h2_cl, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('section_7_m_p', 'Текст в модалке ( после оставления заявки)', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('section_7_m_p', $data->section_7_m_p, ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Текст в модалке, при неуспешной заявке </label>
                        <div class="col-sm-9">
                            {{  Form::text('section_7_m_error', $data->section_7_m_error, ['class' => 'form-control']) }}
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">title</label>
                        <div class="col-sm-9">
                            {{  Form::text('title', $data->title, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">seo_description</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_description', $data->seo_description, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">seo_keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', $data->seo_keywords, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">seo_robots </label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', $data->seo_robots, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1"> seo_canonical </label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', $data->seo_canonical, ['class' => 'form-control']) }}
                        </div>
                    </div>


                </div>



                </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@include('backend.modals.image')

@include('backend.modals.image-remove')

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
