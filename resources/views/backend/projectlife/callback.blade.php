@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки жизни проекта</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('project_callbacks_export', request()->all()) }}" class="btn btn-primary"><span class="fa-download"></span> export</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="5%">id</th>
                            <th width="10%">Имя
                                <br>
                                <a class="glyphicon glyphicon-chevron-down" href="?sort=name&orderby=asc"></a>
                                <a class="glyphicon glyphicon-chevron-up" href="?sort=name&orderby=desc"></a>
                            </th>
                            <th width="15%">Фамилия
                                <br>
                                <a class="glyphicon glyphicon-chevron-down" href="?sort=surname&orderby=asc"></a>
                                <a class="glyphicon glyphicon-chevron-up" href="?sort=surname&orderby=desc"></a>
                            </th>
                            <th width="20%">Email
                            </th>
                            <th width="20%">Телефон
                                <br>
                                <a class="glyphicon glyphicon-chevron-down" href="?sort=phone&orderby=asc"></a>
                                <a class="glyphicon glyphicon-chevron-up" href="?sort=phone&orderby=desc"></a>
                            </th>
                            <th width="15%">Workshop
                            </th>
                            <th width="10%">Статус
                                <br>
                                <a class="glyphicon glyphicon-chevron-down" href="?sort=viewed&orderby=asc"></a>
                                <a class="glyphicon glyphicon-chevron-up" href="?sort=viewed&orderby=desc"></a>

                            </th>

                            <th width="5%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['route' => ['project_callbacks'], 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            {{--<th>{{ Form::text('username', request()->username, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Автор"]) }}</th>--}}
{{--                            <th>{{ Form::text('email', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Email"]) }}</th>--}}
                            <th>
                            </th>
                            <th>
                            </th>
                            <th>
                            </th>
                            <th>
                            </th>
                            <th>
                                {{--<select name="workshop_id" class="form-control" data-active="{{ request()->course_id }}">--}}
                                    {{--<option value="">Все заявки</option>--}}
                                    {{--@foreach($workshops as $workshop)--}}
                                        {{--<option value="{{ $workshop->id }}">{{ $workshop->title }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            </th>

                            <th>
                                {{--<button type="submit" class="btn btn-sm btn-success">--}}
                                    {{--<i class="fa fa-search">--}}

                                    {{--</i>--}}
                                {{--</button>--}}
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $message)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->id !!}</span>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->surname !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $message->phone !!}</span>
                                </td>


                                <td class="user">
                                    @foreach($message->workshop as $t)
                                        {{$t->title}}<br>
                                    @endforeach
                                </td>

                                <td class="user">
                                    @if($message->viewed == 1)
                                        <span class="badge badge-success badge-roundless upper">Просмотрено</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Новое</span>
                                    @endif
                                </td>



                                <td>
                                    <a href="{!! route('project_message_show', ['id' => $message->id]) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-eye"></i>
                                    </a>
                                </td>

{{----}}
                                {{--<td>--}}
                                    {{--<a href="{!! url('backend/reviews/' . $message->id . '/edit') !!}" class="btn btn-sm btn-success">--}}
                                        {{--<i class="linecons-pencil"></i>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $data->appends([request(),'sort' => request('sort'),'orderby' => request('orderby')])->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection