@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление партнёра </h1>
            <p class="description">Добавление партнёра </p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('partners') }}">Партнёры</a>
                </li>
                <li class="active">
                    <strong> Редактирование партнёра  </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['route' => ['project_partners_update', $partners->id], 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $partners->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('title', 'Название*', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('title', $partners->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('link', 'Ссылка*', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('link', $partners->link, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'укажите ссылку'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('type', 'type', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                <select name="type" class="form-control">
                                    <option {{ $partners->type == 'general' ? 'selected' : '' }} value="general">Генеральный</option>
                                    <option {{ $partners->type == 'premium' ? 'selected' : '' }} value="premium">Премиум</option>
                                    <option {{ $partners->type == 'media' ? 'selected' : '' }} value="media">Медиа</option>
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="row form-group">
                        <div class="col-sm-3">

                            {{ Form::label('logo', 'Логотип', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="logo" src="{{$partners->logo}}"
                                         class="img-circle js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('logo', ['accept' => 'image/*', 'data-img' => 'logo'])}}
                                </div>
                            </div>
                            {{ Form::hidden('logo_old', $partners->logo) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('short_text', 'Краткий текст *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('short_text', $partners->short_text , ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => ' '])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('text', 'Полный текст', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('text', $partners->text, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('partners')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>

    </div>

    {{ Form::close() }}

    <div class="hidden">
            {!! Form::open(['route' => ['project_partners_dell', $partners->id], 'method' => 'post']) !!}
            {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
    {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
