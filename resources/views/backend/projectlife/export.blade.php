<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>name</th>
        <th>surname</th>
        <th>email</th>
        <th>phone</th>
        <th>workshop</th>
        <th>viewed</th>
        <th>created</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->surname }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->phone }}</td>
            <td>
                {{ isset($item->workshop['0']['title']) ? $item->workshop['0']['title'] : '-'}}
            </td>
            <td>{{ $item->viewed ? 'да':'нет'}}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>