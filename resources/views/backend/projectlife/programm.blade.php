@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Программа </h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{route('project_programms_add')}}" class="btn btn-success"><span class="fa-plus"></span> Добавить мероприятие</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="20%">Название  </th>
                            <th width="5%">Workshop</th>
                            <th width="10%">Время</th>
                            <th width="15%">Комната</th>
                            <th width="10%">Краткий текст</th>
                            <th width="20%">Полный текст</th>
                            <th width="10%">Спикеры</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $programm)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $programm->title !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                   @php $workshop = ($programm->workshop == 1) ?  'Да': 'Нет' @endphp
                                    <span class="email">{!! $workshop !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{!! $programm->time !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{!! $programm->room !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $programm->short_text }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $programm->text }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    @foreach($programm['speakers'] as $speaker)
                                        <span class="email">{{ $speaker->name }}</span> <br>
                                        @endforeach
                                </td>

                                <td>
                                    <a href="{{route('project_programms_edit',['id' => $programm->id])}}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
