@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование таблицы </h1>
            <p class="description">Редактирование таблицы </p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('project_programms') }}">Программа</a>
                </li>
                <li class="active">
                    <strong>Редактирование таблицы  </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <link rel="stylesheet" href="/life3.0/css/style.css" type="text/css" />
    <div class="col-lg-7 col-md-9">
        <div class="box-dark box-timeline">
            <p class="h4">Timeline</p>
            <div class="timeline">
                <div class="table-responsive-sm">
                    <table>
                        {{ Form::open(['route' => ['project_timeline_update'], 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true, 'multiple'=>'multiple']) }}
                        <tr class="first">
                            <th></th>
                            <td class="border" data-room="gray">Grey room</td>
                            <td class="border-green" data-room="green">Green room</td>
                            <td class="border-blue" data-room="blue">Blue room</td>
                            <td class="border-red" data-room="red">Red room</td>
                            <td class="border-yellow" data-room="class">Class room</td>
                        </tr>
                        <tr>
                            <th>{{Form::text('st[1][time]', $time['0']['time'], ['class' => 'form-control', 'data-attt' => 'dd','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            </th>
                            <td colspan="5" class="border">
                                <select name = 'st[1][all]'>
                                    {{--<option selected value = >  </option>--}}
                                    @foreach( $project as $r)
                                        @if($r->time == $time['0']['time'] && $r->room == 'all' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>{{  Form::text('st[2][time]', $time['1']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            </th>
                            <td data-room="gray">
                                <select name = 'st[2][gray]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['1']['time'] && $r->room == 'gray' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th>{{  Form::text('st[3][time]', $time['2']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td data-room="gray">
                                <select name = 'st[3][gray]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['2']['time'] && $r->room == 'gray' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="green">
                                <select name = 'st[3][green]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['2']['time'] && $r->room == 'green' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="blue">
                                <select name = 'st[3][blue]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['2']['time'] && $r->room == 'blue' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td rowspan="2" class="border" data-room="red">
                                <select name = 'st[3][red]'>
                                    @foreach($workshops as $r)
                                        @if($r->time == $time['2']['time'] && $r->room == 'red' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </td>
                            <td rowspan="2" class="border" data-room="class">
                                <select name = 'st[3][class]'>
                                    @foreach($workshops as $r)
                                        @if($r->time == $time['2']['time'] && $r->room == 'class' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[4][time]', $time['3']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td data-room="gray">
                                <select name = 'st[4][gray]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['3']['time'] && $r->room == 'gray' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </td>
                            <td data-room="green">
                                <select name = 'st[4][green]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['3']['time'] && $r->room == 'green' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="blue">
                                <select name = 'st[4][blue]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['3']['time'] && $r->room == 'blue' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[5][time]', $time['4']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}

                            <td colspan="5" class="border">Кофебрейк</td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[6][time]', $time['5']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td data-room="gray">
                                <select name = 'st[6][gray]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['5']['time'] && $r->room == 'gray' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="green">
                                <select name = 'st[6][green]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['5']['time'] && $r->room == 'green' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="blue">
                                <select name = 'st[6][blue]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['5']['time'] && $r->room == 'blue' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td rowspan="3" class="border" data-room="red">
                                <select name = 'st[6][red]'>
                                @foreach($workshops as $r)
                                    @if($r->time == $time['5']['time'] && $r->room == 'red' )
                                        <option selected value = {{$r->id}}>{{$r->title}}</option>
                                    @else
                                        @if($loop->first)
                                            <option selected value ={{null}}>Выберете</option>
                                        @endif
                                        <option value = {{$r->id}}>{{$r->title}}</option>
                                    @endif
                                @endforeach
                                </select>
                            </td>
                            <td rowspan="3" class="border" data-room="class">
                                <select name = 'st[6][class]'>
                                    @foreach($workshops as $r)
                                        @if($r->time == $time['5']['time'] && $r->room == 'class' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[7][time]', $time['6']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td data-room="gray">
                                <select name = 'st[7][gray]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['6']['time'] && $r->room == 'gray' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="green">
                                <select name = 'st[7][green]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['6']['time'] && $r->room == 'green' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td data-room="blue">
                                <select name = 'st[7][blue]'>
                                    @foreach( $project as $r)
                                        @if($r->time == $time['6']['time'] && $r->room == 'blue' )
                                            <option selected value = {{$r->id}}>{{$r->title}}</option>
                                        @else
                                            @if($loop->first)
                                                <option selected value ={{null}}>Выберете</option>
                                            @endif
                                            <option value = {{$r->id}}>{{$r->title}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[8][time]', $time['7']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td data-room="gray">тайный спикер</td>
                            <td data-room="green">тайный спикер</td>
                            <td data-room="blue">тайный спикер</td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[9][time]', $time['8']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td colspan="5" class="border">Выступление от партнеров</td>
                        </tr>

                        <tr>
                            <th>{{  Form::text('st[10][time]', $time['9']['time'], ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'time'])}}
                            <td colspan="5" class="border">Закрытие</td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
        <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>

            {{ Form::close() }}
        </button>


@endsection



