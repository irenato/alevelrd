@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Партнеры </h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{route('partners_add')}}" class="btn btn-success"><span class="fa-plus"></span> Добавить партнёра</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="20%">Название  </th>
                            <th width="10%">Логотип</th>
                            <th width="10%">Краткий текст</th>
                            <th width="20%">Полный текст</th>
                            <th width="10%">Ссылка</th>
                            <th width="10%">Тип</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($partners as $partner)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->title !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">

                                    <img src="{{$partner->logo}}" class="img-responsive" alt="{{$partner->logo}}" />
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->short_text !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->text !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $partner->link }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $partner->type }}</span>
                                </td>


                                <td>
                                    <a href="{{route('partners_edit',['id' => $partner->id])}}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
