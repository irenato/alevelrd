@extends('backend.layouts.app')
@section('pagetitle')
<div class="page-title">
    <div class="title-env">
        <h1 class="title">Заявки жизни проекта</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
            </li>
            <li>
                <a href="{{ route('project_callbacks') }}">Все сообщения</a>
            </li>
            <li class="active">
                <strong>Заявки хаба: {{ \Carbon\Carbon::parse($message->created_at)->format('d-m-Y H:i:s') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection
@section('content')
<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Основная информация</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">Имя пользователя</div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{ $message->name }}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">Фамилия</div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{ $message->surname }}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">Телефон</div>
                        <div class="col-sm-9">
                            {!! $message->phone !!}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">email</div>
                        <div class="col-sm-9">
                            {!! $message->email !!}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">Workshop</div>
                        <div class="col-sm-9">
                            @foreach($message->workshop as $t)
                                {{$t->title}}<br>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('project_callbacks')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
            </button>
        </div>
    </div>
</div>

{{ Form::close() }}
<div class="hidden">
    {!! Form::open(['route' => ['project_message_del', $message->id], 'method' => 'post']) !!}
    {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
    {!! Form::close() !!}
</div>

@endsection

