@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Сообщения с формы обратной связи</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('callbackapplications.update-all') }}" class="btn btn-blue"><span class="fa-plus"></span> Применить все</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="15%">Имя</th>
                            <th width="30%">Phone</th>
                            <th width="30%">Создана</th>
                            <th width="20%">Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($applications as $application)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->username !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->phone !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ \Carbon\Carbon::parse($application->created_at)->format('d-m-Y H:i:s') }}</span>
                                </td>
                                <td class="user">
                                    @if($application->viewed == 1)
                                        <span class="badge badge-success badge-roundless upper">Просмотрено</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Новое</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! route('callbackapplications.show', ['callbackapplication' => $application->id]) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $applications->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
