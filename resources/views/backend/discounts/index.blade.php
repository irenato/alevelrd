@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Скидки</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/discounts/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить скидку</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="15%">Скидка</th>
                            <th width="60%">Описаниес</th>
                            <th width="15%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($discounts as $discount)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $discount->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $discount->value }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $discount->description }}</span>
                                </td>

                                <td>
                                    <a href="{!! url('backend/discounts/' . $discount->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection