@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки (партнеры)</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('eventapplications.update-all') }}" class="btn btn-blue"><span class="fa-plus"></span> Применить все</a>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('eventapplications.export', request()->all()) }}" class="btn btn-primary"><span class="fa-download"></span> export</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width=5%">id</th>
                            <th width="15%">Мероприятие</th>
                            <th width="15%">Ф.И.О.</th>
                            <th width="15%">Email</th>
                            <th width="15%">Телефон</th>
                            <th width="15%"></th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/eventapplications', 'method' => 'GET']) }}
                            <th></th>
                            <th>
                                <select name="event_id" class="form-control">
                                    <option value="">Выберите</option>
                                    @foreach($events as $event)
                                        <option value="{{ $event->id }}"
                                                @if(request()->event_id == $event->id) selected="selected" @endif>{{ $event->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>{{ Form::text('username', request()->username, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Ф.И.О."]) }}</th>
                            <th>{{ Form::text('email', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Email"]) }}</th>
                            <th>{{ Form::text('phone', request()->phone, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Телефон"]) }}</th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите
                                    </option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif> Подтвержденные
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Новые
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">
                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($applications as $application)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->event->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->username !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->phone !!}</span>
                                </td>
                                <td class="user">
                                    @if($application->active !== 1)
                                        <a href="{!! url('backend/eventapplications/' . $application->id . '/edit') !!}" class="badge badge-red badge-roundless upper">Новая</a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $applications->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection