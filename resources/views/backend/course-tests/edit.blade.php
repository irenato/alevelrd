@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование</h1>
            <p class="description">Редактирование</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('course-tests.index') }}">Курсы</a>
                </li>
                <li class="active">
                    <strong>Редактирование</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {!! Form::open(['url' => route('course-tests.update', ['course' => $course->id]),'method'=>'PUT','autocomplete'=>'off', 'novalidate'=> 'novalidate', 'class'=>'form-horizontal form-label-left']) !!}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">{{ $course->title }}</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        @forelse($tests as $test)
                            <div class="row form-group">
                                <label class="col-sm-6">{{ $test->question }}</label>
                                <div class="col-sm-6">
                                    {!! Form::select('test_answer_id[' . $test->id . ']', $test->answers()->pluck('answer', 'id')->prepend('Please Select',''), (isset($answersIds[$test->id]) ? $answersIds[$test->id] : null), ['class'=>'form-control select2_single', 'value' => old('designer_id')]) !!}
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <a href="{{ route('course-tests.index') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                    <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                        <b>Сохранить</b>
                    </button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
        @endsection

        @section('styles')
            <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
            <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
            <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
        @endsection

        @section('scripts')
            <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
            <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
            <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
            <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
            <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\Course\CourseRequest')->ignore('') !!}
@endsection
