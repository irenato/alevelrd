@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Отзывы</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('reviews.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить отзыв</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="5%">id</th>
                            <th width="5%">Фото</th>
                            <th width="15%">Автор</th>
                            <th width="20%">Email</th>
                            <th width="20%">Курс</th>
                            <th width="15%">Статус</th>
                            <th width="15%">Источник</th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/reviews', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>{{ Form::text('username', request()->username, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Автор"]) }}</th>
                            <th>{{ Form::text('email', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Email"]) }}</th>
                            <th>
                                <select name="course_id" class="form-control" data-active="{{ request()->course_id }}">
                                    <option value="">Выберите</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->title }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите</option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif>Активен
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Заблокирован
                                    </option>
                                </select>
                            </th>
                            <th>
                                <select name="from_student" class="form-control">
                                    <option value=""
                                            @if(!request()->has('from_student') || !request()->filled('from_student')) selected="selected" @endif>Выберите</option>
                                    <option value="1"
                                            @if(request()->from_student == 1) selected="selected" @endif>Студет
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('from_student') && request()->from_student == 0) selected="selected" @endif>Компания
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($reviews as $review)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $review->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('reviews')->url(  $review->id . '/' . $review->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $review->username !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $review->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $review->course->title !!}</span>
                                </td>
                                <td class="user">
                                    @if($review->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td class="user">
                                    @if($review->from_student == 1)
                                        <span class="badge badge-success badge-roundless upper">Студент</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Компания</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/reviews/' . $review->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $reviews->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection