<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>username</th>
        <th>email</th>
        <th>phone</th>
        <th>city</th>
        <th>course</th>
        <th>created</th>
        <th>status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->username }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->city->title }}</td>
            <td>{{ $item->course->title }}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->active }}</td>
        </tr>
    @endforeach
    </tbody>
</table>