@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование страницы</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/creatorpage/' . $data->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $data->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">

                        <!-- О нас -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('description', 'Описание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{  Form::textarea('description', $data->description, ['class' => 'form-control']) }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', $data->seo_title, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', $data->seo_keywords, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', $data->seo_robots, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', $data->seo_canonical, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', $data->seo_description, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
