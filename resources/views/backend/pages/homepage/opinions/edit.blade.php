@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование записи</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('opinions.index') }}">О нас говорят</a>
                </li>
                <li class="active">
                    <strong>Редактирование записи</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/opinions/' . $partner->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $partner->id) }}
    {{ Form::hidden('old_logo', $partner->logo) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="logo" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('opinions')->url(  $partner->id . '/' . $partner->logo  ), 100, 100) }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> logo
                                        <div class="invisible">
                                            {{Form::file('logo', ['accept' => 'image/*', 'data-img' => 'logo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', $partner->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('link', 'Ссылка', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-link"></i></a>
                                    </div>
                                    {{  Form::text('link', $partner->link, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Ссылка'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('description', 'Описание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{  Form::textarea('description', $partner->description, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ route('opinions.index') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['opinions.destroy', $partner->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
