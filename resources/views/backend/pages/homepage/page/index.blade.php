@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование страницы</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/homepage/' . $data->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $data->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', $data->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- О нас -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('about_section_thumbnail', 'Блок "О нас" (изображение)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="about_section_thumbnail" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->about_section_thumbnail ), 100, 100) }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('about_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'about_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('about_section_thumbnail_old', $data->about_section_thumbnail) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('about_section_title', 'Блок "О нас" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('about_section_title', $data->about_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('about_section_content', 'Блок "О нас" (контент)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::textarea('about_section_content', $data->about_section_content, ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- О нас говорят -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('opinion_section_thumbnail', 'Блок "О нас говорят" (изображение)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="opinion_section_thumbnail" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->opinion_section_thumbnail ), 100, 100) }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('opinion_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'opinion_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('opinion_section_thumbnail_old', $data->opinion_section_thumbnail) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('opinion_section_title', 'Блок "О нас говорят" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('opinion_section_title', $data->opinion_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- Награды -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('rewards_section_thumbnail', 'Блок "Награды" (изображение)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="rewards_section_thumbnail" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->rewards_section_thumbnail ), 100, 100) }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('rewards_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'rewards_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('rewards_section_thumbnail_old', $data->rewards_section_thumbnail) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('rewards_section_title', 'Блок "Награды" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('rewards_section_title', $data->rewards_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('nearby_courses_title', 'Ближайшие курсы', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('nearby_courses_title', $data->nearby_courses_title, ['class' => 'form-control', 'id' => 'nearby_courses_title', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Описание области ближайших курсов'])}}
                                </div>
                            </div>
                        </div>

                        <!-- Скидки -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount_section_thumbnail', 'Блок "Скидки" (изображение)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="discount_section_thumbnail" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->discount_section_thumbnail ), 100, 100) }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('discount_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'discount_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('discount_section_thumbnail_old', $data->discount_section_thumbnail) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount_section_title', 'Блок "Скидки" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('discount_section_title', $data->discount_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- Партнеры -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('partners_section_title', 'Блок "Партнеры" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('partners_section_title', $data->partners_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- Расписание -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('schedule_section_title', 'Блок "Расписание" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('schedule_section_title', $data->schedule_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <!-- Преподователи -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('teachers_section_title', 'Блок "Преподователи" (название)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('teachers_section_title', $data->teachers_section_title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', $data->seo_title, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', $data->seo_keywords, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', $data->seo_robots, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', $data->seo_canonical, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', $data->seo_description, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@include('backend.modals.image')

@include('backend.modals.image-remove')

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
