@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование акции</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li class="active">
                    <strong>Редактирование акции</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/actions/' . $action->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $action->id) }}
    {{ Form::hidden('old_thumbnail', $action->thumbnail) }}
    {{ Form::hidden('old_background', $action->thumbnail) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#" class="js_form-link">
                                            <img id="photo_img" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('action')->url(  $action->thumbnail  ), 100, 100) }}"
                                                 class="img-circle"
                                                 alt="user-pic"/>
                                        </a>
                                        <div class="invisible">
                                            {{Form::file('thumbnail', ['accept' => 'image/*', 'id' => 'photo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="background" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('action')->url(  $action->background  ), 100, 100) }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> Основное изображение
                                        <div class="invisible">
                                            {{Form::file('background', ['accept' => 'image/*', 'data-img' => 'background'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', $action->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('total_places', 'Всего мест *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('total_places', $action->total_places, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Всего мест'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('occupied_places', 'Занятых мест *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('occupied_places', $action->occupied_places, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Занятых мест'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount', 'Скидка (%) *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('discount', $action->discount, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Скидка (%)'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('end_date', 'Завершение акции', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-calendar"></i></a>
                                    </div>
                                    {{  Form::text('end_date', \Carbon\Carbon::parse($action->end_date)->format('Y-m-d H:i:s'), ['class' => 'form-control datetimepicker', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Завершение акции'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-3" for="field-1">Описание</label>
                            <div class="col-sm-9">
                                {{  Form::textarea('description', $action->description, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, $action->active, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/reviews') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
