<div class="modal fade" id="gallery-image-modal">
    <div class="modal-dialog">
        {{ Form::open(['url' => '/']) }}
        <div class="modal-content">
            <div class="modal-gallery-image">
                <img src="{{ asset('assets/images/album-image-full.jpg') }}" class="img-responsive"/>
            </div>
            <div class="modal-body">
                <input type="hidden" name="image[id]">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="control-label">Title</label>
                            <input name="image[title]" type="text" class="form-control" id="field-1" placeholder="Enter image title">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="control-label">Alt</label>
                            <textarea name="image[alt]" class="form-control autogrow" id="field-2" placeholder="Enter image description" style="min-height: 80px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-gallery-top-controls">
                <button type="button" class="btn btn-xs btn-white" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-xs btn-secondary" data-image-save>Сохранить</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>