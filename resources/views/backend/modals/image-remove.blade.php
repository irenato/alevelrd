<div class="modal fade" id="gallery-image-delete-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Подтверждение удаления изображения</h4>
            </div>
            <div class="modal-body">
                Точно удалить изображение ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-danger" data-image-delete>Удалить</button>
            </div>
        </div>
    </div>
</div>