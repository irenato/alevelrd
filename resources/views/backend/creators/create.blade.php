@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление пользователя</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/teachers') }}">Создатели сайта</a>
                </li>
                <li class="active">
                    <strong>Добавление</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('creators.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate', 'files' => true]) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="thumbnail" src="{{ url('assets/images/user-4.png') }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('username', 'Имя *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('username', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Имя'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('specialization', 'Специализация *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('specialization', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Специализация'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 control-label" for="field-1">Описание</label>
                            <div class="col-sm-9">
                                {{  Form::textarea('description', null, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 control-label" for="js_clients_phones">Ссылка на профиль</label>
                            <div class="col-sm-10 js_parent">
                                <div class="input-group js_section">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-link"></i></a>
                                    </div>
                                    {{  Form::text('links[html_class][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                    {{  Form::text('links[link][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                    <div class="input-group-addon js_remove_item">
                                        <a href="#">-</a>
                                    </div>
                                    <div class="input-group-addon js_add_item">
                                        <a href="#">+</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="text">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Краткое описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('introtext', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('description', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/creators') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
