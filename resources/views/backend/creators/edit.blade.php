@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/creators') }}">Создатели сайта</a>
                </li>
                <li class="active">
                    <strong>Редактирование</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/creators/' . $creator->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $creator->id) }}
    {{ Form::hidden('old_thumbnail', $creator->thumbnail) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <div class="user-img">
                                    <a href="#" class="js_form-link">
                                        <img id="photo_img" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('creators')->url(  $creator->id . '/' . $creator->thumbnail  ), 100, 100) }}"
                                             class="img-circle"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('thumbnail', ['accept' => 'image/*', 'id' => 'photo'])}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('username', 'Имя *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('username', $creator->username, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Фамилия'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('specialization', 'Специализация *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-check"></i></a>
                                    </div>
                                    {{  Form::text('specialization', $creator->specialization, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Специализация'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 control-label" for="field-1">Описание</label>
                            <div class="col-sm-9">
                                {{  Form::textarea('description', $creator->description, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 control-label" for="js_clients_phones">Ссылка на профиль</label>
                            <div class="col-sm-10 js_parent">
                                @if(is_array($creator->social_links))
                                    @foreach ($creator->social_links as $key => $value)
                                        <div class="input-group js_section">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa-link"></i></a>
                                            </div>
                                            {{  Form::text('links[html_class][]', $value['html_class'], ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                            {{  Form::text('links[link][]', $value['link'], ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                            <div class="input-group-addon js_remove_item">
                                                <a href="#">-</a>
                                            </div>
                                            <div class="input-group-addon js_add_item">
                                                <a href="#">+</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="input-group js_section">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa-link"></i></a>
                                        </div>
                                        {{  Form::text('links[html_class][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'HTML class'])}}
                                        {{  Form::text('links[link][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ссылка на профиль'])}}
                                        <div class="input-group-addon js_remove_item">
                                            <a href="#">-</a>
                                        </div>
                                        <div class="input-group-addon js_add_item">
                                            <a href="#">+</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group-separator __web-inspector-hide-shortcut__"></div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ url('backend/creators') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['creators.destroy', $creator->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
