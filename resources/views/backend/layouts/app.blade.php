<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <title>{{ config('app.name', 'A-Level') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="{{ asset('assets/css/fonts/linecons/css/linecons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/fonts/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/xenon-core.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/xenon-forms.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/xenon-components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/xenon-skins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/chosen/chosen.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/dropzone/css/dropzone.css') }}">

    <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-body">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
    <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
    <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
    <div class="sidebar-menu toggle-others fixed">
        <script>
            if (localStorage['sidebar'] == 'true')
                $(".sidebar-menu").addClass('collapsed');
        </script>git
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- logo -->
                <div class="logo">
                    <h3><p>{{ config('app.name') }}</p></h3>
                </div>
                <!-- This will toggle the mobile menu and will be visible only on mobile devices -->
                <div class="mobile-menu-toggle visible-xs">
                    <a href="#" data-toggle="user-info-menu">
                        <i class="fa-bell-o"></i>
                        <span class="badge badge-success">7</span>
                    </a>
                    <a href="#" data-toggle="mobile-menu">
                        <i class="fa-bars"></i>
                    </a>
                </div>
            </header>
            @include('backend.navigations.main')
        </div>
    </div>

    <div class="main-content">
        <nav class="navbar user-info-navbar" role="navigation"><!-- User Info, Notifications and Menu Bar -->
            <ul class="user-info-menu left-links list-inline list-unstyled">
                <li class="hidden-sm hidden-xs">
                    <a href="#" data-toggle="sidebar">
                        <i class="fa-bars"></i>
                    </a>
                </li>
            </ul>
            @include('backend.navigations.userinfo')
        </nav>
        @yield('pagetitle')
        <br/>
        <div class="panel panel-default panel-headerless">
            <div class="panel-body layout-variants">
                <div class="row">
                    @include('backend.layouts.alerts')
                    @yield('content')
                </div>
            </div>
        </div>

        <footer class="main-footer sticky footer-type-1">
            <div class="footer-inner">
                <!-- Add your copyright text here -->
                <div class="footer-text">
                    &copy; {{ date('Y') }}
                    <strong>A-Level</strong>
                </div>
                <!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
                <div class="go-up">
                    <a href="#" rel="go-top">
                        <i class="fa-angle-up"></i>
                    </a>
                </div>
            </div>
        </footer>
    </div>
</div>


@yield('styles')
<!-- Bottom Scripts -->
<script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/TweenMax.min.js') }}"></script>
<script src="{{ url('assets/js/resizeable.js') }}"></script>
<script src="{{ url('assets/js/joinable.js') }}"></script>
<script src="{{ url('assets/js/xenon-api.js') }}"></script>
<script src="{{ url('assets/js/xenon-toggles.js') }}"></script>
<script src="{{ url('assets/js/chosen/chosen.jquery.min.js') }}"></script>
@yield('scripts')
<!-- JavaScripts initializations and stuff -->
<script src="{{ url('assets/js/dropzone/dropzone.min.js') }}"></script>
<script src="{{ url('assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('assets/js/xenon-custom.js') }}"></script>
<script src="{{ url('assets/js/toastr/toastr.min.js') }}"></script>
<script src="{{ url('js/admin.js') }}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
</body>
</html>
