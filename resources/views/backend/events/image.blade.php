<div class="col-md-3 col-sm-4 col-xs-6 js-image-item">
    <input type="hidden" name="images[]" value="{{ $image->id }}">
    <div class="album-image">
        <a href="#" class="thumb" data-action="edit">
            <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('events')->url(  $course->id . '/' . $image->name  ), 300, 250) }}" class="img-responsive" />
        </a>

        <a href="#" class="name">
            <span>{{ $imageName }}</span>
            <em>{{ $dateCreated }}</em>
        </a>

        <div class="image-options">
            <a href="#" data-action="edit"><i class="fa-pencil"></i></a>
            <a href="#" data-action="trash"><i class="fa-trash"></i></a>
        </div>

        <div class="image-checkbox">
            @if($image->cover == 1) <div class="label label-info">Превью</div> @endif
        </div>
    </div>
</div>