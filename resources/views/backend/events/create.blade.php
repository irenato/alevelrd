@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление ивента</h1>
            <p class="description">Добавление нового ивента</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/events') }}">Ивенты</a>
                </li>
                <li class="active">
                    <strong>Добавление ивента</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('events.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate', 'files' => true]) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#text" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Дополнительная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#programm" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Timeline</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="thumbnail" src="{{ url('assets/images/user-4.png') }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> Основное изображение
                                        <div class="invisible">
                                            {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        @if(count($templates) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3 control-label" for="organization_id">Шаблон</label>
                                <div class="col-sm-9">
                                    <select name="template" id="template" class="form-control">
                                        @foreach($templates as $template)
                                            <option value="{{ $template }}">{{ $template }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('alias', 'URL', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('alias', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'URL'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('cost', 'Цена', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('cost', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Цена'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('begin', 'Дата старта', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('begin', null, ['class' => 'form-control datepicker', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($cities) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="city_id">Город</label>
                                <div class="col-sm-9">
                                    <select name="city_id" id="city_id" class="form-control">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(count($teachers) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3 control-label" for="organization_id">Учителя</label>
                                <div class="col-sm-9">
                                    <select name="teacher_id[]" id="teacher_id" multiple
                                            class="form-control js_ch_multiple">
                                        @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->id }}">{{ $teacher->first_name . ' ' . $teacher->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, null, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('is_external', 'Внешняя ссылка', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('is_external', 1, null, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="text">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Краткое описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('introtext', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('description', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Правила (заголовок)</label>
                        <div class="col-sm-9">
                            {{  Form::text('rule_title', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Правила (описание)</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('rule_description', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane js_section" id="programm">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {{ Form::label('timeline_title', 'Title', ['class' => 'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{  Form::text('timeline_title', 'Программа курса', ['required' => 'required', 'class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения'])}}
                            </div>
                        </div>
                        <table class="table table-bordered table-striped" id="example-2">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tbody class="middle-align js_parent">

                            <tr class="js_section">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('date', 'Дата', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[date][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Дата'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('interval', 'Интервал', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[interval][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Практика'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('date', 'Миниатюра', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{Form::file('timeline[thumbnail][]', ['accept' => 'image/*'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('description', 'Описание', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('timeline[description][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Практика'])}}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="js_section hidden">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('date', 'Дата', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[date][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Дата'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('interval', 'Интервал', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[interval][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Практика'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('date', 'Миниатюра', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{Form::file('timeline[thumbnail][]', ['accept' => 'image/*'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('timeline[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('description', 'Описание', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('timeline[description][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Практика'])}}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-aligned">
                        <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/events') }}" class="btn btn-white"><span class="fa-arrow-left"></span>
                    Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Event\EventRequest')->ignore('') !!}
@endsection
