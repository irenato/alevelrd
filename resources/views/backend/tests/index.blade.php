@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Тесты</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('tests.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">Ключ</th>
                            <th width="60%">Вопрос</th>
                            <th width="20%">Тип(ы)</th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => route('tests.index'), 'method' => 'GET']) }}
                            <th>{{ Form::text('key', request()->key, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Ключ"]) }}</th>
                            <th>{{ Form::text('question', request()->question, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Текст вопроса"]) }}</th>
                            <th></th>
                            <th class="hidden-xs hidden-sm">
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">
                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tests as $test)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $test->key !!}</span>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $test->question !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $test->tests_ttypes->pluck('name')->implode(', ') !!}</span>
                                </td>
                                <td>
                                    <a href="{!! route('tests.edit', $test->id) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $tests->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
