@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Настройки</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li><a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a></li>
                <li><a href="{{ url('backend/settings') }}">Настройки</a></li>
                <li class="active"><strong>Добавить</strong></li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Добавить запись</div>
        </div>

        <div class="panel-body">
            {{ Form::open(['url' => 'backend/settings', 'method' => 'POST', 'class' => 'validate form-horizontal', "novalidate" => 'novalidate', 'autocomplete' => 'off']) }}
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        {{ Form::label('display_name', "Ключ", ['class' => 'control-label col-sm-3']) }}
                        <div class="col-sm-9">
                            {{  Form::text('key', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => __('settings.required'), "placeholder" => ''])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('display_name', "Значени", ['class' => 'control-label col-sm-3']) }}
                        <div class="col-sm-9">
                            {{  Form::text('value', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => __('settings.required'), "placeholder" => ''])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', "Описание", ['class' => 'control-label col-sm-3']) }}
                        <div class="col-sm-9">
                            {{  Form::textarea('description', null, ['class' => 'form-control', "placeholder" => '', 'rows' => 5])}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group-separator __web-inspector-hide-shortcut__"></div>
            <div class="col-xs-12">
                <div class="form-group">
                    <a href="{{ url('backend/settings') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                    <button type="submit" class="btn btn-success"><span class="fa-save"></span> <b>Сохранить</b></button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('styles')
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0;
        }

        div {
            border: 0px solid silver;
        }

        label {
            border: 0px solid red;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
@endsection