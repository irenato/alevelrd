@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Телеграм (каналы)</h1>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Название</th>
                            <th>Ключ</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($configs as $config)
                            <tr>
                                <td>{!! $config->id !!}</td>
                                <td>{!! $config->title !!}</td>
                                <td>{!! $config->key !!}</td>
                                <td>
                                    <a href="{{ url('backend/telegram-configs/' . $config->id . '/edit') }}" class="btn btn-secondary btn-sm btn-icon icon-left">
                                        <span class="linecons-pencil"></span>
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4"><p class="text-center">Результатов не найдено</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection