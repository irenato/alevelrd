@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Настройки</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li><a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a></li>
                <li><a href="{{ url('backend/telegram-configs') }}">Телеграм (каналы)</a></li>
                <li class="active"><strong>Редактировать</strong></li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Редактирование</div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => 'backend/telegram-configs/' . $config->id, 'method' => 'PUT', 'class' => 'validate form-horizontal', "novalidate" => 'novalidate', 'autocomplete' => 'off']) !!}
                    {!! Form::hidden('id', $config->id) !!}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {{ Form::label('title', "Название", ['class' => 'control-label col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{  Form::text('title', $config->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => __('settings.required'), "placeholder" => ''])}}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('key', "Ключ", ['class' => 'control-label col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{  Form::text('key', $config->key, ['readonly' => 'readonly', 'class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => __('settings.required'), "placeholder" => ''])}}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('bot_id', "TELEGRAM_BOT_TOKEN", ['class' => 'control-label col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{  Form::textarea('bot_id', $config->bot_id, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => __('settings.required'), "placeholder" => ''])}}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('ids', "ids получателей (разделитель: ',')", ['class' => 'control-label col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{  Form::textarea('ids', $config->ids, ['class' => 'form-control', "placeholder" => '', 'rows' => 5])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-separator __web-inspector-hide-shortcut__"></div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <a href="{{ url('backend/settings') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                            <button type="submit" class="btn btn-success"><span class="fa-save"></span> <b>Сохранить</b></button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
