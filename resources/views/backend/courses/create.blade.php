@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление курса</h1>
            <p class="description">Добавление нового курса</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/courses') }}">Курсы</a>
                </li>
                <li class="active">
                    <strong>Добавление курса</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('courses.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate', 'files' => true]) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#text" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Дополнительная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#programm" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Программа</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#" class="js_form-link">
                                            <img id="photo_img" src="{{ url('assets/images/user-4.png') }}"
                                                 class="img-circle"
                                                 alt="user-pic"/>
                                        </a> Лого
                                        <div class="invisible">
                                            {{Form::file('logo', ['accept' => 'image/*', 'id' => 'photo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="thumbnail" src="{{ url('assets/images/user-4.png') }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> Основное изображение
                                        <div class="invisible">
                                            {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        @if(count($types) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3 control-label" for="organization_id">Тип</label>
                                <div class="col-sm-9">
                                    <select name="type_id" id="type_id" class="form-control">
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(count($logo_html_classes) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="organization_id">HTML-класс логотипа</label>
                                <div class="col-sm-9">
                                    <select name="logo_class" id="logo_class" class="form-control">
                                        @foreach($logo_html_classes as $item)
                                            <option value="{{ $item }}"
                                                    @if ($loop->first) selected="selected" @endif>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('alias', 'URL', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('alias', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'URL'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('bx_id', 'Bitrix24 id', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('bx_id', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Bitrix24 id'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('cost', 'Цена', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('cost', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Цена'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount_end_date', 'Конечная дата работы скидки', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('discount_end_date', null, ['class' => 'form-control datepicker', 'id' => 'discount_end_date', 'data-message-required' => 'Конечная дата работы скидки', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount', 'Скидка', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('discount', 15, ['class' => 'form-control', "placeholder" => 'Скидка'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('price_type', 'Тип цены', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('price_type', 'грн/месяц', ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тип цены'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('duration', 'Продолжительность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('duration', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Продолжительность'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('schedule', 'Расписание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::textarea('schedule', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Расписание'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('begin', 'Дата старта', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('begin', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('begin', 'Начало', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('begin', null, ['class' => 'form-control datepicker', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('start_date', 'Начало (для календаря)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('start_date', null, ['class' => 'form-control datepicker', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($cities) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="cities_ids">Города</label>
                                <div class="col-sm-9">
                                    <select name="cities_ids[]" id="cities_ids" multiple
                                            class="form-control js_ch_multiple">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(count($teachers) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3 control-label" for="organization_id">Учителя</label>
                                <div class="col-sm-9">
                                    <select name="teacher_id[]" id="teacher_id" multiple
                                            class="form-control js_ch_multiple">
                                        @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->id }}">{{ $teacher->first_name . ' ' . $teacher->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('amount_places', 'Количество мест', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('amount_places', 0, ['class' => 'form-control', "placeholder" => 'Количество мест'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('free_places', 'Свободных мест', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('free_places', 0, ['class' => 'form-control', "placeholder" => 'Свободных мест'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('employment', 'Гарантированное трудоустройство', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('employment', 1, 1, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, 1, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="text">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Краткое описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('introtext', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('description', null, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Курс интересен для тех... *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('advantages', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane js_section" id="programm">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped" id="example-2">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tbody class="middle-align js_parent">

                            <tr class="js_section">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('cost', 'Период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[period][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Краткое описание программы', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[introtext][]', null, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Вся теория за период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[practice][]', null, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label" for="field-1">Курс интересен для тех...
                                            *</label>
                                        <div class="col-sm-9">
                                            {{  Form::textarea('advantages', null, ['class' => 'form-control ckeditor']) }}
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="js_section hidden">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('cost', 'Период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[period][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Краткое описание программы', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[introtext][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Вся теория за период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[practice][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    row
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-aligned">
                        <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/teachers') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\Course\CourseRequest')->ignore('') !!}
@endsection
