@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование курса</h1>
            <p class="description">Редактирование курса</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/courses') }}">Курсы</a>
                </li>
                <li class="active">
                    <strong>Редактирование курса</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/courses/' . $course->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $course->id) }}
    {{ Form::hidden('old_thumbnail', $course->thumbnail) }}
    {{ Form::hidden('old_logo', $course->logo) }}
    {{ Form::hidden('old_video', $course->video) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#images" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-home"></i></span>
                        <span class="hidden-xs">Изображения</span>
                    </a>
                </li>
                <li>
                    <a href="#text" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Дополнительная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#programm" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Программа</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#" class="js_form-link">
                                            <img id="photo_img"
                                                 src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('courses')->url(  $course->id . '/' . $course->logo  ), 100, 100) }}"
                                                 class="img-circle"
                                                 alt="user-pic"/>
                                        </a> Лого
                                        <div class="invisible">
                                            {{Form::file('logo', ['accept' => 'image/*', 'id' => 'photo'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-10 col-sm-8">
                                    <div class="user-img">
                                        <a href="#">
                                            <img data-img="thumbnail"
                                                 src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('courses')->url(  $course->id . '/' . $course->thumbnail  ), 100, 100) }}"
                                                 class="img-circle js_image_link"
                                                 alt="user-pic"/>
                                        </a> Основное изображение
                                        <div class="invisible">
                                            {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        @if(count($types) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="organization_id">Тип</label>
                                <div class="col-sm-9">
                                    <select name="type_id" id="type_id" class="form-control"
                                            data-active="{{ $course->type_id }}">
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(count($logo_html_classes) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="organization_id">HTML-класс логотипа</label>
                                <div class="col-sm-9">
                                    <select name="logo_class" id="logo_class" class="form-control">
                                        @foreach($logo_html_classes as $item)
                                            <option value="{{ $item }}"
                                                    @if ($item == $course->logo_class) selected="selected" @endif>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', $course->title, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('alias', 'URL', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('alias', $course->alias, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'URL'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('video', 'Видео', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{Form::file('video', ['accept' => 'video/*'])}}
                                    {{ $course->video }}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('bx_id', 'Bitrix24 id', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('bx_id', $course->bx_id, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Bitrix24 id'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('cost', 'Цена', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('cost', $course->cost, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Цена'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount_end_date', 'Конечная дата работы скидки', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('discount_end_date', $course->discount_end_date, ['class' => 'form-control datepicker', 'id' => 'discount_end_date', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Конечная дата работы скидки'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('discount', 'Скидка', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('discount', $course->discount, ['class' => 'form-control', "placeholder" => 'Скидка'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('price_type', 'Тип цены', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('price_type', $course->price_type, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тип цены'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('duration', 'Продолжительность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('duration', $course->duration, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Продолжительность'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('schedule', 'Расписание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::textarea('schedule', $course->schedule, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Расписание'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('begin', 'Начало', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('begin', $course->begin, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('start_date', 'Начало (для календаря)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('start_date', $course->start_date, ['class' => 'form-control datepicker', 'data-message-required' => 'Поле обязательно для заполнения', 'data-format'=>'yyyy-mm-dd', "placeholder" => 'Дата старта'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($cities) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="cities_ids">Города</label>
                                <div class="col-sm-9">
                                    <select name="cities_ids[]" id="cities_ids" multiple
                                            class="form-control js_ch_multiple">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}"
                                                    @if(in_array($city->id, $courses_cities)) selected="selected" @endif>{{ $city->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        @if(count($teachers) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="organization_id">Учителя</label>
                                <div class="col-sm-9">
                                    <select name="teacher_id[]" id="teacher_id" multiple
                                            class="form-control js_ch_multiple">
                                        @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->id }}"
                                                    @if(in_array($teacher->id, $teacher_courses)) selected="selected" @endif>{{ $teacher->first_name . ' ' . $teacher->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('amount_places', 'Количество мест', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('amount_places', $course->amount_places, ['class' => 'form-control', "placeholder" => 'Количество мест'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('free_places', 'Свободных мест', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('free_places', $course->free_places, ['class' => 'form-control', "placeholder" => 'Свободных мест'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('employment', 'Гарантированное трудоустройство', ['class' => '']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('employment', 1, $course->employment, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, $course->active, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="text">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Краткое описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('introtext', $course->introtext, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Описание *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('description', $course->description, ['class' => 'form-control ckeditor']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="field-1">Курс интересен для тех... *</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('advantages', $course->advantages, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane not-flow" id="images">
                    <div class="gallery-env">
                        <div class="album-header">
                            <ul class="album-options list-unstyled list-inline">
                                <li>
                                    <a href="#" id="upload-image"
                                       data-id="{{ $course->id }}"
                                       data-url="{{ url('backend/courses/image/upload/' . $course->id . '/') }}"
                                       data-url-get="{{ url('backend/courses/image/get/' . $course->id . '/') }}"
                                       data-url-get-simple="{{ url('backend/courses/image/get/') }}"
                                       data-url-set="{{ url('backend/courses/image/set/') }}"
                                       data-url-del="{{ url('backend/courses/image/delete/') }}"
                                       data-url-sort="{{ url('backend/courses/image/sort/') }}"
                                       data-url-active="{{ url('backend/courses/image/active/' . $course->id . '/') }}">
                                        <i class="fa-upload"></i>
                                        Add Images
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="album-sorting-info">
                            <div class="album-sorting-info-inner clearfix">
                                <a href="#"
                                   class="btn btn-secondary btn-xs btn-single btn-icon btn-icon-standalone pull-right"
                                   data-action="sort">
                                    <i class="fa-save"></i>
                                    <span>Save Current Order</span>
                                </a>
                                <i class="fa-arrows-alt"></i>
                                Drag images to sort them
                            </div>
                        </div>
                        <div class="album-images row">
                            <p>Изображений у продукта нет.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane js_section" id="programm">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped" id="example-2">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tbody class="middle-align js_parent">
                            @forelse ($course->programs as $program)
                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('cost', 'Период', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('program[period][]', $program->period, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Период'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('program[title][]', $program->title, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('total_theory', 'Краткое описание программы', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::textarea('program[introtext][]', $program->introtext, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('total_theory', 'Вся теория за период', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::textarea('program[practice][]', $program->practice, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                                </div>
                                            </div>
                                        </div>
                                        {{ Form::hidden('program[id][]', $program->id) }}
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#"
                                               class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('cost', 'Период', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('program[period][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Период'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('program[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('total_theory', 'Краткое описание программы', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::textarea('program[introtext][]', null, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('total_theory', 'Вся теория за период', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::textarea('program[practice][]', null, ['class' => 'form-control ckeditor', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#"
                                               class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            <tr class="js_section hidden">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('cost', 'Период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[period][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('title', 'Тема', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::text('program[title][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Тема'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Краткое описание программы', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[introtext][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            {{ Form::label('total_theory', 'Вся теория за период', ['class' => 'control-label']) }}
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {{  Form::textarea('program[practice][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Вся теория за период'])}}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-aligned">
                        <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', $course->seo_title, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', $course->seo_keywords, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', $course->seo_robots, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', $course->seo_canonical, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', $course->seo_description, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/courses') }}" class="btn btn-white" target="_blank"><span
                            class="fa-arrow-left"></span> Назад</a>
                <a href="{{ route('course', ['course_alias' => $course->alias]) }}" class="btn btn-white"><span
                            class="fa-eye"></span> Просмотреть</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>Сохранить</b>
                </button>
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>
@endsection

@include('backend.modals.image')

@include('backend.modals.image-remove')

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Course\CourseRequest')->ignore('') !!}
@endsection
