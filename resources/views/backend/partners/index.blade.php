@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Партнеры</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/partners/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить парнера</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="10%">Фото</th>
                            <th width="25%">Название</th>
                            <th width="25%">Ссылка</th>
                            <th width="15%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/partners', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>{{ Form::text('title', request()->title, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Название"]) }}</th>
                            <th>{{ Form::text('link', request()->link, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Ссылка"]) }}</th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/partners/sort/') }}">
                        @forelse($partners as $partner)
                            <tr class="js-sortable-item" data-id="{{ $partner->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('partners')->url(  $partner->id . '/' . $partner->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <a href="{!! $partner->link !!}" class="email" target="_blank">{!! $partner->link !!}</a>
                                </td>
                                <td>
                                    <a href="{!! url('backend/partners/' . $partner->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $partners->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection