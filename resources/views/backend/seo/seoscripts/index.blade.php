@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Скрипты</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('seoscripts.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="75%">Ключ</th>
                            <th width="15%">Активность</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $item)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $item->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $item->key !!}</span>
                                </td>
                                <td class="user">
                                    @if($item->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/seoscripts/' . $item->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $data->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection