@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Скрипты</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('redirects.create') }}" class="btn btn-success"><span class="fa-plus"></span>
                Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="40%">Источник</th>
                            <th width="40%">Цель</th>
                            <th width="10%">Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $item)
                            <tr class="js-parent-form" data-route="{{ url('backend/redirects/' . $item->id ) }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $item->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <textarea type="text" name="source" cols="50">{!! $item->source !!}</textarea>
                                </td>
                                <td class="user">
                                    <textarea type="text" name="target"  cols="50">{!! $item->target !!}</textarea>
                                </td>
                                <td class="user">
                                    {!! Form::open(['route' => ['redirects.destroy', $item->id], 'method' => 'delete']) !!}
                                    {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $data->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection