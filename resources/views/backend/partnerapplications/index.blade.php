@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки (партнеры)</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('partnerapplications.update-all') }}" class="btn btn-blue"><span class="fa-plus"></span> Применить все</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="15%">Компания</th>
                            <th width="20%">Email</th>
                            <th width="15%">Телефон</th>
                            <th width="15%">Создана</th>
                            <th width="20%">Просмотрено</th>
                            <th width="10%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/partnerapplications', 'method' => 'GET']) }}
                            <th></th>
                            <th>{{ Form::text('company', request()->company, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Автор"]) }}</th>
                            <th>{{ Form::text('email', request()->email, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Email"]) }}</th>
                            <th>{{ Form::text('phone', request()->phone, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Телефон"]) }}</th>
                            <th></th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('viewed') || !request()->filled('viewed')) selected="selected" @endif>Выберите
                                    </option>
                                    <option value="1"
                                            @if(request()->viewed == 1) selected="selected" @endif>Просмотрена
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('viewed') && request()->viewed == 0) selected="selected" @endif>Новая
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">
                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($applications as $application)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->company !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->email !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $application->phone !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ \Carbon\Carbon::parse($application->created_at)->format('d-m-Y H:i:s') }}</span>
                                </td>
                                <td class="user">
                                    @if($application->viewed == 1)
                                        <span class="badge badge-success badge-roundless upper">Просмотрена</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Новая</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! route('partnerapplications.show', ['partnerapplication' => $application->id]) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $applications->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection