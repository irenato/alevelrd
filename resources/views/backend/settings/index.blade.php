@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Настройки</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/settings/create') }}" class="btn btn-success">
                <span class="fa-plus"></span> Создать
            </a>
            <a href="{{ route('clear-cache') }}" class="btn btn-warning">
                <span class="fa-refresh"></span> Очистить кеш
            </a>
            <a href="{{ route('optimize-images') }}" class="btn btn-warning">
                <span class="fa-image"></span> Сжать изображения
            </a>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th>Ключ</th>
                            <th>Значение</th>
                            <th>Описание</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($settings as $setting)
                            <tr>
                                <td>{!! $setting->key !!}</td>
                                <td>{!! $setting->value !!}</td>
                                <td>{!! $setting->description !!}</td>
                                <td>
                                    <a href="{{ url('backend/settings/'.$setting->id.'/edit') }}" class="btn btn-secondary btn-sm btn-icon icon-left">
                                        <span class="linecons-pencil"></span>
                                        Редактировать
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4"><p class="text-center">Результатов не найдено</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-12 text-center-sm">
                            <ul class="pagination pagination-sm no-margin">
                                {{ $settings->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection