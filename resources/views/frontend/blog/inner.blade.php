@extends('frontend.layouts.app-blog')

@push('seo_fields')
    <meta name="description"
          content="{{ $article->seo_description }}">
    <meta name="keywords" content="{{ $article->seo_keywords }}">
    <meta name="robots" content="{{ $article->seo_robots }}">
    @if($article->seo_canonical)
        <link rel="canonical" href="{{ $article->seo_canonical }}"/>
    @endif
@endpush

@section('content')
    <main class="blog-page padding-page">
        <div class="container relative-container">
            <aside class="blog-subscribe sticky">
                <div class="blog-top__interest">
                    <div class="blog-top__interest-title"><img src="/images/fire.svg" alt="#">Интересно сейчас</div>
                    @forelse($recentlyArticles as $recentlyArticle)
                        <a href="{{ route('blog.article', $recentlyArticle->alias) }}"
                           class="blog-top__item-title">{{ $recentlyArticle->title }}</a>
                    @empty
                    @endforelse
                </div>
                <button class="add-info-btn"><i class="icon icon-contract"></i>Прислать материал</button>
                <div class="subscribe-form">
                    <div class="subscribe-title">Подписаться</div>
                    <form action="{{ route('blog.subscribe') }}" method="POST" class="js_validate js-form">
                        <div class="input-field">
                            <input type="email" name="email" data-validate="email" required placeholder="Email"><span
                                    class="error-text">Заполните поле</span>
                        </div>
                        <div class="checkin__text js-error-area" style="display: none"></div>
                        <div class="form-btn">
                            <button type="submit" class="btn btn--orange">Подписаться</button>
                        </div>
                    </form>
                </div>
            </aside>
            <section class="sticky-container blog-one">
                <div style="background: url({{ $article->backgroundInner }}) no-repeat" class="blog-one__title-wrapp">
                    <p class="blog-one__title">{{ $article->title }}</p>
                </div>
                <div class="blog-one__info">
                    @forelse($article->authors_articles as $author)
                        <div class="blog-info__name">
                            <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}"
                                                             alt="{{ $author->fullName }}"></div>
                            {{ $author->fullName }}
                        </div>
                    @empty
                    @endforelse
                    @forelse($article->tags_articles as $tag)
                        @if(isset($tag->htmlClass))
                            <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                        @endif
                    @empty
                    @endforelse
                    <div class="blog-info__date">{{ $article->updated }}</div>
                </div>
                <div class="blog-one__text">
                    {!! $article->content !!}
                </div>
                <div class="blog-one__tags">
                    @forelse($article->tags_articles as $tag)
                        @if(isset($tag->htmlClass))
                            <span class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</span>
                        @endif
                    @empty
                    @endforelse
                </div>
                <div class="blog-one__views" data-id="{{ $article->id }}">
                    <div class="blog-info__watch blog-views__item"><i class="icon icon-eye"></i>{{ $article->viewed }}
                    </div>
                    <div class="blog-info__like blog-views__item"><i class="icon icon-like"></i><span
                                class="js-result">{{ $article->likes }}</span></div>
                    <div class="blog-info__dislike blog-views__item"><i class="icon icon-dislike"></i><span
                                class="js-result">{{ $article->dislikes }}</span></div>
                </div>
            </section>
            <section class="all-blog-items sticky-container">
                <div class="dark-wrapp flex-wrap">
                    <div class="dark-wrapp__title">Выбор нашей школы</div>
                    @forelse($ourArticles as $ourArticle)
                        <a href="{{ route('blog.article', $ourArticle->alias) }}" class="dark-wrapp__item">
                            <div class="dark-wrapp__img"><img src="{{ $ourArticle->backgroundArticle }}"
                                                              alt="{{ $ourArticle->title }}"></div>
                            <div class="dark-wrapp__item-title">{{ $ourArticle->title }}</div>
                            <div class="dark-wrapp__info flex-wrap" data-id="{{ $ourArticle->id }}">
                                <div class="blog-info__watch blog-top__item"><i
                                            class="icon icon-eye"></i>{{ $ourArticle->viewed }}</div>
                                <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span
                                            class="js-result">{{ $ourArticle->likes }}</span></div>
                                <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span
                                            class="js-result">{{ $ourArticle->dislikes }}</span></div>
                            </div>
                        </a>
                    @empty
                    @endforelse
                </div>
                @forelse($article->related as $relatedArticle)
                    @if(++$i % 4 !== 0)
                        <div class="blog-item flex-wrap">
                            <div class="blog-item__descr">
                                <div class="blog-item__top flex-wrap">
                                    @forelse($relatedArticle->tags_articles as $tag)
                                        @if(isset($tag->htmlClass))
                                            <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                                        @endif
                                    @empty
                                    @endforelse
                                    <div class="blog-info">
                                        <div class="blog-info__right" data-id="{{ $relatedArticle->id }}">
                                            <div class="blog-info__date">{{ $relatedArticle->updated }}</div>
                                            <div class="blog-info__watch blog-top__item"><i
                                                        class="icon icon-eye"></i>{{ $relatedArticle->viewed }}</div>
                                            <div class="blog-info__like blog-top__item"><i
                                                        class="icon icon-like"></i><span
                                                        class="js-result">{{ $relatedArticle->likes }}</span></div>
                                            <div class="blog-info__dislike blog-top__item"><i
                                                        class="icon icon-dislike"></i><span
                                                        class="js-result">{{ $relatedArticle->dislikes }}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-title"><a
                                            href="{{ route('blog.article', $relatedArticle->alias) }}">{{ $relatedArticle->title }} </a>
                                </div>
                                @forelse($relatedArticle->authors_articles as $author)
                                    <div class="blog-info__name">
                                        <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}"
                                                                         alt="{{ $author->fullName }}"></div>
                                        {{ $author->fullName }}
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <div class="blog-item__img">
                                <a href="{{ route('blog.article', $relatedArticle->alias) }}">
                                    <img src="{{ $relatedArticle->backgroundArticle }}"
                                         alt="{{ $relatedArticle->title }}">
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="blog-top__big blog-item">
                            <a href="{{ route('blog.article', $relatedArticle->alias) }}">
                                <img src="{{ $relatedArticle->backgroundArticle }}" alt="{{ $relatedArticle->title }}"
                                     class="scale-img">
                            </a>
                            @forelse($relatedArticle->tags_articles as $tag)
                                @if(isset($tag->htmlClass))
                                    <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                                @endif
                            @empty
                            @endforelse
                            <div class="blog-item__bottom">
                                <div class="blog-title"><a
                                            href="{{ route('blog.article', $relatedArticle->alias) }}">{{ $relatedArticle->title }}</a>
                                </div>
                                <div class="blog-info">
                                    @forelse($relatedArticle->authors_articles as $author)
                                        <div class="blog-info__name">
                                            <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}"
                                                                             alt="{{ $author->fullName }}"></div>
                                            {{ $author->fullName }}
                                        </div>
                                    @empty
                                    @endforelse
                                    <div class="blog-info__right">
                                        <div class="blog-info__right" data-id="{{ $relatedArticle->id }}">
                                            <div class="blog-info__date">{{ $relatedArticle->updated }}</div>
                                            <div class="blog-info__watch blog-top__item"><i
                                                        class="icon icon-eye"></i>{{ $relatedArticle->viewed }}</div>
                                            <div class="blog-info__like blog-top__item"><i
                                                        class="icon icon-like"></i><span
                                                        class="js-result">{{ $relatedArticle->likes }}</span></div>
                                            <div class="blog-info__dislike blog-top__item"><i
                                                        class="icon icon-dislike"></i><span
                                                        class="js-result">{{ $relatedArticle->dislikes }}</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @empty
                @endforelse
            </section>
        </div>
    </main>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="/css/blog.min.css">
@endpush

@push('scripts')
    <script defer src="/js/commons.min.js"></script>
    <script src="/js/blog.js" defer></script>
    <script type="text/javascript" src="/js/article/lib.min.js"></script>
    <script type="text/javascript" src="/js/article/main.js"></script>
@endpush