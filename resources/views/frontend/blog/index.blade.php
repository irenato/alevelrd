@extends('frontend.layouts.app-blog')

@push('seo_fields')
    @isset($blog)
        <meta name="description"
              content="{{ $blog->seo_description }}">
        <meta name="keywords" content="{{ $blog->seo_keywords }}">
        <meta name="robots" content="{{ $blog->seo_robots }}">
        @if($blog->seo_canonical)
            <link rel="canonical" href="{{ $blog->seo_canonical }}"/>
        @endif
    @endisset
@endpush
@section('content')
    <main class="blog-page padding-page">
        <div class="container">
            <section class="blog-top flex-wrap">
                <div class="blog-top__left">
                    <div class="blog-top__interest">
                        <div class="blog-top__interest-title"><img src="/images/fire.svg" alt="#">Интересно сейчас</div>
                        @forelse($recentlyArticles as $recentlyArticle)
                            <a href="{{ route('blog.article', $recentlyArticle->alias) }}" class="blog-top__item-wrapp">
                                <div class="blog-top__item-title">{{ $recentlyArticle->title }}</div>
                                <div class="blog-top__item-info flex-wrap" data-id="{{ $recentlyArticle->id }}">
                                    <div class="blog-top__item-name blog-top__item">{{ $recentlyArticle->authors_articles->pluck('fullName')->implode(', ') }}</div>
                                    <div class="blog-top__item-watch blog-top__item"><i class="icon icon-eye"></i>{{ $recentlyArticle->viewed }}</div>
                                    <div class="blog-top__item-like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $recentlyArticle->likes }}</span></div>
                                    <div class="blog-top__item-dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $recentlyArticle->dislikes }}</span></div>
                                </div>
                            </a>
                        @empty
                        @endforelse
                    </div>
                </div>
                @isset($mostViewedArticles)
                    @forelse($mostViewedArticles->forPage(1, 1) as $mostViewedArticle)
                        <div class="blog-top__right"><a href="{{ route('blog.article', $mostViewedArticle->alias) }}" class="blog-top__big blog-item">
                                <img src="{{ $mostViewedArticle->backgroundArticle }}" alt="{{ $mostViewedArticle->title }}" class="scale-img">
                                @forelse($mostViewedArticle->tags_articles as $tag)
                                    @if(isset($tag->htmlClass))
                                        <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                                    @endif
                                @empty
                                @endforelse
                                <div class="blog-item__bottom">
                                    <div class="blog-title">{{ $mostViewedArticle->title }}</div>
                                    <div class="blog-info">
                                        @forelse($mostViewedArticle->authors_articles as $author)
                                            <div class="blog-info__name">
                                                <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}" alt="{{ $author->fullName }}"></div>
                                                {{ $author->fullName }}
                                            </div>
                                        @empty
                                        @endforelse
                                        <div class="blog-info__right" data-id="{{ $mostViewedArticle->id }}">
                                            <div class="blog-info__date">{{ $mostViewedArticle->updated }}</div>
                                            <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $mostViewedArticle->viewed }}</div>
                                            <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $mostViewedArticle->likes }}</span></div>
                                            <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $mostViewedArticle->dislikes }}</span></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                    @endforelse
                @endisset
            </section>
            <section class="all-blog-items sticky-container">
                <aside class="blog-subscribe sticky">
                    <button class="add-info-btn"><i class="icon icon-contract"></i>Прислать материал</button>
                    <div class="subscribe-form">
                        <div class="subscribe-title">Подписаться</div>
                        <form action="{{ route('blog.subscribe') }}" method="POST" class="js_validate js-form">
                            <div class="input-field">
                                <input type="email" name="email" data-validate="email" required placeholder="Email"><span class="error-text">Заполните поле</span>
                            </div>
                            <div class="checkin__text js-error-area" style="display: none"></div>
                            <div class="form-btn">
                                <button type="submit" class="btn btn--orange">Подписаться</button>
                            </div>
                        </form>
                    </div>
                </aside>

                @forelse($articles->forPage(1, 5) as $article)
                    <a href="{{ route('blog.article', $article->alias) }}" class="blog-item flex-wrap">
                        <div class="blog-item__descr">
                            <div class="blog-item__top flex-wrap">
                                @forelse($article->tags_articles as $tag)
                                    @if(isset($tag->htmlClass))
                                        <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                                    @endif
                                @empty
                                @endforelse
                                <div class="blog-info">
                                    <div class="blog-info__right" data-id="{{ $article->id }}">
                                        <div class="blog-info__date">{{ $article->updated }}</div>
                                        <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $article->viewed }}</div>
                                        <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $article->likes }}</span></div>
                                        <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $article->dislikes }}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-title">{{ $article->title }}</div>
                            @forelse($article->authors_articles as $author)
                                <div class="blog-info__name">
                                    <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}" alt="{{ $author->fullName }}"></div>
                                    {{ $author->fullName }}
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="blog-item__img"><img src="{{ $article->thumbnailMini }}" alt="{{ $article->title }}"></div>
                    </a>
                @empty
                @endforelse
                @forelse($mostViewedArticles->forPage(2, 1) as $mostViewedArticle)
                    <a href="{{ route('blog.article', $mostViewedArticle->alias) }}" class="blog-top__big blog-item">
                        <img src="{{ $mostViewedArticle->backgroundArticle }}" alt="{{ $mostViewedArticle->title }}" class="scale-img">
                        @forelse($mostViewedArticle->tags_articles as $tag)
                            @if(isset($tag->htmlClass))
                                <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                            @endif
                        @empty
                        @endforelse
                        <div class="blog-item__bottom">
                            <div class="blog-title">{{ $mostViewedArticle->title }}</div>
                            <div class="blog-info">
                                @forelse($mostViewedArticle->authors_articles as $author)
                                    <div class="blog-info__name">
                                        <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}" alt="{{ $author->fullName }}"></div>
                                        {{ $author->fullName }}
                                    </div>
                                @empty
                                @endforelse
                                <div class="blog-info__right" data-id="{{ $mostViewedArticle->id }}">
                                    <div class="blog-info__date">{{ $mostViewedArticle->updated }}</div>
                                    <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $mostViewedArticle->viewed }}</div>
                                    <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $mostViewedArticle->likes }}</span></div>
                                    <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $mostViewedArticle->dislikes }}</span></div>
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                @endforelse
                @if($ourArticles)
                    <div class="dark-wrapp flex-wrap">
                        <div class="dark-wrapp__title">Выбор нашей школы</div>
                        @forelse($ourArticles as $ourArticle)
                            <a href="{{ route('blog.article', $ourArticle->alias) }}" class="dark-wrapp__item">
                                <div class="dark-wrapp__img"><img src="{{ $ourArticle->backgroundArticle }}" alt="{{ $ourArticle->title }}"></div>
                                <div class="dark-wrapp__item-title">{{ $ourArticle->title }}</div>
                                <div class="dark-wrapp__info flex-wrap" data-id="{{ $ourArticle->id }}">
                                    <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $ourArticle->viewed }}</div>
                                    <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $ourArticle->likes }}</span></div>
                                    <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $ourArticle->dislikes }}</span></div>
                                </div>
                            </a>
                        @empty
                        @endforelse
                    </div>
                @endif
                @forelse($articles->forPage(2, 5) as $article)
                    <a href="{{ route('blog.article', $article->alias) }}" class="blog-item flex-wrap">
                        <div class="blog-item__descr">
                            <div class="blog-item__top flex-wrap">
                                @forelse($article->tags_articles as $tag)
                                    @if(isset($tag->htmlClass))
                                        <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                                    @endif
                                @empty
                                @endforelse
                                <div class="blog-info">
                                    <div class="blog-info__right" data-id="{{ $article->id }}">
                                        <div class="blog-info__date">{{ $article->updated }}</div>
                                        <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $article->viewed }}</div>
                                        <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $article->likes }}</span></div>
                                        <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $article->dislikes }}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-title">{{ $article->title }}</div>
                            @forelse($article->authors_articles as $author)
                                <div class="blog-info__name">
                                    <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}" alt="{{ $author->fullName }}"></div>
                                    {{ $author->fullName }}
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="blog-item__img"><img src="{{ $article->thumbnailMini }}" alt="{{ $article->title }}"></div>
                    </a>
                @empty
                @endforelse
                @forelse($mostViewedArticles->forPage(3, 1) as $mostViewedArticle)
                    <a href="{{ route('blog.article', $mostViewedArticle->alias) }}" class="blog-top__big blog-item">
                        <img src="{{ $mostViewedArticle->backgroundArticle }}" alt="{{ $mostViewedArticle->title }}" class="scale-img">
                        @forelse($mostViewedArticle->tags_articles as $tag)
                            @if(isset($tag->htmlClass))
                                <div class="blog-item__tag {{ $tag->htmlClass->html_class }}">{{ $tag->name }}</div>
                            @endif
                        @empty
                        @endforelse
                        <div class="blog-item__bottom">
                            <div class="blog-title">{{ $mostViewedArticle->title }}</div>
                            <div class="blog-info">
                                @forelse($mostViewedArticle->authors_articles as $author)
                                    <div class="blog-info__name">
                                        <div class="blog-info__pic"><img src="{{ $author->thumbnailMini }}" alt="{{ $author->fullName }}"></div>
                                        {{ $author->fullName }}
                                    </div>
                                @empty
                                @endforelse
                                <div class="blog-info__right" data-id="{{ $mostViewedArticle->id }}">
                                    <div class="blog-info__date">{{ $mostViewedArticle->updated }}</div>
                                    <div class="blog-info__watch blog-top__item"><i class="icon icon-eye"></i>{{ $mostViewedArticle->viewed }}</div>
                                    <div class="blog-info__like blog-top__item"><i class="icon icon-like"></i><span class="js-result">{{ $mostViewedArticle->likes }}</span></div>
                                    <div class="blog-info__dislike blog-top__item"><i class="icon icon-dislike"></i><span class="js-result">{{ $mostViewedArticle->dislikes }}</span></div>
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                @endforelse
            </section>
        </div>
    </main>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="/css/blog.min.css">


@endpush

@push('scripts')
    <script defer src="/js/commons.min.js"></script>
    <script src="/js/blog.js" defer></script>
    <script type="text/javascript" src="/js/lib.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
@endpush