<div class="triggers">
    <button class="trigger trigger--lightblue" data-goto><span class="trigger__icon"><i class="icon-city"></i></span><span class="trigger__text"><span>О школе</span></span></button>
    <button class="trigger trigger--orange" data-goto><span class="trigger__icon"><i class="icon-document"></i></span><span class="trigger__text"> Курсы</span></button>
    <button class="trigger trigger--darkblue" data-goto><span class="trigger__icon"><i class="icon-calendar"></i></span><span class="trigger__text"><span>Календарь</span></span></button>
    <button class="trigger trigger--orange" data-goto><span class="trigger__icon"><i class="icon-users"></i></span><span class="trigger__text"><span>Учителя</span></span></button>
    @if($action)
        <button class="trigger trigger--orange trigger--sale" data-goto><span class="trigger__icon"><i class="icon-tag"></i></span><span class="trigger__text"><span>Акция</span></span></button>
    @endif
    <button class="trigger trigger--lightblue" data-goto><span class="trigger__icon"><i class="icon-partners"></i></span><span class="trigger__text"><span>Партнеры</span></span></button>
    <button class="trigger trigger--darkblue" data-goto><span class="trigger__icon"><i class="icon-information"></i></span><span class="trigger__text"><span>Контакты</span></span></button>
</div>