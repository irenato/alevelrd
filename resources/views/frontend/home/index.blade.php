@extends('frontend.layouts.app')

@push('styles')
    <link rel="stylesheet" href="css/index.min.css">
@endpush

@section('title', $data->seo_title)

@push('seo_fields')
    <meta property="og:title" content="{{ $data->seo_title }}" />
    <meta name="description"
          content="{{ $data->seo_description }}">
    <meta name="keywords" content="{{ $data->seo_keywords }}">
    <meta name="robots" content="{{ $data->seo_robots }}">
    @if($data->seo_canonical)
        <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
    @endif
@endpush

@section('content')

    <body class="body index with-sale loading">
    <div class="main" data-window data-hash="home" id="home">
        <p class="main__h1">{!! $data->title !!} </p>
    </div>
    <!-- Scenes-->
    <div class="scenes">

        @push('action-countdown')
            @if($action)
                <div class="sale"
                     style="background-image: url({{ Storage::disk('action')->url($action->background) }});"><i
                            class="icon-close" id="closeSale"></i>
                    {{--<p class="sale__heading">{{ $action->title }}</p>--}}
                    {{--<p class="sale__subheading">До окончания осталось</p>--}}
                    {{--<div class="sale__time sale-time" id="clockdiv" data-date="{{ $action->end_date }}">
                        <div class="sale-time__item">
                            <div class="sale-time__days js-timer-days"></div>
                            <span>дней</span>
                        </div>
                        <div class="sale-time__item">
                            <div class="sale-time__hours js-timer-hours"></div>
                            <span>часов</span>
                        </div>
                        <div class="sale-time__item">
                            <div class="sale-time__minutes js-timer-minutes"></div>
                            <span>мин.</span>
                        </div>
                        <div class="sale-time__item">
                            <div class="sale-time__seconds js-timer-seconds"></div>
                            <span>сек.</span>
                        </div>
                    </div>--}}


                    <button class="btn btn--orange sale__btn" data-goto="5">Подробнее</button>
                </div>
            @endif
        @endpush
    <!-- About us-->
        <section class="layer" data-window data-hash="about" id="about">
            <p class="mobile__heading">О нас</p>
            <div class="window">
                <div class="window-left">
                    <div class="blur-img" id="about-blur-img"
                         style="background-image: url({{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->about_section_thumbnail ), 375, 420) }});"></div>
                </div>
                <div class="window-right">
                    <div class="window-about tabs">
                        <div class="tabs__links">
                            <div class="tabs__link">
                                <input name="about" type="radio" id="r1" value="1" checked>
                                <label for="r1">О нас</label>
                            </div>
                            @if($reviews)
                                <div class="tabs__link">
                                    <input name="about" type="radio" id="r2" value="2">
                                    <label for="r2">О нас говорят</label>
                                </div>
                            @endif
                            <div class="tabs__link">
                                <input name="about" type="radio" id="r3" value="3">
                                <label for="r3">Наши награды</label>
                            </div>
                            <div class="tabs__link">
                                <input name="about" type="radio" id="r4" value="4">
                                <label for="r4">Скидки</label>
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabs__item tabs__item--1"
                                 data-backgroung="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->about_section_thumbnail ), 375, 420) }}">
                                <p class="window-about__heading">{{ $data->about_section_title }}</p>
                                <div class="content-editor editor noscroll" id="editor" data-noscroll="true"
                                     data-simplebar data-simplebar-auto-hide="false">
                                    <div class="content-editor__inner">
                                        {!! $data->about_section_content !!}
                                    </div>
                                </div>
                            </div>
                            @if($discounts)
                                <div class="tabs__item tabs__item--4 sell-outer"
                                     data-backgroung="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->discount_section_thumbnail ), 375, 420) }}">
                                    <div class="sell noscroll" id="sell" data-noscroll="true" data-simplebar
                                         data-simplebar-auto-hide="false">
                                        <div class="content-editor__inner">
                                            <p class="window-popup__heading">{{ $data->discount_section_title }}</p>
                                            @foreach($discounts as $discount)
                                                <div class="sell__row">
                                                    <div class="sell__percent {{ $discount->class }}">{{ $discount->value }}</div>
                                                    <div class="sell__text">{!! $discount->description !!} </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="tabs__item tabs__item--2"
                                 data-backgroung="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->opinion_section_thumbnail ), 375, 420) }}">
                                <p class="window-about__heading">{{ $data->opinion_section_title }}</p>
                                @if($reviews)
                                    <div class="content-editor editor noscroll" id="editor-about" data-noscroll="true"
                                         data-simplebar data-simplebar-auto-hide="false">
                                        <div class="content-editor__inner">
                                            @foreach($reviews as $review)
                                                <img data-noscroll="true"
                                                     data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('reviews')->url(  $review->id . '/' . $review->thumbnail  ), 100, 100) }}"
                                                     alt="{{ $review->username }}">
                                                <span data-noscroll="true">{{ $review->username }}</span>
                                                <p data-noscroll="true">{!! $review->review !!}</p>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if($rewards)
                                <div class="tabs__item tabs__item--3"
                                     data-backgroung="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  $data->rewards_section_thumbnail ), 375, 420) }}">
                                    <div class="content-editor editor victory-list noscroll" id="editor-victory"
                                         data-noscroll="true" data-simplebar data-simplebar-auto-hide="false">
                                        <div class="content-editor__inner">
                                            @foreach($rewards as $reward)
                                                <div class="victory"
                                                     data-noscroll="true"
                                                     data-title="{!! $reward->title !!}"
                                                     data-description="{!!  strip_tags($reward->description) !!}"
                                                     data-image="{{ Storage::disk('rewards')->url(  $reward->id . '/' . $reward->thumbnail ) }}">
                                                    <img data-src="{{  Storage::disk('rewards')->url(  $reward->id . '/' . $reward->thumbnail ) }}"
                                                         alt="victory"
                                                         data-noscroll="true">
                                                    <button class="btn btn--orange btn--small victory__btn"
                                                            data-noscroll="true">Подробнее
                                                    </button>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                @include('frontend.modals.regard-info')

            </div>
        </section>
        <!-- Courses-->
        @if($courses)
            <section class="layer courses" data-window data-hash="courses" id="courses">
                <p class="mobile__heading">Курсы</p>
                <div class="scene__window">
                    <div class="slider-container">
                        <div id="coverflow" class="noscroll" data-noscroll="true">
                            <ul class="flip-items">
                                @foreach($courses as $course)
                                    <li data-course="{{ $loop->iteration-1 }}" data-daysLeft="{{ $course->days_left }}"
                                        data-discount="{{ $course->discount_price }}">
                                        <a href="{{ route('course', ['course_alias' => $course->alias]) }}"
                                           target="_blank">
                                            <div class="cover courses__item @if ($course->employment) courses__item--employment @endif">
                                                <figure class="course @if($action or $course->discount_price > 0) course--sale @endif">
                                                    <div class="course__title @if(mb_strlen($course->title) > 10) course__title--small @endif"
                                                         data-before="Курс"
                                                         data-after="{{ $course->type->visible ? $course->type->title : '' }}">{!! $course->title !!}</div>
                                                    <div class="course__img"><img
                                                                data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('courses')->url(  $course->id . '/' . $course->logo  ), 505, 730) }}"
                                                                alt="course-item"></div>
                                                    @if($action && $course->cost_old)
                                                        <span class="course__sale"><span>Акция</span></span>
                                                    @endif

                                                    @if($course->days_left > 0)
                                                        <div class="course__content">
                                                            <div class="course__price"
                                                                 data-old="{{ $course->cost }} {{ $course->price_type }}">
                                                                {{ $course->discount_price }}
                                                                <small>{{ $course->price_type }}</small>
                                                            </div>
                                                            <div class="course__schedule">{{ $course->begin }}</div>
                                                            @if($course->free_places > 0)
                                                                <div class="course__vacancy">Осталось
                                                                    мест:<span>{{ $course->free_places }}</span>
                                                                    <span class="btn btn--small btn--orange"
                                                                          target="_blank">Подробнее</span>
                                                                </div>
                                                            @else
                                                                <div class="course__vacancy course__vacancy--close">
                                                                    <span>Набор закрыт</span>
                                                                    <span class="btn btn--small btn--orange"
                                                                          target="_blank">Подробнее</span>
                                                                </div>
                                                            @endif
                                                        </div>

                                                    @else
                                                        <div class="course__content">
                                                            <div class="course__price"
                                                                 @if($course->cost_old) data-old="{{ $course->cost_old }} {{ $course->price_type }}" @endif>
                                                                {{ $course->action_cost }}
                                                                <small>{{ $course->price_type }}</small>
                                                            </div>

                                                            <div class="course__schedule">{{ $course->begin }}</div>
                                                            @if($course->free_places > 0)
                                                                <div class="course__vacancy">Осталось
                                                                    мест:<span>{{ $course->free_places }}</span>
                                                                    <span class="btn btn--small btn--orange"
                                                                          target="_blank">Подробнее</span>
                                                                </div>
                                                            @else
                                                                <div class="course__vacancy course__vacancy--close">
                                                                    <span>Набор закрыт</span>
                                                                    <span class="btn btn--small btn--orange"
                                                                          target="_blank">Подробнее</span>
                                                                </div>
                                                            @endif
                                                        </div>

                                                    @endif
                                                </figure>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="slider-nav">
                            <div id="navigation">
                                <ul>
                                    @foreach($courses as $course)
                                        <li data-course="{{ $loop->iteration-1 }}">
                                            <i class="{{ $course->logo_class }}"></i>
                                            <span>{{ $course->title }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    <!-- Calendar-->
        <section class="layer" data-window data-hash="calendar" id="calendar">
            <p class="mobile__heading">{{ $data->schedule_section_title }}</p>
            <div class="scene__window s_w_calendar">
                <div class="window-info">
                    <div class="window-info-photo calendar js-events" data-route="{{ route('get-events') }}">
                        <div class="calendar__front">
                            <div class="calendar__header"><span class="calendar__date">10 апреля 2017</span><span
                                        class="calendar__day">Пятница</span></div>
                            <div class="calendar__body">
                                <div class="calendar__datepicker" id="dp"></div>
                            </div>
                        </div>
                        <div class="calendar__back"><img class="window-info-photo__img" data-src="" src="" alt="Event"
                                                         id="calendar-big-img">
                            <div class="window-info-photo__content"><span
                                        class="window-info-photo__h2 calendar__back-day"></span>
                                <p class="window-info-photo__p calendar__back-date"></p>
                            </div>
                        </div>
                    </div>
                    <div class="window-info-list event">
                        <div class="event__front">
                            <div class="window-info-list__block" data-noscroll="true">

                                <div class="window-info__controls " ><span
                                            class="window-info__heading window-info__heading--dark">Ближайшие мероприятия</span>
                                </div>
                                <div class="content-editor__inner" data-simplebar data-simplebar-auto-hide="false">
                                    <ul class="window-info__list noscroll" id="events" data-noscroll="true"></ul>
                                    <!-- <button class="btn btn--orange btn--ml btn--width">Подписаться на новости</button> -->
                                </div>
                            </div>

                        </div>
                        <div class="event__back">
                            <div class="window-info__controls window-info__controls--mb">
                                <button class="calendar__goback"><i class="icon-arrow-left"></i>Все мероприятия</button>
                            </div>
                            <p class="calendar__name"></p>
                            <div class="content-editor editor noscroll" id="editor" data-noscroll="true"
                                 data-simplebar data-simplebar-auto-hide="false">
                                <p class="calendar__description content-editor__inner" id="calendar-full-descr"
                                   data-noscroll="true"></p>
                            </div>

                            <div>
                                <a href="#" class="btn btn--white btn--width js-go-to-event">Записаться</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Teachers-->
        <section class="layer" data-window data-hash="teachers">
            <p class="mobile__heading js-teachers"
               data-route="{{ route('get-teachers') }}">{{ $data->teachers_section_title }}</p>
            <div class="scene__window">
                <div class="window-info" id="teachers">
                    <div class="window-info-photo">
                        <div class="blur-img"></div>
                        <div class="window-info-photo__content"><span class="window-info-photo__h2"></span>
                            <p class="window-info-photo__p"></p>
                            <p class="window-info-photo__descr"></p>
                            <ul class="window-info-photo__socials"></ul>
                        </div>
                    </div>
                    <div class="window-info-list">
                        <div class="window-info-list__block">
                            <div class="window-info__controls noscroll" data-noscroll="true"><span
                                        class="window-info__heading">Наши преподы</span></div>

                            <div class="content-editor__inner" data-simplebar data-simplebar-auto-hide="false">
                                <ul class="window-info__list" id="our-teachers"></ul>
                            </div>

                            <div class="window-info__row">
                                <button class="btn btn--orange btn--ml js-open-modal-teacher">Стать преподавателем
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Stock-->
        @if($action)
            <section class="layer js-sell" data-window data-hash="sell">
                <p class="mobile__heading">Акция</p>
                <div class="scene__window">
                    <div class="window-info" id="stock-sell">
                        <div class="window-info-photo stock__bg">
                            <div class="stock__img"
                                 style="background-image: url({{ Storage::disk('action')->url(  $action->thumbnail  ) }})"
                                 alt="{{ $action->title }}"></div>
                        </div>
                        <div class="window-info-list">
                            <div class="stock noscroll" data-noscroll="true">
                                <div class="stock__heading">Акция</div>
                                <div class="stock__subheading">{{ $action->title }}</div>
                                <div class="stock__sell">Осталось:</div>
                                <div class="stock__row">
                                    <div class="stock__block">
                                        <span>{{ $action->total_places - $action->occupied_places }}</span> мест
                                    </div>
                                    <div class="stock__block">
                                        <span>{{ \Carbon\Carbon::now()->diffInDays($action->end_date, false) }}</span>
                                        дней
                                    </div>
                                </div>
                                <div class="stock__editor" data-noscroll="true">
                                    <div class="stock__p" data-noscroll="true" data-simplebar
                                         data-simplebar-auto-hide="false">
                                        {!! $action->description !!}
                                    </div>
                                </div>
                                <div class="stock__row">
                                    <button class="btn btn--orange stock__btn js-open-modal-request">Воспользоваться
                                        акцией
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif




    <!-- Partners-->

        <section class="layer" data-window data-hash="partners">
            <p class="mobile__heading js-partners"
               data-route="{{ route('get-partners') }}">{{ $data->partners_section_title }}</p>
            <div class="scene__window">
                <div class="window-info" id="partners">
                    <div class="window-info-photo">
                        <div class="blur-img"></div>
                        <div class="window-info-photo__content">
                            <a class="js-partner-link">
                                <span class="window-info-photo__h2"></span>
                            </a>
                            <p class="window-info-photo__p"></p>
                            <p class="window-info-photo__descr"></p>
                            <ul class="window-info-photo__socials"></ul>
                        </div>
                    </div>
                    <div class="window-info-list">
                        <div class="window-info-list__block" data-noscroll="true">
                            <div class="window-info__controls"><span class="window-info__heading">Наши партнеры</span>
                            </div>
                            <div classs="content-editor__inner" data-simplebar data-simplebar-auto-hide="false">
                                <ul class="window-info__list noscroll" id="our-partners" data-noscroll="true"></ul>
                            </div>
                            <div class="window-info__row">
                                <button class="btn btn--orange btn--ml js-open-modal-partner">Стать партнером</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        @endsection

        @push('start-button')
            @if(isset($nearbyCourses) && !($nearbyCourses->isEmpty()))
                <div class="start">
                    <div class="nearby-courses">
                        <div class="near-courses-title"><h4>{{ $data->nearby_courses_title }}</h4></div>
                        <ul>
                            @foreach($nearbyCourses as $nearbyCourse)
                                <li>
                                    <a href="{{ route('course', ['course_alias' => $nearbyCourse->alias]) }}">
                                        {{ $nearbyCourse->title }}
                                        <span>{{ $nearbyCourse->type->visible ? $nearbyCourse->type->title : '' }}</span>
                                    </a>
                                    <div>
                                        <span>{{ $nearbyCourse->begin }}</span>
                                        <span>{!! $nearbyCourse->free_places . '/' . $nearbyCourse->amount_places !!}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button class="start__btn btn btn--orange" data-goto="2">Посмотреть все курсы</button>
                </div>
            @endif
        @endpush

        @push('goto')
            <div class="goto">
                <div class="goto__prev"><i class="icon-arrow-top"></i></div>
                <div class="goto__next"><i class="icon-arrow-down"></i></div>
            </div>
        @endpush

        @push('scene-bg')
            <div class="scene-bg">
                <div class="bg bg-bg"></div>
                <div class="bg bg-7"></div>
                <div class="bg bg-6"></div>
                <div class="bg bg-5"></div>
                <div class="bg bg-4"></div>
                <div class="bg bg-3"></div>
                <div class="bg bg-2"></div>
                <div class="bg bg-1"></div>
                <div class="bg bg-pc"></div>
                <div class="bg bg-text" ></div>
                <div class="bg bg-logo"></div>
            </div>
        @endpush

        @push('page-menu')
            @include('frontend.home.page-menu')
        @endpush

        @push('scripts')
            <script defer src="/js/commons.min.js"></script>
            <script defer src="js/index.min.js"></script>
            <script>
                window.onload = function () {
                    [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
                        img.setAttribute('src', img.getAttribute('data-src'));
                        img.onload = function () {
                            img.removeAttribute('data-src');
                        };
                    });
                };

            </script>
    @endpush
