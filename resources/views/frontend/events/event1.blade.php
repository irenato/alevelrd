@extends('frontend.layouts.app-event')

@section('title', $event->seo_title)

@push('seo_fields')
    <meta property="og:title" content="{{ $event->seo_title }}" />
    <meta name="description"
          content="{{ $event->seo_description }}">
    <meta name="keywords" content="{{ $event->seo_keywords }}">
    <meta name="robots" content="{{ $event->seo_robots }}">
    @if($event->seo_canonical)
        <link rel="canonical" href="{{ $event->seo_canonical  }}"/>
    @endif
@endpush
@section('content')
    <div class="wrapper-blue shadow pb-0">
        <div class="bg-cloud pb-115">
            <div class="container">
                <div class="row pt-25 mb-100">
                    <div class="col-md-4 d-flex justify-content-center justify-content-md-start mb-15 mb-md-0 ">
                        <div><a href="/"><img class="img-responsive logo w-100" src="/img/alevelua.svg" alt=""></a></div>
                    </div>
                    <div class="col-md-8 d-flex align-items-end justify-content-end flex-column flex-lg-row hub-r"><a
                                class="text-white display-20 mr-lg-70 mr-md-10 regular mb-20 mb-lg-0" href="/">Вернуться
                            к сайту</a>
                        <a href="/#calendar"
                           class="btn btn-blue rounded-10 px-15 py-10 px-lg-25 py-lg-15 semibold display-20">Другие наши
                            мероприятия</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex flex-column align-items-center justify-content-center order-1 mb-70">
                        <p class="display-48 text-white mb-65 bold text-center text-md-left">{{ $event->title }}</p>
                        <a href="#register" class="btn btn-orange rounded-10 px-25 py-15 semibold display-20 js-create-lead">Зарегистрироваться</a>
                    </div>
                    <div class="col-md-12 order-0 order-md-1 mb-20 mb-md-0 d-flex align-items-center justify-content-center">
                        <img class="img-responsive"
                             src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('events')->url(  $event->id . '/' . $event->thumbnail), 690, 372) }}"
                             alt="{{ $event->title }}"></a>
                    </div>
                </div>
            </div>
            <div class="bg-white pt-80 pb-110">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="display-36 text-center mb-40 text-black regular text-uppercase bold">Содержание и
                                суть интенсива</p>
                        </div>
                        <div class="col-md-12">
                            <div class="display-20 mb-40 semibold">
                                {!! $event->description !!}
                            </div>
                        </div>
                        @if($event->rule_title || $event->rule_description)
                            <div class="col-md-12">
                                @if($event->rule_title)
                                    <p class="text-orange display-18 regular">{{ $event->rule_title }}</p>
                                @endif
                                @if($event->rule_description)
                                    {!! $event->rule_description !!}
                                @endif
                                <a class="btn btn-orange rounded-10 px-25 py-15 semibold display-20 js-create-lead" href="#register">Зарегистрироваться</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="bg-white pt-70 pb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="display-36 text-center text-black mb-40 bold text-uppercase">Спикеры</p>
                        </div>
                        @if($event->events_teachers)
                            <div class="col-12">
                                @forelse($event->events_teachers as $teacher)
                                    <div class="row">
                                        <div class="col-md-6 order-1">
                                            <p class="display-36 text-black bold">{{ $teacher->fullName }}</p>
                                            <p class="text-grey display-20 regular">{!! $teacher->specialization !!}</p>
                                            <p class="text-black display-20 semibold mb-40">
                                                {!! $teacher->description !!}
                                            </p>
                                        </div>
                                        <div class="col-md-6 order-0 order-md-1 mb-20 mb-md-0 d-flex justify-content-center align-items-center">
                                            <img class="img-responsive w-300"
                                                 data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('teachers')->url(  $teacher->id . '/' . $teacher->thumbnail  ), 210, 210) }}"
                                                 alt="{{ $teacher->fullName }}">
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <div class="col-12 order-2"><a
                                        class="btn btn-orange rounded-10 px-25 py-15 semibold display-20 js-create-lead"
                                        href="#register">Зарегистрироваться</a></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="bg-white pt-75 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mb-40">
                            <p class="display-36 text-center text-black mb-0 bold text-uppercase">{{ $event->timeline_title }}</p>
                        </div>
                    </div>
                    @if($event->timeline)
                        @forelse($event->timeline as $key => $value)
                            @if(isset($value['date']))
                                <div class="row">
                                    <div class="col-md-12 mb-30">
                                        <p class="display-54 text-center text-orange mb-0 bold">{{ $value['date'] }}</p>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                @if(isset($value['thumbnail']))
                                    <div class="col-md-12">
                                        <p class="text-center mb-20 d-flex align-items-center justify-content-center">
                                            <img class="img-responsive img-icon"
                                                 data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('events')->url(  $event->id . '/' . (isset($value['thumbnail']) ?  $value['thumbnail'] : '')), 64, 64) }}"
                                                 alt="{{ isset($value['title']) ? $value['title'] : '' }}">
                                        </p>
                                    </div>
                                @endif
                                @if(isset($value['interval']))
                                    <div class="col-md-12 mb-25">
                                        <p class="display-18 text-center text-black mb-0 regular">{{ $value['interval'] }}</p>
                                    </div>
                                @endif
                                @if(isset($value['title']))
                                    <div class="col-md-12 mb-20">
                                        <p class="display-30 text-center text-blue mb-0 bold">{{ $value['title'] }}</p>
                                    </div>
                                @endif
                                @if(isset($value['description']))
                                    <div class="col-md-12 mb-20 d-flex justify-content-center">
                                        <p class="display-18 text-center text-black mb-0 text regular">
                                            {!! $value['description'] !!}
                                        </p>
                                    </div>
                                @endif
                            </div>

                            <div class="row d-flex justify-content-center">
                                <div class="col-4">
                                    <div class="d-block underline mb-40 w-100"></div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    @endif
                </div>
            </div>
            <div class="wrapper-blue wrapper-btn-orange">
                <div class="container" id="register">
                    <div class="row d-flex justify-content-center pb-75 mt-90">
                        <div class="col-md-6">
                            <form class="d-flex flex-column align-items-center js-form" id="form" method="post"
                                  autocomplete="off" action="{{ route('add-event-application') }}">
                                <p class="display-36 text-center text-uppercase text-white bold">РЕГИСТРАЦИЯ</p>
                                <input class="form-control px-10 mb-30 input text-white" type="text" name="username"
                                       placeholder="Фамилия и Имя"/>
                                <input class="form-control px-10 mb-30 input text-white" type="email" name="email"
                                       placeholder="Email"/>
                                <input class="form-control px-10 mb-50 input text-white  js-phone " type="text"
                                       name="phone" placeholder="+38 (050) 000-0000"/>
                                <input type="hidden" name="event_id" value="{{ $event->id }}"/>
                                <div class="checkin__text js-error-area" style="display: none"></div>
                                <button type="submit"
                                        class="btn rounded-10 px-15 py-10 px-lg-25 py-lg-15 semibold display-20">
                                    Зарегистрироваться
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white pt-90 pb-110">
                <div class="container mb-70">
                    <div class="row">
                        <div class="col-md-12 mb-100">
                            <p class="display-36 text-center mb-0 text-black text-uppercase bold">ПАРТНЕРЫ</p>
                        </div>
                        <div class="col-12">
                            <div class="owl-carousel owl-theme">
                                @for ($i = 0; $i < count($partners); $i++)
                                    <div class="owl-carousel__item">
                                        <img class="my-10 img-fluid d-block"
                                             data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('partners')->url(  $partners[$i]->id . '/' . $partners[$i]->thumbnail  ), 239, 239) }}"
                                             alt="{{ $partners[$i]->title }}">
                                        @if(isset($partners[++$i]))
                                            <img class="my-10 img-fluid"
                                                 data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('partners')->url(  $partners[$i]->id . '/' . $partners[$i]->thumbnail  ), 239, 239) }}"
                                                 alt="{{ $partners[$i]->title }}">
                                        @endif
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script>
        window.onload = function() {
            [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
                img.setAttribute('src', img.getAttribute('data-src'));
                img.onload = function () {
                    img.removeAttribute('data-src');
                };
            });                };

    </script>
@endpush