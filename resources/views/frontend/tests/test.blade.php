@extends('frontend.layouts.test')

@push('styles')
    <link rel="stylesheet" href="/css/testing.css">
@endpush

@isset($data)
    @section('title', $data->seo_title)

@push('seo_fields')
    <meta name="description"
          content="{{ $data->seo_description }}">
    <meta name="keywords" content="{{ $data->seo_keywords }}">
    <meta name="robots" content="{{ $data->seo_robots }}">
    @if($data->seo_canonical)
        <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
    @endif
@endpush

@endisset

@section('content')

    <body class="body testing-page">
    <section class="section course-main">
        <div class="double-bg">
            <div class="double-bg__left"></div>
            <div class="double-bg__right"></div>
        </div>
        <div class="window">
            <div class="window-left"><img class="window-left__img" src="/img/testing/choose.png" alt="army"></div>
            <div class="window-right">
                <p class="testing__heading" id="choose__p">Тестирование на выбор курса</p>
                <div class="animator" id="questions">
                    <form action="{{ route('tests.question', ['id' => $nextTest->id ?? null]) }}" method="POST" class="testing-start" data-slide-testing="1">
                        {{ csrf_field() }}
                        <div class="test__body-wrapper">
                            <div class="test__question">{{ $test->question }}</div>
                            <ul class="test__answers">
                                @forelse($test->answers as $answer)
                                    <li class="test__answer answer">
                                        <div class="answer__radio">
                                            <input type="radio" name="answer_id" id="answer-{{ $answer->id }}" value="{{ $answer->id }}" required="required">
                                            <label  for="answer-{{ $answer->id }}"></label>
                                        </div>
                                        <label for="answer-{{ $answer->id }}" class="answer__p">{{ $answer->key . ') ' . $answer->answer }}</label>
                                    </li>
                                @empty
                                @endforelse
                                <input type="hidden" name="test_id" value="{{ $test->id }}">
                            </ul>
                        </div>
                        <div class="">
                            @isset($prevTest)
                                <a href="{{ route('tests.question', ['id' => $prevTest->id]) }}" class="btn btn--gray  prev-question-btn">Предыдущий вопрос</a>
                            @endisset
                            <button type="submit" class="btn btn--orange next-question-btn">Следующий вопрос</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.tests.template-parts')
    <!-- Loading JS Start-->
    @push('scripts')
        <script defer src="/js/commons.js"></script>
        <script defer src="/js/testing.js"></script>
    @endpush
    <!-- Loading JS End-->
    </body>

@endsection


