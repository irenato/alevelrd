<div class="burger__wrapper">
    <div class="burger" id="menu">
        <ul class="burger__list">
            <li class="burger__item"></li>
            <li class="burger__item"></li>
            <li class="burger__item"></li>
        </ul>
        <span class="burger__text text-white">Menu</span>
    </div>
</div>
<!-- Вспомогательный класс .navigate--row-->
<div class="navigate"><a class="navigate__btn btn btn--darkblue" href="#request" id="request" onClick="fbq('track', 'Lead');"><i class="icon-envelope"></i><span class="btn__span">Оставить заявку</span></a><a class="navigate__btn btn btn--blue"
                                                                                                                                                                                href="{{ route('hub') }}"
                                                                                                                                                                                target="_blank"><i
                class="icon-city"></i><span class="btn__span">A-Level HUB</span></a>
    <div class="select navigate__select" id="cities">
        <div class="icon-before icon-location"></div>
        <select class="cities">
            @forelse($cities as $city)
                <option value="{{ $city->id }}" @if($city->id == session('city')) selected="selected" @endif>{{ $city->title }}</option>
            @empty
            @endforelse
        </select>
        <div class="icon-after icon-arrow-down"></div>
    </div>
</div>
<section class="menu">
    <div class="menu__left-orange" id="submenu" data-noscroll="true">
        <div class="menu-inside" data-noscroll="true">
            <button class="menu-inside__hide"><i class="icon-arrow-left"></i><span>Скрыть</span></button>
            <ul class="nav-courses">
                @forelse($courses as $course)
                    <li class="nav-courses__item"><a href="{{ route('course', ['course_alias' => $course->alias]) }}" target="_blank"><i class="{{ $course->logo_class }}"></i>{!! $course->title !!}
                            <br> {!! $course->type->visible ? $course->type->title : '' !!}</a></li>
                @empty
                @endforelse
            </ul>
            <div class="menu__row"><a class="menu__link" href="http://a-level.com.ua/hub/" target="_blank">A-Level hub</a><a class="menu__link" href="http://a-level.com.ua/hub/" target="_blank">Выбор курса</a></div>
        </div>
    </div>
    <div class="menu__right-orange"></div>
    <div class="menu__left-blue">
        <button class="mobile-menu-close"><i class="icon-close mobile-menu-close__icon"></i></button>
        <div class="menu__content" id="mainMenu" data-noscroll="true"><a class="menu__logo" href="/"><img src="./img/logo.png" alt="A-Level"></a>
            <ul class="nav-menu">
                <li class="nav-menu__item"><a class="nav-menu__link" href="{{ route('home') }}#about" data-goto="1"><i class="icon-city"></i>О школе</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="{{ route('home') }}#calendar" data-goto="3"><i class="icon-calendar"></i>Календарь</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" id="courses-btn" href="{{ route('home') }}#courses"><i class="icon-arrow-right"></i>Курсы</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="{{ route('home') }}#teachers" data-goto="4"><i class="icon-users"></i>Учителя</a></li>
                <li class="nav-menu__item">
                    <!-- Если есть акции надо data-goto="6" а если нету 5--><a class="nav-menu__link" href="{{ route('home') }}#partners" data-goto="6"><i class="icon-partners"></i>Партнеры</a>
                </li>
                <li class="nav-menu__item">
                    <!-- Если есть акции надо data-goto="7" а если нету 6--><a class="nav-menu__link" href="{{ route('home') }}#contacts" data-goto="7"><i class="icon-information"></i>Контакты</a>
                </li>
            </ul>
            <a class="menu__link" href="{{ route('hub') }}">A-Level hub</a><a class="menu__link menu__link--orange" href="#stock-sell" id="stock" data-goto="5">Акции</a><a class="menu__link"
                                                                                                                                                                                                    href="http://a-level.com.ua/hub/"
                                                                                                                                                                                                    target="_blank">Выбор
                курса</a>
            <ul class="social">
                @if($city_data->social_links)
                    @foreach($city_data->social_links as $item)
                        @foreach($item as $key => $value)
                            @if($value)
                                <li class="social__item"><a class="{{ $key }}" href="{{ $value }}" target="_blank"></a></li>
                            @endif
                        @endforeach
                    @endforeach
                @endif
            </ul>
            <div class="menu__contacts menu-contacts">
                @if($city_data->email)
                    <a class="menu-contacts__item" href="mailto:{{ $city_data->email }}">
                        <i class="icon-envelope"></i>
                        {{ $city_data->email }}
                    </a>
                @endif
                <div class="menu-contacts__item">
                    <i class="icon-smartphone"></i>
                    <div class="phones">
                        @if($city_data->phones)
                            @foreach($city_data->phones as $phone)
                                <a href="tel:{{ \App\Helpers\Helpers::filterPhone($phone) }}">{{ $phone }}</a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <a class="menu-contacts__item" href="https://goo.gl/maps/SutFBzXh1iT2" target="_blank">
                    <i class="icon-location"></i>{!! $city_data->address !!}
                </a>
            </div>
        </div>
    </div>
    <div class="menu__right-blue"></div>
</section>
<section class="preloader"><img class="preloader__gear" src="/img/gears.svg" alt="preloader"></section>
