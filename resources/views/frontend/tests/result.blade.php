@extends('frontend.layouts.test')

@isset($data)
    @section('title', $data->seo_title)

@push('seo_fields')
    <meta name="description"
          content="{{ $data->seo_description }}">
    <meta name="keywords" content="{{ $data->seo_keywords }}">
    <meta name="robots" content="{{ $data->seo_robots }}">
    @if($data->seo_canonical)
        <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
    @endif
@endpush

@endisset

@push('styles')
    <link rel="stylesheet" href="/css/testing.css">
@endpush

@section('content')

    <body class="body testing-page">
    <section class="section course-main">
        <div class="double-bg">
            <div class="double-bg__left"></div>
            <div class="double-bg__right"></div>
        </div>
        <div class="window">
            <div class="window-left"><img class="window-left__img" src="/img/testing/choose.png" alt="army"></div>
            <div class="window-right">
                <div class="result">
                    <p class="result__heading">Вам подходит курс:<a id="your-course" href="{{ route('course', ['course_alias' => $course->alias]) }}"> {{ $course->title }}*</a></p>
                    {!! $course->description !!}
                </div>
            </div>
        </div>
    </section>
    @include('frontend.tests.template-parts')
    <!-- Loading JS Start-->
    @push('scripts')
        <script defer src="/js/commons.js"></script>
        <script defer src="/js/testing.js"></script>
    @endpush
    <!-- Loading JS End-->
    </body>

@endsection


