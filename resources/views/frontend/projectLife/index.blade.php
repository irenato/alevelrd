<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124729347-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-124729347-1');
        setTimeout("ga('send','event','Интересующийся Посетитель','время на странице более 1 минут', location.pathname)",60000);
    </script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$data->title}}</title>
    <meta name="description" content="{{ $data->seo_description }}">
    <meta name="keywords" content="{{ $data->seo_keywords }}">
    <meta name="robots" content="{{ $data->seo_robots }}">
    @if($data->seo_canonical)
        <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
    @endif
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=cyrillic-ext" rel="stylesheet">

    <link rel="stylesheet" href="life3.0/css/bootstrap-reboot.min.css" type="text/css" />
    <link rel="stylesheet" href="life3.0/css/bootstrap-grid.min.css" type="text/css" />
    <link rel="stylesheet" href="life3.0/css/fullpage.css" type="text/css" />
    <link rel="stylesheet" href="life3.0/css/slick.css" type="text/css" />

    <link rel="stylesheet" href="life3.0/css/style.css" type="text/css" />

    <link rel="stylesheet" type="text/css" href="life3.0/css/tooltipster.main.min.css" />




    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js "></script>
    <![endif]-->

</head>

<body>
<header id="header">
    <div class="container">
        <div class="logo-wrap">
            <a href="https://a-level.com.ua/" target="_blank">
                <img src="life3.0/images/logo.svg" alt="logo">
            </a>
        </div>

        <div class="logo-wrap"  style="margin-left: 1%;">
            <a href="#">
                <img src="life3.0/images/logo_khgs.png" alt="logo">
            </a>
        </div>
    </div>
</header>
<ul id="navigation">
    <li data-menuanchor="main" class="active"><a href="#main"><span>01</span></a>
    </li>
    <li data-menuanchor="about"><a href="#about"><span>02</span></a>
    </li>
    <li data-menuanchor="spikers"><a href="#spikers"><span>03</span></a>
    </li>
    <li data-menuanchor="programm"><a href="#programm"><span>04</span></a>
    </li>
    <li data-menuanchor="timeline"><a href="#timeline"><span>05</span></a>
    </li>
    <li data-menuanchor="partners"><a href="#partners"><span>06</span></a>
    </li>
    <li data-menuanchor="registration"><a href="#registration"><span>07</span></a>
    </li>
</ul>
<div id="fullpage">
    <div id="section-1" class="section" data-anchor="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="box-info">
                        <span class="date">{{$data->date}}</span>
                        <span class="place">Fabrika space</span>
                    </div>
                    <h1>{{$data['section_1_h1']}}</h1>
                    <p class="h2">{{$data['section_1_h2']}}</p>
                    {!! $data['section_1_p'] !!}
                    <div class="box-full-info">
                        <p class="title">{{$data['section_1_p_cl_title']}}</p>
                        {{--<p class="opacity-60"><span class="blue">{{1500 - $count}}</span><span class="orange">/1500 мест</span>--}}
                        {{--</p>--}}
                        <div id="clockdiv">
                            <span class="days"></span> дней
                            <span class="hours"></span> час.
                            <span class="minutes"></span>мин.
                            {{--Seconds: <span class="seconds"></span>--}}
                        </div>
                        <p class="opacity-60"><span class="orange">Регистрация закрыта</span>
                        </p>

                        {{--<p>15 дней 12 час. 43 мин.</p>--}}
                    </div>
                    {{--<a href="#registration" class="btn">Зарегистрироваться</a>--}}
                    <a style="background: grey" class="btn">Зарегистрироваться</a>
                    <a href="{{$data->link}}" target="_blank" class="btn btn-play"><i class="play"></i></a>
                </div>
                <div class="image">
                    <img src="{{$data['section_1_p_img']}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <div id="section-2" class="section" data-anchor="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <p class="h1">{{$data['section_2h1_cl']}}</p>
                    {!! $data['section_2p'] !!}
                    <div class="bonuses">
                        <span class="drink">300 л. кофе и чая</span>
                        <span class="food">120 кг. печенья и вкуснях</span>
                    </div>

                    <a href="#programm" class="btn btn-schedule">Программа мероприятия</a>
                </div>
                <div class="col-lg-5">
                    <div class="gallery-about">

                        @foreach($sliders as $slider)
                            <div class="item">
                                <div class="image">
                                    <img src="{{$slider->img}}" alt="">
                                </div>
                                <div class="text">{{$slider->text}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="section-3" class="section" data-anchor="spikers">
        <div class="container">
            <p class="h1">Спикеры</p>
            <div class="carousel-speakers">
                @foreach($speakers as $speaker)
                    <div class="item-wrap">
                        <div class="item">
                            <div class="image">
                                <div class="img">
                                    <img src="{{$speaker->img}}" alt="">
                                </div>
                                <div class="img-hover">
                                    <img src="{{$speaker->img_hover}}" alt="">
                                </div>
                            </div>
                            <div class="info">
                                <p class="name">{{$speaker->name}}</p>
                                <p class="short-text">{{$speaker->short_text}}</p>
                                <div class="text">
                                    {!! $speaker->text !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="carousel-speakers-nav">
                <button type="button" class="slick-prev"></button>
                <button type="button" class="slick-next"></button>
            </div>

        </div>
    </div>
    <div id="section-4" class="section" data-anchor="programm">
        <div class="container">
            <p class="h1">{{$data['section_4_h1']}}</p>
            <div class="box-dark box-programm scroll">
                <div class="row">
                    @foreach($programms as $programm)
                        <div class="item col-xl-4 col-lg-6 tooltip" data-tooltip-content="#tooltip_content_{{$loop->iteration}}">
                            <p class="h4">{{$programm->title}} <span class="{{$programm->room}}">{{ucfirst($programm->room)}} room</span>
                            </p>
                            <p> {{$programm->short_text}}</p>
                            <div class="photo">
                                @foreach($programm['speakers'] as $speaker)
                                    <div class="img">
                                        <img src="{{$speaker['img']}}" alt=" ">
                                        {{--<img src="life3.0/images/media/photo-sm-1.jpg" alt="ssss">--}}
                                    </div>
                                @endforeach
                            </div>
                            <!-- Hidden zone start-->
                            <div class="tooltip_templates">
                                <div id="tooltip_content_{{$loop->iteration}}">
                                    <!-- ID for data-tooltip-content -->
                                    <p class="h3">{{$programm->title}}</p>
                                    <div class="row info">
                                        <div class="col">
                                            <span class="title">Время проведения:</span>
                                            {{$programm->time}}
                                        </div>
                                        <div class="col">
                                            <span class="title">Место проведения:</span>
                                            @if($programm->room == 'gray')
                                                Gray room
                                            @elseif($programm->room == 'green')
                                                Green room
                                            @elseif($programm->room == 'blue')
                                                Blue room
                                            @elseif($programm->room == 'red')
                                                Red room
                                            @elseif($programm->room == 'class')
                                                Class room
                                            @endif
                                        </div>
                                    </div>
                                    @if(count($programm['speakers']) > 0 )
                                        <span class="title">Спикеры:</span>
                                        @foreach($programm['speakers'] as $speaker)
                                            <div class="sreaker">
                                                <div class="image">
                                                    <img src="{{$speaker['img']}}" alt="" />
                                                </div>
                                                <p class="name">{{$speaker['name']}}</p>
                                                <p>{{$speaker['short_text']}}</p>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="text">
                                        {!! $programm->text !!}
                                    </div>
                                </div>
                            </div>
                            <!-- Hidden zone end -->
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <div id="section-5" class="section" data-anchor="timeline">
        <div id="building" class="building">
            <img src="life3.0/images/building.png" alt="" usemap="#image-map">

            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 974 774" style="enable-background:new 0 0 974 774;" xml:space="preserve">
            <g class="room gray-room">
                <rect x="398" y="152" class="gray" width="273" height="111" />
                <g>
                    <path d="M446.1,236h11.1v13.9c-3,1-6.3,1.5-9.9,1.5c-4,0-7-1.1-9.2-3.4s-3.2-5.6-3.2-9.8c0-4.1,1.2-7.4,3.6-9.7s5.7-3.5,9.9-3.5

						c1.6,0,3.1,0.2,4.6,0.5s2.7,0.7,3.8,1.2l-2.2,5.4c-1.9-0.9-3.9-1.4-6.1-1.4c-2,0-3.6,0.7-4.7,2s-1.7,3.2-1.7,5.7

						c0,2.4,0.5,4.2,1.5,5.5s2.4,1.9,4.3,1.9c1,0,2-0.1,2.8-0.3v-4h-4.6V236z" />
                    <path d="M474,230.8c0.6,0,1.2,0,1.7,0.1l0.4,0.1l-0.6,6.5c-0.6-0.1-1.3-0.2-2.3-0.2c-1.5,0-2.6,0.3-3.3,1s-1,1.7-1,3v9.8H462

						v-19.9h5.1l1.1,3.2h0.3c0.6-1.1,1.4-1.9,2.4-2.6S473,230.8,474,230.8z" />
                    <path d="M488.5,251.4c-3.3,0-5.9-0.9-7.7-2.6s-2.8-4.3-2.8-7.5c0-3.4,0.8-5.9,2.5-7.7s4.1-2.7,7.3-2.7c3,0,5.3,0.8,7,2.3

						s2.5,3.8,2.5,6.8v3.1H485c0,1.1,0.5,2,1.2,2.6s1.8,0.9,3.1,0.9c1.2,0,2.3-0.1,3.3-0.3s2.1-0.6,3.3-1.2v4.9

						c-1.1,0.6-2.2,0.9-3.3,1.1S490.1,251.4,488.5,251.4z M488.1,235.5c-0.8,0-1.5,0.3-2,0.8s-0.9,1.3-1,2.4h5.9c0-1-0.3-1.7-0.8-2.3

						S489,235.5,488.1,235.5z" />
                    <path d="M498.8,231.1h7.2l3.4,11.4c0.2,0.6,0.3,1.3,0.3,2.2h0.1c0.1-0.8,0.2-1.5,0.4-2.1l3.5-11.5h7l-7.9,21.2

						c-1,2.7-2.2,4.6-3.7,5.7s-3.4,1.7-5.8,1.7c-0.9,0-1.9-0.1-2.8-0.3V254c0.6,0.1,1.3,0.2,2.1,0.2c0.6,0,1.1-0.1,1.6-0.4

						s0.9-0.6,1.2-1s0.7-1.1,1.1-2.1L498.8,231.1z" />
                    <path d="M544.5,230.8c0.6,0,1.2,0,1.7,0.1l0.4,0.1l-0.6,6.5c-0.6-0.1-1.3-0.2-2.3-0.2c-1.5,0-2.6,0.3-3.3,1s-1,1.7-1,3v9.8h-6.9

						v-19.9h5.1l1.1,3.2h0.3c0.6-1.1,1.4-1.9,2.4-2.6S543.5,230.8,544.5,230.8z" />
                    <path d="M568.4,241.1c0,3.3-0.9,5.8-2.6,7.6s-4.2,2.7-7.4,2.7c-3,0-5.4-0.9-7.2-2.8s-2.7-4.4-2.7-7.6c0-3.2,0.9-5.8,2.6-7.6

						s4.2-2.7,7.4-2.7c2,0,3.7,0.4,5.2,1.2s2.7,2,3.5,3.6S568.4,239,568.4,241.1z M555.5,241.1c0,1.7,0.2,3,0.7,3.9s1.2,1.3,2.3,1.3

						c1.1,0,1.8-0.4,2.3-1.3s0.7-2.2,0.7-3.9c0-1.7-0.2-3-0.7-3.8s-1.2-1.3-2.3-1.3c-1.1,0-1.8,0.4-2.3,1.3S555.5,239.3,555.5,241.1z" />
                    <path d="M591.3,241.1c0,3.3-0.9,5.8-2.6,7.6s-4.2,2.7-7.4,2.7c-3,0-5.4-0.9-7.2-2.8s-2.7-4.4-2.7-7.6c0-3.2,0.9-5.8,2.6-7.6

						s4.2-2.7,7.4-2.7c2,0,3.7,0.4,5.2,1.2s2.7,2,3.5,3.6S591.3,239,591.3,241.1z M578.4,241.1c0,1.7,0.2,3,0.7,3.9s1.2,1.3,2.3,1.3

						c1.1,0,1.8-0.4,2.3-1.3s0.7-2.2,0.7-3.9c0-1.7-0.2-3-0.7-3.8s-1.2-1.3-2.3-1.3c-1.1,0-1.8,0.4-2.3,1.3S578.4,239.3,578.4,241.1z" />
                    <path d="M619.7,251.1v-10.8c0-1.3-0.2-2.4-0.6-3s-1-1-1.7-1c-1.1,0-1.8,0.5-2.3,1.4s-0.7,2.3-0.7,4.2v9.3h-6.9v-10.8

						c0-1.3-0.2-2.4-0.5-3s-0.9-1-1.7-1c-1.1,0-1.9,0.5-2.4,1.4s-0.7,2.5-0.7,4.7v8.7h-6.9v-19.9h5.2l0.9,2.5h0.4

						c0.5-0.9,1.3-1.6,2.3-2.1s2.1-0.7,3.4-0.7c2.9,0,4.9,0.9,6,2.6h0.5c0.6-0.8,1.3-1.5,2.3-1.9s2.1-0.7,3.3-0.7

						c2.4,0,4.1,0.6,5.2,1.8s1.7,3,1.7,5.5v13H619.7z" />
                </g>
            </g>
                <g class="room blue-room">
                    <rect x="398" y="273" class="blue" width="136" height="111.2" />
                    <g>
                        <path d="M407.2,364.6h4c1.6,0,2.7,0.2,3.5,0.7c0.8,0.5,1.1,1.2,1.1,2.2c0,0.6-0.2,1.2-0.5,1.6s-0.8,0.7-1.3,0.9v0.1

						c0.7,0.2,1.2,0.5,1.5,0.9c0.3,0.4,0.5,1,0.5,1.7c0,1-0.4,1.9-1.2,2.4c-0.8,0.6-1.8,0.9-3.2,0.9h-4.4V364.6z M410.3,369h0.9

						c0.4,0,0.8-0.1,1-0.3c0.2-0.2,0.4-0.5,0.4-0.8c0-0.6-0.5-1-1.5-1h-0.9V369z M410.3,371.3v2.4h1.1c1,0,1.4-0.4,1.4-1.2

						c0-0.4-0.1-0.7-0.4-0.9s-0.6-0.3-1.1-0.3H410.3z" />
                        <path d="M420.9,376.1h-3.1v-12.2h3.1V376.1z" />
                        <path d="M429.3,376.1l-0.4-1.1h-0.2c-0.3,0.4-0.6,0.7-1.1,0.9c-0.5,0.2-1,0.3-1.6,0.3c-1,0-1.7-0.3-2.3-0.8

						c-0.5-0.6-0.8-1.4-0.8-2.4v-5.8h3.1v4.8c0,0.6,0.1,1,0.2,1.3s0.4,0.4,0.8,0.4c0.5,0,0.9-0.2,1.1-0.6c0.2-0.4,0.3-1.1,0.3-2.1v-3.9

						h3.1v8.9H429.3z" />
                        <path d="M438,376.2c-1.5,0-2.6-0.4-3.4-1.2c-0.8-0.8-1.2-1.9-1.2-3.3c0-1.5,0.4-2.6,1.1-3.4c0.8-0.8,1.8-1.2,3.2-1.2

						c1.3,0,2.4,0.3,3.1,1s1.1,1.7,1.1,3v1.4h-5.5c0,0.5,0.2,0.9,0.5,1.2c0.3,0.3,0.8,0.4,1.4,0.4c0.5,0,1-0.1,1.5-0.2

						c0.5-0.1,0.9-0.3,1.5-0.5v2.2c-0.5,0.2-1,0.4-1.5,0.5C439.3,376.2,438.7,376.2,438,376.2z M437.8,369.1c-0.4,0-0.7,0.1-0.9,0.3

						s-0.4,0.6-0.4,1.1h2.6c0-0.4-0.1-0.8-0.4-1C438.5,369.3,438.2,369.1,437.8,369.1z" />
                        <path d="M453.1,367c0.3,0,0.5,0,0.8,0.1l0.2,0l-0.3,2.9c-0.3-0.1-0.6-0.1-1-0.1c-0.7,0-1.2,0.2-1.5,0.5c-0.3,0.3-0.5,0.7-0.5,1.3

						v4.4h-3.1v-8.9h2.3l0.5,1.4h0.1c0.3-0.5,0.6-0.8,1.1-1.1C452.2,367.2,452.7,367,453.1,367z" />
                        <path d="M463.7,371.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2c-0.8-0.8-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6

						C463.6,369.9,463.7,370.7,463.7,371.6z M458,371.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7

						c0-0.8-0.1-1.3-0.3-1.7c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S458,370.8,458,371.6z" />
                        <path d="M473.9,371.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2c-0.8-0.8-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6

						C473.7,369.9,473.9,370.7,473.9,371.6z M468.2,371.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7

						c0-0.8-0.1-1.3-0.3-1.7c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S468.2,370.8,468.2,371.6z" />
                        <path d="M486.5,376.1v-4.8c0-0.6-0.1-1-0.2-1.3c-0.2-0.3-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6c-0.2,0.4-0.3,1-0.3,1.9v4.1h-3.1

						v-4.8c0-0.6-0.1-1-0.2-1.3c-0.2-0.3-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6c-0.2,0.4-0.3,1.1-0.3,2.1v3.9h-3.1v-8.9h2.3l0.4,1.1

						h0.2c0.2-0.4,0.6-0.7,1-0.9c0.4-0.2,1-0.3,1.5-0.3c1.3,0,2.2,0.4,2.7,1.2h0.2c0.3-0.4,0.6-0.6,1-0.9c0.4-0.2,0.9-0.3,1.5-0.3

						c1,0,1.8,0.3,2.3,0.8c0.5,0.5,0.8,1.4,0.8,2.4v5.8H486.5z" />
                    </g>
                </g>
                <g class="room green-room">
                    <rect x="536.9" y="273" class="green" width="134.1" height="111.2" />
                    <g>
                        <path d="M551.8,369.4h4.9v6.2c-1.3,0.5-2.8,0.7-4.4,0.7c-1.8,0-3.1-0.5-4.1-1.5c-1-1-1.4-2.5-1.4-4.4c0-1.8,0.5-3.3,1.6-4.3

						c1.1-1,2.5-1.5,4.4-1.5c0.7,0,1.4,0.1,2,0.2c0.6,0.1,1.2,0.3,1.7,0.5l-1,2.4c-0.8-0.4-1.7-0.6-2.7-0.6c-0.9,0-1.6,0.3-2.1,0.9

						s-0.7,1.4-0.7,2.5c0,1.1,0.2,1.9,0.7,2.4c0.4,0.6,1.1,0.8,1.9,0.8c0.5,0,0.9,0,1.3-0.1v-1.8h-2V369.4z" />
                        <path d="M564.2,367c0.3,0,0.5,0,0.8,0.1l0.2,0l-0.3,2.9c-0.3-0.1-0.6-0.1-1-0.1c-0.7,0-1.2,0.2-1.5,0.5s-0.5,0.7-0.5,1.3v4.4h-3.1

						v-8.9h2.3l0.5,1.4h0.1c0.3-0.5,0.6-0.8,1.1-1.1S563.8,367,564.2,367z" />
                        <path d="M570.7,376.2c-1.5,0-2.6-0.4-3.4-1.2c-0.8-0.8-1.2-1.9-1.2-3.3c0-1.5,0.4-2.6,1.1-3.4c0.8-0.8,1.8-1.2,3.2-1.2

						c1.3,0,2.4,0.3,3.1,1s1.1,1.7,1.1,3v1.4h-5.5c0,0.5,0.2,0.9,0.5,1.2s0.8,0.4,1.4,0.4c0.5,0,1-0.1,1.5-0.2s0.9-0.3,1.5-0.5v2.2

						c-0.5,0.2-1,0.4-1.5,0.5S571.4,376.2,570.7,376.2z M570.5,369.1c-0.4,0-0.7,0.1-0.9,0.3s-0.4,0.6-0.4,1.1h2.6c0-0.4-0.1-0.8-0.4-1

						C571.2,369.3,570.9,369.1,570.5,369.1z" />
                        <path d="M580.6,376.2c-1.5,0-2.6-0.4-3.4-1.2c-0.8-0.8-1.2-1.9-1.2-3.3c0-1.5,0.4-2.6,1.1-3.4c0.8-0.8,1.8-1.2,3.2-1.2

						c1.3,0,2.4,0.3,3.1,1s1.1,1.7,1.1,3v1.4H579c0,0.5,0.2,0.9,0.5,1.2s0.8,0.4,1.4,0.4c0.5,0,1-0.1,1.5-0.2s0.9-0.3,1.5-0.5v2.2

						c-0.5,0.2-1,0.4-1.5,0.5S581.3,376.2,580.6,376.2z M580.4,369.1c-0.4,0-0.7,0.1-0.9,0.3s-0.4,0.6-0.4,1.1h2.6c0-0.4-0.1-0.8-0.4-1

						C581.1,369.3,580.8,369.1,580.4,369.1z" />
                        <path d="M591.8,376.1v-4.8c0-0.6-0.1-1-0.3-1.3c-0.2-0.3-0.4-0.4-0.8-0.4c-0.5,0-0.9,0.2-1.1,0.6c-0.2,0.4-0.3,1.1-0.3,2.1v3.9

						h-3.1v-8.9h2.3l0.4,1.1h0.2c0.3-0.4,0.6-0.7,1.1-0.9c0.5-0.2,1-0.3,1.6-0.3c1,0,1.7,0.3,2.3,0.9c0.5,0.6,0.8,1.4,0.8,2.4v5.8

						H591.8z" />
                        <path d="M606.4,367c0.3,0,0.5,0,0.8,0.1l0.2,0l-0.3,2.9c-0.3-0.1-0.6-0.1-1-0.1c-0.7,0-1.2,0.2-1.5,0.5s-0.5,0.7-0.5,1.3v4.4h-3.1

						v-8.9h2.3l0.5,1.4h0.1c0.3-0.5,0.6-0.8,1.1-1.1S605.9,367,606.4,367z" />
                        <path d="M617,371.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2s-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6C616.8,369.9,617,370.7,617,371.6z

						 M611.3,371.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7c0-0.8-0.1-1.3-0.3-1.7

						c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S611.3,370.8,611.3,371.6z" />
                        <path d="M627.2,371.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2s-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6

						C627,369.9,627.2,370.7,627.2,371.6z M621.5,371.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7

						c0-0.8-0.1-1.3-0.3-1.7c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S621.5,370.8,621.5,371.6z" />
                        <path d="M639.8,376.1v-4.8c0-0.6-0.1-1-0.2-1.3c-0.2-0.3-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6c-0.2,0.4-0.3,1-0.3,1.9v4.1h-3.1

						v-4.8c0-0.6-0.1-1-0.2-1.3c-0.2-0.3-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6c-0.2,0.4-0.3,1.1-0.3,2.1v3.9H629v-8.9h2.3l0.4,1.1h0.2

						c0.2-0.4,0.6-0.7,1-0.9c0.4-0.2,1-0.3,1.5-0.3c1.3,0,2.2,0.4,2.7,1.2h0.2c0.3-0.4,0.6-0.6,1-0.9c0.4-0.2,0.9-0.3,1.5-0.3

						c1,0,1.8,0.3,2.3,0.8c0.5,0.5,0.8,1.4,0.8,2.4v5.8H639.8z" />
                    </g>
                </g>
                <g class="room red-room">
                    <rect x="398" y="549.1" class="red" width="136.3" height="111" />
                    <g>
                        <path d="M410.3,646.9v4.2h-3.1v-11.4h3.7c3.1,0,4.7,1.1,4.7,3.4c0,1.3-0.6,2.3-1.9,3.1l3.3,5h-3.5l-2.4-4.2H410.3z M410.3,644.6

						h0.6c1.1,0,1.6-0.5,1.6-1.4c0-0.8-0.5-1.2-1.6-1.2h-0.6V644.6z" />
                        <path d="M422.2,651.2c-1.5,0-2.6-0.4-3.4-1.2c-0.8-0.8-1.2-1.9-1.2-3.3c0-1.5,0.4-2.6,1.1-3.4c0.8-0.8,1.8-1.2,3.2-1.2

						c1.3,0,2.4,0.3,3.1,1s1.1,1.7,1.1,3v1.4h-5.5c0,0.5,0.2,0.9,0.5,1.2c0.3,0.3,0.8,0.4,1.4,0.4c0.5,0,1-0.1,1.5-0.2

						c0.5-0.1,0.9-0.3,1.5-0.5v2.2c-0.5,0.2-1,0.4-1.5,0.5C423.5,651.2,422.9,651.2,422.2,651.2z M422,644.1c-0.4,0-0.7,0.1-0.9,0.3

						s-0.4,0.6-0.4,1.1h2.6c0-0.4-0.1-0.8-0.4-1C422.7,644.3,422.4,644.1,422,644.1z" />
                        <path d="M430.5,651.2c-0.6,0-1.2-0.2-1.7-0.5c-0.5-0.4-0.8-0.9-1.1-1.6s-0.4-1.5-0.4-2.4c0-1.4,0.3-2.6,0.9-3.4s1.4-1.2,2.4-1.2

						c0.5,0,0.9,0.1,1.3,0.3c0.4,0.2,0.7,0.5,1,1h0.1c-0.1-0.7-0.1-1.4-0.1-2.1v-2.4h3.1v12.2h-2.3l-0.7-1.1h-0.1

						C432.3,650.8,431.6,651.2,430.5,651.2z M431.8,648.8c0.5,0,0.8-0.2,1-0.5c0.2-0.3,0.3-0.8,0.3-1.4v-0.2c0-0.8-0.1-1.3-0.3-1.7

						c-0.2-0.3-0.6-0.5-1.1-0.5c-0.4,0-0.7,0.2-0.9,0.6c-0.2,0.4-0.3,0.9-0.3,1.6c0,0.7,0.1,1.2,0.3,1.6

						C431,648.6,431.4,648.8,431.8,648.8z" />
                        <path d="M447.6,642c0.3,0,0.5,0,0.8,0.1l0.2,0l-0.3,2.9c-0.3-0.1-0.6-0.1-1-0.1c-0.7,0-1.2,0.2-1.5,0.5c-0.3,0.3-0.5,0.7-0.5,1.3

						v4.4h-3.1v-8.9h2.3l0.5,1.4h0.1c0.3-0.5,0.6-0.8,1.1-1.1C446.6,642.2,447.1,642,447.6,642z" />
                        <path d="M458.2,646.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2c-0.8-0.8-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6

						C458,644.9,458.2,645.7,458.2,646.6z M452.4,646.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7

						c0-0.8-0.1-1.3-0.3-1.7c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S452.4,645.8,452.4,646.6z" />
                        <path d="M468.4,646.6c0,1.4-0.4,2.6-1.2,3.4c-0.8,0.8-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2c-0.8-0.8-1.2-1.9-1.2-3.4

						c0-1.4,0.4-2.6,1.2-3.4c0.8-0.8,1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6c0.7,0.4,1.2,0.9,1.5,1.6

						C468.2,644.9,468.4,645.7,468.4,646.6z M462.6,646.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7

						c0-0.8-0.1-1.3-0.3-1.7c-0.2-0.4-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S462.6,645.8,462.6,646.6z" />
                        <path d="M481,651.1v-4.8c0-0.6-0.1-1-0.2-1.3s-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6s-0.3,1-0.3,1.9v4.1h-3.1v-4.8

						c0-0.6-0.1-1-0.2-1.3s-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6s-0.3,1.1-0.3,2.1v3.9h-3.1v-8.9h2.3l0.4,1.1h0.2

						c0.2-0.4,0.6-0.7,1-0.9s1-0.3,1.5-0.3c1.3,0,2.2,0.4,2.7,1.2h0.2c0.3-0.4,0.6-0.6,1-0.9s0.9-0.3,1.5-0.3c1,0,1.8,0.3,2.3,0.8

						s0.8,1.4,0.8,2.4v5.8H481z" />
                    </g>
                </g>
                <g class="room class-room">
                    <rect x="536.7" y="549.1" class="class" width="134.3" height="111" />
                    <g>
                        <path d="M552.3,642c-0.7,0-1.3,0.3-1.7,0.9c-0.4,0.6-0.6,1.4-0.6,2.5c0,2.2,0.8,3.3,2.5,3.3c0.5,0,1-0.1,1.5-0.2

						c0.5-0.1,1-0.3,1.4-0.5v2.6c-1,0.4-2,0.6-3.2,0.6c-1.7,0-3-0.5-4-1.5c-0.9-1-1.4-2.4-1.4-4.3c0-1.2,0.2-2.2,0.7-3.1

						s1.1-1.6,1.9-2.1s1.8-0.7,2.9-0.7c1.2,0,2.4,0.3,3.5,0.8l-0.9,2.4c-0.4-0.2-0.8-0.4-1.3-0.5S552.8,642,552.3,642z" />
                        <path d="M560.5,651.1h-3.1v-12.2h3.1V651.1z" />
                        <path d="M568.4,651.1l-0.6-1.2h-0.1c-0.4,0.5-0.8,0.9-1.3,1s-1,0.3-1.7,0.3c-0.8,0-1.5-0.3-2-0.8s-0.7-1.2-0.7-2.1

						c0-0.9,0.3-1.6,1-2.1s1.6-0.7,2.9-0.8l1.5,0v-0.1c0-0.7-0.4-1.1-1.1-1.1c-0.6,0-1.5,0.2-2.5,0.7l-0.9-2c1-0.5,2.3-0.8,3.9-0.8

						c1.1,0,2,0.3,2.6,0.8s0.9,1.3,0.9,2.4v5.8H568.4z M566.1,649.1c0.4,0,0.7-0.1,0.9-0.4s0.4-0.5,0.4-0.9v-0.7l-0.7,0

						c-1,0-1.5,0.4-1.5,1.1C565.3,648.8,565.5,649.1,566.1,649.1z" />
                        <path d="M579.4,648.3c0,0.9-0.3,1.7-1,2.1s-1.6,0.7-2.8,0.7c-0.7,0-1.2,0-1.7-0.1s-1-0.2-1.5-0.4v-2.4c0.5,0.2,1,0.4,1.6,0.5

						s1.1,0.2,1.5,0.2c0.7,0,1-0.2,1-0.5c0-0.2-0.1-0.3-0.3-0.4s-0.7-0.4-1.6-0.7c-0.8-0.3-1.4-0.7-1.7-1.1s-0.5-1-0.5-1.6

						c0-0.8,0.3-1.5,1-1.9s1.5-0.7,2.7-0.7c0.6,0,1.1,0.1,1.6,0.2s1,0.3,1.6,0.6l-0.8,2c-0.4-0.2-0.8-0.3-1.3-0.5s-0.8-0.2-1.1-0.2

						c-0.5,0-0.8,0.1-0.8,0.4c0,0.2,0.1,0.3,0.3,0.4s0.7,0.3,1.5,0.7c0.6,0.3,1.1,0.5,1.4,0.8s0.5,0.5,0.7,0.9S579.4,647.9,579.4,648.3

						z" />
                        <path d="M587.9,648.3c0,0.9-0.3,1.7-1,2.1s-1.6,0.7-2.8,0.7c-0.7,0-1.2,0-1.7-0.1s-1-0.2-1.5-0.4v-2.4c0.5,0.2,1,0.4,1.6,0.5

						s1.1,0.2,1.5,0.2c0.7,0,1-0.2,1-0.5c0-0.2-0.1-0.3-0.3-0.4s-0.7-0.4-1.6-0.7c-0.8-0.3-1.4-0.7-1.7-1.1s-0.5-1-0.5-1.6

						c0-0.8,0.3-1.5,1-1.9s1.5-0.7,2.7-0.7c0.6,0,1.1,0.1,1.6,0.2s1,0.3,1.6,0.6l-0.8,2c-0.4-0.2-0.8-0.3-1.3-0.5s-0.8-0.2-1.1-0.2

						c-0.5,0-0.8,0.1-0.8,0.4c0,0.2,0.1,0.3,0.3,0.4s0.7,0.3,1.5,0.7c0.6,0.3,1.1,0.5,1.4,0.8s0.5,0.5,0.7,0.9S587.9,647.9,587.9,648.3

						z" />
                        <path d="M599.1,642c0.3,0,0.5,0,0.8,0.1l0.2,0l-0.3,2.9c-0.3-0.1-0.6-0.1-1-0.1c-0.7,0-1.2,0.2-1.5,0.5s-0.5,0.7-0.5,1.3v4.4h-3.1

						v-8.9h2.3l0.5,1.4h0.1c0.3-0.5,0.6-0.8,1.1-1.1S598.7,642,599.1,642z" />
                        <path d="M609.8,646.6c0,1.4-0.4,2.6-1.2,3.4s-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2s-1.2-1.9-1.2-3.4c0-1.4,0.4-2.6,1.2-3.4

						s1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6s1.2,0.9,1.5,1.6S609.8,645.7,609.8,646.6z M604,646.6c0,0.8,0.1,1.3,0.3,1.7

						s0.5,0.6,1,0.6c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7c0-0.8-0.1-1.3-0.3-1.7s-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6

						S604,645.8,604,646.6z" />
                        <path d="M620,646.6c0,1.4-0.4,2.6-1.2,3.4s-1.9,1.2-3.3,1.2c-1.3,0-2.4-0.4-3.2-1.2s-1.2-1.9-1.2-3.4c0-1.4,0.4-2.6,1.2-3.4

						s1.9-1.2,3.3-1.2c0.9,0,1.6,0.2,2.3,0.6s1.2,0.9,1.5,1.6S620,645.7,620,646.6z M614.2,646.6c0,0.8,0.1,1.3,0.3,1.7s0.5,0.6,1,0.6

						c0.5,0,0.8-0.2,1-0.6s0.3-1,0.3-1.7c0-0.8-0.1-1.3-0.3-1.7s-0.5-0.6-1-0.6c-0.5,0-0.8,0.2-1,0.6S614.2,645.8,614.2,646.6z" />
                        <path d="M632.6,651.1v-4.8c0-0.6-0.1-1-0.2-1.3s-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6s-0.3,1-0.3,1.9v4.1h-3.1v-4.8

						c0-0.6-0.1-1-0.2-1.3s-0.4-0.4-0.8-0.4c-0.5,0-0.8,0.2-1,0.6s-0.3,1.1-0.3,2.1v3.9h-3.1v-8.9h2.3l0.4,1.1h0.2

						c0.2-0.4,0.6-0.7,1-0.9s1-0.3,1.5-0.3c1.3,0,2.2,0.4,2.7,1.2h0.2c0.3-0.4,0.6-0.6,1-0.9s0.9-0.3,1.5-0.3c1,0,1.8,0.3,2.3,0.8

						s0.8,1.4,0.8,2.4v5.8H632.6z" />
                    </g>
                </g>
          </svg>

        </div>

        <div class="container">
            <div class="row justify-content-end">
                <div class="col-lg-7 col-md-12">
                    <div class="box-dark box-timeline">
                        <p class="h4">Timeline</p>

                        <div class="timeline">
                            <div class="table-responsive-sm">
                                <table>
                                    <tr class="first">
                                        <th></th>
                                        <td class="border" data-room="gray">Gray room</td>
                                        <td class="border-green" data-room="green">Green room</td>
                                        <td class="border-blue" data-room="blue">Blue room</td>
                                        <td class="border-red" data-room="red">Red room</td>
                                        <td class="border-yellow" data-room="class">Class room</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['0']['time']}}</th>
                                        <td colspan="5" class="border">Регистрация</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['1']['time']}}</th>
                                        <td data-room="gray">Открытие</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['2']['time']}}</th>
                                        <td data-room="gray">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['2']['time'] && $r->room == 'gray' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="green">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['2']['time'] && $r->room == 'green' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="blue">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['2']['time'] && $r->room == 'blue' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td rowspan="2" class="border" data-room="red">
                                            @foreach( $workshop as $r)
                                                @if($r->time == $time['2']['time'] && $r->room == 'red' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td rowspan="2" class="border" data-room="class">
                                            @foreach( $workshop as $r)
                                                @if($r->time == $time['2']['time'] && $r->room == 'class' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['3']['time']}}</th>
                                        <td data-room="gray">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['3']['time'] && $r->room == 'gray' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach</td>
                                        <td data-room="green">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['3']['time'] && $r->room == 'green' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach</td>
                                        </td>
                                        <td data-room="blue">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['3']['time'] && $r->room == 'blue' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach</td>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['4']['time']}}</th>
                                        <td colspan="5" class="border">Кофебрейк</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['5']['time']}}</th>
                                        <td data-room="gray">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['5']['time'] && $r->room == 'gray' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="green">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['5']['time'] && $r->room == 'green' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="blue">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['5']['time'] && $r->room == 'blue' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td rowspan="2" class="border" data-room="red">
                                            @foreach( $workshop as $r)
                                                @if($r->time == $time['5']['time'] && $r->room == 'red' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td rowspan="2" class="border" data-room="class">
                                            @foreach( $workshop as $r)
                                                @if($r->time == $time['5']['time'] && $r->room == 'class' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['6']['time']}}</th>
                                        <td data-room="gray">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['6']['time'] && $r->room == 'gray' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="green">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['6']['time'] && $r->room == 'green' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td data-room="blue">
                                            @foreach( $programms as $r)
                                                @if($r->time == $time['6']['time'] && $r->room == 'blue' )
                                                    {{$r->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['7']['time']}}</th>
                                        <td data-room="gray">тайный спикер</td>
                                        <td data-room="green">тайный спикер</td>
                                        <td data-room="blue">тайный спикер</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['8']['time']}}</th>
                                        <td colspan="5" class="border">Выступление от партнеров</td>
                                    </tr>

                                    <tr>
                                        <th>{{ $time['9']['time']}}</th>
                                        <td colspan="5" class="border">Закрытие</td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="section-6" class="section" data-anchor="partners">
        <div class="container">
            <div class="box-top">
                <div class="box-dark">
                    <p class="h4">{{$data['section_6_h4_1']}}</p>
                    <div class="carousel-partners carousel-partners-general">

                        @if(isset($partners))
                            @foreach($partners as $partner)
                                @if($partner->type =='general')
                                    <div class="item tooltip2" data-tooltip-content="#tooltip_partners_{{$loop->iteration}}">
                                        <div class="image">
                                            <a href="{{$partner->link}}" target="_blank">
                                                <img src="{{$partner->logo}}" alt="">
                                            </a>
                                        </div>
                                        <p>{{$partner->short_text}}</p>
                                        <!-- Hidden zone start-->
                                        <div class="tooltip_partners">
                                            <div id="tooltip_partners_{{$loop->iteration}}">
                                                {!! $partner->text !!}
                                            </div>
                                        </div>
                                        <!-- Hidden zone end-->
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="box-dark">
                    <p class="h4">{{$data['section_6_h4_2']}}</p>
                    <div class="carousel-partners carousel-partners-premium">

                        @foreach($partners as $partner)
                            @if($partner->type =='premium')
                                <div class="item tooltip2" data-tooltip-content="#tooltip_partners_{{$loop->iteration}}">
                                    <div class="image">
                                        <a href="{{$partner->link}}" target="_blank">
                                            <img src="{{$partner->logo}}" alt="">
                                        </a>
                                    </div>
                                    <p>{{$partner->short_text}}</p>
                                    <!-- Hidden zone start-->
                                    <div class="tooltip_partners">
                                        <div id="tooltip_partners_{{$loop->iteration}}">
                                            {!! $partner->text !!}
                                        </div>
                                    </div>
                                    <!-- Hidden zone end-->
                                </div>
                            @endif
                        @endforeach


                    </div>
                </div>
            </div>
            <div class="box-dark">
                <p class="h4">{{$data['section_6_h4_3']}}</p>
                <div class="carousel-partners carousel-partners-media">

                    @foreach($partners as $partner)
                        @if($partner->type =='media')
                            <div class="item">
                                <div class="image">
                                    <a href="{{$partner->link}}">
                                        <img src="{{$partner->logo}}" alt="">
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div id="section-7" class="section" data-anchor="registration">
        <div class="container">
            <p class="h1">Регистрация</p>
            <form id="registerclient">
                <div class="boxes-registration">
                    <div class="box-registration box-dark">
                        <div class="box-full-info">
                            <p class="title">До начала осталось</p>
                            {{--<p class="opacity-60"><span class="blue">{{1500 - $count}}</span><span class="orange">/1500 мест</span>--}}

                            {{--<p>15 дней 12 час. 43 мин.</p>--}}
                            <div id="clockdiv2">
                                <span class="days"></span> дней
                                <span class="hours"></span> час.
                                <span class="minutes"></span>мин.
                                {{--Seconds: <span class="seconds"></span>--}}
                            </div>
                            <p class="opacity-60"><span class="orange">Регистрация закрыта</span>
                            </p>
                        </div>
                        <div class="form" id="register">
                            <input name="name" disabled required type="text" placeholder="Имя" >
                            <input name="surname" disabled required type="text" placeholder="Фамилия">
                            <input name="phone" disabled required class="phone-mask" pattern=".{18,18}" title="Введите телефон полностью" type="tel" placeholder="Телефон">
                            <input name="email" disabled type="email"  required placeholder="E-mail" >
                            <!-- при валидации желательно добавлять класс error sssssss-->
                            <input id="workshop" disabled type="checkbox">
                            <label for="workshop">Хочу на Воркшоп</label>
                            <button type="submit" disabled  class="btn btn-md"> Зарегистрироваться </button>
                            <!--
                                          после успешной отправки вызвать всплывашку $('#thankModal').modal('show');
                                          data-toggle="modal" data-target="#thankModal" с кнопки убрать
                                      -->
                        </div>



                    </div>
                    <div id="workshopBlock" class="box-registration box-dark">
                        <div class="row">
                            @foreach($workshop as $work)
                                @if(count($work->callback) >=$work->seats )
                                    <div class="col-6">
                                        <span>{{$work->title}}</span>
                                    </div>
                                    <div class="col-6 d-flex flex-column align-items-end">
                                        <div> Время:{{$work->time}} </div>
                                        <div> Все места заняты </div>
                                    </div>
                                    <div class="col-12"> <br> <hr class="w-100"> <br> </div>
                                @else
                                    <div class="col-6">
                                        <input  class="workshop{{$loop->iteration}}" name="{{$work->time}}" id="{{$work->title}}" value="{{$work->id}}" type="radio">
                                        <label for="{{$work->title}}">{{$work->title}}</label>
                                    </div>
                                    <div class="col-6 d-flex flex-column align-items-end">
                                        <div> Время: {{$work->time}} </div>
                                        <div> Места: {{count($work->callback)}}/{{$work->seats}} </div>
                                    </div>
                                    <div class="col-12"> <br> <hr class="w-100"> <br> </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="thankModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <p class="h2 js-edit">{{$data['section_7_m_h2_cl']}}</p>
            <div class="js-error">{!! $data['section_7_m_p'] !!}</div>
            <p><img src="life3.0/images/thank.png" alt="">
            </p>
            <button type="button" class="btn btn-md" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>

<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <p class="h2 js-edit">{!! $data['section_7_m_error'] !!}</p>
            <p><img src="life3.0/images/thank.png" alt="">
            </p>
            <button type="button" class="btn btn-md" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>

<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- JQuery mask for phone !-->
<script type="text/javascript" src="alevel-hub/js/jquery.maskedinput-1.3.js?ver=4.6"></script>

<script src="life3.0/js/util.js"></script>
<script src="life3.0/js/modal.js"></script>
<script src="life3.0/js/jquery.fullpage.min.js"></script>
<script src="life3.0/js/slick.js"></script>
<script src="life3.0/js/jquery.mCustomScrollbar.min.js"></script>
<script src="life3.0/js/tooltipster.bundle.min.js"></script>
<script src="life3.0/js/script.js"></script>




</body>

</html>