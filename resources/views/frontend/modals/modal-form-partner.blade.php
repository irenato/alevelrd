<div class="modal" id="modalPartner">
    <div class="modal-feedback">
        <div class="modal-feedback__wrapper">
            <form class="checkin js-form" action="{{ route('add-partner') }}" method="post">
                <div class="checkin__heading">Партнерство</div>
                <div class="checkin-reg">Наша компания
                    <input class="checkin__input checkin__input--offset" type="text" name="company" placeholder="Навание компании">, хотим предложить партнерство <span class="no-br pd-both">A-Level</span> Ukraine Свяжитесь с нами по телефону
                    <input class="checkin__input checkin__phone js-mask" type="text" name="phone" placeholder="+38(050) 000-00-00"> 
                    <br>
                    или по e-mail
                    <input class="checkin__input checkin__email" name="email" type="text" placeholder="Ваш email">
                </div>
                <div class="checkin__text js-error-area" style="display: none"></div>
                <div class="form__btn">
                    <button class="btn btn--orange btn--bigger" type="submit">Отправить заявку</button>
                </div>
                <div class="checkin__text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</div>
            </form>
        </div>
        <div class="modal-close"><i class="icon-close"></i></div>
    </div>
</div>