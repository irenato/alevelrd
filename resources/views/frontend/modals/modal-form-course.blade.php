<div class="modal" id="modalCourses">
    <div class="modal-courses">
        <div class="modal-courses__wrapper">
            <div class="modal-courses__heading">Все курсы</div>
            <div class="courses-modal">
                <div class="courses-modal__item"></div>
            </div>
        </div>
        <div class="modal-close"><i class="icon-close"></i></div>
    </div>
</div>