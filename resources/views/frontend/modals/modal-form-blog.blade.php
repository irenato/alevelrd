<div class="modal noscroll" data-noscroll="true" id="modalBlog">
    <div class="modal-feedback">
        <div class="modal-feedback__wrapper">
            <form class="checkin js-review-form" action="{{ route('blog.offer-article') }}" method="post" id="blogForm" enctype="multipart/form-data">
                <div class="checkin__heading">Прислать материалы</div>
                <div class="checkin-reg">Я,
                    <input class="checkin__input checkin__input--offset" type="text" placeholder="Фамилия и Имя"
                           name="username">, хочу опубликовать статью с фото:
                    <input type="file" name="thumbnail"><br>
                    С данным заголовок: <input class="checkin__input checkin__input--offset" type="text" placeholder="Заголовок"
                                               name="title">
                    и следующим контентом: <textarea name="content"></textarea>
                    Мой e-mail
                    <input class="checkin__input checkin__email" name="email" type="text" placeholder="Ваш email">
                </div>
                <div class="checkin__text js-error-area" style="display: none"></div>
                <div class="form__btn">
                    <button class="btn btn--orange btn--bigger" type="submit">Отправить заявку</button>
                </div>
                <div class="checkin__text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО
                    ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра.
                    Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные
                    третьим лицам.
                </div>
            </form>
        </div>
        <div class="modal-close"><i class="icon-close"></i></div>
    </div>
</div>