<div class="modal" id="modalAnswer">
    <div class="modal-feedback">
        <div class="modal-feedback__wrapper">
            <div class="checkin"><img class="img-fluid checkin__img" src="/img/scratch.png">
                <div class="checkin__heading js-title"></div>
                <p class="checkin__p js-message"></p>
                <div class="form__btn"><a class="btn btn--orange btn--bigger modal-btn" href="{{ route('home') }}">На главную</a></div>
            </div>
        </div>
        <div class="modal-close js-complete-lead"><i class="icon-close"></i></div>
    </div>
</div>