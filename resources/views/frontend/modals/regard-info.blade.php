<div class="window-popup js-regard-info">
    <div class="window-popup-left">
        <div class="window-popup__row window-popup__row--btn">
            <div class="btn btn--orange" id="hide-popup">Назад</div>
        </div>
        <a href="" data-fancybox class="image-link js-fancybox">
            <img src="" alt>
        </a>
    </div>
    <div class="window-popup-right">
        <p class="window-popup__heading js-regard-title"></p>
        <div class="window-popup__content noscroll" data-noscroll="true" id="editor-popup">
            <p class="js-regard-description"></p>
        </div>
    </div>
    <button class="window-popup-close"><i class="icon-close window-popup-close__icon"></i></button>
</div>