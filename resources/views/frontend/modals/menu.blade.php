<section class="menu">

    <div class="menu__left-blue">
        <button class="mobile-menu-close"><i class="icon-close mobile-menu-close__icon"></i></button>
        <div class="menu__content" id="mainMenu" data-noscroll="true">
            <a class="menu__logo" href="/" target="_blank"><img src="/img/logo.png" alt="A-Level"></a>
            <div class="menu__logo menu__logo--mobile"><img src="/img/logo.png" alt="A-Level"></div>
            <ul class="nav-menu">
                <li class="nav-menu__item nav-menu__item--mobile"><a class="nav-menu__link" href="/"><i
                                class="icon-monitor"></i>Главная</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="/#about"><i class="icon-city"></i>О школе</a>
                </li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="/#calendar"><i class="icon-calendar"></i>Календарь</a>
                </li>
                <li class="nav-menu__item"><a class="nav-menu__link" id="courses-btn" href="/#courses"><i
                                class="icon-arrow-right"></i>Курсы</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="/#teachers"><i class="icon-users"></i>Учителя</a>
                </li>

                <li class="nav-menu__item"><a class="nav-menu__link" href="/#partners"><i class="icon-partners"></i>Партнеры</a>
                </li>

                <li class="nav-menu__item"><a class="nav-menu__link" href="/blog"><i class="icon-blog"></i>Блог</a>
                </li>


                <li class="nav-menu__item"><a class="nav-menu__link" href="/#contacts"><i class="icon-information"></i>Контакты</a>
                </li>
            </ul>

            <a class="menu__link" href="{{ route('hub') }}" target="_blank">A-Level hub</a>
            @if($action)
                <a class="menu__link menu__link--orange" href="#stock-sell" id="stock" data-goto="5">Акция</a>
        @endif
        <!-- <a class="menu__link" href="http://a-level.com.ua/hub/" target="_blank">Выбор курса</a> -->
            <ul class="social">
                @if($city_data->social_links)
                    @foreach($city_data->social_links as $item)
                        @foreach($item as $key => $value)
                            @if($value)
                                <li class="social__item"><a class="{{ $key }}" href="{{ $value }}" target="_blank"></a>
                                </li>
                            @endif
                        @endforeach
                    @endforeach
                @endif
            </ul>
            <div class="menu__contacts menu-contacts">
                @if($city_data->email)
                    <a class="menu-contacts__item" href="mailto:{{ $city_data->email }}">
                        <i class="icon-envelope"></i>
                        {{ $city_data->email }}
                    </a>
                @endif
                <div class="menu-contacts__item">
                    <i class="icon-smartphone"></i>
                    <div class="phones">
                        @if($city_data->phones)
                            @foreach($city_data->phones as $phone)
                                <a href="tel:{{ \App\Helpers\Helpers::filterPhone($phone) }}">{{ $phone }}</a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <a class="menu-contacts__item" href="https://goo.gl/maps/SutFBzXh1iT2" target="_blank">
                    <i class="icon-location"></i>{!! $city_data->address !!}
                </a>
            </div>
        </div>
    </div>
    <div class="menu__right-blue"></div>
    <div class="menu__left-orange" id="submenu" data-noscroll="true">
        <div class="menu-inside" data-noscroll="true">
            <button class="menu-inside__hide"><i class="icon-arrow-left"></i><span>Скрыть</span></button>
            <ul class="nav-courses noscroll" data-noscroll="true" data-simplebar data-simplebar-auto-hide="false">

                @forelse($courses as $course)
                    <li class="nav-courses__item">
                        <a href="{{ route('course', ['course_alias' => $course->alias]) }}" target="_blank">
                            <i class="{{ $course->logo_class }}"></i>
                            <span>{!! $course->title !!}
                                <br> <em>{!! $course->type->visible ? $course->type->title : '' !!}</em></span>
                        </a>
                    </li>
                @empty
                @endforelse

            </ul>
            <!-- <div class="menu__row"><a class="menu__link" href="http://a-level.com.ua/hub/" target="_blank">A-Level hub</a><a class="menu__link" href="http://a-level.com.ua/hub/" target="_blank">Выбор курса</a></div> -->
        </div>
    </div>
    <div class="menu__right-orange"></div>
</section>