@extends('frontend.layouts.app')

@push('styles')
    <link rel="stylesheet" href="/css/course.min.css">

    @if($courseData->type->title == 'Повышение квалификации')
        <style>
            @media(max-width: 768px){
                .course-home__descr{
                    font-size: 22px;
                }
            }
        </style>
    @endif
@endpush

@section('title', $courseData->seo_title)

@push('seo_fields')
    <meta property="og:title" content="{{ $courseData->seo_title }}" />
    <meta name="description"
          content="{{ $courseData->seo_description }}">
    <meta name="keywords" content="{{ $courseData->seo_keywords }}">
    <meta name="robots" content="{{ $courseData->seo_robots }}">
    @if($courseData->seo_canonical)
        <link rel="canonical" href="{{ $courseData->seo_canonical }}"/>
    @endif
@endpush

@section('content')
    <body class="body course loading">
    <div class="scene-bg"
         style="background-image: url({{ \App\Helpers\Helpers::getImageCache(Storage::disk('courses')->url(  $courseData->id . '/' . $courseData->thumbnail  ), 1920, 1080) }})"></div>
    <section class="section course-home is-active" data-slide="1">
        @if($courseData->type->title == 'Повышение квалификации')
            <video id="course-home__video" class="course-home__video" loop autoplay>
                <source src="{{ Storage::disk('courses')->url(  $courseData->id . '/' . $courseData->video  ) }}"
                        type="video/mp4">

            </video>

            <div id="video-mute" class="button_voice__noisy"></div>

        @else
            <video class="course-home__video" loop autoplay muted>
                <source src="{{ Storage::disk('courses')->url(  $courseData->id . '/' . $courseData->video  ) }}"
                        type="video/mp4">

            </video>

        @endif
        <div class="blur-c"></div>


        <div id="timer" class="course-home__container" data-deadline data-daysLeft="{{ $courseData->days_left }}"
             data-discount="{{ $courseData->discount_price }}">
            <div class="course-home__course @if(mb_strlen($courseData->title) > 10) course-home__course--small @endif"
                 @if($courseData->days_left > 0) style="margin-top: 22px; padding-bottom: 0px" @endif>
                <span class="course-home__h1" data-before="Курс">{{ $courseData->title }}</span><span
                        class="course-home__descr"
                         >{{ $courseData->type->visible ? $courseData->type->title : '' }}</span>
            </div>
            @if($courseData->employment == 1)
                <p class="course-home__p">С
                    <mark>гарантированным</mark>&nbsp;трудоустройством
                </p>
            @endif

            @if( $courseData->days_left > 0)
                <div class="sale-timer">
                    <h1>До конца акции</h1>
                    <div id="clockdiv">
                        <div>
                            <span class="days"></span>
                            <div class="smalltext">Дней</div>
                        </div>
                        <div>
                            <span class="hours"></span>
                            <div class="smalltext">Часов</div>
                        </div>
                        <div>
                            <span class="minutes"></span>
                            <div class="smalltext">Минут</div>
                        </div>
                        <div>
                            <span class="seconds"></span>
                            <div class="smalltext">Секунд</div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="course-home__info info" @if($courseData->days_left > 0) style="padding: 30px" @endif >
                <ul class="info__list">
                    <li class="info__item"><i class="icon-startup info__icon"></i>
                        <div class="info__content">
                            <div class="info__name">Старт курса</div>
                            <div class="info__descr">{{ $courseData->begin }}</div>
                        </div>
                    </li>
                    <li class="info__item"><i class="icon-multiple-users info__icon"></i>
                        <div class="info__content">
                            <div class="info__name">Количество мест</div>
                            <div class="info__descr">{{ $courseData->free_places . ' свободно из ' . $courseData->amount_places }}</div>
                        </div>
                    </li>
                    <li class="info__item"><i class="icon-calendar-clock info__icon"></i>
                        <div class="info__content">
                            <div class="info__name">Длительность</div>
                            <div class="info__descr">{{ $courseData->duration }}
                            </div>
                        </div>
                    </li>
                    <li class="info__item"><i class="icon-clock info__icon"></i>
                        <div class="info__content">
                            <div class="info__name">Цена курса</div>
                            @if( $courseData->days_left > 0)
                                <div class="info__descr sale-cost">{{ $courseData->action_cost }} {{ $courseData->price_type }}</div>
                                <div class="info__descr">{{ $courseData->discount_price }} {{ $courseData->price_type }}</div>

                            @else
                                <div class="info__descr">{{ $courseData->action_cost }} {{ $courseData->price_type }}</div>

                                @if($courseData->price_type !== 'грн/курс' && (int)$courseData->duration > 0)
                                    <div class="info__descr">{{ $courseData->action_cost * (int)$courseData->duration }}
                                        грн/курс
                                    </div>
                                @endif


                            @endif
                        </div>
                    </li>
                </ul>
            </div>
            <button class="btn btn--orange btn--bigger js-open-modal-request"
                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс</a>
                <!-- <button class="course-home__start" data-page="2" data-inside="1"><i class="icon-left"></i></button> -->
        </div>
    </section>
    <section class="section course-main" data-slide="2">
        <div class="double-bg">
            <div class="double-bg__left"></div>
            <div class="double-bg__right"></div>
        </div>
        <div class="window">
            <div class="window-left"><img class="window-left__img"
                                          data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('courses')->url(  $courseData->id . '/' . $courseData->logo  ), 328, 274) }}"
                                          alt="army"></div>
            <div class="window-right">
                <div>
                    <div class="course-about" data-slide-course="1">
                        <p class="course-about__heading">{{ $courseData->title }} {{ $courseData->type->visible ? $courseData->type->title : '' }}</p>



                        <div class="content-editor editor noscroll"  data-noscroll="true"
                             data-simplebar data-simplebar-auto-hide="false">
                            <div class="content-editor__inner">
                                {!! $courseData->description !!}
                            </div>
                        </div>
                        <div class="course-about__row">
                            <button class="btn btn--orange btn--big js-open-modal-request"
                                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                            </button>
                        </div>
                    </div>
                    <div class="course-interesting" data-slide-course="2">
                        <p class="course-interesting__heading">Курс интересен для тех, <br> кто хочет...</p>
                        <div class="course-interesting__editor noscroll" data-noscroll="true">
                            {!! $courseData->advantages !!}
                        </div>
                        <div class="course-interesting__row">
                            <button class="btn btn--orange btn--big js-open-modal-request"
                                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                            </button>
                        </div>
                    </div>
                    <div class="course-program" data-slide-course="3">
                        <p class="course-program__heading">Программа курса</p>
                        <div class="course-program-content">
                            <nav class="course-program-duration">
                                <ul class="course-program-duration__list">
                                    @forelse($courseData->programs as $program)
                                        <li class="course-program-duration__item @if($loop->first) course-program-duration__item--active @endif"
                                            data-month="{{ $loop->index+1 }}">
                                            <span class="course-program-duration__month">{{ $program->period }}</span>
                                            <span class="course-program-duration__descr">{{ $program->title }}</span>
                                            <!-- <a class="course-program-duration__link" href="/testing.html" target="_blank">Пройти тест</a> -->
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                            </nav>
                            @forelse($courseData->programs as $program)
                                <div  class="editor article-course course-program-description @if($loop->first) course-program-description--active @endif noscroll"
                                     data-month="{{ $loop->index+1 }}" data-noscroll="true" style="overflow: hidden">
                                    <ul class="course-program-description__list">
                                        <div >{!! $program->introtext !!}</div>

                                    </ul>
                                </div>
                                <div class="js-details hidden" data-month="{{ $loop->index+1 }}">
                                    {!! $program->practice !!}
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="course-program__btns">
                            <button class="btn btn--orange btn--big js-open-modal-request"
                                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                            </button>
                            <button class="btn btn--blue btn--big" id="showPopupProgram">Вся теория за&nbsp;<span
                                        class="month">1</span>&nbsp;мес.
                            </button>
                        </div>
                        <div class="course-program-popup">
                            <button class="course-program-popup__btn"><i class="icon-close"></i></button>
                            <!--  сюда выведем детали первого или выбранного периода -->
                            <div class="course-program-popup__heading js-change-month">Изучаемая теория за&nbsp;<span
                                        class="month">1</span>&nbsp;мес
                            </div>
                            <div class="course-program-popup__container editor article-course noscroll"
                                 data-noscroll="true">

                            </div>

                            <div class="course-program-popup__btns">
                                <div class="btn btn--blue btn--big course-program-popup__btn">Назад</div>
                                <button class="btn btn--orange btn--big js-open-modal-request"
                                        onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                                </button>
                                <!-- <a class="btn btn--blue btn--big js-change-month" href="/testing.html" target="_blank">Пройти тест за&nbsp;<span class="month">1</span>&nbsp;месяц</a> -->
                            </div>
                            <!--  сюда выведем детали первого или выбранного периода (конец) -->
                        </div>
                    </div>
                    <div class="course-place" data-slide-course="4">
                        <ul class="course-place__details details">
                            <li class="details__item"><i class="icon-startup details__icon"></i>
                                <div class="details__content">
                                    <div class="details__name">Старт курса</div>
                                    <div class="details__descr">{!! $courseData->begin !!}</div>
                                </div>
                            </li>
                            <li class="details__item"><i class="icon-calendar-clock details__icon"></i>
                                <div class="details__content">
                                    <div class="details__name">Длительность</div>
                                    <div class="details__descr">
                                        {!! $courseData->duration !!}
                                    </div>
                                </div>
                            </li>
                            <li class="details__item"><i class="icon-multiple-users details__icon"></i>
                                <div class="details__content">
                                    <div class="details__name">Количество мест</div>
                                    <div class="details__descr">{!! $courseData->free_places . ' свободно из ' . $courseData->amount_places !!}</div>
                                </div>
                            </li>
                            <li class="details__item"><i class="icon-clock details__icon"></i>
                                <div class="details__content">
                                    <div class="details__name">Занятия</div>
                                    <div class="details__descr details__descr--margin">{!! $courseData->schedule !!}</div>
                                </div>
                            </li>
                        </ul>
                        <div class="auditory course-place__auditory">
                            <div class="auditory__projector"></div>
                            <div class="auditory__place"><i class="icon-user"></i></div>
                            <div class="auditory__places">
                                <ul class="auditory__list">
                                    <li class="auditory__item auditory__item--darken"><i class="icon-user"></i><i
                                                class="icon-user"></i></li>
                                    <li class="auditory__item auditory__item--lighten"><i class="icon-user"></i><i
                                                class="icon-user"></i></li>
                                    <li class="auditory__item auditory__item--lighten"><i class="icon-user"></i><i
                                                class="icon-user icon--darken"></i>
                                        <div class="auditory-popup">
                                            <p>Упс. <br> Это место уже занято</p>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="auditory__list">
                                    <li class="auditory__item auditory__item--darken"><i class="icon-user"></i><i
                                                class="icon-user"></i>
                                        <div class="auditory-popup">
                                            <p>Возможно это твое место :)</p>
                                            <button class="btn btn--orange btn--small js-open-modal-request">Занять
                                            </button>
                                        </div>
                                    </li>
                                    <li class="auditory__item auditory__item--darken"><i
                                                class="icon-user icon--darken"></i><i
                                                class="icon-user icon--darken"></i></li>
                                    <li class="auditory__item auditory__item--lighten"><i class="icon-user"></i><i
                                                class="icon-user"></i></li>
                                </ul>
                                <ul class="auditory__list">
                                    <li class="auditory__item auditory__item--lighten"><i class="icon-user"></i><i
                                                class="icon-user icon--darken"></i></li>
                                    <li class="auditory__item auditory__item--lighten"><i
                                                class="icon-user icon--darken"></i><i class="icon-user"></i></li>
                                    <li class="auditory__item auditory__item--darken"><i
                                                class="icon-user icon--darken"></i><i
                                                class="icon-user icon--darken"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="course-price" data-slide-course="5">
                        <p class="course-price__heading">Стоимость</p>
                        <div class="course-price__container">
                            <div class="course-price__left">

                                <div class="course-price__wrap" @if($courseData->days_left > 0 and $courseData->price_type !== 'грн/курс') style="display: flex; flex-direction: column" @endif>
                                    @if($courseData->cost !== $courseData->action_cost)
                                        <span class="course-price__old">{!! $courseData->cost !!} {{ $courseData->price_type }}</span>
                                        <br>
                                    @endif

                                    @if($courseData->days_left > 0)
                                        <span class="course-price__old">{!! $courseData->action_cost !!} {{ $courseData->price_type }}</span>
                                        <br>
                                        <span class="course-price__big course-price__big--lightblue"><strong>{!! $courseData->discount_price !!}
                                                &nbsp</strong>{{ $courseData->price_type }}</span><br>
                                        @if($courseData->price_type !== 'грн/курс')

                                            <span class="course-price__big course-price__big--lightblue"><strong>{{ $courseData->discount_price * (int)$courseData->duration }}&nbsp</strong> грн/курс</span>
                                        @endif
                                    @else
                                        <span class="course-price__big course-price__big--lightblue"><strong>{!! $courseData->action_cost !!}
                                                &nbsp</strong>{{ $courseData->price_type }}</span><br>
                                        @if($courseData->price_type !== 'грн/курс')

                                            <span class="course-price__big course-price__big--lightblue"><strong>{{ $courseData->action_cost * (int)$courseData->duration }}</strong> грн/курс</span>
                                        @endif
                                    @endif
                                </div>
                                @if($action)
                                    <div class="course-price__wrap">
                                        <div class="sale-course" id="clockdiv" data-date="{{ $action->end_date }}">
                                            <div class="js-timer-days sale-course__number"></div>
                                            <span class="sale-course__text">&nbsp;д.&nbsp;</span>
                                            <div class="js-timer-hours sale-course__number"></div>
                                            <span class="sale-course__text">&nbsp;ч.&nbsp;</span>
                                            <div class="js-timer-minutes sale-course__number"></div>
                                            <span class="sale-course__text">&nbsp;мин.&nbsp;</span>
                                            <div class="js-timer-seconds sale-course__number"></div>
                                            <span class="sale-course__text">&nbsp;сек.&nbsp;</span>
                                        </div>
                                        <span class="course-price__small">до окончания акции</span>
                                    </div>
                                @endif
                                <div class="course-price__wrap">
                                <span class="course-price__big course-price__big--darkblue">
                                    <strong>{!! $courseData->amount_places !!}&nbsp</strong>
                                    человек
                                </span>
                                    <span class="course-price__small">в группе</span></div>
                                <div class="course-price__wrap">
                                <span class="course-price__big course-price__big--orange">
                                    Осталось мест:
                                    <strong>&nbsp{!! $courseData->free_places !!}&nbsp</strong>
                                </span>
                                    <span class="course-price__small">до начала курса!</span>
                                </div>
                            </div>
                            <div class="course-price__right">
                                <div class="course-price__h2">Стать нашим студентом очень просто:</div>
                                <ul class="course-price__list">
                                    <li><i class="icon-telephone"></i>Оставить заявку или позвонить</li>
                                    <li><i class="icon-businessman-ascending-by-stair-steps"></i><strong>Прийти с
                                            паспортом и оплатой</strong></li>
                                    <li><i class="icon-contract"></i>Заполнить анкету и договор</li>
                                    <li><i class="icon-ribbon-badge-award"></i><strong>Поздравляем, ты студент курса
                                            «{{ $courseData->title }}»</strong></li>
                                </ul>
                            </div>
                        </div>
                        <div class="course-price__btns">
                            <button class="btn btn--orange btn--big js-open-modal-request"
                                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                            </button>
                        </div>
                    </div>
                    <div class="course-teachers" data-slide-course="6">
                        <div class="window-info-list__block">

                            <div class="window-info__controls noscroll" data-noscroll="true"><p
                                        class=" course-teachers__heading ">Преподаватели курса</p></div>

                            <div class="content-editor__inner" data-simplebar data-simplebar-auto-hide="false">

                                <ul class="course-teachers__list window-info__list">
                                    @forelse ($courseData->courses_teachers as $teacher)
                                        <li class="course-teachers__item" data-noscroll="true">
                                            <img class="course-teachers__img"
                                                 data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('teachers')->url(  $teacher->id . '/' . $teacher->thumbnail  ), 70, 70) }}"
                                                 alt="{{ $teacher->first_name . ' ' . $teacher->last_name }}"
                                                 data-noscroll="true">
                                            <div class="course-teachers__content" data-noscroll="true">
                                                <p class="course-teachers__h3"
                                                   data-noscroll="true">{{ $teacher->first_name . ' ' . $teacher->last_name }}</p>
                                                <div class="course-teachers__description course-teachers__details"
                                                     data-noscroll="true">{!! $teacher->description !!}</div>
                                                <p class="course-teachers__description"
                                                   data-noscroll="true">{{ $teacher->specialization }}</p>
                                            </div>
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>

                            </div>

                            <div class="course-teachers__btns">
                                <btn class="btn btn--big btn--orange js-open-modal-request"
                                     onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                                </btn>
                            </div>
                        </div>



                    </div>
                    <div class="reviews" data-slide-course="7">
                        <div class="reviews__heading">Отзывы о курсе</div>
                        <div class="reviews__tab">
                            <ul class="tabs">
                                <li class="active" rel="tab1">От студентов</li>
                                <li rel="tab2">От компаний</li>
                            </ul>
                            <div class="tab__wrapper">
                                <div class="tab_container">
                                    <p class="d_active tab_drawer_heading" rel="tab1">От студентов</p>
                                    <div class="tab_content noscroll" data-noscroll="true" id="tab1">
                                        <div class="reviews-slider owl-carousel">
                                            @forelse($courseData->students_reviews as $review)
                                                <div class="reviews-slider__item">
                                                    <div class="mention">
                                                        <div class="mention__img"><img
                                                                    data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('reviews')->url(  $review->id . '/' . $review->thumbnail  ), 64, 64) }}"
                                                                    alt="{!! $review->username !!}"></div>
                                                        <div class="mention__content">
                                                            <div class="mention__top">
                                                                <div class="mention__name">{{ $review->username }}</div>
                                                                <div class="mention__course">{{ $courseData->title }}</div>
                                                            </div>
                                                            <div class="mention__description">
                                                                {!! $review->review !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="reviews-slider__item">
                                                    <div class="mention">
                                                        <div class="mention__img"><i class="icon-speech-chat"></i></div>
                                                        <div class="mention__content">
                                                            <div class="mention__top">
                                                                <div class="mention__name"></div>
                                                                <div class="mention__course">Отзывов пока нет</div>
                                                            </div>
                                                            <div class="mention__description">
                                                                Твой отзыв может быть первым
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                    <!-- #tab1-->
                                    <p class="tab_drawer_heading" rel="tab2">От компаний</p>
                                    <div class="tab_content noscroll" data-noscroll="true" id="tab2">
                                        <div class="reviews-slider owl-carousel">

                                            @forelse($courseData->companies_reviews as $review)
                                                <div class="reviews-slider__item mention">
                                                    <div class="mention__img"><img
                                                                data-src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('reviews')->url(  $review->id . '/' . $review->thumbnail  ), 64, 64) }}"
                                                                alt="{!! $review->username !!}"></div>
                                                    <div class="mention__content">
                                                        <div class="mention__top">
                                                            <div class="mention__name">{{ $review->username }}</div>
                                                            <div class="mention__course">{{ $courseData->title }}</div>
                                                        </div>
                                                        <div class="mention__description">
                                                            {!! $review->review !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="reviews-slider__item">
                                                    <div class="mention">
                                                        <div class="mention__img"><i class="icon-speech-chat"></i></div>
                                                        <div class="mention__content">
                                                            <div class="mention__top">
                                                                <div class="mention__name"></div>
                                                                <div class="mention__course">Отзывов пока нет</div>
                                                            </div>
                                                            <div class="mention__description">
                                                                Твой отзыв может быть первым
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforelse

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- / tabs-->
                        </div>
                        <div class="reviews__btns">
                            <button class="btn btn--orange btn--big js-open-modal-request"
                                    onClick="fbq('track', 'InitiateCheckout');">Записаться на курс
                            </button>
                            <button class="btn btn--blue btn--big" id="feedback">Оставить отзыв</button>
                        </div>
                        <div class="course-reviews-popup">
                            <button class="course-reviews-popup__btn"><i class="icon-close"></i></button>
                            <p class="reviews__heading">Ваш отзыв</p>
                            @include('frontend.course.form-review')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="section checkin-section" data-slide="3">
        <div class="checkin">
            <div class="checkin-content">
                @include('frontend.course.form-feedback')
            </div>
        </div>
    </section>

    @endsection

    @push('page-menu')
        @include('frontend.course.page-menu')
    @endpush

    @push('scripts')
        <script defer src="/js/commons.min.js"></script>
        <script defer src="/js/course.min.js"></script>
        <script>
            window.onload = function () {
                [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
                    img.setAttribute('src', img.getAttribute('data-src'));
                    img.onload = function () {
                        img.removeAttribute('data-src');
                    };
                });
            };

        </script>
    @endpush
