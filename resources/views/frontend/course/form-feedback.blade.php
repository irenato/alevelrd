<form class="checkin js-form" action="{{ route('add-feedback') }}" method="post" id="courseForm">
    <div class="checkin__heading">Записаться на курс</div>
    <div class="checkin-reg">Я,
        <input class="checkin__input checkin__input--offset" type="text" placeholder="Фамилия и Имя" name="username">, хочу записаться в&nbsp;<span class="no-br">A-Level</span>&nbsp;Ukraine на курс
        <select class="checkin__select" id="choose-course" name="course_id">
            @forelse($courses as $item)
                <option value="{{ $item->id }}" @if($item->id == $courseData->id) selected="selected" @endif>{{ $item->title }}</option>
            @empty
            @endforelse

        </select>
        <!--Мой город -
        <select class="checkin__select" id="choose-city" name="city_id">
            @forelse($cities as $city)
                <option value="{{ $city->id }}"  @if($city->id == session('city')) selected="selected" @endif>{!! $city->title !!}</option>
            @empty
            @endforelse
        </select>-->
        Свяжитесь со мной по телефону
        <input class="checkin__input checkin__phone js-mask" type="text" name="phone" placeholder="+38(050) 000-00-00"> 
        <br>
        или по e-mail
        <input class="checkin__input checkin__email" name="email" type="text" placeholder="Ваш email">
    </div>
    <div class="checkin__text js-error-area" style="display: none"></div>
    <div class="form__btn">
        <button class="btn btn--orange btn--bigger" type="submit">Отправить заявку</button>
    </div>
    <div class="checkin__text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для
        просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.
    </div>
</form>