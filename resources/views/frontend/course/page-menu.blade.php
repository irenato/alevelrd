<div class="triggers">
    <!-- <button class="trigger trigger--lightblue" data-page="1"><span class="trigger__icon"><i class="icon-city"></i></span><span class="trigger__text"><span>Главная</span></span></button> -->
    <button class="trigger trigger--lightblue" data-page="2" data-inside="1"><span class="trigger__icon"><i class="icon-register"></i></span><span class="trigger__text"><span>Описание</span></span></button>
    <button class="trigger trigger--orange" data-page="2" data-inside="2"><span class="trigger__icon"><i class="icon-document"></i></span><span class="trigger__text"> Информация</span></button>
    <button class="trigger trigger--darkblue" data-page="2" data-inside="3"><span class="trigger__icon"><i class="icon-copy"></i></span><span class="trigger__text"><span>Программа</span></span></button>
    <button class="trigger trigger--orange" data-page="2" data-inside="4"><span class="trigger__icon"><i class="icon-users"></i></span><span class="trigger__text"><span>Места</span></span></button>
    <button class="trigger trigger--orange trigger--sale" data-page="2" data-inside="5"><span class="trigger__icon"><i class="icon-wallet"></i></span><span class="trigger__text"><span>Стоимость</span></span></button>
    <button class="trigger trigger--lightblue" data-page="2" data-inside="6"><span class="trigger__icon"><i class="icon-user-profile"></i></span><span class="trigger__text"><span>Учителя</span></span></button>
    <button class="trigger trigger--orange" data-page="2" data-inside="7"><span class="trigger__icon"><i class="icon-speech-chat"></i></span><span class="trigger__text"><span>Отзывы</span></span></button>
    <button class="trigger trigger--lightblue" data-page="3"><span class="trigger__icon"><i class="icon-man-with-computer"></i></span><span class="trigger__text"><span>Записаться</span></span></button>
</div>