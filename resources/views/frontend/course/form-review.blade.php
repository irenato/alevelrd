<form class="reviews__form js-review-form js-form" action="{{ route('add-review') }}" method="post" enctype="multipart/form-data">
    <div class="reviews__top">
        <div class="reviews__photo choose-photo">
            <input class="choose-photo__input" type="file" id="choose-photo" name="thumbnail" accept="image/x-png,image/jpg,image/jpeg" data-max-size="3145728">
            <label class="choose-photo__label" for="choose-photo" title="Допустимые форматы изображений: png, jpg, jpeg">
                <i class="icon-photo-camera"></i>
            </label>
            <img class="choose-photo__img" src="" alt="test">
        </div>
        <div class="reviews__who">
            <input class="reviews__name" type="text" name="username" placeholder="Фамилия и Имя">
            <input class="reviews__email" type="text" name="email" placeholder="E-mail">
            <select class="reviews__select" name="from_student">
                <option value="1">студент</option>
                <option value="0">предствитель компании</option>
            </select>
            <select class="reviews__select" name="course_id">
                @forelse($courses as $item)
                    <option value="{{ $item->id }}" @if($item->id == $courseData->id) selected="selected" @endif>{{ $item->title }}</option>
                @empty
                @endforelse
            </select>
        </div>
    </div>
    <div class="checkin__text js-error-area" style="display: none"></div>
    <div class="reviews__bottom text-wrap">
        <textarea class="reviews__textarea js-symbols-left" name="review" placeholder="Расскажите, например, что Вам понравилось больше всего, о качестве преподавания, адекватности цен и удобстве расположения курсов..."></textarea>
        <span class="remaining-symbols js-remainingSymbol"><em>0</em>/1000</span>
        <p>*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не
            будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
        <button class="btn btn--blue" type="submit">Оставить отзыв</button>
    </div>
</form>