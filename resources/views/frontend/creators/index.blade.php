@extends('frontend.layouts.app-creators')

@section('title', $data->seo_title)

@push('seo_fields')
<meta name="description"
      content="{{ $data->seo_description }}">
<meta name="keywords" content="{{ $data->seo_keywords }}">
<meta name="robots" content="{{ $data->seo_robots }}">
@if($data->seo_canonical)
    <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
@endif
@endpush

@section('content')
    <main class="col p-0 students-main" data-route="{{ route('get-team') }}">
        <div class="row col w-100 m-0 p-0">
            <div class="col-lg-6 d-flex flex-column align-items-center justify-content center p-0 pl-lg-0 pr-lg-10 students-main__left">
                <p class="text-center regular display-18 mb-60 mt-60">{{ $data->seo_title }}</p>
                <p class="text-center regular display-30 students-main__description mb-40">{!! $data->description !!}</p>
                <a class="btn text-dark mb-25 students-main__btn" href="#1" data-about="1"></a>
                <a class="btn text-dark mb-25 students-main__btn" href="#2" data-about="2"></a>
                <a class="btn text-dark mb-25 students-main__btn" href="#3" data-about="3"></a>
                <a class="btn text-dark mb-25 students-main__btn" href="#4" data-about="4"></a>
                <a class="btn text-dark mb-25 students-main__btn" href="#5" data-about="5"></a>
                <a class="btn text-dark mb-25 students-main__btn" href="#6" data-about="6"></a>
            </div>
            <div class="col-lg-6 students-grid p-0 pr-lg-0 pl-lg-10">
                <a class="students-grid__item students-grid__item--a bg-cover display-36" data-about="1" href="#1"></a>
                <a class="students-grid__item students-grid__item--b bg-cover display-36" data-about="2" href="#2"></a>
                <a class="students-grid__item students-grid__item--c bg-cover display-36" data-about="3" href="#3"></a>
                <a class="students-grid__item students-grid__item--d bg-cover display-36" data-about="4" href="#4"></a>
                <a class="students-grid__item students-grid__item--e bg-cover display-36" data-about="5" href="#5"></a>
                <a class="students-grid__item students-grid__item--f bg-cover display-36" data-about="6" href="#6"></a>
            </div>
        </div>
    </main>
    <div class="students-detail col p-0">
        <div class="row col p-0 m-0">
            <div class="col-md-6 js-photo bg-cover students-detail__photo">
                <div class="students-detail__btns">
                    <a class="students-detail__btn students-detail__prev" href=""><i class="icon-arrow-left text-white display-24"></i></a>
                    <a class="students-detail__btn students-detail__next" href=""><i class="icon-arrow-right text-white display-24"></i></a>
                    <a class="students-detail__btn students-detail__close" href="#"><i class="icon-close text-white display-20"></i></a>
                </div>
            </div>
            <div class="col-md-6 p-0 m-0 d-flex flex-column">
                <div class="students-detail__header flex-wrap">
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="1" href="#1"></a>
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="2" href="#2"></a>
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="3" href="#3"></a>
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="4" href="#4"></a>
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="5" href="#5"></a>
                    <a class="students-detail__item bg-cover rounded-circle my-15 mx-10" data-about="6" href="#6"></a>
                </div>
                <div class="students-detail__main d-flex flex-column align-items-center m-auto">
                    <div class="students-detail__name regular display-48 text-center mb-55 mt-40 mt-md-0"></div>
                    <div class="students-detail__role regular display-24 text-center mb-70"></div>
                    <div class="students-detail__description regular display-24 text-center mb-35"></div>
                    <div class="students-detail__socials"></div>
                </div>
            </div>
        </div>
    </div>
@endsection