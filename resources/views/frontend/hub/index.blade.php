@extends('frontend.layouts.hub_app')

@section('content')
<body class="loading">
<div class="new_preloader">
    <img src="alevel-hub/images/preloader/logo-3-2.png" alt="#">
    <img src="alevel-hub/images/preloader/gears.svg" alt="Preloader image">
</div>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-2">
                <a href="/hub"  class="logo">
                    <img src="alevel-hub/images/logo.png" class="img-responsive" alt="A-level hub logo" />
                </a>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-7">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul id="navbar" class="navbar-collapse collapse nav navbar-nav header-menu">
                    <li><a href="#advantages">о нас</a></li>
                    <li><a href="#gallery">галерея</a></li>
                    <li><a href="#locations">стоимость</a></li>
                    <li><a href="#other-questions">контакты</a></li>
                </ul>
                <div class="header-phone">
                    <span class="hubicon-smartphone"></span><a href="tel:{{$data['phone']}}" class="btn-soft text-phone">{{$data['phone']}}</a>
                </div>
            </div>
            <div class="hidden-xs col-sm-12 col-md-3">
                <div class="header-contacts mobile-hidden">
                    <button id="invite-btn" class="fill" data-toggle="modal" data-target="#sendRequest">Записаться на экскурсию</button>
                </div>
            </div>
        </div>
    </div>
</header>
<section id="introduction">
    <div class="flexbox-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="introduction-information">
                    <span class="title">{!! $data['section_introduction_title'] !!}</span>
                    <span class="description">{!! $data['section_introduction_description'] !!}</span>
                </div>
                <div class="button-list">
                    <button id="request-tour" data-toggle="modal" data-target="#sendRequest">Записаться на экскурсию</button>
                    <button id="gallery-hub-button" data-scroll="#gallery">Галерея</button>
                </div>
            </div>
        </div>
    </div>
    <div class="mouse-container">
        <span class="hubicon-mouse"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
    </div>
</section>
<section id="class-rooms">
    <div class="container">
        <div class="row">
            <div class="information">
                <span class="title">{!!$data['section_class_rooms_title']!!}</span>
                <span class="description">{!!$data['section_class_rooms_description']!!}</span>
            </div>
        </div>
        <div class="row">
            @Foreach($rooms as $room)
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="room-container">
                    <div class="room-image"><img data-src="{{$room['room_image']}}" class="img-responsive" alt="{{$room['room-name']}}" /></div>
                    <span class="room-name">{{$room['room_name']}}</span>
                    <div class="room-capacity">
                        <span class="capacity-name">Вместимость:</span>
                        <span class="capacity-text">{{$room['capacity_text']}}</span>
                    </div>
                    <div class="room-more">
                        <button class="room-more-button" data-location="#hall" data-scroll="#locations">Подробнее</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section id="advantages">
    <div class="container">
        <div class="row">
            <div class="information">
                <span class="title">Наши преимущества</span>
                <span class="description">Все эти помещения включают в себя удивительные преимущества.</span>
            </div>
        </div>
        <div class="row">
            <div class="advantages-container">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-projector"></span>
                        <p>Широкоформатный проектор</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-screen"></span>
                        <p>Экран</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-wifi"></span>
                        <p>WiFi</p>
                    </div>
                </div>
                <div class="sm-clearfix"></div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-projector_screen"></span>
                        <p>Флипчарт</p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-light_bulb"></span>
                        <p>Хорошее освещение</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-paint_roller"></span>
                        <p>Новый ремонт, кондиционеры</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-cofee_maker"></span>
                        <p>Кофе-брейк</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="advantage">
                        <span class="hubicon-baking"></span>
                        <p>Печеньки :)</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>

<section id="locations">
    <div class="container">
        <div class="row">
            <div class="information">
                <span class="title">Локации</span>
            </div>
        </div>
        <div class="row">
            <ul class="tab-buttons nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#allroom">Все</a></li>
                @foreach($rooms as $room )
                <li><a data-toggle="pill" href="#{{$room['room_key']}}">{{$room['room_name']}}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="allroom" class="tab-panel fade in active">
                    <div class="location-container">
                        <div class="col-xs-12 col-md-8">
                            <div class="map">
                                <img class="img-responsive" data-src="alevel-hub/images/map/all.png" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="location-information">
                                <ul>
                                    {!!$all_room_info['room_information']!!}
                                </ul>
                                <button id="order-location-allroom" data-toggle="modal" data-target="#sendRequest">Заказать</button>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($rooms as $room)
                <div id="{{$room['room_key']}}" class="tab-panel fade">
                    <div class="location-container">
                        <div class="col-xs-12 col-md-8">
                            <div class="map">
                                <img class="img-responsive" data-src="{{$room['room_image_location']}}" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="location-information">
                                {!! $room['room_information'] !!}
                                <button id="order-location-{!!$room['room_key']!!}" data-toggle="modal" data-target="#sendRequest">Заказать</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{--<div id="greyroom" class="tab-panel fade">--}}
                    {{--<div class="location-container">--}}
                        {{--<div class="col-xs-12 col-md-8">--}}
                            {{--<div class="map">--}}
                                {{--<img class="img-responsive" src="alevel-hub/images/map/grey.png" />--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-md-4">--}}
                            {{--<div class="location-information">--}}
                                {{--<ul>--}}
                                    {{--<li>Проводите конференции и семинары</li>--}}
                                    {{--<li>Большая стена с проектором для презентаций</li>--}}
                                    {{--<li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>--}}
                                    {{--<li>Удобное расположение пространства</li>--}}
                                    {{--<li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>--}}
                                    {{--<li>Стоимость: 250 грн/час</li>--}}
                                {{--</ul>--}}
                                {{--<button id="order-location-greyroom" data-toggle="modal" data-target="#sendRequest">Заказать</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div id="blueroom" class="tab-panel fade">--}}
                    {{--<div class="location-container">--}}
                        {{--<div class="col-xs-12 col-md-8">--}}
                            {{--<div class="map">--}}
                                {{--<img class="img-responsive" src="alevel-hub/images/map/blue.png" />--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-md-4">--}}
                            {{--<div class="location-information">--}}
                                {{--<ul>--}}
                                    {{--<li>Проводите конференции и семинары</li>--}}
                                    {{--<li>Большая стена с проектором для презентаций</li>--}}
                                    {{--<li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>--}}
                                    {{--<li>Удобное расположение пространства</li>--}}
                                    {{--<li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>--}}
                                    {{--<li>Стоимость: 250 грн/час</li>--}}
                                {{--</ul>--}}
                                {{--<button id="order-location-blueroom" data-toggle="modal" data-target="#sendRequest">Заказать</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div id="redroom" class="tab-panel fade">--}}
                    {{--<div class="location-container">--}}
                        {{--<div class="col-xs-12 col-md-8">--}}
                            {{--<div class="map">--}}
                                {{--<img class="img-responsive" src="alevel-hub/images/map/red.png" />--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-md-4">--}}
                            {{--<div class="location-information">--}}
                                {{--<ul>--}}
                                    {{--<li>Проводите конференции и семинары</li>--}}
                                    {{--<li>Большая стена с проектором для презентаций</li>--}}
                                    {{--<li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>--}}
                                    {{--<li>Удобное расположение пространства</li>--}}
                                    {{--<li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>--}}
                                    {{--<li>Стоимость: 250 грн/час</li>--}}
                                {{--</ul>--}}
                                {{--<button id="order-location-redroom" data-toggle="modal" data-target="#sendRequest">Заказать</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</section>
<section id="get-info">
    <div class="container">
        <div class="row">
            <div class="flexbox-container">
                <div class="information">
                    <span class="title">{!! $data['section_get_info_title'] !!}</span>
                    <button id="get-request" data-toggle="modal" data-target="#sendRequest">Отправить заявку</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="gallery">
    <div class="container">
        <div class="row">
            <div class="information">
                <span class="title">Галерея</span>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-theme">
                @foreach($images as $img)
                <div class="item">
                    {{--<img class="img-responsive" src="{{$img['image']}}" />--}}
                    <img class="img-responsive" data-src="{{($img['image'])}}" />


                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<section id="about-us">
    <div class="container">
        <div class="row">
            <div class="content">
                {!! $data['section_about_us_content'] !!}
            </div>
            <div class="button-container"><button id="show-more-text">Показать больше</button></div>
        </div>
    </div>
</section>
<section id="other-questions">
    <div class="container">
        <div class="row">
            <div class="information">
                <span class="title">Остались вопросы?</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-push-2 col-sm-8">
                <form role="form" id="contactForm" data-toggle="validator" class="shake">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input name="name" type="text" class="inputText form-control" id="name" required="">
                            <span class="floating-label">Ваше имя</span>
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="email" name="email" class="inputText form-control checkin__email" id="email" required="">
                            <span class="floating-label">Ваш Email</span>
                        </div>
                        <div class="form-group col-sm-12 textarea">
                            <textarea id="message" name="text" class="inputText form-control" rows="5" required=""></textarea>
                            <span class="floating-label">Вопрос</span>
                        </div>
                        <div class="col-xs-12 text-center">
                            <button type="submit" id="questions-form-submit">Отправить</button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer id="contacts">
    <div class="container">
        <div class="row">
            <div class="contact-information">
                <div class="col-xs-12 col-sm-4 col-md-push-2 col-md-3 information-block">
                    <span class="hubicon-envelope-1"></span>
                    <a href="mailto:{!! $data['mail'] !!}" class="information-text">{!! $data['mail'] !!}</a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-push-2 col-md-3 information-block">
                    <span class="hubicon-smartphone"></span>
                    <a href="tel:{!! $data['phone'] !!}" class="information-text">{!! $data['phone'] !!}</a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-push-2 col-md-3 information-block information-block-location">
                    <span class="hubicon-location-point"></span>
                    <span class="information-text">
             {{$data['address']}}
              </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="footer-logo">
                    <a href="/hub" class="footer-logo-link">
                        <img data-src="alevel-hub/images/logo.png" class="img-responsive" alt="A-level hub logo" />
                    </a>
                </div>
                <div class="footer-menu">
                    <ul class="footer-menu-list">
                        <li><a href="#advantages">О нас</a></li>
                        <li><a href="#gallery">Галерея</a></li>
                        <li><a href="#locations">Стоимость</a></li>
                        <li><a href="#other-questions">Контакты</a></li>
                    </ul>
                </div>
                <div class="footer-copyright">
                    <span>©2018 A-Level HUB все права защищены</span>
                </div>
            </div>
            <div class="col-xs-12 col-xs-sm-12 col-md-8">
                <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2565.2756555944634!2d36.228599715842286!3d49.98744117941416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a0f6bbf24153%3A0xe835d1b1176f4218!2z0L_Quy4g0J_QsNCy0LvQvtCy0YHQutCw0Y8sIDYsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCDQo9C60YDQsNC40L3QsCwgNjEwMDA!5e0!3m2!1sru!2sru!4v1531855716236" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="sendRequest" tabindex="-1" role="dialog">
    <div class="modal-dialog cascading-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <img data-src="alevel-hub/images/logo-black.png" class="img-responsive" alt="A-level hub logo" />
                <span class="modal-title">Оставить заявку</span>
            </div>

            <form role="form" id="modalRequestForm" data-toggle="validator" class="shake">
{{--                {{ Form::open(['url' => route('send_message'), 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'modalRequestForm']) }}--}}

                <div class="form-group col-sm-12">
{{--                    {{  Form::text('name', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения'])}}--}}

                    <input name="name" type="text" class="inputText form-control" id="name" required="">
                    <span class="floating-label">Ваше имя</span>
                </div>
                <div class="form-group col-sm-12">
                    <input type="tel" name="tel" class="inputText phone-mask form-control" id="phone" required="">
{{--                    {{  Form::text('tel', null, ['class' => 'phone-mask,inputText', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения'])}}--}}

                    <span class="floating-label">Ваш телефон</span>
                </div>
                <div class="col-xs-12 text-center">
                    <button type="submit" id="modal-form-submit">Отправить заявку</button>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
        </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
<div class="modal fade" id="success-call" tabindex="-1" role="dialog">
    <div class="modal-dialog cascading-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <img data-src="alevel-hub/images/logo-black.png" class="img-responsive" alt="A-level hub logo" />
                <span class="modal-title">Заявка отправлена!</span>
            </div>

            <div class="modal-message">
                <p>Ваша заявка успешно отправлена!</p>
                <p>Вы всегда можете позвонить нам по телефону: <span class="hubicon-smartphone"></span><a href="tel:{{$data['phone']}}" class="modal-phone">{{$data['phone']}}</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="bad-call" tabindex="-1" role="dialog">
    <div class="modal-dialog cascading-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <img data-src="alevel-hub/images/logo-black.png" class="img-responsive" alt="A-level hub logo" />
                <span class="modal-title">Что-то пошло не так!</span>
            </div>

            <div class="modal-message">
                <p>Мы не смогли принять Вашу заявку!</p>
                <p>Свяжитесь с нами по телефону: <span class="hubicon-smartphone"></span><a href="tel:{{$data['phone']}}" class="modal-phone">{{$data['phone']}}</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="alevel-hub/js/bootstrap.min.js"></script>
<!-- Callback for form include !-->
<script src="alevel-hub/js/callback.js"></script>
<!-- Owl !-->
<script src="alevel-hub/js/owl.carousel.min.js"></script>
<!-- JQuery mask for phone !-->
<script type="text/javascript" src="alevel-hub/js/jquery.maskedinput-1.3.js?ver=4.6"></script>
<!-- Common js !-->
<script src="alevel-hub/js/common.js"></script>
<!-- Callback ajax sender !-->
<script src="alevel-hub/js/callback.js"></script>

<script>
    window.onload = function() {
        [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
            img.setAttribute('src', img.getAttribute('data-src'));
            img.onload = function () {
                img.removeAttribute('data-src');
            };
        });                };

</script>
</body>
</html>
@endsection