<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    @stack('seo_fields')
    <meta property="og:image" content="/img/icons/logo.png" />
    <title>@yield('title')</title>
    @stack('styles')
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
    {!! $gtm !!}
    {!! $fb_pixels !!}
</head>
<body>
<noscript>
    <div><strong>Для полной функциональности этого сайта необходимо включить JavaScript. Вот<a href="#" target="_blank">инструкции,</a></strong>как включить JavaScript в вашем браузере.
        <link rel="stylesheet" type="text/css" href="css/without-js.css">
    </div>
</noscript>
<!--if lte IE 9
p.off-js
  | Ваш браузер устарел, пожалуйста
  b обновите
  |  его.
-->
<!-- wrapper start-->
<div class="wrapper">
    <!-- header SECTION-->
    <header class="header">
        <div class="container">
            <div class="header__left">
                <div class="burger__wrapper">
                    <div id="menu" class="burger burger__black">
                        <ul class="burger__list">
                            <li class="burger__item"></li>
                            <li class="burger__item"></li>
                            <li class="burger__item"></li>
                        </ul>
                        <span class="burger__text">Menu</span>
                    </div>
                </div>
            </div>
            <div class="header__center flex-wrap">
                <div class="header__logo"><a href="{{ route('home') }}"><img src="/images/logo.png"
                                               alt="A-Level"></a></div>
                <div class="header__back"><a href="{{ route('home') }}" class="header__back-link"><i class="icon icon-left-arrow"></i>Вернуться на главную</a></div>
            </div>
            <div class="header__right">
                <div class="navigate">
                    <button id="request" class="navigate__btn btn btn--darkblue js-open-modal-request"><i class="icon-envelope"></i><span class="btn__span">Оставить заявку</span></button>
                </div>
            </div>
        </div>
    </header>
    <!-- header SECTION-->
    @yield('content')
    <footer class="footer">
        <div class="footer-wrap">
            <div class="container flex-wrap">
                <ul class="footer__soc-list flex-wrap">
                    @if($city_data->social_links)
                        @foreach($city_data->social_links as $item)
                            @foreach($item as $key => $value)
                                @if($value)
                                    <li><a href="{{ $value }}" target="_blank" rel="nofollow"><i class="icon {{ $key }}"></i></a></li>
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                </ul>
                <div class="footer__contacts flex-wrap">
                    <div class="footer__contacts-item">
                        @if($city_data->email)
                            <div class="icon icon-envelope"></div>
                            <div class="footer-info"><a href="{{ $city_data->email }}" class="footer-item">{{ $city_data->email }}</a></div>
                        @endif
                    </div>
                    <div class="footer__contacts-item">
                        @if($city_data->phones)
                            <div class="icon icon-mobile-phone"></div>
                            <div class="footer-info">
                                @foreach($city_data->phones as $phone)
                                    <a href="tel:{{ Helpers::filterPhone($phone) }}" class="footer-item">{{ $phone }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="footer__contacts-item">
                        <div class="icon icon-pin"></div>
                        <div class="footer-info"><span class="footer-item">{!! $city_data->address !!}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@include('frontend.modals.menu')
@include('frontend.modals.modal-form-feedback')
@include('frontend.modals.modal-form-blog')
@include('frontend.modals.modal-answer-blog')
<script>
    window.custom_var = {
        routeLikes: '{{ route('blog.likes') }}',
    };
</script>
@stack('scripts')

<script src="/js/functions.js" defer></script>

</body>
</html>