<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Custom Browsers Color Start-->
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#3A4446"/>
    <meta property="og:image" content="/img/icons/logo.png" />
@stack('seo_fields')
<!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#3A4446"/>
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#3A4446"/>
    <!-- Custom Browsers Color End--><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <!-- Loading CSS Start-->
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/landing.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- Loading CSS End-->

    {!! $gtm !!}

    {!! $fb_pixels !!}

</head>

@yield('content')

<section class="layer footer-map">
    <div class="scene__window">
        <div class="map" data-lat="{!! $city_data->lat !!}" data-lng="{!! $city_data->lng !!}">
            <div class="map__top" id="map"></div>
            <div class="map__middle contacts">
                <div class="contacts-container">
                    <div class="contacts__item"><i class="icon-envelope"></i>
                        @if($city_data->email)
                            <div class="contacts__content"><a href="mailto:{{ $city_data->email }}" title="{{ $city_data->email }}">{{ $city_data->email }}</a></div>
                        @endif
                    </div>
                    <div class="contacts__item"><i class="icon-smartphone"></i>
                        <div class="contacts__content">
                            @if($city_data->phones)
                                @foreach($city_data->phones as $phone)
                                    <a href="tel:{{ \App\Helpers\Helpers::filterPhone($phone) }}">{{ $phone }}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="contacts__item"><i class="icon-location"></i>
                        <div class="contacts__content">
                            <address>{!! $city_data->address !!}</address>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="map__bottom footer">
                <div class="footer-container">
                    <div class="footer__item footer__item--first">
                        <a href="{{ route('home') }}">
                            <img class="footer__logo" src="/img/alevelua.svg" alt="">
                        </a>
                        <a class="footer__hub" href="#" title="{{ route('hub') }}">A-Level hub</a>
                        <a class="footer__dev" href="{{ route('team') }}"
                           title="#">Разработано выпускниками нашей школы</a>
                        <ul class="footer-social">
                            @if($city_data->social_links)
                                @foreach($city_data->social_links as $item)
                                    @foreach($item as $key => $value)
                                        @if($value)
                                            <li class="social__item"><a class="{{ $key }}" href="{{ $value }}" target="_blank"></a></li>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                        </ul>
                        <p class="footer__copy">&#169;{{ date('Y') }} A-Level Все права защищены</p>
                    </div>
                    <nav class="footer__item">
                        <ul class="footer-nav">
                            <li class="footer-nav__item"><a href="#about">О школе</a></li>
                            <li class="footer-nav__item"><a href="#calendar">Календарь</a></li>
                            <li class="footer-nav__item"><a href="#courses">Курсы</a></li>
                            <li class="footer-nav__item"><a href="#teachers">Учителя</a></li>
                            <li class="footer-nav__item"><a href="#partners">Партнеры</a></li>
                            <li class="footer-nav__item"><a href="#contacts">Контакты</a></li>
                            <li class="footer-nav__item"><a href="#english">English тест</a></li>
                            <li class="footer-nav__item"><a href="#">Выбор курса</a></li>
                            <li class="footer-nav__item"><a href="#">Профильный тест</a></li>
                        </ul>
                    </nav>
                    <nav class="footer__item">
                        <ul class="footer-nav">
                            @forelse($courses as $course)
                                <li class="footer-nav__item">
                                    <a href="{{ route('course', ['course_alias' => $course->alias]) }}">{{ $course->title }}</a>
                                </li>
                            @empty
                            @endforelse
                        </ul>
                    </nav>
                    <form class="footer__item footer__item--form js-form" id="callbackForm" action="{{ route('add-callback') }}" method="post"><span class="footer__question">Остались вопросы?</span>
                        <input class="footer__input" type="text" placeholder="Ваше имя" name="username">
                        <input class="footer__input" type="text" placeholder="Ваш Email" name="email">
                        <textarea class="footer__textarea" placeholder="Вопрос" name="message"></textarea>
                        <div class="checkin__text js-error-area" style="display: none"></div>
                        <button class="footer__btn" type="submit">Отправить</button>
                    </form>
                </div>
            </footer>
        </div>
    </div>

<!-- Loading JS Start-->
    <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}"></script>
    @stack('scripts')
    <script async src='https://www.google-analytics.com/analytics.js'></script>
    <script defer src="/js/commons.min.js"></script>
    <script defer src="/js/landing.min.js"></script>
    <script defer src="/js/functions.js"></script>
    <!-- Loading JS End-->
</section>
</html>