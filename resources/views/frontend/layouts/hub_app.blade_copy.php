<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description"
          content="{{ $data->seo_description }}">
    <meta name="keywords" content="{{ $data->seo_keywords }}">
    <meta name="robots" content="{{ $data->seo_robots }}">
    @if($data->seo_canonical)
        <link rel="canonical" href="{{ $data->seo_canonical }}"/>
@endif
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{$data->title}}</title>

    <!-- favicon !-->
    <link rel="icon" href="alevel-hub/images/logo-favicon.png" sizes="32x32">
    <link rel="icon" href="alevel-hub/images/logo-favicon.png" sizes="192x192">

    <!-- Styles -->
    <link href="alevel-hub/style.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="alevel-hub/css/bootstrap.min.css" rel="stylesheet">

    <!-- Owl -->
    <link href="alevel-hub/css/owl/owl.carousel.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
@yield('content')