<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="Courses" content="A-Level Ukraine">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Custom Browsers Color Start-->
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#3A4446"/>
    <meta property="og:image" content="/img/icons/logo.png" />

@stack('seo_fields')
<!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#3A4446"/>
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#3A4446"/>
    <!-- Custom Browsers Color End--><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <!-- Loading CSS Start-->
    <link rel="stylesheet" href="css/libs.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/landing.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- Loading CSS End-->
</head>
<body class="d-block d-md-flex flex-column minh-100">
<header class="students-header px-10 px-md-40 py-25 bg-white d-flex align-items-center flex-column flex-sm-row">
    <a class="students-header__logo mr-sm-90 ml-sm-0 mb-20 mb-sm-0" href="/" target="_blank">
        <img class="img-fluid" class="d-flex align-items-center" href="{{ route('home') }}">
        <i class="icon-arrow-left display-24 mr-10 text-dark"></i>
        <span class="display-20 regular text-dark">Вернуться на главную</span>
    </a>
</header>

@yield('content')

<!-- Loading JS Start-->
<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script defer src="js/commons.min.js"></script>
<script defer src="js/students.js"></script>
<!-- Loading JS End-->
</body>
</html>