<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel="icon" href="/img/icons/logo.png" sizes="32x32">
    <link rel="icon" href="/img/icons/logo.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="img/icons/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:image" content="/img/icons/logo.png" />
@stack('seo_fields')
<!-- Custom Browsers Color Start-->
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#0099ba"/>
    <!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#0099ba"/>
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#0099ba"/>
    <!-- Custom Browsers Color End--><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <!-- Loading CSS Start-->
    <link rel="stylesheet" href="/css/libs.css">
    @stack('styles')
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- Loading CSS End-->
</head>

@yield('content')

@stack('scripts')

</html>