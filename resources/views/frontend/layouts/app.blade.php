<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel="icon" href="/img/icons/logo.png" sizes="32x32">
    <link rel="icon" href="/img/icons/logo.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="/img/icons/logo.png">
    <meta property="og:image" content="/img/icons/logo.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
@stack('seo_fields')
<!-- Custom Browsers Color Start-->
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#0099ba"/>
    <!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#0099ba"/>
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#0099ba"/>
    <!-- Custom Browsers Color End--><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous" defer></script>
    <script>


    </script>

    <!-- Loading CSS Start-->
    <link rel="stylesheet" href="/css/libs.min.css">
    @stack('styles')
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;amp;subset=cyrillic"
          rel="stylesheet">
    <!-- Loading CSS End-->

    {!! $gtm !!}

    {!! $fb_pixels !!}
</head>

@yield('content')

<!-- Footer-->
@if(\Request::route()->getName() == 'home')
    <section class="layer footer-map" data-window data-hash="contacts" id="contacts">
        <p class="mobile__heading">Контакты</p>
        <div class="scene__window">
            <div class="map" data-lat="{!! $city_data->lat !!}" data-lng="{!! $city_data->lng !!}">
                <div class="map__top" id="map"></div>
                <div class="map__middle contacts">
                    <div class="contacts-container">
                        <div class="contacts__item"><i class="icon-envelope"></i>
                            <div class="contacts__content">
                                @if($city_data->email)
                                    <a href="mailto:{{ $city_data->email }}"
                                       title="{{ $city_data->email }}">{{ $city_data->email }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="contacts__item"><i class="icon-smartphone"></i>
                            <div class="contacts__content">
                                @if($city_data->phones)
                                    @foreach($city_data->phones as $phone)
                                        <a href="tel:{{ \App\Helpers\Helpers::filterPhone($phone) }}">{{ $phone }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="contacts__item"><i class="icon-location"></i>
                            <div class="contacts__content">
                                <address>{!! $city_data->address !!}</address>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="map__bottom footer">
                    <div class="footer-container">
                        <div class="footer__item footer__item--first">
                            <a class="footer__logo-link" href="#home">
                                <img class="footer__logo" src="img/alevelua.svg" alt="">
                            </a>
                            <a class="footer__hub" href="#" title="{{ route('hub') }}" target="_blank">A-Level hub</a>
                            <!--<a class="footer__dev" href="{{ route('team') }}" target="_blank">Разработано выпускниками
                                нашей школы</a> -->
                            <ul class="footer-social">
                                @if($city_data->social_links)
                                    @foreach($city_data->social_links as $item)
                                        @foreach($item as $key => $value)
                                            @if($value)
                                                <li class="social__item"><a class="{{ $key }}" href="{{ $value }}"
                                                                            target="_blank"></a></li>
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                            </ul>
                            <p class="footer__copy">&#169;{{ date('Y') }} A-Level Все права защищены</p>
                        </div>
                        <nav class="footer__item">
                            <ul class="footer-nav">
                                <li class="footer-nav__item"><a href="#about">О школе</a></li>
                                <li class="footer-nav__item"><a href="#calendar">Календарь</a></li>
                                <li class="footer-nav__item"><a href="#courses">Курсы</a></li>
                                <li class="footer-nav__item"><a href="#teachers">Учителя</a></li>
                                <li class="footer-nav__item"><a href="#partners">Партнеры</a></li>
                                <li class="footer-nav__item"><a href="#contacts">Контакты</a></li>
                                <!-- <li class="footer-nav__item"><a href="#">English тест</a></li> -->
                                <!-- <li class="footer-nav__item"><a href="#">Выбор курса</a></li> -->
                                <!-- <li class="footer-nav__item"><a href="#">Профильный тест</a></li> -->
                            </ul>
                        </nav>
                        <nav class="footer__item">
                            <ul class="footer-nav">
                                @forelse($courses as $course)
                                    <li class="footer-nav__item"><a
                                                href="{{ route('course', ['course_alias' => $course->alias]) }}"
                                                target="_blank">{!! $course->title !!}</a></li>
                                @empty
                                @endforelse
                            </ul>
                        </nav>
                        <form class="footer__item footer__item--form js-form" id="callbackForm"
                              action="{{ route('add-callback') }}" method="post"><span class="footer__question">Остались вопросы?</span>
                            <input class="footer__input" type="text" placeholder="Ваше имя" name="username"
                                   maxlength="150">
                            <input class="footer__input js-mask" type="text" placeholder="Ваш телефон" name="phone">
                            <div class="text-wrap">
                                <textarea class="footer__textarea js-symbols-left" placeholder="Вопрос" name="message"
                                          maxlength="1000"></textarea>
                                <span class="remaining-symbols js-remainingSymbol"><em>0</em>/1000</span>
                            </div>
                            <div class="checkin__text js-error-area" style="display: none"></div>
                            <button class="footer__btn" type="submit">Отправить</button>
                        </form>
                    </div>
                </footer>
            </div>
        </div>
    </section>
    </div>
@endif

@stack('page-menu')

@stack('goto')
<div class="gotop">
    <a class="gotop__prev js-gotop__prev" href="#home">
        <i class="icon-desktop-monitor"></i>
    </a>
</div>
<div class="burger__wrapper">
    <div class="burger" id="menu">
        <ul class="burger__list">
            <li class="burger__item"></li>
            <li class="burger__item"></li>
            <li class="burger__item"></li>
        </ul>
        <span class="burger__text">Menu</span>
    </div>
    <a class="burger__logo" href="/" target="_blank"><img src="/img/alevelua.svg" alt="A-Level"></a>
</div>

@stack('action-countdown')

@stack('start-button')

<!-- Вспомогательный класс .navigate--row-->
<div class="navigate">
    <button class="navigate__btn btn btn--darkblue js-open-modal-request" id="request" onClick="fbq('track', 'Lead');">
        <i class="icon-envelope"></i><span class="btn__span">Оставить заявку</span></button>
    <a class="navigate__btn btn btn--blue" href="{{ route('hub') }}" target="_blank"><i class="icon-city"></i><span
                class="btn__span">A-Level HUB</span></a>

    <div class="select navigate__select cities-id" id="cities">
        <div class="icon-before icon-location"></div>

        <select class="cities js_change-city" data-route="{{ route('update-city') }}">
            @forelse($cities as $city)
                <option value="{{ $city->id }}"
                        @if($city->id == session('city')) selected="selected" @endif>{{ $city->title }}</option>
            @empty
            @endforelse
        </select>

        <div class="icon-after icon-arrow-down"></div>
    </div>
</div>


<section class="preloader"><img class="preloader__gear" src="/img/gears.svg" alt="preloader"></section>

@include('frontend.modals.menu')

@include('frontend.modals.modal-answer')

@include('frontend.modals.modal-form-feedback')

@include('frontend.modals.modal-form-partner')

@include('frontend.modals.modal-form-teacher')

@include('frontend.modals.modal-form-course')

@stack('scene-bg')
<!-- Loading JS Start-->


<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}" defer></script>
@stack('scripts')
<script async src='https://www.google-analytics.com/analytics.js'></script>
<script src="/js/functions.js" defer></script>
<!-- Loading JS End-->
</body>
</html>
