<?php

    use App\Helpers\Helpers;
    use App\Models\Course\Course;
    use Illuminate\Database\Seeder;

    class CourseUpdateTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker   = Faker\Factory::create();
            $classes = explode('|', Helpers::getConfig('course_logo_html_classes'));
            $courses = Course::get();
            foreach ($courses as $course) {
                $course->logo_class  = array_random($classes);
                $course->duration    = $faker->text(12);
                $course->schedule    = $faker->text(12);
                $course->free_places = $faker->randomDigit();
                $course->update();
            }
        }
    }
