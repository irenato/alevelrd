<?php

    use App\Models\Blog\Article;
    use App\Models\Blog\Author;
    use App\Models\Blog\Tag;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Storage;

    class ArticleSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            Storage::deleteDirectory('public/images/articles/');
            Storage::makeDirectory('public/images/articles/', 755);
            factory(Article::class, 24)->create()->each(function ($article) use ($faker) {
                Storage::makeDirectory('public/images/articles/' . $article->id, 755);
                $article->thumbnail  = $faker->image(storage_path('app/public/images/articles/' . $article->id), 400, 300, 'technics', false);
                $article->background = $faker->image(storage_path('app/public/images/articles/' . $article->id), 400, 300, 'technics', false);
                $article->update();
                $article->authors_articles()->sync(Author::inRandomOrder()->first()->id);
                $article->tags_articles()->sync(Tag::inRandomOrder()->first()->id);
                $relatedArticle = Article::where('id', '!=', $article->id)
                    ->inRandomOrder()->limit(10)->get()->pluck('id');
                if ($relatedArticle) {
                    $article->related()->sync($relatedArticle);
                }
            });
        }
    }
