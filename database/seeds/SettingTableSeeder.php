<?php

    use App\Models\Setting\Setting;
    use Illuminate\Database\Seeder;

    class SettingTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $data = [
                [
                    'key'         => 'links_html_classes',
                    'value'       => 'icon-facebook|icon-vk|icon-youtube|icon-telegram|icon-instagram|icon-linkedin',
                    'description' => 'HTML-классы для ссылок на соцсети',
                ],
                [
                    'key'         => 'course_logo_html_classes',
                    'value'       => 'icon-code|icon-html5|icon-java|icon-diagram|icon-php|icon-tag|icon-layers|icon-blank|icon-c',
                    'description' => 'почта администратора',
                ],
                [
                    'key'         => 'admin_email',
                    'value'       => 'a.level.ua@gmail.com',
                    'description' => 'HTML-классы лого курсов',
                ],
            ];

            foreach ($data as $item) {
                Setting::create($item);
            }

        }
    }
