<?php

    use App\Models\Application\Teachapplication;
    use Illuminate\Database\Seeder;

class TeachapplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Teachapplication::class, 250)->create();
    }
}
