<?php

    use App\Models\User\Permission;
    use Illuminate\Database\Seeder;

    class PermissionTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $permission = [

                // settings
                [
                    'name'         => 'settings',
                    'display_name' => 'Настройки',
                    'description'  => 'Доступ к созданию/редактированию раздела настроек',
                ],

                // courses
                [
                    'name'         => 'courses',
                    'display_name' => 'Курсы',
                    'description'  => 'Доступ к курсам',
                ],

                // teachers
                [
                    'name'         => 'teachers',
                    'display_name' => 'Учителя',
                    'description'  => 'Доступ к учителям',
                ],

                // students
                [
                    'name'         => 'students',
                    'display_name' => 'Студенты',
                    'description'  => 'Доступ к студентам',
                ],

                // promocodes
                [
                    'name'         => 'promocodes',
                    'display_name' => 'Промокоды',
                    'description'  => 'Доступ к промокодам',
                ],

                // partners
                [
                    'name'         => 'partners',
                    'display_name' => 'Партнеры',
                    'description'  => 'Доступ к партнерам',
                ],

                // applications
                [
                    'name'         => 'applications',
                    'display_name' => 'Заявки',
                    'description'  => 'Доступ к заявкам',
                ],

                // locations
                [
                    'name'         => 'locations',
                    'display_name' => 'Локации',
                    'description'  => 'Доступ к локациям',
                ],

                // regards
                [
                    'name'         => 'rewards',
                    'display_name' => 'Награды',
                    'description'  => 'Доступ к наградам',
                ],

                // hub
                [
                    'name'         => 'hub',
                    'display_name' => 'Хаб',
                    'description'  => 'Доступ к хабу',
                ],


                // hub
                [
                    'name'         => 'projectlife',
                    'display_name' => 'Жизнь проекта',
                    'description'  => 'Доступ к жизни проекта',
                ],

            ];

            foreach ($permission as $key => $value) {
                Permission::create($value);
            }
        }
    }
