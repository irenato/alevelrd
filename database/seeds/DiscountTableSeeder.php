<?php

    use App\Models\Discount\Discount;
    use Illuminate\Database\Seeder;

    class DiscountTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $values = [
                [
                    'value'       => '5%',
                    'description' => 'если вы пришли по рекомендации ученика курсов',
                    'class'       => 'sell__percent--lightblue',
                ],
                [
                    'value'       => '10%',
                    'description' => 'если вы наш выпускник и хотите записаться на другой курс',
                    'class'       => 'sell__percent--darkblue',
                ],
                [
                    'value'       => '15%',
                    'description' => 'если вы привели двух и более друзей',
                    'class'       => 'sell__percent--lightorange',
                ],
                [
                    'value'       => '20%',
                    'description' => 'если вы закончили больше двух наших курсов',
                    'class'       => 'sell__percent--darkorange',
                ],
            ];

            foreach ($values as $value){
                Discount::create($value);
            }
        }
    }
