<?php

    use App\Models\Course\Course;
    use App\Models\Teacher\Teacher;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Storage;

    class TeacherSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Teacher::class, 25)->create();
            $teachers = Teacher::get();
            foreach ($teachers as $teacher) {
                Storage::deleteDirectory('public/images/teachers/' . $teacher->id);
                Storage::makeDirectory('public/images/teachers/' . $teacher->id, 0777);
                $teacher->logo = $faker->image(storage_path('app/public/images/teachers/' . $teacher->id), 400, 300, 'cats', false);
                $teacher->thumbnail = $faker->image(storage_path('app/public/images/teachers/' . $teacher->id), 400, 300, 'cats', false);
                $teacher->courses_teachers()->sync(Course::all()->random(1)->first()->id);
                $teacher->update();
            }
        }
    }
