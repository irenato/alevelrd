<?php

    use App\Models\Course\Course;
    use App\Models\Student\Student;
    use Illuminate\Database\Seeder;

    class StudentSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Student::class, 25)->create();
            $students = Student::get();
            foreach ($students as $student) {
                Storage::deleteDirectory('public/images/students/' . $student->id);
                Storage::makeDirectory('public/images/students/' . $student->id, 0777);
                $student->thumbnail = $faker->image(storage_path('app/public/images/students/' . $student->id), 400, 300, 'cats', false);
                $student->courses_students()->sync(Course::all()->random(1)->first()->id);
                $student->update();
            }
        }
    }
