<?php

    use App\Models\Test\Ttype;
    use App\Models\Test\Test;
    use Illuminate\Database\Seeder;

    class TtypeTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $names = [
                [
                    'name' => 'Базовый'
                ],
                [
                    'name' => 'Гуманитарный'
                ],
                [
                    'name' => 'Teхнический'
                ],
            ];

            foreach ($names as $key => $value) {
                $ttype = Ttype::create($value);
                switch ($key) {
                    case 0:
                        $testsIds = range(1, 9);
                        break;
                    case 1:
                        $testsIds = range(10, 13);
                        break;
                    default:
                        $testsIds = range(14, 17);
                        break;
                }
                $ttype->tests_ttypes()->sync(Test::find($testsIds));
            }
        }
    }
