<?php

use Illuminate\Database\Seeder;
use App\Models\User\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'password' => bcrypt('123456'),
                'email' => 'admin@example.com',
            ]
        ];
        foreach ($users as $key => $value) {
            $user = User::create($value);
            $user->roles()->sync([1]);
        }
    }
}
