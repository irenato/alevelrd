<?php

    use App\Models\Review\Review;
    use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        factory(Review::class, 100)->create();
        $reviews = Review::get();
        foreach ($reviews as $review) {
            Storage::deleteDirectory('public/images/reviews/' . $review->id);
            Storage::makeDirectory('public/images/reviews/' . $review->id, 0777);
            $review->thumbnail = $faker->image(storage_path('app/public/images/reviews/' . $review->id), 400, 300, 'animals', false);
            $review->update();
        }
    }
}
