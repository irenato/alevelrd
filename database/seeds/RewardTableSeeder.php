<?php

    use App\Models\Reward\Reward;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Storage;

    class RewardTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Reward::class, 100)->create();
            $rewards = Reward::get();
            foreach ($rewards as $reward) {
                Storage::deleteDirectory('public/images/rewards/' . $reward->id);
                Storage::makeDirectory('public/images/rewards/' . $reward->id, 0777);
                $reward->thumbnail = $faker->image(storage_path('app/public/images/rewards/' . $reward->id), 400, 300, 'nature', false);
                $reward->update();
            }
        }
    }
