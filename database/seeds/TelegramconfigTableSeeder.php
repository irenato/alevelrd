<?php

    use App\Models\Setting\Telegramconfig;
    use Illuminate\Database\Seeder;

    class TelegramconfigTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $data = [
                [
                    'title'  => 'Обратная связь',
                    'key'    => 'callback_form',
                    'bot_id' => '437145957:AAGP_EUDYR4y2PJxY5on5XUaqi-Ws52uh4Q',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
                [
                    'title'  => 'Заявки (учителя)',
                    'key'    => 'from_teacher',
                    'bot_id' => '437145957:AAGP_EUDYR4y2PJxY5on5XUaqi-Ws52uh4Q',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
                [
                    'title'  => 'Заявки (студенты)',
                    'key'    => 'from_student',
                    'bot_id' => '349282442:AAHK68h2TTIulRVzKVZzU4I172qP_kMmPRM',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
                [
                    'title'  => 'Заявки (партнеры)',
                    'key'    => 'from_partner',
                    'bot_id' => '349282442:AAHK68h2TTIulRVzKVZzU4I172qP_kMmPRM',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
                [
                    'title'  => 'Заявки (ивенты)',
                    'key'    => 'event_form',
                    'bot_id' => '349282442:AAHK68h2TTIulRVzKVZzU4I172qP_kMmPRM',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
                [
                    'title'  => 'Заявки (жизнь проекта)',
                    'key'    => 'from_project',
                    'bot_id' => '349282442:AAHK68h2TTIulRVzKVZzU4I172qP_kMmPRM',
                    'ids'    => '281441680, 281441680, 281441680',
                ],
            ];

            foreach ($data as $item){
                Telegramconfig::create($item);
            }
        }
    }
