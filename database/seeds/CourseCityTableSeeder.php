<?php

    use App\Models\Course\Course;
    use App\Models\Locations\City;
    use Illuminate\Database\Seeder;

class CourseCityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = City::get()->pluck('id');
        $courses = Course::get();
        foreach ($courses as $course){
            $course->courses_cities()->sync($cities);
        }
    }
}
