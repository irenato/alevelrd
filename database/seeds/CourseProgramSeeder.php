<?php

    use App\Models\Course\Course;
    use App\Models\Course\CourseProgram;
    use Illuminate\Database\Seeder;

    class CourseProgramSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker   = Faker\Factory::create();
            $courses = Course::get();
            foreach ($courses as $course) {
                $i = 6;
                while ($i > 0) {
                    CourseProgram::create([
                        'course_id' => $course->id,
                        'period'    => $i . ' month',
                        'title'     => $faker->colorName,
                        'introtext' => $faker->text(55),
                        'practice'  => $faker->text(200),
                    ]);
                    --$i;
                }
            }
        }
    }
