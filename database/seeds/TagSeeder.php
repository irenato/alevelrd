<?php

    use App\Models\Blog\Htmlclass;
    use App\Models\Blog\Tag;
    use Illuminate\Database\Seeder;

    class TagSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $htmlClasses = Htmlclass::get();
            foreach ($htmlClasses as $htmlClass) {
                factory(Tag::class, 3)->create(['htmlclass_id' => $htmlClass->id]);
            }
        }
    }
