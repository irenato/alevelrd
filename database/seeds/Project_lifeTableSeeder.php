<?php

use Illuminate\Database\Seeder;

class Project_lifeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'title' => 'Жизнь проекта 3.0',
                'section_1_h1'  => 'Жизнь проекта 3.0',
                'section_1_h2' => 'Самая большая в Украине конференция для неайтишников',
                'section_1_p' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>',
                'section_1_p_cl_title' => 'До начала осталось',
                'section_1_p_img' => 'dddd.jpg',
                'section_2h1_cl' => 'Что же Вас ждет на конференции?',
                'section_2p' => '       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa</p>
                    <p>Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                        consequuntur.</p>',
                'section_4_h1' => 'Программа мероприятия',
                'section_6_h4_1' => 'Генеральные партнеры',
                'section_6_h4_2' => 'Премиум партнеры',
                'section_6_h4_3' => 'Медиа партнеры',
                'section_7_m_h2_cl' => 'Ваша заявка успешно отправленна!',
                'section_7_m_p' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
            <p>',

            ]
        ];

        foreach ($data as $item) {
            \App\Models\Project_life\Project_life::create($item);
        }

    }
}
