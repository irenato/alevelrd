<?php

    use \App\Models\Blog\Author;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Storage;

    class AuthorSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            Storage::deleteDirectory('public/images/authors/');
            Storage::makeDirectory('public/images/authors/', 755);
            factory(Author::class, 7)->create()->each(function ($author) use ($faker) {
                Storage::makeDirectory('public/images/authors/' . $author->id, 755);
                $author->thumbnail = $faker->image(storage_path('app/public/images/authors/' . $author->id), 400, 300, 'technics', false);
                $author->update();
            });
        }
    }
