<?php

    use App\Models\Partner\Partner;
    use Illuminate\Database\Seeder;

    class PartnerTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Partner::class, 25)->create();
            $partners = Partner::get();
            foreach ($partners as $partner) {
                Storage::deleteDirectory('public/images/partners/' . $partner->id);
                Storage::makeDirectory('public/images/partners/' . $partner->id, 0777);
                $partner->logo      = $faker->image(storage_path('app/public/images/partners/' . $partner->id), 400, 300, 'cats', false);
                $partner->thumbnail = $faker->image(storage_path('app/public/images/partners/' . $partner->id), 400, 300, 'cats', false);
                $partner->update();
            }
        }
    }
