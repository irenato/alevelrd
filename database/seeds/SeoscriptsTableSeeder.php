<?php

    use App\Models\Seo\Seoscript;
    use Illuminate\Database\Seeder;

    class SeoscriptsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $data = [
                [
                    'key'   => 'gtm',
                    'value' => " <script src=\"https://apis.google.com/js/platform.js\" async defer>
                                {
                                    lang: 'ru'
                                }
                            </script>
                            <script>
                                (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                            (i[r].q = i[r].q || []).push(arguments)
                                        }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                        m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
                        
                                ga('create', 'UA-77235759-1', 'auto');
                                ga('send', 'pageview');
                            </script>",
                ],
                [
                    'key'   => 'fb_pixels',
                    'value' => "<script>
                                !function(f,b,e,v,n,t,s)
                                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                  n.queue=[];t=b.createElement(e);t.async=!0;
                                  t.src=v;s=b.getElementsByTagName(e)[0];
                                  s.parentNode.insertBefore(t,s)}(window, document,'script',
                                'https://connect.facebook.net/en_US/fbevents.js');
                                fbq('init', '334969380181575');
                                fbq('track', 'PageView');
                              </script>",
                ],
            ];
            foreach ($data as $item) {
                Seoscript::create($item);
            }
        }
    }
