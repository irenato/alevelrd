<?php

    use App\Models\Application\Stapplication;
    use Illuminate\Database\Seeder;

class StapplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Stapplication::class, 250)->create();
    }
}
