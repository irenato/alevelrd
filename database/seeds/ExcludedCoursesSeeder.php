<?php

use Illuminate\Database\Seeder;
use App\Models\Setting\Setting;

class ExcludedCoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            Setting::create([
                'key'         => 'excluded_courses',
                'value'       => '8',
                'description' => 'Id курсов, на которые не распространяется акция через запятую',
            ]);
    }
}
