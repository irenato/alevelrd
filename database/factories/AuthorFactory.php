<?php

    use App\Models\Blog\Author;
    use Faker\Generator as Faker;

    $factory->define(Author::class, function (Faker $faker) {
        return [
            App::getLocale() => [
                'first_name' => $faker->firstName,
                'last_name'  => $faker->lastName,
                ],
        ];
    });
