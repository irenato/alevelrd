<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Application\CourseApplication;
use App\Models\Application\JobApplication;
use App\Models\Application\Stapplication;
use App\Models\Application\Teachapplication;
use App\Models\Course\Course;
use App\Models\Course\Type;
use App\Models\Event\Event;
use App\Models\Locations\City;
use App\Models\Partner\Partner;
use App\Models\Review\Review;
use App\Models\Reward\Reward;
use App\Models\Student\Student;
use App\Models\Teacher\Teacher;
use Carbon\Carbon;

$factory->define(App\User::class, function(Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

// type
$factory->define(Type::class, function(Faker\Generator $faker) {
    $title = $faker->company;
    return [
        'title'    => $faker->colorName,
        'position' => rand(0, 999),
    ];
});

// courses
$factory->define(Course::class, function(Faker\Generator $faker) {
    $title = $faker->company;
    return [
        'alias'             => str_slug($title),
        'cost'              => $faker->numberBetween(1500, 8000),
        'title'             => $title,
        'introtext'         => $faker->text(32),
        'description'       => $faker->text(200),
        'start_date'        => $faker->dateTimeBetween('now', '+' . rand(1, 4) .
            ' month'),
        'amount_places'     => rand(10, 25),
        'free_places'       => rand(0, 10),
        'seo_title'         => $title,
        'discount'          => rand(10, 15),
        'position'          => rand(0, 999),
        'discount_end_date' => $faker->dateTimeBetween('1 month', '2 months'),
        'type_id'           => Type::all()->random(1)->first()->id,
        'begin'             => $faker->dateTimeBetween('1 month', '2 months'),
    ];
});

// teachers
$factory->define(Teacher::class, function(Faker\Generator $faker) {
    return [
        'alias'          => $faker->slug(12),
        'first_name'     => $faker->firstName(),
        'last_name'      => $faker->lastName(),
        'specialization' => $faker->jobTitle,
        'introtext'      => $faker->text(32),
        'description'    => $faker->text(200),
    ];
});

// students
$factory->define(Student::class, function(Faker\Generator $faker) {
    return [
        'alias'      => $faker->slug(12),
        'first_name' => $faker->firstName(),
        'last_name'  => $faker->lastName(),
        'birthdate'  => $faker->dateTimeThisCentury,
        'email'      => $faker->email,
        'phone'      => $faker->phoneNumber,
        'city_id'    => City::all()->random(1)->first()->id,
    ];
});

// partners
$factory->define(Partner::class, function(Faker\Generator $faker) {
    return [
        'link'        => $faker->url,
        'title'       => $faker->company,
        'description' => $faker->text(),
    ];
});

// reviews
$factory->define(Review::class, function(Faker\Generator $faker) {
    return [
        'username'  => $faker->firstName(),
        'email'     => $faker->email,
        'active'    => $faker->boolean,
        'course_id' => Course::all()->random(1)->first()->id,
        'review'    => $faker->text(),
    ];
});

// rewards
$factory->define(Reward::class, function(Faker\Generator $faker) {
    return [
        'title'       => $faker->colorName,
        'description' => $faker->text(),
    ];
});

// application (student)
$factory->define(Stapplication::class, function(Faker\Generator $faker) {
    return [
        'username'    => $faker->firstName(),
        'email'       => $faker->email,
        'phone'       => $faker->phoneNumber,
        'active'      => $faker->boolean,
        'course_id'   => Course::all()->random(1)->first()->id,
        'city_id'     => City::all()->random(1)->first()->id,
        'description' => $faker->text(200),
    ];
});

// application (teacher)
$factory->define(Teachapplication::class, function(Faker\Generator $faker) {
    return [
        'username'    => $faker->firstName(),
        'email'       => $faker->email,
        'phone'       => $faker->phoneNumber,
        'active'      => $faker->boolean,
        'course_id'   => Course::all()->random(1)->first()->id,
        'city_id'     => City::all()->random(1)->first()->id,
        'description' => $faker->text(128),
    ];
});

// events
$factory->define(Event::class, function(Faker\Generator $faker) {
    return [
        'title'       => $faker->colorName,
        'alias'       => $faker->slug(12),
        'begin'       => $faker->dateTimeBetween('now', '+1 month'),
        'cost'        => $faker->randomDigitNotNull,
        'introtext'   => $faker->text(),
        'description' => $faker->text(),
    ];
});



