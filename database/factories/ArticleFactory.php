<?php

    use App\Models\Blog\Article;
    use Faker\Generator as Faker;

    $factory->define(Article::class, function (Faker $faker) {
        return [
            'alias'          => $faker->slug . '-' . $faker->randomDigit,
            'likes'          => $faker->randomDigit,
            'dislikes'       => $faker->randomDigit,
            'viewed'         => $faker->randomDigit,
            'position'       => $faker->randomDigit,
            'is_outer'       => $faker->boolean,
            App::getLocale() => [
                'title'           => $faker->sentence(7),
                'content'         => $faker->text,
                'seo_title'       => $faker->colorName,
                'seo_keywords'    => $faker->text(32),
                'seo_description' => $faker->text(32),
                'seo_robots'      => 'noindex, nofollow',
                'seo_canonical'   => $faker->url,
                'seo_content'     => $faker->text,
            ],
        ];
    });
