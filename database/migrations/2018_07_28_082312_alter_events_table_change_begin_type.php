<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterEventsTableChangeBeginType extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('events', function (Blueprint $table) {
                $table->text('introtext')->nullable();
                $table->string('template')->nullable()->change();
                $table->dateTime('begin')->nullable()->change();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('events', function (Blueprint $table) {
                $table->dropColumn('introtext');
            });
        }
    }
