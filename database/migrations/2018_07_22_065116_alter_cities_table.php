<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterCitiesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('cities', function (Blueprint $table) {
                $table->string('address')->nullable();
                $table->string('lat')->nullable();
                $table->string('lng')->nullable();
                $table->boolean('active')->default(true);
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('cities', function (Blueprint $table) {
                $table->dropColumn('address');
                $table->dropColumn('lat');
                $table->dropColumn('lng');
                $table->dropColumn('active');
            });
        }
    }
