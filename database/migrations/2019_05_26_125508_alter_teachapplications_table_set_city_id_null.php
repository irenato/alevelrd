<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTeachapplicationsTableSetCityIdNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachapplications', function ($table) {
            $table->dropForeign('teachapplications_city_id_foreign');
            $table->integer('city_id')->unsigned()->nullable()->change();
            $table->foreign('city_id')->references('id')->on('cities')
                ->onDelete("NO ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
