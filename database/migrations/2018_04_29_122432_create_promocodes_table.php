<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreatePromocodesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('promocodes', function (Blueprint $table) {
                $table->increments('id');
                $table->date('valid_until');
                $table->boolean('reusable')->default(true);
                $table->boolean('active')->default(true);
                $table->string('code', 64);
                $table->integer('discount');
                $table->boolean('in_percent')->default(true);
                $table->softDeletes();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('promocodes');
        }
    }
