<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectLifeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_life', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title','255')->nullable();
            $table->string('seo_description','255')->nullable();
            $table->string('seo_keywords','255')->nullable();
            $table->string('seo_robots','255')->nullable();
            $table->string('seo_canonical','255')->nullable();
            $table->string('section_1_h1','255')->nullable();
            $table->string('section_1_h2','255')->nullable();
            $table->text('section_1_p')->nullable();
            $table->string('section_1_p_cl_title','255')->nullable();
            $table->string('section_1_p_img','255')->nullable();
            $table->string('section_2h1_cl','255')->nullable();
            $table->text('section_2p')->nullable();
            $table->string('section_4_h1','255')->nullable();
            $table->string('section_6_h4_1','255')->nullable();
            $table->string('section_6_h4_2','255')->nullable();
            $table->string('section_6_h4_3','255')->nullable();
            $table->string('section_7_m_h2_cl','255')->nullable();
            $table->text('section_7_m_p')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_life');
    }
}
