<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ttypes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('test_ttype', function (Blueprint $table) {
            $table->integer('ttype_id')->unsigned();
            $table->integer('test_id')->unsigned();
            $table->foreign('test_id')->references('id')->on('tests')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ttype_id')->references('id')->on('ttypes')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('course_ttype', function (Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('ttype_id')->unsigned();
            $table->foreign('ttype_id')->references('id')->on('ttypes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_ttype');
        Schema::dropIfExists('course_ttype');
        Schema::dropIfExists('ttypes');
    }
}
