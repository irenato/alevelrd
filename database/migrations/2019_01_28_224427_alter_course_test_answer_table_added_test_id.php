<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterCourseTestAnswerTableAddedTestId extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('course_test_answer', function (Blueprint $table) {
                $table->integer('test_id')->unsigned();
                $table->foreign('test_id')->references('id')->on('tests');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('course_test_answer', function (Blueprint $table) {
                $table->renameColumn('test_answer_id', 'answer_id');
                $table->dropColumn('test_id');
            });
            Schema::rename('course_test_answer', 'course_answer');
            Schema::table('course_answer', function (Blueprint $table) {
                $table->unique(['course_id', 'answer_id']);
            });
            Schema::rename('course_answer', 'course_test_answer');
            Schema::table('course_test_answer', function (Blueprint $table) {
                $table->renameColumn('answer_id', 'test_answer_id');
            });
        }
    }
