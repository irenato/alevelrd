<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterCoursesTableAddOldPrice extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('courses', function (Blueprint $table) {
                $table->string('cost', 32)->nullable()->change();
                $table->string('cost_old', 32)->nullable();
                $table->string('logo_class', 32)->nullable();
                $table->string('duration', 128)->nullable();
                $table->text('schedule')->nullable();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('courses', function (Blueprint $table) {
                $table->dropColumn('schedule');
                $table->dropColumn('logo_class');
                $table->dropColumn('duration');
                $table->dropColumn('cost_old');
            });
        }
    }
