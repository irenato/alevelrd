<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateArticlesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // html-class for link to tag
            Schema::create('htmlclasses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('html_class');
                $table->timestamps();
            });

            Schema::create('tags', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('htmlclass_id')->unsigned()->nullable();
                $table->string('name');
                $table->string('alias');
                $table->timestamps();
                $table->foreign('htmlclass_id')->references('id')
                    ->on('htmlclasses')
                    ->onDelete('set null');
                $table->unique('name');
                $table->unique('alias');
            });

            Schema::create('authors', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('active')->default(true);
                $table->string('thumbnail')->default(true);
                $table->timestamps();
            });

            Schema::create('author_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id')->unsigned();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('locale')->index();
                $table->unique(['author_id', 'locale']);
                $table->timestamps();
                $table->foreign('author_id')
                    ->references('id')
                    ->on('authors')
                    ->onDelete('cascade');
            });

            Schema::create('articles', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('active')->default(true);
                $table->string('alias');
                $table->string('thumbnail')->nullable();
                $table->string('background')->nullable();
                $table->integer('likes')->default(0);
                $table->integer('dislikes')->default(0);
                $table->integer('viewed')->default(0);
                $table->integer('position')->default(0);
                $table->timestamps();
                $table->unique('alias');
            });

            Schema::create('article_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('article_id')->unsigned();
                $table->string('title')->nullable();
                $table->text('content')->nullable();
                $table->string('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->string('seo_keywords')->nullable();
                $table->string('seo_robots')->nullable();
                $table->string('seo_canonical')->nullable();
                $table->text('seo_content')->nullable();
                $table->string('locale')->index();
                $table->unique(['article_id', 'locale']);
                $table->timestamps();
                $table->foreign('article_id')
                    ->references('id')
                    ->on('articles')
                    ->onDelete('cascade');
            });

            Schema::create('article_tag', function (Blueprint $table) {
                $table->integer('tag_id')->unsigned();
                $table->integer('article_id')->unsigned();
                $table->unique(['tag_id', 'article_id']);
                $table->foreign('tag_id')->references('id')->on('tags')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('article_id')->references('id')->on('articles')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('article_author', function (Blueprint $table) {
                $table->integer('author_id')->unsigned();
                $table->integer('article_id')->unsigned();
                $table->unique(['article_id', 'author_id']);
                $table->foreign('author_id')->references('id')->on('authors')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('article_id')->references('id')->on('articles')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('related_articles', function (Blueprint $table) {
                $table->integer('article_id')->unsigned()->index();
                $table->integer('related_id')->unsigned();
                $table->unique(['article_id', 'related_id']);
                $table->foreign('article_id')->references('id')->on('articles')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('related_id')->references('id')->on('articles')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('related_articles');
            Schema::dropIfExists('tag_article');
            Schema::dropIfExists('author_article');
            Schema::dropIfExists('author_translations');
            Schema::dropIfExists('authors');
            Schema::dropIfExists('article_translations');
            Schema::dropIfExists('articles');
            Schema::dropIfExists('tag_article');
            Schema::dropIfExists('tags');
            Schema::dropIfExists('htmlclasses');
        }
    }
