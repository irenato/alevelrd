<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCallbackapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('callbackapplications', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->string('phone', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('callbackapplications', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->string('email');
        });
    }
}
