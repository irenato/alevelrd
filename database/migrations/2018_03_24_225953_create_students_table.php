<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('students', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('active')->default(true);
                $table->string('alias');
                $table->string('thumbnail')->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('email', 64);
                $table->string('phone', 24);
                $table->date('birthdate');
                $table->integer('city_id')->integer()->unsigned();
                $table->integer('position')->default(0);
                $table->timestamps();
                $table->softDeletes();
                $table->unique('alias');
                $table->foreign('city_id')->references('id')->on('cities')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('students');
        }
    }
