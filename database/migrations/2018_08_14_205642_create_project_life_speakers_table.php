<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectLifeSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_life_speakers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img',255)->nullable();
            $table->string('img_hover',255)->nullable();
            $table->string('name',255)->nullable();
            $table->string('short_text',255)->nullable();
            $table->text('text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_life_speakers');
    }
}
