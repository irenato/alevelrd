<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hub', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title','255');
            $table->string('section_introduction_title','255');
            $table->string('section_introduction_description','255');
            $table->string('mail','55');
            $table->string('phone','25');
            $table->string('address','255');
            $table->string('section_class_rooms_title','255');
            $table->string('section_class_rooms_description','255');
            $table->text('section_locations_location_information')->nullable();
            $table->string('section_get_info_title','255');
            $table->string('seo_keywords')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_robots')->nullable();
            $table->string('seo_canonical')->nullable();
            $table->string('seo_description')->nullable();
            $table->mediumText('section_about_us_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hub');
    }
}
