<?php

use App\Models\Course\Course;
use App\Models\Redirect\Redirect;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redirects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('source', 191)->nullable()->index();
            $table->string('target', 191)->nullable()->index();
            $table->timestamps();
        });
        $courses = Course::get();
        foreach ($courses as $course) {
            Redirect::create([
                'target' => '/course/' . $course->alias,
                'source' => '/course/' . $course->alias . '/test'
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redirects');
    }
}
