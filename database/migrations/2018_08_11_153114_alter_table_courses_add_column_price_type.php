<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterTableCoursesAddColumnPriceType extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('courses', function ($table) {
                $table->string('price_type')->default('грн/месяц');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('courses', function (Blueprint $table) {
                $table->dropColumn(['price_type']);
            });
        }
    }
