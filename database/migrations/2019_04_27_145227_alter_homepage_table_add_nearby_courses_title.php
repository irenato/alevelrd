<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHomepageTableAddNearbyCoursesTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('homepages', function($table) {
            $table->string('nearby_courses_title')
                ->default('Регистрируйся и приходи на пробные уроки и мастер классы');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homepages', function(Blueprint $table) {
            $table->dropColumn('nearby_courses_title');
        });
    }
}
