<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTeachersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('teachers', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('active')->default(true);
                $table->string('alias');
                $table->string('thumbnail')->nullable();
                $table->string('logo')->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('specialization');
                $table->string('introtext')->nullable();
                $table->text('description')->nullable();
                $table->string('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->string('seo_keywords')->nullable();
                $table->string('seo_robots')->nullable();
                $table->string('seo_canonical')->nullable();
                $table->text('links')->nullable();
                $table->integer('position')->default(0);
                $table->timestamps();
                $table->softDeletes();
                $table->unique('alias');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('teachers');
        }
    }
