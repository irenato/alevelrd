<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hub_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('room_key','255');
            $table->string('room_name','255');
            $table->string('room_image','255');
            $table->string('room_image_location','255');
            $table->string('capacity_text','55');
            $table->string('room_information','900');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hub_rooms');
    }
}
