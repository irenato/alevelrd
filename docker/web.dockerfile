FROM nginx:1.10

ADD docker/vhost.conf /etc/nginx/conf.d/default.conf
ADD docker/nginx.conf /etc/nginx/nginx.conf