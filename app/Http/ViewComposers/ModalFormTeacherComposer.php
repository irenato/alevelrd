<?php

    namespace App\Http\ViewComposers;


    use App\Helpers\Helpers;
    use App\Models\Contact\Phone;
    use App\Models\Contact\Sociallink;
    use App\Models\Course\Course;
    use App\Models\Locations\City;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\View\View;

    /**
     * Created by PhpStorm.
     * User: artj
     * Date: 18.01.18
     * Time: 11:10
     */
    class ModalFormTeacherComposer
    {

        /**
         * Bind data to the view.
         *
         * @param  View $view
         *
         * @return void
         */
        public function compose(View $view)
        {
            $courses = Cache::remember('courses-' . md5(serialize(request()->all()) . '-teacher'),
                env('CACHE_LIFETIME', 15), function () {
                    return Course::where([
                            'active' => true,
                        ])->whereHas('courses_cities', function ($query) {
                            $query->where('city_id', Helpers::getCurrentCity());
                        })->orderBy('position')->get();
                });

            $view->with([
                'courses' => $courses,
            ]);
        }

    }