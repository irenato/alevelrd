<?php

    namespace App\Http\ViewComposers;


    use App\Helpers\Helpers;
    use App\Models\Contact\Phone;
    use App\Models\Contact\Sociallink;
    use App\Models\Course\Action;
    use App\Models\Course\Course;
    use App\Models\Locations\City;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\View\View;

    /**
     * Created by PhpStorm.
     * User: artj
     * Date: 18.01.18
     * Time: 11:10
     */
    class LayoutAppComposer
    {
        /**
         * Bind data to the view.
         *
         * @param  View $view
         *
         * @return void
         */
        public function compose(View $view)
        {
            $gtm          = Cache::remember('value-' . md5(serialize(request()->all()) . '-gtm'),
                env('CACHE_LIFETIME', 15), function () {
                    return Helpers::getSeoscript('gtm');
                });
            $fb_pixels    = Cache::remember('value-' . md5(serialize(request()->all()) . '-fb_pixels'),
                env('CACHE_LIFETIME', 15), function () {
                    return Helpers::getSeoscript('fb_pixels');
                });
            $cities       = Cache::remember('cities-' . md5(serialize(request()->all()) . '-cities'),
                env('CACHE_LIFETIME', 15), function () {
                    return City::get();
                });
            $city_data    = City::where([
                'id'     => Helpers::getCurrentCity(),
                'active' => 1,
            ])->first();
            $admin_email  = Cache::remember('admin_email-' . md5(serialize(request()->all()) . '-admin_email'),
                env('CACHE_LIFETIME', 15), function () {
                    return Helpers::getConfig('admin_email');
                });
            $action       = Cache::remember('actions-' . md5(serialize(request()->all()) . '-actions'),
                env('CACHE_LIFETIME', 15), function () {
                    return Action::where('active', true)->where('end_date', '>', Carbon::now())->first();
                });
            $courses      = Cache::remember('courses-' . md5(serialize(request()->all()) . '-courses'),
                env('CACHE_LIFETIME', 15), function () {
                    return Course::with('type')->where([
                        'active' => true,
                    ])->whereHas('courses_cities', function ($query) {
                        $query->where('city_id', Helpers::getCurrentCity());
                    })->orderBy('position')->get();
                });
            $view->with([
                'gtm'          => $gtm,
                'action'       => $action,
                'fb_pixels'    => $fb_pixels,
                'cities'       => $cities,
                'courses'      => $courses,
                'city_data'    => $city_data,
                'admin_email'  => $admin_email,
            ]);
        }

    }