<?php
    /**
     * Created by PhpStorm.
     * User: bredevil
     * Date: 19.01.2019
     * Time: 15:04
     */

    namespace App\Http\Helpers;


    use App\Models\Course\Course;
    use App\Models\Test\Test;
    use Illuminate\Support\Facades\Cache;

    class TestHelpers
    {
        /**
         * @param $answer
         *
         * @return array
         */
        public static function prepareAnswersData()
        {
            $coursesAnswers = [];
            Cache::remember('tests-' . md5(serialize(request()->all())),
                env('CACHE_LIFETIME', 15), function () use (&$coursesAnswers) {
                    Course::with(['test_answers', 'courses_ttypes'])->whereHas('courses_ttypes', function ($query) {
                        return $query->whereId(2);
                    })
                        ->get()->each(function ($course) use (&$coursesAnswers) {
                            return $coursesAnswers['humanitarian'][$course->id] = $course->test_answers->pluck('id')->toArray();
                        });
                    Course::with(['test_answers', 'courses_ttypes'])->whereHas('courses_ttypes', function ($query) {
                        return $query->whereId(3);
                    })
                        ->get()->each(function ($course) use (&$coursesAnswers) {
                            return $coursesAnswers['technical'][$course->id] = $course->test_answers->pluck('id')->toArray();
                        });
                });
            return $coursesAnswers;
        }

        /**
         * @param $testId
         * @param $answerId
         *
         * @return mixed
         */
        public static function check($testId, $answerId)
        {
            $answers = json_decode(session()->get('answers'), true);
            // type of tests - humanitarian or technical
            $type = '';
            // all tests answers
            $coursesAnswers = self::prepareAnswersData();
            if (!$answers) {
                $answers = [];
            }
            if ($testId) {
                $answers[$testId] = $answerId;
                $amountAnswers    = count($answers);
                // saving current answers to session
                session()->put('answers', json_encode($answers));
                foreach ($coursesAnswers['humanitarian'] as $course_id => $courseAnswers) {
                    $coincidence = 0;
                    if ($amountAnswers == 9) {
                        $basicly = (array_slice($courseAnswers, 0, 9));

                        $coincidence = count(array_intersect($basicly, $answers));

                        if ((($coincidence - 2) < $amountAnswers && $amountAnswers < ($coincidence + 3))) {
                            // WD
                            if (count($basicly) == count($courseAnswers)) {
                                return ['courseId' => $course_id];
                            }
                            // 10.01 - begin humanitarian
                            return ['testId' => Test::with('answers')->whereHas('tests_ttypes', function ($query) {
                                return $query->whereId(2);
                            })->first()->id];
                        }
                    } elseif ($amountAnswers > 9) {
                        // only humanitarian
                        $humanitarian        = (array_slice($courseAnswers, 9));
                        $humanitarianAnswers = (array_slice($answers, 9));
                        // similarity of user`s answers & answers of course
                        $coincidence         = count(array_intersect($humanitarian, $humanitarianAnswers));
                        if (count(array_intersect($courseAnswers, $answers)) == count($courseAnswers)) {
                            return ['courseId' => $course_id];
                        }
                    }
                    if (!isset($amounts['humanitarian'])) {
                        $amounts['humanitarian'] = [];
                    }
                    // user selected the path of humanities...
                    if ($coincidence > 0 && $amountAnswers > 9) {
                        $type = 'humanitarian';
                    }
                    array_push($amounts['humanitarian'], count($courseAnswers));
                }
                if (!$type) {
                    foreach ($coursesAnswers['technical'] as $course_id => $courseAnswers) {
                        $coincidence = 0;
                        if ($amountAnswers == 9) {
                            return ['testId' => Test::with('answers')->whereHas('tests_ttypes', function ($query) {
                                return $query->whereId(3);
                            })->first()->id];
                        } elseif ($amountAnswers > 9) {
                            // only technical
                            $technical        = (array_slice($courseAnswers, 9));
                            $technicalAnswers = (array_slice($answers, 9));
                            // similarity of user`s answers & answers of course
                            $coincidence = count(array_intersect($technical, $technicalAnswers));
                            if (count(array_intersect($courseAnswers, $answers)) == count($courseAnswers)) {
                                return ['courseId' => $course_id];
                            }
                            if (!isset($amounts['technical'])) {
                                $amounts['technical'] = [];
                            }
                            // user selected the path of humanities...
                            if (isset($coincidence) && $coincidence > 0) {
                                $type = 'technical';
                            }
                            array_push($amounts['technical'], count($courseAnswers));
                        }
                    }
                }
                if ($type && max($amounts[$type]) == $amountAnswers) {
                    return ['courseId' => self::sortAnswers($coursesAnswers[$type], $answers)];
                } elseif (isset($amounts['technical']) && max($amounts['technical']) == $amountAnswers) {
                    $humanCourse = self::sortAnswers($coursesAnswers['humanitary'], $answers);
                    $techCourse  = self::sortAnswers($coursesAnswers['technical'], $answers);
                    return ['courseId' => $humanCourse >= $techCourse ? $humanCourse : $techCourse];
                }
            }
        }

        /**
         * @param $coursesAnswers
         * @param $answers
         *
         * @return mixed
         */
        public static function sortAnswers($coursesAnswers, $answers)
        {
            $data = [];
            foreach ($coursesAnswers as $course_id => $courseAnswers) {
                $data[$course_id] = count(array_intersect($courseAnswers, $answers));
            }
            if ($data) {
                return array_keys($data, max($data))[0];
            }
        }
    }