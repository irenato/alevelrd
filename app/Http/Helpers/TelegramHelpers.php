<?php

    namespace App\Helpers;

    use App\Models\Setting\Telegramconfig;
    use Telegram\Bot\Api;

    class TelegramHelpers
    {
        /**
         * @param $key
         * @param $title
         * @param $data
         *
         * @return \Telegram\Bot\Objects\Message
         */
        public static function send($key, $title, $data)
        {
            $message  = view('chunks.telegram.message', [
                'title' => $title,
                'data'  => $data,
            ])->render();
            $config   = Telegramconfig::where('key', $key)->first();
            $telegram = new Api($config->bot_id);
            $ids      = explode(',', $config->ids);
            try {
                foreach ($ids as $id) {
                    $telegram->sendMessage([
                        'chat_id'    => trim($id),
                        'text'       => $message,
                        'parse_mode' => 'html',
                    ]);
                }
            } catch (\Exception $e) {
                echo $e;
            }
        }
    }