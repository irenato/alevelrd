<?php

namespace App\Helpers;

use App\Models\Course\Action;
use App\Models\Course\Course;
use App\Models\Locations\City;
use App\Models\Redirect\Redirect;
use App\Models\Seo\Seoscript;
use App\Models\Setting\Setting;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\Teacher\Teacher;
use Spatie\ImageOptimizer\OptimizerChain;

class Helpers
{
    /**
     * get setting
     *
     * @param $request
     *
     * @return string
     */
    public static function getConfig($request)
    {
        $setting = Setting::where('key', $request)->first();
        return isset($setting) ? $setting->value : '';
    }

    /**
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|int|mixed
     */
    public static function getCurrentCity()
    {
        if (!session()->has('city')) {
            session(['city' => City::DEFAULT_CITY]);
        }

        return session('city');
    }

    /**
     * @param $courses
     *
     * @return mixed
     */
    public static function checkActionOutDate($courses)
    {
        foreach ($courses as $course) {
            if ($course->is_action_enabled && $course->days_left == 0) {
                $course->is_action_enabled = false;
                $course->update();
            }
        }
        return $courses;
    }

    /**
     * @param $image
     * @param $drive
     * @param $image_old
     *
     * @return string
     */
    public static function saveImage($image, $drive, $image_old)
    {
        if ($image) {
            $result = md5($image->getClientOriginalName()) . '.'
                . $image->getClientOriginalExtension();
            Storage::disk($drive)
                ->put(File::get($image));
            if ($image_old) {
                Storage::delete('public/images/' . $drive . '/' . $image_old);
            }
        } else {
            $result = $image_old;
        }
        return $result;
    }

    /**
     * @param $key
     *
     * @return string
     */
    public static function getSeoscript($key)
    {
        $data = Seoscript::where([
            'key'    => $key,
            'active' => 1,
        ])->first();
        return isset($data) ? $data->value : '';
    }

    /**
     * уникальный алиас
     *
     * @param     $model
     * @param     $alias
     * @param int $step
     *
     * @return mixed
     */
    public static function checkAlias($model, $alias, $step = 0, $id = false)
    {
        if (class_exists($model)) {
            if ($id
                && $model::where(['alias' => $alias, 'id' => $id])->exists()
            ) {
                return $alias;
            }
            if ($model::where('alias', $alias)->exists()) {
                return self::checkAlias($model, $alias . '-' . ++$step, $step);
            }
            return $alias;
        }
    }

    /**
     * Кеш изображений
     */
    public static function getImageCache($path, $width, $height, $folder = null)
    {
        if (!File::exists(public_path() . '/' . $path)
            || !is_file(public_path() . '/' . $path)
        ) {
            $path = '/assets/images/noimage.png';
        }

        $ext = explode('.', $path);
        if (end($ext) == 'svg') {
            return $path;
        }
        $name = md5($path . $width . $height) . '.' . end($ext);
        if (!File::exists(public_path() . '/assets/images/cache')) {
            File::makeDirectory(public_path() . '/assets/images/cache', 0777);
        }
        if ($folder != null
            && !File::exists(public_path() . '/assets/images/cache/' . $folder)
        ) {
            File::makeDirectory(public_path() . '/assets/images/cache/'
                . $folder, 0777);
        }
        $filename = public_path() . '/assets/images/cache/' . ($folder != null
                ? $folder . '/' : '') . $name;
        if (!File::exists($filename)) {
            Image::make(public_path() . '/' . $path)->fit($width, $height)
                ->save($filename);
        }
        return asset('/assets/images/cache/' . ($folder != null ? $folder . '/'
                : '') . $name);
    }

    /**
     * @param $data
     *
     * @return array
     */
    public static function getJsonData($data)
    {
        $var = get_object_vars($data);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }

    /**
     * @param $phone
     *
     * @return mixed
     */
    public static function filterPhone($phone)
    {
        return str_replace([' ', '-', '(', ')'], '', $phone);
    }

    /**
     *
     */

    public static function prepareConData($data)
    {
        foreach ($data as $item) {
            $item = (array)$item;
        }
        return json_encode((array)$data);
    }

    /**
     * @param $data
     *
     * @return array
     */
    public static function mutateLinks($data)
    {
        $links = [];
        if ($data) {
            $i = -1;
            foreach ($data['link'] as $item) {
                if ($item) {
                    $links[++$i] = [
                        $data['html_class'][$i] => $item,
                    ];
                }
            }
        }
        return $links;
    }

    /**
     * @return int
     */
    public static function getDiscount()
    {
        $action = Cache::remember('actions-' . md5(serialize(request()->all())),
            env('CACHE_LIFETIME', 15), function() {
                return Action::where([
                    'active' => true,
                ])
                    ->where('end_date', '>', date('Y-m-d H:i:s'))
                    ->first();
            });

        return $action ? $action->discount : 0;
    }

    /**
     * @param $array
     *
     * @return array
     */
    public static function arrayFilter($array)
    {
        return array_filter($array, function($item) {
            return $item != "";
        });
    }

    /**
     * @param $events
     * @param $courses
     *
     * @return Collection
     */
    public static function prepareCalendarData($events, $courses)
    {
        $data = [];
        if ($events) {
            foreach ($events as $event) {
                array_push($data, [
                    'title'               => $event->title,
                    'description'         => $event->filteredIntrotext,
                    'begin'               => $event->begin,
                    'smallImage'          => $event->smallImage,
                    'bigImage'            => $event->bigImage,
                    'fullLink'            => $event->fullLink,
                    'is_external'         => $event->is_external,
                    'filteredDescription' => $event->filteredDescription,
                    'filteredIntrotext'   => $event->filteredIntrotext,
                ]);
            }
        }
        if ($courses) {
            foreach ($courses as $course) {
                array_push($data, [
                    'title'               => $course->title,
                    'description'         => 'Старт курса ' . $course->title
                        . ' ожидается '
                        . date('d.m.Y', strtotime($course->start_date))
                        . '. <br> Приходи, мы ждем тебя!',
                    'begin'               => $course->start_date,
                    'smallImage'          => $course->smallImage,
                    'bigImage'            => $course->bigImage,
                    'fullLink'            => $course->fullLink,
                    'filteredDescription' => $course->filteredDescription,
                    'filteredIntrotext'   => $course->filteredIntrotext,
                    'is_external'         => 0,
                ]);
            }
        }
        return collect($data)->sortBy(function ($event) {
            return strtotime($event['begin']);
        })->values()->all();
    }

    /**
     * @return mixed
     */
    public static function showRedirects()
    {
        return Redirect::get()->pluck('source', 'target');
    }

    /**
     * Create lead in bx24
     *
     * @param $data
     */
    public static function createBxLead($data)
    {
        $queryUrl
                  = 'https://24.a-level.com.ua/rest/1/i28z211ul3188j4k/crm.lead.add.json';
        $userName = explode(' ', $data['username']);
        $course   = Course::find($data['course_id']);
        // если bx_id не найден - статус "Еще не выбрал"
        $courseIdBX = $course->bx_id ? $course->bx_id : 1036;
        $queryData  = http_build_query([
            'fields' => [
                'TITLE'             => $data['username'],
                'NAME'              => $userName[0],
                'LAST_NAME'         => isset($userName[1]) ? $userName[1] : '',
                'ASSIGNED_BY_ID'    => 532,
                'UF_CRM_1524745771' => $courseIdBX,
                'COMMENTS'          => '',
                "STATUS_ID"         => "NEW",
                "OPENED"            => "Y",
                "SOURCE_ID"         => "WEB",
                "PHONE"             => [
                    [
                        "VALUE"      => $data['phone'],
                        "VALUE_TYPE" => "MOBILE",
                    ],
                ],
                "EMAIL"             => [
                    [
                        "VALUE"      => $data['email'],
                        "VALUE_TYPE" => "HOME",
                    ],
                ],
            ],
        ]);
        $curl       = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST           => 1,
            CURLOPT_HEADER         => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $queryUrl,
            CURLOPT_POSTFIELDS     => $queryData,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, 1);
        if (array_key_exists('error', $result)) {
            echo "Ошибка при сохранении лида: " . $result['error_description']
                . "";
        }
    }

    public static function optimize(string $currentPath): string
    {
        return app(OptimizerChain::class)->optimize($currentPath,
            storage_path('/optimized/') . $currentPath);
    }
}