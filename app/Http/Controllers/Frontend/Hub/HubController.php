<?php

    namespace App\Http\Controllers\Frontend\Hub;


use App\Helpers\TelegramHelpers;
use App\Http\Controllers\Controller;
use App\Models\Hub\Hub;
use App\Models\Hub\Hub_image;
use App\Models\Hub\Hub_message;
use App\Models\Hub\Hub_room;
use Illuminate\Http\Request;


class HubController extends Controller
{
    //
    public function index(Request $request)
    {
        $data = Hub::first();
        $rooms = Hub_room::where('room_key','!=','allroom')->get();
        $all_room_info = Hub_room::where('room_key','allroom')->first();
        $images = Hub_image::all();

        return view('frontend.hub.index',['data'=>$data,'rooms' => $rooms,'images' => $images,'all_room_info' => $all_room_info]);
    }

    public function message(Request $request)
    {
        $message = New Hub_message();
        TelegramHelpers::send('callback_form', 'Новая заявка "HUB"', $request);
        $message->name = $request->name;
        $message->email = $request->email;
        $message->tel = $request->tel;
        $message->text = $request->text;
        $message->save();

    }
}
