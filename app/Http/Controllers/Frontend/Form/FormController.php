<?php

namespace App\Http\Controllers\Frontend\Form;

use App\Helpers\Helpers;
use App\Helpers\TelegramHelpers;
use App\Http\Controllers\Backend\Application\TeachapplicationController;
use App\Http\Requests\Application\CallbackapplicationRequest;
use App\Http\Requests\Application\EventapplicationRequest;
use App\Http\Requests\Application\PartnerapplicationRequest;
use App\Http\Requests\Application\StapplicationRequest;
use App\Http\Requests\Application\TeachapplicationRequest;
use App\Http\Requests\Review\ReviewRequest;
use App\Models\Application\Callbackapplication;
use App\Models\Application\Eventapplication;
use App\Models\Application\Partnerapplication;
use App\Models\Application\Stapplication;
use App\Models\Application\Teachapplication;
use App\Models\Event\Event;
use App\Models\Review\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FormController extends Controller
{
    /**
     * @param StapplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addFeedback(StapplicationRequest $request)
    {
        if ($request->ajax()) {
            Helpers::createBxLead($request->all());
            TelegramHelpers::send('from_student', 'Новая заявка (студент)',
                $request);
            if (Stapplication::create($request->all())) {
                return response()->json([
                    'title'   => __('messages.callback_form_success_title'),
                    'message' => __('messages.callback_form_success'),
                ], 200);
            }
        }
    }

    /**
     * @param StapplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCallback(CallbackapplicationRequest $request)
    {
        if ($request->ajax()) {
            TelegramHelpers::send('callback_form',
                'Сообщение с формы обратной связи', $request);
            if (Callbackapplication::create($request->all())) {
                return response()->json([
                    'title'   => __('messages.callback_form_success_title'),
                    'message' => __('messages.callback_form_success'),
                ], 200);
            }
        }
    }

    /**
     * @param StapplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTeacher(TeachapplicationRequest $request)
    {
        if ($request->ajax()) {
            TelegramHelpers::send('from_teacher', 'Новая заявка (препод)',
                $request);
            if (Teachapplication::create($request->all())) {
                return response()->json([
                    'title'   => __('messages.callback_form_success_title'),
                    'message' => __('messages.callback_form_success'),
                ], 200);
            }
        }
    }

    /**
     * @param PartnerapplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPartner(PartnerapplicationRequest $request)
    {
        if ($request->ajax()) {
            TelegramHelpers::send('from_partner', 'Новая заявка (партнер)',
                $request);
            if (Partnerapplication::create($request->all())) {
                return response()->json([
                    'title'   => __('messages.callback_form_success_title'),
                    'message' => __('messages.callback_form_success'),
                ], 200);
            }
        }
    }

    /**
     * @param Eventapplication $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEventApplication(EventapplicationRequest $request)
    {
        if ($request->ajax()) {
            TelegramHelpers::send('from_partner',
                'Новая заявка (' . Event::find($request->event_id)->title . ')',
                $request);
            if (Eventapplication::create($request->all())) {
                return response()->json([
                    'title'   => __('messages.callback_form_success_title'),
                    'message' => __('messages.callback_form_success'),
                ], 200);
            }
        }
    }


    /**
     * @param ReviewRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addReview(ReviewRequest $request)
    {
        if ($request->ajax()) {
            TelegramHelpers::send('callback_form', 'Новый отзыв', $request);
            $review = Review::create([
                'username'     => $request->username,
                'email'        => $request->email,
                'course_id'    => $request->course_id,
                'review'       => $request->review,
                'active'       => false,
                'from_student' => $request->from_student,
            ]);
            if ($request->thumbnail) {
                $review->thumbnail
                    = md5($request->thumbnail->getClientOriginalName()) . '.'
                    . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('reviews')
                    ->put($review->id . '/' . $review->thumbnail,
                        File::get($request->thumbnail));
                $review->update();
            }
            return response()->json([
                'title'   => __('messages.review_form_success_title'),
                'message' => __('messages.review_form_success'),
            ], 200);
        }
    }
}
