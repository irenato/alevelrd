<?php

    namespace App\Http\Controllers\Frontend\Creatorpage;

    use App\Helpers\Helpers;
    use App\Models\Course\Action;
    use App\Models\Course\Course;
    use App\Models\Creator\Creator;
    use App\Models\Discount\Discount;
    use App\Models\Event\Event;
    use App\Models\Page\Creatorpage;
    use App\Models\Page\Homepage;
    use App\Models\Page\Opinion;
    use App\Models\Partner\Partner;
    use App\Models\Reward\Reward;
    use App\Models\Teacher\Teacher;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CreatorpageController extends Controller
    {
        /**
         * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
         */
        public function index()
        {
            return view('frontend.creators.index', [
                'data'      => Creatorpage::first()
            ]);
        }

        /**
         * Учителя
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function getTeam(Request $request)
        {
            if ($request->ajax()) {
                return response()->json([
                    'data' => Creator::orderBy('position')->get(),
                ], 200);
            }
        }

        /**
         * Партнеры
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function getPartners(Request $request)
        {
            if ($request->ajax()) {
                return response()->json([
                    'data' => Partner::orderBy('position')->get(),
                ], 200);
            }
        }

        /**
         * Мероприятия
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function getEvents(Request $request)
        {
            if ($request->ajax()) {
                return response()->json([
                    'data' => Event::with(['events_teachers'])
                        ->where('begin', '>', Carbon::now())
                        ->orderBy('begin')->get(),
                ], 200);
            }
        }

        /**
         * Смена города
         *
         * @param Request $request
         */
        public function updateCity(Request $request)
        {
            $request->session()->put('city', $request->city);
            return redirect()->back();
        }
    }
