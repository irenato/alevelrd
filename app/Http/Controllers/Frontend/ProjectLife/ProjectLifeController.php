<?php

namespace App\Http\Controllers\Frontend\ProjectLife;

use App\Helpers\TelegramHelpers;
use App\Http\Controllers\Controller;
use App\Models\Project_life\Project_life;
use App\Models\Project_life\Project_life_partner;
use App\Models\Project_life\Project_life_slider;
use App\Models\Project_life\Project_life_speaker;
use App\Models\Project_life\Project_programm;
use App\Models\Project_life\Project_to_callback;
use App\Models\Project_life\Projectlife_callbacks;
use App\Models\Project_life\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectLifeController extends Controller
{

    public function index()
    {
        if (Auth::check()) {
            $data  = Project_life::first();
            $count = count(Projectlife_callbacks::all()) + $data->amount;
            return view('frontend.projectLife.index', [
                'data'      => $data,
                'sliders'   => Project_life_slider::all(),
                'speakers'  => Project_life_speaker::all(),
                'programms' => Project_programm::with('speakers')
                    ->orderBy('time', 'asc')->get(),
                'time'      => Time::all(),
                'workshop'  => Project_programm::where('workshop', 1)
                    ->with('callback')->get(),
                'partners'  => Project_life_partner::all(),
                'count'     => $count,
            ]);
        } else {
            abort(404);
        }
    }


    public function register(Request $request)
    {
        $chek = Projectlife_callbacks::where('phone', $request->phone)->first();
        if ($chek) {
            return;
        }
        $new          = new Projectlife_callbacks();
        $new->name    = $request->name;
        $new->surname = $request->surname;
        $new->phone   = $request->phone;
        $new->email   = $request->email;
        $new->save();

        isset($request->workshop1) ? $new->workshop()
            ->attach($request->workshop1) : '';
        isset($request->workshop2) ? $new->workshop()
            ->attach($request->workshop2) : '';
        isset($request->workshop3) ? $new->workshop()
            ->attach($request->workshop3) : '';
        isset($request->workshop4) ? $new->workshop()
            ->attach($request->workshop4) : '';
        echo 'succes';

        TelegramHelpers::send('from_project', 'Новая заявка (Жизнь проекта)',
            $request);

//        return response (\GuzzleHttp\json_encode(['status'=>'ok']), 200)->header('Content-Type', 'application/json');

    }

}
