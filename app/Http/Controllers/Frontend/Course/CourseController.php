<?php

namespace App\Http\Controllers\Frontend\Course;

use App\Helpers\Helpers;
use App\Http\Controllers\Frontend\Redirect\RedirectController;
use App\Http\ViewComposers\ModalFormFeedbackComposer;
use App\Models\Course\Action;
use App\Models\Course\Course;
use App\Models\Locations\City;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public function index(Request $request, $alias)
    {
        $course = Course::with([
            'courses_teachers',
            'programs',
            'images',
            'reviews',
        ])
            ->where([
                'alias'  => $alias,
                'active' => 1,
            ])->first();
        if ($course) {
            $courses = Course::where([
                'active' => true,
            ])->whereHas('courses_cities', function ($query) {
                $query->where('city_id', Helpers::getCurrentCity());
            })->orderBy('position')->get();
            return view('frontend.course.index', [
                'courseData' => $course,
                'courses'    => $courses,
                'cities'     => City::orderBy('title')->get(),
                'action'     => Action::where('active', true)->where('end_date', '>', Carbon::now())->first(),
            ]);
        }else{
            return app(RedirectController::class)->index($request->path());
        }
    }
}
