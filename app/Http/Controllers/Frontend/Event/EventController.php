<?php

    namespace App\Http\Controllers\Frontend\Event;

    use App\Helpers\Helpers;
    use App\Models\Course\Course;
    use App\Models\Event\Event;
    use App\Models\Partner\Partner;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;

    class EventController extends Controller
    {
        public function index(Request $request, $alias)
        {
            if (Auth::check()) {
                $where = ['alias'  => $alias];
            }else{
                $where = [
                    'alias'  => $alias,
                    'active' => 1,
                ];
            }
            $event = Event::with([
                'events_teachers',
            ])->where($where)->firstOrFail();
            return view('frontend.events.' . explode('.', $event->template)[0], [
                'event'    => $event,
                'partners' => Partner::orderBy('position')->get(),
            ]);
        }
    }
