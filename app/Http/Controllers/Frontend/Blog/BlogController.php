<?php

namespace App\Http\Controllers\Frontend\Blog;

use App\Helpers\TelegramHelpers;
use App\Http\Requests\Blog\OfferArticleRequest;
use App\Http\Requests\Blog\SubscriberRequest;
use App\Models\Blog\Article;
use App\Models\Blog\Author;
use App\Models\Blog\Blog;
use App\Models\Blog\Subscriber;
use App\Repositories\Blog\BlogRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

class BlogController extends Controller
{

    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new BlogRepository($request);
    }

    public function index(Request $request)
    {
        $query              = Article::with($this->repository::MAIN_RELATIONS)
            ->whereActive(true);
        $recentlyArticles   = (clone $query)->latest()->limit(3)->get();
        $mostViewedArticles = (clone $query)->orderBy('viewed', 'DESC')
            ->limit(3)->get();
        $articles           = (clone $query)->whereNotIn('id',
            $recentlyArticles->merge($mostViewedArticles)->pluck('id'))
            ->paginate(10);

        return view('frontend.blog.index', [
            'blog' => Blog::first(),
            'recentlyArticles'   => $recentlyArticles,
            'mostViewedArticles' => $mostViewedArticles,
            'ourArticles'        => (clone $query)->whereIsOuter(false)
                ->latest()
                ->limit(4)->get(),
            'articles'           => $articles,
        ]);
    }

    public function inner(Request $request, string $alias): View
    {
        return view('frontend.blog.inner',
            array_merge($this->repository->viewInner($alias), [
                'i' => -1,
            ])
        );
    }

    public function likes(Request $request): JsonResponse
    {
        return response()->json($this->repository->likes());
    }

    public function subscribe(SubscriberRequest $request): JsonResponse
    {
        Subscriber::create($request->all());
        return response()->json([
            'title' => __('messages.subscribe_form_success'),
        ]);
    }

    public function offerArticle(OfferArticleRequest $request): JsonResponse
    {
        $this->repository->offerArticle();
        TelegramHelpers::send('callback_form', 'Предложение новой статьи',
            $request);
        return response()->json([
            'title'   => __('messages.subscribe_offer_article_success'),
            'message' => __('messages.subscribe_offer_article_message'),
        ]);
    }
}
