<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Helpers\Helpers;
use App\Models\Course\Action;
use App\Models\Course\Course;
use App\Models\Discount\Discount;
use App\Models\Event\Event;
use App\Models\Page\Homepage;
use App\Models\Page\Opinion;
use App\Models\Partner\Partner;
use App\Models\Review\Review;
use App\Models\Reward\Reward;
use App\Models\Teacher\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Helper\Helper;

class HomeController extends Controller
{
    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        $courses   = Cache::remember('courses-main-'
            . md5(serialize(request()->all()) . '-' . session('city')),
            env('CACHE_LIFETIME', 15), function() {
                return Course::with(['courses_teachers', 'type'])->where([
                    'active' => true,
                ])->whereHas('courses_cities', function($query) {
                    $query->where('city_id', Helpers::getCurrentCity());
                })->orderBy('position')->get();
            });
        $discounts = Cache::remember('discounts-main-'
            . md5(serialize(request()->all()) . '-discounts'),
            env('CACHE_LIFETIME', 15), function() {
                return Discount::orderBy('id', 'asc')->get();
            });
        $rewards   = Cache::remember('rewards-main-'
            . md5(serialize(request()->all()) . '-rewards'),
            env('CACHE_LIFETIME', 15), function() {
                return Reward::orderBy('position')->get();
            });
        $nearbyCourses
                   = Cache::remember('nearbyCourses-'
            . md5(serialize(request()->all())),
            env('CACHE_LIFETIME', 15), function() {
                return Course::query()
                    ->whereNotNull('start_date')
                    ->where('start_date', '>', Carbon::now())
                    ->whereActive(true)
                    ->orderBy('start_date')
                    ->limit(3)
                    ->get();
            });
        $reviews   = Cache::remember('reviews-main-'
            . md5(serialize(request()->all()) . '-reviews'),
            env('CACHE_LIFETIME', 15), function() {
                return Review::inRandomOrder()->get();
            });
        $courses   = Helpers::checkActionOutDate($courses);
        return view('frontend.home.index', [
            'action'        => Action::where('active', true)
                ->where('end_date', '>', Carbon::now())->first(),
            'data'          => Homepage::first(),
            'courses'       => $courses,
            'discounts'     => $discounts,
            'rewards'       => $rewards,
            'reviews'       => $reviews,
            'nearbyCourses' => $nearbyCourses,
        ]);
    }

    /**
     * Учителя
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeachers(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'data' => Teacher::with(['courses_teachers'])
                    ->where('active', 1)
                    ->where('is_teacher', 1)
                    ->inRandomOrder()
                    ->get(),
            ], 200);
        }
    }

    /**
     * Партнеры
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPartners(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'data' => Partner::inRandomOrder()->get(),
            ], 200);
        }
    }

    /**
     * Мероприятия
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEvents(Request $request)
    {
        if ($request->ajax()) {
            $events = Event::with(['events_teachers'])
                ->where('begin', '>', Carbon::now())
                ->where('city_id', Helpers::getCurrentCity());
            if (!Auth::check()) {
                $events = $events->whereActive(true);
            }
            $events  = $events->orderBy('begin')->get();
            $courses = Course::with(['courses_teachers'])
                ->where('start_date', '>', Carbon::now())
                ->whereHas('courses_cities', function($query) {
                    $query->where('city_id', Helpers::getCurrentCity());
                })->whereActive(true)
                ->orderBy('start_date')->get();
            return response()->json([
                'data' => Helpers::prepareCalendarData($events, $courses),
            ], 200);
        }
    }

    /**
     * Смена города
     *
     * @param Request $request
     */
    public function updateCity(Request $request)
    {
        $request->session()->put('city', $request->city);
        return redirect()->back();
    }
}
