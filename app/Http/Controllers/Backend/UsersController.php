<?php

    namespace App\Http\Controllers;

    use App\Http\Requests\User\UserRequest;
    use App\Models\User\Role;
    use App\Models\User\User;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Auth;
    use Illuminate\Support\Facades\Hash;

    class UsersController extends Controller
    {

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $users = User::with(['roles_users']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $users
                                    ->where('active', $value);
                                break;
                            default:
                                $users = $users
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.users.index', [
                'users' => $users->paginate(50),
                'roles' => Role::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.users.create', [
                'roles' => Role::get(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(UserRequest $request)
        {
            $user           = new User();
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            $user->roles_users()->sync([$request->role_id]);
            return redirect('backend/users')->with('success', 'Пользователь успешно добавлен.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.users.edit', [
                'user'  => User::find($id),
                'roles' => Role::get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(UserRequest $request, $id)
        {
            $user        = User::find($id);
            $user->name  = $request->name;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = Hash::make($request->password);
            }
            $user->update();
            $user->roles_users()->sync([$request->role_id]);
            return redirect('backend/users')->with('success', 'Пользователь успешно обнолен.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $user = User::find($id);
            $user->roles_users()->detach();
            $user->delete();
            return redirect('backend/users/')
                ->with('success', 'Запись успешно удалена');
        }
    }
