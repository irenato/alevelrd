<?php

    namespace App\Http\Controllers\Backend\Event;

    use App\Helpers\Helpers;
    use App\Http\Requests\Event\EventImageRequest;
    use App\Models\Event\Event;
    use App\Models\Event\EventImage;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class EventImageController extends Controller
    {
        /**
         * Загрузка изображений
         *
         * @param Request $request
         * @param         $id
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function uploadImage(EventImageRequest $request, $id)
        {
            $filename = md5($request->file->getClientOriginalName()) . '.' . $request->file->getClientOriginalExtension();
            Storage::disk('events')
                ->put($id . '/' . $filename, File::get($request->file));
            $image           = new EventImage();
            $image->event_id = $id;
            $image->name     = $filename;
            $image->sort     = 1;
            $image->save();
            return response()->json(['status' => true]);
        }

        /**
         * Получение изображений
         */
        public function getImages(Request $request, $id)
        {
            $res   = '';
            $event = Event::with('images')->find($id);
            if ($event->images->count() > 0) {
                foreach ($event->images as $image) {
                    $imageName   = explode('/', $image->name);
                    $dateCreated = Carbon::parse($image->created_at)->toFormattedDateString();
                    $res         .= view('backend.events.image', [
                        'image'       => $image,
                        'course'      => $event,
                        'imageName'   => end($imageName),
                        'dateCreated' => $dateCreated,
                    ])->render();
                }
            }
            return response()->json(['images' => $res]);
        }


        /**
         * Удаление изображения
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function deleteImage(Request $request)
        {
            $image = EventImage::find($request->id);
            Storage::delete('public/images/events/' . $image->event_id . '/' . $image->name);
            $image->delete();
            return response()->json(['status' => true]);
        }

        /**
         * Получение информации о изображениии
         */
        public function getImage(Request $request)
        {
            $image      = EventImage::find($request->id);
            $image->src = Helpers::getImageCache(Storage::disk('events')->url($image->event_id . '/' . $image->name), 600, 400, true);
            return response()->json($image->toArray());
        }

        /**
         * Сохранение информации о изображении
         */
        public function setImage(Request $request)
        {
            $image        = EventImage::find($request->image['id']);
            $image->title = $request->image['title'];
            $image->alt   = $request->image['alt'];
            $image->save();
            return response()->json(['status' => true]);
        }

        /**
         * Сортировка изображений
         */
        public function sortImage(Request $request)
        {
            if (count($request->images) > 0) {
                foreach ($request->images as $k => $image) {
                    $image       = EventImage::find($image);
                    $image->sort = $k;
                    $image->update();
                }
            }
            return response()->json(['status' => true]);
        }

        /**
         * Делаем изобаржение превью товара
         */
        public function coverImage(Request $request)
        {
            EventImage::where('event_id', '=', $request->product_id)->update(['cover' => 0]);
            $image        = EventImage::find($request->image_id);
            $image->cover = 1;
            $image->save();
            return response()->json(['status' => true]);
        }
    }
