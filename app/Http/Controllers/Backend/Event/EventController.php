<?php

    namespace App\Http\Controllers\Backend\Event;

    use App\Helpers\Helpers;
    use App\Models\Event\Event;
    use App\Models\Locations\City;
    use App\Models\Teacher\Teacher;
    use Carbon\Carbon;
    use function foo\func;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class EventController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $events = Event::with(['city', 'events_teachers']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value)) {
                        switch ($key) {
                            case 'page':
                            case 'limit':
                                break;
                            case 'active':
                                $events
                                    ->where('active', $value);
                                break;
                            case 'teacher':
                                $events->orWhereHas('events_teachers', function ($query) use ($value) {
                                    $query->where('teacher_id', $value);
                                });
                                break;
                            default:
                                $events
                                    ->where($key, 'like', $value . "%");
                                break;
                        }
                    }
                }
            }
            return view('backend.events.index', [
                'events'   => $events->latest()->paginate(50),
                'teachers' => Teacher::orderBy('first_name')->get(),
                'cities'   => City::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $templates = scandir(resource_path('views/frontend/events'));
            return view('backend.events.create', [
                'teachers'  => Teacher::orderBy('first_name')->get(),
                'templates' => array_filter($templates, function ($template) {
                    return !in_array($template, ['.', '..']);
                }),
                'cities'    => City::get(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $timeline = [];
            $event    = Event::create([
                'title'            => $request->title,
                'cost'             => $request->cost,
                'city_id'          => $request->city_id,
                'template'         => $request->template,
                'alias'            => $request->has('alias') && !is_null($request->alias) ?
                    $request->alias : str_slug($request->title),
                'introtext'        => $request->introtext,
                'description'      => $request->description,
                'active'           => $request->has('active'),
                'is_external'      => $request->has('is_external'),
                'rule_title'       => $request->rule_title,
                'timeline_title'   => $request->timeline_title,
                'rule_description' => $request->rule_description,
                'begin'            => $request->begin,
                'seo_title'        => $request->has('seo_title') && !is_null($request->seo_title) ? $request->seo_title : ($request->filrst_name . ' ' . $request->last_name),
                'seo_robots'       => $request->seo_robots,
                'seo_keywords'     => $request->seo_keywords,
                'seo_canonical'    => $request->seo_canonical,
                'seo_description'  => $request->seo_description,
            ]);
            if ($request->timeline) {
                foreach ($request->timeline as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            switch ($key) {
                                case 'thumbnail':
                                    $timeline[$k][$key] = md5($v->getClientOriginalName()) . '.' . $v->getClientOriginalExtension();
                                    Storage::disk('events')
                                        ->put($event->id . '/' . $timeline[$k][$key], File::get($v));
                                    break;
                                default:
                                    $timeline[$k][$key] = $v;
                                    break;
                            }
                        }
                    }
                }
                if ($timeline) {
                    $event->timeline = json_encode($timeline, JSON_UNESCAPED_UNICODE);
                }
            }
            if ($request->thumbnail) {
                $event->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('events')
                    ->put($event->id . '/' . $event->thumbnail, File::get($request->thumbnail));
            }
            $event->update();
            if ($request->teacher_id) {
                $event->events_teachers()->sync($request->teacher_id);
            }
            return redirect('backend/events/' . $event->id . '/edit')->with('success', 'Запись успешно добавлена.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $event     = Event::with(['events_teachers', 'images'])->find($id);
            $templates = scandir(resource_path('views/frontend/events'));
            return view('backend.events.edit', [
                'event'           => $event,
                'events_teachers' => $event->events_teachers()->pluck('teacher_id')->toArray(),
                'teachers'        => Teacher::orderBy('first_name')->get(),
                'templates'       => array_filter($templates, function ($template) {
                    return !in_array($template, ['.', '..']);
                }),
                'cities'          => City::get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $timeline                = [];
            $event                   = Event::find($id);
            $event->title            = $request->title;
            $event->cost             = $request->cost;
            $event->city_id          = $request->city_id;
            $event->template         = $request->template;
            $event->alias            = $request->has('alias') && !is_null($request->alias) ?
                $request->alias : str_slug($request->title);
            $event->introtext        = $request->introtext;
            $event->active           = $request->has('active');
            $event->is_external      = $request->has('is_external');
            $event->description      = $request->description;
            $event->timeline_title   = $request->timeline_title;
            $event->rule_title       = $request->rule_title;
            $event->rule_description = $request->rule_description;
            $event->begin            = $request->begin;
            $event->seo_title        = $request->seo_title;
            $event->seo_robots       = $request->seo_robots;
            $event->seo_canonical    = $request->seo_canonical;
            $event->seo_description  = $request->seo_description;
            if ($request->thumbnail) {
                $event->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('events')
                    ->put($event->id . '/' . $event->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/events/' . $event->id . '/' . $request->thumbnail_old);
                }
            }
            if ($request->timeline) {
                foreach ($request->timeline as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            switch ($key) {
                                case 'thumbnail':
                                    $timeline[$k][$key] = md5($v->getClientOriginalName()) . '.' . $v->getClientOriginalExtension();
                                    Storage::disk('events')
                                        ->put($event->id . '/' . $timeline[$k][$key], File::get($v));
                                    if (isset($request->timeline['thumbnail_old']) && isset($request->timeline['thumbnail_old'][$k])) {
                                        Storage::delete('public/images/events/' . $event->id . '/' . $request->timeline['thumbnail_old'][$k]);
                                    }
                                    break;
                                case "thumbnail_old":
                                    if (!isset($request->timeline['thumbnail'][$k])) {
                                        $timeline[$k]['thumbnail'] = $v;
                                    }
                                    break;
                                default:
                                    $timeline[$k][$key] = $v;
                                    break;
                            }
                        }
                    }
                }
                if ($timeline) {
                    $event->timeline = json_encode($timeline, JSON_UNESCAPED_UNICODE);
                }
            }
            $event->update();
            if ($request->teacher_id) {
                $event->events_teachers()->sync($request->teacher_id);
            }

            return redirect()->back()->with('success', 'Запись успешно обновлена.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $event = Event::with(['events_teachers', 'images'])->find($id);
            Storage::deleteDirectory('public/images/events/' . $event->id . '/');
            $event->events_teachers()->detach();
            $event->delete();
            return redirect('backend/events/')
                ->with('success', 'Запись успешно удалена');
        }
    }
