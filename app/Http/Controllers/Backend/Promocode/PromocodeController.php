<?php

    namespace App\Http\Controllers\Backend\Promocode;

    use App\Http\Requests\Promocode\PromocodeRequest;
    use App\Models\Promocode\Promocode;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Carbon\Carbon;

    class PromocodeController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return view('backend.promocodes.index', [
                'promocodes' => Promocode::latest()->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.promocodes.create', [
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(PromocodeRequest $request)
        {
            Promocode::create([
                'code'        => $request->code,
                'valid_until' => Carbon::parse($request->valid_until),
                'discount'    => (int)$request->discount,
                'reusable'    => isset($request->reusable),
                'in_percent'  => isset($request->in_percent),
                'active'      => isset($request->active),
            ]);
            return redirect('backend/promocodes/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.promocodes.edit', [
                'promocode' => Promocode::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(PromocodeRequest $request, $id)
        {
            $promocode              = Promocode::find($request->id);
            $promocode->code        = $request->code;
            $promocode->valid_until = Carbon::parse($request->valid_until);
            $promocode->discount    = (int)$request->discount;
            $promocode->reusable    = isset($request->reusable);
            $promocode->in_percent  = isset($request->in_percent);
            $promocode->active      = isset($request->active);
            $promocode->update();
            return redirect('backend/promocodes/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $promocode = Promocode::find($id);
            $promocode->delete();
            return redirect('backend/promocodes/')
                ->with('success', 'Запись удалена успешно');
        }
    }
