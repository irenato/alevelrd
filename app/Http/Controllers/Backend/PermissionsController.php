<?php

    namespace App\Http\Controllers;

    use App\Http\Requests\Permission\PermissionRequest;
    use App\Models\User\Permission;
    use Illuminate\Http\Request;

    class PermissionsController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $permissions = Permission::paginate(10);
            return view('backend.permissions.index', compact('permissions'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.permissions.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param PermissionsRequest|Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(PermissionRequest $request)
        {
            $permission               = new Permission();
            $permission->name         = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description  = $request->description;
            $permission->save();
            return redirect('backend/permissions')->with('success', 'Права доступа успешно созданы.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $permission = Permission::find($id);
            return view('backend.permissions.edit', compact('permission'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(PermissionRequest $request, $id)
        {
            $permission               = Permission::find($id);
            $permission->id           = $id;
            $permission->name         = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description  = $request->description;
            $permission->update();
            return redirect('backend/permissions')->with('success', 'Права доступа успешно обнолена.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }

        /**
         * @param Request $request
         */
        public function remove(Request $request)
        {
            Permission::find($request->id)->delete();
        }
    }
