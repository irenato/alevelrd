<?php

    namespace App\Http\Controllers\Backend\Teacher;

    use App\Helpers\Helpers;
    use App\Http\Requests\Teacher\TeacherRequest;
    use App\Models\Course\Course;
    use App\Models\Teacher\Teacher;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;
    use Symfony\Component\Console\Helper\Helper;

    class TeacherController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $teachers = Teacher::with(['courses_teachers']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $teachers
                                    ->where('active', $value);
                                break;
                            default:
                                $teachers = $teachers
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.teachers.index', [
                'teachers' => $teachers->orderBy('position')->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.teachers.create', [
                'courses' => Course::all(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(TeacherRequest $request)
        {
            $links   = [];
            $teacher = Teacher::create([
                'first_name'      => $request->first_name,
                'last_name'       => $request->last_name,
                'specialization'  => $request->specialization,
                'alias'           => Helpers::checkAlias('App\Models\Teacher\Teacher', $request->has('alias') && !is_null($request->alias) ?
                    $request->alias : str_slug($request->filrst_name . '-' . $request->last_name)),
                'active'          => isset($request->active),
                'is_teacher'      => isset($request->is_teacher),
                'introtext'       => $request->introtext,
                'description'     => $request->description,
                'seo_title'       => $request->has('seo_title') && !is_null($request->seo_title) ? $request->seo_title : ($request->filrst_name . ' ' . $request->last_name),
                'seo_robots'      => $request->seo_robots,
                'seo_keywords'    => $request->seo_keywords,
                'seo_canonical'   => $request->seo_canonical,
                'seo_description' => $request->seo_description,
            ]);
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $teacher->links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            if ($request->logo) {
                $teacher->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('teachers')
                    ->put($teacher->id . '/' . $teacher->logo, File::get($request->logo));
            }
            if ($request->thumbnail) {
                $teacher->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('teachers')
                    ->put($teacher->id . '/' . $teacher->thumbnail, File::get($request->thumbnail));
            }
            $teacher->update();
            if ($request->course_id) {
                $teacher->courses_teachers()->sync($request->course_id);
            }
            return redirect('backend/teachers')->with('success', 'Учитель успешно добавлен.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit(Teacher $teacher)
        {
            return view('backend.teachers.edit', [
                'teacher'         => $teacher,
                'teacher_courses' => $teacher->courses_teachers->pluck('id')->toArray(),
                'courses'         => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(TeacherRequest $request, $id)
        {
            $links                    = [];
            $teacher                  = Teacher::find($id);
            $teacher->first_name      = $request->first_name;
            $teacher->last_name       = $request->last_name;
            $teacher->specialization  = $request->specialization;
            $teacher->active          = isset($request->active);
            $teacher->is_teacher      = isset($request->is_teacher);
            $teacher->alias           = Helpers::checkAlias('App\Models\Teacher\Teacher', $request->has('alias') && !is_null($request->alias) ?
                $request->alias : str_slug($request->filrst_name . '-' . $request->last_name), $request->id);
            $teacher->introtext       = $request->introtext;
            $teacher->description     = $request->description;
            $teacher->seo_title       = $request->seo_title;
            $teacher->seo_robots      = $request->seo_robots;
            $teacher->seo_keywords    = $request->seo_keywords;
            $teacher->seo_canonical   = $request->seo_canonical;
            $teacher->seo_description = $request->seo_description;
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $teacher->links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            if ($request->thumbnail) {
                $teacher->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('teachers')
                    ->put($teacher->id . '/' . $teacher->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/teachers/' . $teacher->id . '/' . $request->thumbnail_old);
                }
            }
            if ($request->logo) {
                $teacher->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('teachers')
                    ->put($teacher->id . '/' . $teacher->logo, File::get($request->logo));
                if ($request->logo_old) {
                    Storage::delete('public/images/teachers/' . $teacher->id . '/' . $request->logo_old);
                }
            }
            $teacher->update();
            if ($request->course_id) {
                $teacher->courses_teachers()->sync($request->course_id);
            }
            return redirect('backend/teachers')->with('success', 'Учитель успешно обновлен.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy(Teacher $teacher)
        {
            Storage::deleteDirectory('public/images/teachers/' . $teacher->id . '/');
            $teacher->courses_teachers()->detach();
            $teacher->delete();
            return redirect('backend/teachers/')
                ->with('success', 'Учитель успешно удален');
        }

        /**
         * Sorting
         *
         * @param Request $request
         */
        public function sort(Request $request)
        {
            foreach ($request->items as $key => $value) {
                $item           = Teacher::find($value);
                $item->position = $key;
                $item->update();
            }
        }
    }
