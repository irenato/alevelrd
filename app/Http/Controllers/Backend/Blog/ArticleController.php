<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Requests\Blog\ArticleRequest;
use App\Models\Blog\Article;
use App\Models\Blog\Author;
use App\Models\Blog\Tag;
use App\Repositories\Article\ArticleRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;

class ArticleController extends Controller
{
    const SEO_ROBOTS
        = [
            'index,follow',
            'noindex,nofollow',
        ];

    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new ArticleRepository($request);
    }

    /**
     * @param Request $request
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request): View
    {
        $articles = Article::with([
            'translations',
            'tags_articles',
            'authors_articles',
        ])->when($request->has('search'), function($query) use ($request) {
            return $query->whereTranslationLike(
                'title', "%" . $request->search . "%"
            )->orWhereTranslationLike(
                'content', "%" . $request->search . "%"
            );
        });
        return view('backend.blog.articles.index', [
            'articles' => $articles->orderBy('position')->paginate(25),
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.blog.articles.create', [
            'authors'    => Author::with('translations')->latest()->get()
                ->pluck('fullName', 'id'),
            'tags'       => Tag::latest()->get()->pluck('name', 'id'),
            'articles'   => Article::with('translations')->get()
                ->pluck('title', 'id'),
            'locales'    => config('translatable.locales'),
            'seo_robots' => array_combine(self::SEO_ROBOTS, self::SEO_ROBOTS),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $article = $this->repository->store();
        return redirect(route('blog.articles.edit',
            $article->id))->with('success', 'Запись успешно создана');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::with([
            'related',
            'translations',
            'tags_articles',
            'authors_articles',
        ])->findOrFail($id);
        return view('backend.blog.articles.edit', [
            'article'    => $article,
            'authors'    => Author::with('translations')->latest()->get()
                ->pluck('fullName', 'id'),
            'tags'       => Tag::latest()->get()->pluck('name', 'id'),
            'articles'   => Article::with('translations')->get()
                ->pluck('title', 'id'),
            'locales'    => config('translatable.locales'),
            'seo_robots' => array_combine(self::SEO_ROBOTS, self::SEO_ROBOTS),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = $this->repository->update($id);
        return redirect(route('blog.articles.edit',
            $article->id))->with('success', 'Запись успешно обновлена');
    }

    /**
     * @param Request $request
     */
    public function sort(Request $request)
    {
        foreach ($request->items as $key => $value) {
            $article           = Article::find($value);
            $article->position = $key;
            $article->update();
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {

        $article = Article::findOrFail($id);
        File::deleteDirectory(storage_path('public/images/articles/' .
            $article->id .
            '/'));
        $article->delete();
        return redirect(route('blog.articles.index'))->with('success',
            'Запись успешно удалена');
    }
}
