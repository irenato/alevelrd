<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Exports\SubscribersExport;
use App\Models\Blog\Subscriber;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        return view('backend.blog.subscribers.index', [
            'subscribers' => Subscriber::latest()->paginate(25),
        ]);
    }

    public function export():
    ?\Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new SubscribersExport(), 'subscribers' . date
            ('Y-m-d') . '.xlsx');
    }
}
