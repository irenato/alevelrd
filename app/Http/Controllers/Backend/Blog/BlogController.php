<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Models\Blog\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::with('translations')->first();
        if ($blog) {
            return redirect(route('blog.blogs-content.edit', $blog->id));
        }
        return redirect(route('blog.blogs-content.create'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.blog.create', [
            'locales'    => config('translatable.locales'),
            'seo_robots' => array_combine(
                ArticleController::SEO_ROBOTS,
                ArticleController::SEO_ROBOTS
            ),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = Blog::create($request->except(['_token']));
        return redirect(route('blog.blogs-content.edit', $blog->id))
            ->with('success', 'Запись успешно создана');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.blog.blog.edit', [
            'locales'    => config('translatable.locales'),
            'blog'       => Blog::findOrFail($id),
            'seo_robots' => array_combine(
                ArticleController::SEO_ROBOTS,
                ArticleController::SEO_ROBOTS
            ),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->update($request->except(['_token', '_method']));
        return redirect(route('blog.blogs-content.edit', $blog->id))
            ->with('success', 'Запись успешно обновлена');
    }
}
