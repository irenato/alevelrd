<?php

    namespace App\Http\Controllers\Backend\Reward;

    use App\Http\Requests\Reward\RewardRequest;
    use App\Models\Reward\Reward;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class RewardController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $rewards = new Reward();
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            default:
                                $rewards = $rewards
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.rewards.index', [
                'rewards' => $rewards->orderBy('position')->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.rewards.create', [
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(RewardRequest $request)
        {
            $reward = Reward::create([
                'title'       => $request->title,
                'description' => $request->description,
            ]);
            if ($request->thumbnail) {
                $reward->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('rewards')
                    ->put($reward->id . '/' . $reward->thumbnail, File::get($request->thumbnail));
                $reward->update();
            }
            return redirect('backend/rewards/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.rewards.edit', [
                'reward' => Reward::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(RewardRequest $request, $id)
        {
            $reward              = Reward::find($id);
            $reward->title       = $request->title;
            $reward->description = $request->description;
            if ($request->thumbnail) {
                $reward->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('rewards')
                    ->put($reward->id . '/' . $reward->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/rewards/' . $reward->id . '/' . $request->thumbnail_old);
                }
            }
            $reward->update();
            return redirect('backend/rewards/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $reward = Reward::find($id);
            Storage::deleteDirectory('public/images/rewards/' . $reward->id . '/');
            $reward->delete();
            return redirect('backend/rewards/')
                ->with('success', 'Запись успешно удалена');
        }

        /**
         * Sorting
         *
         * @param Request $request
         */
        public function sort(Request $request)
        {
            foreach ($request->items as $key => $value) {
                $item           = Reward::find($value);
                $item->position = $key;
                $item->update();
            }
        }
    }
