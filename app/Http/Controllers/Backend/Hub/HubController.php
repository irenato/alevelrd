<?php

    namespace App\Http\Controllers\Backend\Hub;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\Hub\HubPageRequest;
    use App\Models\Hub\Hub;
    use App\Models\Hub\Hub_image;
    use App\Models\Hub\Hub_room;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class HubController extends Controller
    {
        //
        public function index()
        {
            return view('backend.hub.index', ['data' => Hub::first(), 'images' => Hub_image::all()]);
        }

        public function update(HubPageRequest $request)
        {

            if ($request->about_section_thumbnail) {
                $name = md5($request->about_section_thumbnail) . '.' . $request->about_section_thumbnail->getClientOriginalExtension();
                $new  = New Hub_image;
//            $new->image = Storage::disk('hub')->url($name);
                $new->image = '/storage/images/hub/gallery/' . $name;
                Storage::disk('hub')
                    ->put('/gallery/' . $name, File::get($request->about_section_thumbnail));
                $new->save();
            }


            $data = Hub::first();
            if (isset($data)) {
                $data->section_introduction_title       = $request->section_introduction_title;
                $data->section_introduction_description = $request->section_introduction_description;
                $data->section_class_rooms_title        = $request->section_class_rooms_title;
                $data->section_class_rooms_description  = $request->section_class_rooms_description;
                $data->section_get_info_title           = $request->section_get_info_title;
                $data->section_about_us_content         = $request->section_about_us_content;
                $data->phone                            = $request->phone;
                $data->mail                             = $request->mail;
                $data->address                          = $request->address;
                $data->title                            = $request->seo_title;
                $data->seo_keywords                     = $request->seo_keywords;
                $data->seo_robots                       = $request->seo_robots;
                $data->seo_canonical                    = $request->seo_canonical;
                $data->seo_description                  = $request->seo_description;
                $data->save();
            } else {
                $data          = $request->except(['_token', 'id']);
                $data['title'] = $data['seo_title'];
                $hub           = Hub::create($data);
            }


            return redirect(route('admin_hub'))
                ->with('success', 'Запись успешно обновлена');

        }

        public function rooms()
        {

            return view('backend.hub.hub_rooms', ['rooms' => Hub_room::all(), 'images' => Hub_image::all()]);

        }

        public function room_edit($id)
        {

            return view('backend.hub.edit', [
                'room' => Hub_room::find($id),
            ]);

        }

        public function room_update(Request $request)
        {


            $room = Hub_room::find($request->id);
            if ($request->room_image) {
                $name             = md5($request->room_image) . '.' . $request->room_image->getClientOriginalExtension();
                $room->room_image = '/storage/images/hub/rooms/' . $name;
                Storage::disk('hub')
                    ->put('/rooms/' . $name, File::get($request->room_image));
                $oldName = explode('/', $request->room_image_old);
                $oldName = end($oldName);
                Storage::disk('hub')
                    ->delete('/rooms/' . $oldName);
            }

            if ($request->room_image_location) {
                $name                      = md5($request->room_image_location) . '.' . $request->room_image_location->getClientOriginalExtension();
                $room->room_image_location = '/storage/images/hub/map/' . $name;
                Storage::disk('hub')
                    ->put('/map/' . $name, File::get($request->room_image));
                $oldName = explode('/', $request->room_image_old);
                $oldName = end($oldName);
                Storage::disk('hub')
                    ->delete('/map/' . $oldName);
            }

            $room->room_name        = $request->room_name;
            $room->capacity_text    = $request->capacity_text;
            $room->room_information = $request->room_information;
            $room->save();

            return redirect(route('hub_room_edit', ['id' => $request->id]))
                ->with('success', 'Запись успешно обновлена');
        }

        public function add_room_index(Request $request)
        {

            return view('backend.hub.add_room');

        }


        public function room_create(Request $request)
        {

            $room = new Hub_room();
            if ($request->room_image) {
                $name             = md5($request->room_image) . '.' . $request->room_image->getClientOriginalExtension();
                $room->room_image = '/storage/images/hub/rooms/' . $name;
                Storage::disk('hub')
                    ->put('/rooms/' . $name, File::get($request->room_image));
            }
            if ($request->room_image_location) {
                $name                      = md5($request->room_image_location) . '.' . $request->room_image_location->getClientOriginalExtension();
                $room->room_image_location = '/storage/images/hub/map/' . $name;
                Storage::disk('hub')
                    ->put('/map/' . $name, File::get($request->room_image_location));
            }
            $room->room_key         = $request->room_key;
            $room->room_name        = $request->room_name;
            $room->capacity_text    = $request->capacity_text;
            $room->room_information = $request->room_information;
            $room->save();


            return redirect(route('hub_rooms'))
                ->with('success', 'Аудитория успешно добавлена');
        }


        public function room_del(Request $request)
        {
            $room = Hub_room::find($request->id);
            $room->delete();
            return redirect(route('hub_rooms'))
                ->with('success', 'Запись удалена успешно');
        }
    }
