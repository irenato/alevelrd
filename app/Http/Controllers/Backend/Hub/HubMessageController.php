<?php

namespace App\Http\Controllers\Backend\Hub;

use App\Http\Controllers\Controller;
use App\Models\Hub\Hub_message;
use Illuminate\Http\Request;

class HubMessageController extends Controller
{
   public function index()
   {
       return view('backend.hub.hub_message',['messages' => Hub_message::latest('id')->paginate(10)]);
   }

    public function show(Request $request)
    {
        $message = Hub_message::find($request->id);
        $message->viewed = true;
        $message->update();
        return view('backend.hub.hub_message_show',['message' => $message]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAll()
    {
        Hub_message::query()->update(['viewed' => 1]);
        return redirect(route('hub_message'))
            ->with('success', 'Записи успешно обновлены');
    }

}
