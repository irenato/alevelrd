<?php

    namespace App\Http\Controllers\Backend\Contact;

    use App\Models\Contact\Phone;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class PhoneController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return view('backend.contacts.phones.index', [
                'phones' => Phone::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            if ($request->has('phone')) {
                $phones     = [];
                $phones_ids = [];
                foreach ($request->phone as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            if ($key == 'id') {
                                array_push($phones_ids, $v);
                            }
                            $phones[$k][$key] = $v;
                        }
                    }
                }
                if ($phones_ids) {
                    Phone::whereNotIn('id', $phones_ids)->delete();
                }
                if ($phones) {
                    foreach ($phones as $phone) {
                        Phone::updateOrCreate($phone);
                    }
                }
            }
            return redirect('backend/phones')->with('success', 'Данные сохранены успешно.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
