<?php

    namespace App\Http\Controllers\Backend\Discount;

    use App\Http\Requests\Discount\DiscountRequest;
    use App\Models\Discount\Discount;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DiscountController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.discounts.index', [
                'discounts' => Discount::orderBy('id')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.discounts.create', [
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(DiscountRequest $request)
        {
            Discount::create($request->all());
            return redirect('backend/discounts/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.discounts.edit', [
                'discount' => Discount::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $discount              = Discount::find($request->id);
            $discount->value       = $request->value;
            $discount->description = $request->description;
            $discount->class       = $request->class;
            $discount->update();
            return redirect('backend/discounts/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $discount = Discount::find($id);
            $discount->delete();
            return redirect('backend/discounts/')
                ->with('success', 'Запись удалена успешно');
        }
    }
