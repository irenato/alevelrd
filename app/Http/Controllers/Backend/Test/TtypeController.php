<?php

    namespace App\Http\Controllers\Backend\Test;

    use App\Models\Course\Course;
    use App\Models\Test\Ttype;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class TtypeController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $ttypes = Ttype::with(['courses_ttypes']);
            return view('backend.ttypes.index', [
                'ttypes' => $ttypes->paginate(30),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.ttypes.create', [
                'courseIds' => Course::all()->pluck('title', 'id')->toArray(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $ttype = Ttype::create($request->all());
            $ttype->courses_ttypes()->sync($request->courseIds);

            return redirect(route('ttypes.edit', $ttype->id))->with('success', 'Тип теста успешно добавлен.');

        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.ttypes.edit', [
                'ttype'     => Ttype::with(['courses_ttypes'])->findOrFail($id),
                'courseIds' => Course::all()->pluck('title', 'id')->toArray(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $ttype = Ttype::findOrFail($id);
            $ttype->update($request->all());
            $ttype->courses_ttypes()->sync($request->courseIds);

            return redirect(route('ttypes.edit', $ttype->id))->with('success', 'Тип теста успешно обновлен.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Ttype::destroy($id);
            return redirect(route('ttypes.index'))->with('success', 'Тип теста успешно удален.');
        }
    }
