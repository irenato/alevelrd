<?php

    namespace App\Http\Controllers\Backend\Test;

    use App\Models\Test\TestContent;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class TestPageController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $testContent = TestContent::first();
            if (!$testContent) {
                return redirect('backend/test-content/create');
            }
            return redirect('backend/test-content/' . $testContent->id . '/edit');
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.test-content.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $testContent = TestContent::create($request->all());
            return redirect('backend/test-content/' . $testContent->id . '/edit')->with('success', 'Запись успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.test-content.edit', [
                'data' => TestContent::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $testContent = TestContent::find($id);
            $testContent->update($request->all());
            return redirect('backend/test-content/' . $testContent->id . '/edit')->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
