<?php

    namespace App\Http\Controllers\Backend\Test;

    use App\Models\Course\Course;
    use App\Models\Teacher\Teacher;
    use App\Models\Test\Test;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CourseTestController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $courses = Course::with(['courses_teachers']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value)) {
                        switch ($key) {
                            case 'page':
                            case 'limit':
                                break;
                            case 'active':
                                $courses
                                    ->where('active', $value);
                                break;
                            case 'teacher':
                                $courses->orWhereHas('courses_teachers', function ($query) use ($value) {
                                    $query->where('teacher_id', $value);
                                });
                                break;
                            default:
                                $courses
                                    ->where($key, 'like', $value . "%");
                                break;
                        }
                    }
                }
            }
            return view('backend.course-tests.index', [
                'courses'  => $courses->orderBy('position')->paginate(50),
                'teachers' => Teacher::orderBy('first_name')
                    ->where('active', 1)
                    ->where('is_teacher', 1)
                    ->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $course     = Course::with('courses_answers')->findOrFail($id);
            $answersIds = [];
            foreach ($course->courses_answers as $answer) {
                $answersIds[$answer->test_id] = $answer->id;
            };
            return view('backend.course-tests.edit', [
                'course'     => $course,
                'answersIds' => $answersIds,
                'tests'      => Test::with(['answers'])->orderBy('position')->get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $course  = Course::findOrFail($id);
            $answers = [];
            $course->courses_answers()->detach();
            foreach ($request->test_answer_id as $testId => $answer) {
                $answers[$testId]['test_answer_id'] = $answer ?? 0;
            }
            $course->courses_tests()->sync($answers);
            return redirect(route('course-tests.edit', $course->id))->with('success', 'Тесты курса успешно обновлены.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
