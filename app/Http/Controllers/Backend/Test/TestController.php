<?php

    namespace App\Http\Controllers\Backend\Test;

    use App\Models\Test\Test;
    use App\Models\Test\TestAnswer;
    use App\Models\Test\Ttype;
    use App\Repositories\Test\TestRepository;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class TestController extends Controller
    {

        protected $repository;

        public function __construct(Request $request)
        {
            $this->repository = new TestRepository($request);
        }

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $test = Test::with(['answers', 'tests_ttypes']);
            foreach ($request->only(['key', 'question']) as $key => $value) {
                if (isset($value)) {
                    $test
                        ->where($key, 'like', $value . "%");
                }
            }
            return view('backend.tests.index', [
                'tests' => $test->orderBy('position')->paginate(30),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.tests.create', [
                'types' => Ttype::all()->pluck('name', 'id')->toArray(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $test = $this->repository->storeTest();

            return redirect(route('tests.edit', $test->id))->with('success', 'Тест успешно добавлен.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.tests.edit', [
                'test'  => Test::with(['answers', 'tests_ttypes'])->find($id),
                'types' => Ttype::all()->pluck('name', 'id')->toArray(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $test = $this->repository->updateTest($id);

            return redirect(route('tests.edit', $test->id))->with('success', 'Тест успешно обновлен.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Test::destroy($id);
            return redirect(route('tests.index'))->with('success', 'Тест успешно удален.');
        }
    }
