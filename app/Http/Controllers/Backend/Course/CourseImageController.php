<?php

    namespace App\Http\Controllers\Backend\Course;

    use App\Helpers\Helpers;
    use App\Http\Requests\Course\CourseImageRequest;
    use App\Models\Course\Course;
    use App\Models\Course\CourseImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class CourseImageController extends Controller
    {
        /**
         * Загрузка изображений
         *
         * @param Request $request
         * @param         $id
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function uploadImage(CourseImageRequest $request, $id)
        {
            $filename = md5($request->file->getClientOriginalName()) . '.' . $request->file->getClientOriginalExtension();
            Storage::disk('courses')
                ->put($id . '/' . $filename, File::get($request->file));
            $image            = new CourseImage();
            $image->course_id = $id;
            $image->name      = $filename;
            $image->sort      = 1;
            $image->save();
            return response()->json(['status' => true]);
        }

        /**
         * Получение изображений
         */
        public function getImages(Request $request, $id)
        {
            $res    = '';
            $course = Course::with('images')->find($id);
            if ($course->images->count() > 0) {
                foreach ($course->images as $image) {
                    $imageName   = explode('/', $image->name);
                    $dateCreated = Carbon::parse($image->created_at)->toFormattedDateString();
                    $res         .= view('backend.courses.image', [
                        'image'       => $image,
                        'course'      => $course,
                        'imageName'   => end($imageName),
                        'dateCreated' => $dateCreated,
                    ])->render();
                }
            }
            return response()->json(['images' => $res]);
        }


        /**
         * Удаление изображения
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function deleteImage(Request $request)
        {
            $image = CourseImage::find($request->id);
            Storage::delete('public/images/courses/' . $image->course_id . '/' . $image->name);
            $image->delete();
            return response()->json(['status' => true]);
        }

        /**
         * Получение информации о изображениии
         */
        public function getImage(Request $request)
        {
            $image      = CourseImage::find($request->id);
            $image->src = Helpers::getImageCache(Storage::disk('courses')->url($image->course_id . '/' . $image->name), 600, 400, true);
            return response()->json($image->toArray());
        }

        /**
         * Сохранение информации о изображении
         */
        public function setImage(Request $request)
        {
            $image        = CourseImage::find($request->image['id']);
            $image->title = $request->image['title'];
            $image->alt   = $request->image['alt'];
            $image->save();
            return response()->json(['status' => true]);
        }

        /**
         * Сортировка изображений
         */
        public function sortImage(Request $request)
        {
            if (count($request->images) > 0) {
                foreach ($request->images as $k => $image) {
                    $image       = CourseImage::find($image);
                    $image->sort = $k;
                    $image->update();
                }
            }
            return response()->json(['status' => true]);
        }

        /**
         * Делаем изобаржение превью товара
         */
        public function coverImage(Request $request)
        {
            CourseImage::where('course_id', '=', $request->product_id)->update(['cover' => 0]);
            $image        = CourseImage::find($request->image_id);
            $image->cover = 1;
            $image->save();
            return response()->json(['status' => true]);
        }
    }
