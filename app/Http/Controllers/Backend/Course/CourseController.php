<?php

namespace App\Http\Controllers\Backend\Course;

use App\Helpers\Helpers;
use App\Http\Requests\Course\CourseRequest;
use App\Models\Course\Course;
use App\Models\Course\CourseProgram;
use App\Models\Course\Type;
use App\Models\Locations\City;
use App\Models\Teacher\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\JsonResponse;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = Course::with(['courses_teachers']);
        if ($request->all()) {
            foreach ($request->all() as $key => $value) {
                if (isset($value)) {
                    switch ($key) {
                        case 'page':
                        case 'limit':
                            break;
                        case 'active':
                            $courses
                                ->where('active', $value);
                            break;
                        case 'teacher':
                            $courses->orWhereHas('courses_teachers',
                                function($query) use ($value) {
                                    $query->where('teacher_id', $value);
                                });
                            break;
                        default:
                            $courses
                                ->where($key, 'like', $value . "%");
                            break;
                    }
                }
            }
        }
        return view('backend.courses.index', [
            'courses'  => $courses->orderBy('position')->paginate(50),
            'teachers' => Teacher::orderBy('first_name')
                ->where('active', 1)
                ->where('is_teacher', 1)
                ->get(),
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.courses.create', [
            'types'             => Type::orderBy('title')->get(),
            'cities'            => City::orderBy('title')->get(),
            'teachers'          => Teacher::orderBy('first_name')
                ->where('active', 1)
                ->where('is_teacher', 1)
                ->get(),
            'logo_html_classes' => explode('|',
                Helpers::getConfig('course_logo_html_classes')),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $course = Course::create([
            'title'             => $request->title,
            'bx_id'             => $request->bx_id,
            'cost'              => $request->cost,
            'discount'          => $request->discount,
            'price_type'        => $request->price_type,
            'duration'          => $request->duration,
            'schedule'          => $request->schedule,
            'type_id'           => $request->type_id,
            'logo_class'        => $request->logo_class,
            'alias'             => $request->has('alias')
            && !is_null($request->alias) ?
                $request->alias : str_slug($request->title),
            'active'            => isset($request->active),
            'employment'        => isset($request->employment),
            'description'       => $request->description,
            'discount_end_date' => $request->discount_end_date,
            'introtext'         => $request->introtext,
            'advantages'        => $request->advantages,
            'begin'             => $request->begin,
            'start_date'        => $request->start_date,
            'amount_places'     => $request->amount_places,
            'free_places'       => $request->free_places,
            'seo_title'         => $request->has('seo_title')
            && !is_null($request->seo_title) ? $request->seo_title
                : ($request->filrst_name . ' ' . $request->last_name),
            'seo_robots'        => $request->seo_robots,
            'seo_keywords'      => $request->seo_keywords,
            'seo_canonical'     => $request->seo_canonical,
            'seo_description'   => $request->seo_description,
        ]);

        if ($request->program) {
            $this->addPrograms($course, $request);
        }

        if ($request->thumbnail) {
            $course->thumbnail
                = md5($request->thumbnail->getClientOriginalName()) . '.'
                . $request->thumbnail->getClientOriginalExtension();
            Storage::disk('courses')
                ->put($course->id . '/' . $course->thumbnail,
                    File::get($request->thumbnail));
            $course->update();
        }
        if ($request->logo) {
            $course->logo = md5($request->logo->getClientOriginalName()) . '.'
                . $request->logo->getClientOriginalExtension();
            Storage::disk('courses')
                ->put($course->id . '/' . $course->logo,
                    File::get($request->logo));
            $course->update();
        }
        if ($request->teacher_id) {
            $course->courses_teachers()->sync($request->teacher_id);
        }
        if ($request->cities_ids) {
            $course->courses_cities()->sync($request->cities_ids);
        }
        return redirect('backend/courses')->with('success',
            'Курс успешно добавлен.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::with(['programs'])->find($id);
        return view('backend.courses.edit', [
            'course'            => $course,
            'types'             => Type::orderBy('title')->get(),
            'cities'            => City::orderBy('title')->get(),
            'teacher_courses'   => $course->courses_teachers->pluck('id')
                ->toArray(),
            'courses_cities'    => $course->courses_cities->pluck('id')
                ->toArray(),
            'teachers'          => Teacher::orderBy('first_name')
                ->where('active', 1)
                ->where('is_teacher', 1)
                ->get(),
            'logo_html_classes' => explode('|',
                Helpers::getConfig('course_logo_html_classes')),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, $id)
    {
        $course                    = Course::findOrFail($id);
        $course->title             = $request->title;
        $course->bx_id             = $request->bx_id;
        $course->cost              = $request->cost;
        $course->discount          = $request->discount;
        $course->price_type        = $request->price_type;
        $course->duration          = $request->duration;
        $course->schedule          = $request->schedule;
        $course->logo_class        = $request->logo_class;
        $course->type_id           = $request->type_id;
        $course->active            = isset($request->active);
        $course->employment        = isset($request->employment);
        $course->alias             = $request->has('alias')
        && !is_null($request->alias) ? $request->alias
            : str_slug($request->title);
        $course->description       = $request->description;
        $course->advantages        = $request->advantages;
        $course->seo_title         = $request->seo_title;
        $course->seo_robots        = $request->seo_robots;
        $course->seo_keywords      = $request->seo_keywords;
        $course->seo_canonical     = $request->seo_canonical;
        $course->seo_description   = $request->seo_description;
        $course->amount_places     = $request->amount_places;
        $course->free_places       = $request->free_places;
        $course->begin             = $request->begin;
        $course->start_date        = $request->start_date;
        $course->introtext         = $request->introtext;
        $course->discount_end_date = $request->discount_end_date;

        if ($request->thumbnail) {
            $course->thumbnail
                = md5($request->thumbnail->getClientOriginalName()) . '.'
                . $request->thumbnail->getClientOriginalExtension();
            Storage::disk('courses')
                ->put($course->id . '/' . $course->thumbnail,
                    File::get($request->thumbnail));
            if ($request->thumbnail_old) {
                Storage::delete('public/images/courses/' . $course->id . '/'
                    . $request->thumbnail_old);
            }
        }

        if ($request->logo) {
            $course->logo = md5($request->logo->getClientOriginalName()) . '.'
                . $request->logo->getClientOriginalExtension();
            Storage::disk('courses')
                ->put($course->id . '/' . $course->logo,
                    File::get($request->logo));
            $course->update();
            if ($request->logo_old) {
                Storage::delete('public/images/courses/' . $course->id . '/'
                    . $request->logo_old);
            }
        }

        if ($request->video) {
            $course->video = $request->video->getClientOriginalName();
            Storage::disk('courses')
                ->put($course->id . '/' . $course->video,
                    File::get($request->video));
            if ($request->video_old) {
                Storage::delete('public/images/courses/' . $course->id . '/'
                    . $request->video_old);
            }
        }

        $course->update();

        if ($request->program) {
            $this->addPrograms($course, $request);
        }


        if ($request->teacher_id) {
            $course->courses_teachers()->sync($request->teacher_id);
        }
        if ($request->cities_ids) {
            $course->courses_cities()->sync($request->cities_ids);
        } else {
            $course->courses_cities()->detach();
        }
        return redirect('backend/courses')->with('success',
            'Курс успешно обновлен.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        Storage::deleteDirectory('public/images/courses/' . $course->id . '/');
        $course->courses_teachers()->detach();
        $course->delete();
        return redirect('backend/courses/')
            ->with('success', 'Курс успешно удален');
    }

    /**
     * Sorting Courses
     *
     * @param Request $request
     */
    public function sort(Request $request)
    {
        foreach ($request->items as $key => $value) {
            $course           = Course::find($value);
            $course->position = $key;
            $course->update();
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function disableAction(Request $request): JsonResponse
    {
        $course                    = Course::findOrFail($request->id);
        $course->is_action_enabled = $request->is_action_enabled == 'true';
        $course->action_start_date = Carbon::now();
        return response()->json($course->update());
    }

    /**
     * @param Course  $course
     * @param Request $request
     */
    protected function addPrograms(Course $course, Request $request)
    {
        $programs    = [];
        $program_ids = [];
        foreach ($request->program as $key => $value) {
            foreach ($value as $k => $v) {
                if ($v) {
                    $programs[$k][$key] = $v;
                    if ($key == 'id') {
                        array_push($program_ids, $v);
                    }
                }
            }
        }
        if ($program_ids) {
            CourseProgram::where([
                'course_id' => $course->id,
            ])->whereNotIn('id', $program_ids)->delete();
        }
        foreach ($programs as $program) {
            if (isset($program['id'])) {
                $course_program            = CourseProgram::find($program['id']);
                $course_program->course_id = $course->id;
                $course_program->period    = isset($program['period'])
                    ? $program['period'] : '';
                $course_program->title     = isset($program['title'])
                    ? $program['title'] : '';
                $course_program->practice  = isset($program['practice'])
                    ? $program['practice'] : '';
                $course_program->introtext = isset($program['introtext'])
                    ? $program['introtext'] : '';
                $course_program->update();
            } else {
                if (isset($program['period'])) {
                    $course_program            = new CourseProgram();
                    $course_program->course_id = $course->id;
                    $course_program->period    = isset($program['period'])
                        ? $program['period'] : '';
                    $course_program->title     = isset($program['title'])
                        ? $program['title'] : '';
                    $course_program->practice  = isset($program['practice'])
                        ? $program['practice'] : '';
                    $course_program->introtext = isset($program['introtext'])
                        ? $program['introtext'] : '';
                    $course_program->save();
                }
            }
        }
    }


}
