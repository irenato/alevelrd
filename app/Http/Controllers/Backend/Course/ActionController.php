<?php

    namespace App\Http\Controllers\Backend\Course;

    use App\Http\Requests\Action\ActionRequest;
    use App\Models\Course\Action;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Facades\File;

    class ActionController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $action = Action::first();
            if (!$action) {
                return redirect('backend/actions/create');
            }
            return redirect('backend/actions/' . $action->id . '/edit');
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.actions.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(ActionRequest $request)
        {
            $action                  = new Action();
            $action->title           = $request->title;
            $action->description     = $request->description;
            $action->discount        = $request->discount;
            $action->active          = isset($request->active);
            $action->end_date        = $request->end_date;
            $action->total_places    = $request->total_places;
            $action->occupied_places = $request->occupied_places;
            if ($request->thumbnail) {
                $action->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('action')
                    ->put($action->thumbnail, File::get($request->thumbnail));
            }
            if ($request->background) {
                $action->background = md5($request->background->getClientOriginalName()) . '.' . $request->background->getClientOriginalExtension();
                Storage::disk('action')
                    ->put($action->background, File::get($request->background));
            }
            $action->save();

            return redirect('backend/actions/' . $action->id . '/edit')->with('success', 'Акция успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.actions.edit', [
                'action' => Action::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(ActionRequest $request, $id)
        {
            $action                  = Action::find($id);
            $action->title           = $request->title;
            $action->description     = $request->description;
            $action->discount        = $request->discount;
            $action->active          = isset($request->active);
            $action->end_date        = $request->end_date;
            $action->total_places    = $request->total_places;
            $action->occupied_places = $request->occupied_places;
            if ($request->thumbnail) {
                $action->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('action')
                    ->put($action->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/actions/' . $request->thumbnail_old);
                }
            }

            if ($request->background) {
                $action->background = md5($request->background->getClientOriginalName()) . '.' . $request->background->getClientOriginalExtension();
                Storage::disk('action')
                    ->put($action->background, File::get($request->background));
                if ($request->background_old) {
                    Storage::delete('public/images/actions/' . $request->background_old);
                }
            }
            $action->update();
            return redirect('backend/actions/' . $action->id . '/edit')->with('success', 'Акция успешно обновлена');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
