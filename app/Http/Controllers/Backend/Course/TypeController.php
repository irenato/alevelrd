<?php

    namespace App\Http\Controllers\Backend\Course;

    use App\Http\Requests\Course\TypeRequest;
    use App\Models\Course\Course;
    use App\Models\Course\Type;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class TypeController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $types = Type::with(['courses']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value)) {
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $types
                                    ->where('active', $value);
                                break;
                            case 'course_id':
                                $types->orWhereHas('courses', function ($query) use ($value) {
                                    $query->where('id', $value);
                                });
                                break;
                                break;
                            default:
                                $types
                                    ->where($key, 'like', $value . "%");
                                break;
                        }
                    }
                }
            }
            return view('backend.types.index', [
                'types'   => $types->latest('updated_at')->paginate(10),
                'courses' => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.types.create', [

            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(TypeRequest $request)
        {
            $type = Type::create([
                'title'   => $request->title,
                'visible' => isset($request->visible),
            ]);

            if ($request->thumbnail) {
                $type->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('types')
                    ->put($type->id . '/' . $type->thumbnail, File::get($request->thumbnail));
                $type->update();
            }
            return redirect('backend/types')->with('success', 'Запись успешно создана.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.types.edit', [
                'type' => Type::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(TypeRequest $request, $id)
        {
            $type          = Type::find($id);
            $type->title   = $request->title;
            $type->visible = isset($request->visible);
            if ($request->thumbnail) {
                $type->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('types')
                    ->put($type->id . '/' . $type->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/types/' . $type->id . '/' . $request->thumbnail_old);
                }
            }
            $type->update();
            return redirect('backend/types/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $type = Type::find($id);
            Storage::deleteDirectory('public/images/types/' . $type->id . '/');
            $type->delete();
            return redirect('backend/types/')
                ->with('success', 'Запись успешно удалена');
        }
    }
