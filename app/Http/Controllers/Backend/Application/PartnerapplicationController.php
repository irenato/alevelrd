<?php

namespace App\Http\Controllers\Backend\Application;

use App\Models\Application\Partnerapplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerapplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $applications = new Partnerapplication();
        if ($request->all()) {
            foreach ($request->all() as $key => $value) {
                if (isset($value))
                    switch ($key) {
                        case 'page':
                            break;
                        case 'active':
                            $applications
                                ->where('active', $value);
                            break;
                        default:
                            $applications = $applications
                                ->where($key, 'like', "%" . $value . "%");
                            break;
                    }
            }
        }

        return view('backend.partnerapplications.index', [
            'applications' => $applications->latest()->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Partnerapplication::find($id);
        $application->viewed = true;
        $application->update();
        return view('backend.partnerapplications.show', [
            'application' => $application,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAll()
    {
        Partnerapplication::where('viewed', 0)->update(['viewed' => 1]);
        return redirect('backend/partnerapplications/')
            ->with('success', 'Записи успешно обновлены');
    }
}
