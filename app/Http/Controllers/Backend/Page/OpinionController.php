<?php

    namespace App\Http\Controllers\Backend\Page;

    use App\Http\Requests\Pages\OpinionRequest;
    use App\Models\Page\Opinion;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class OpinionController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.pages.homepage.opinions.index', [
                'data' => Opinion::orderBy('position')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.pages.homepage.opinions.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(OpinionRequest $request)
        {
            $partner = Opinion::create([
                'title'       => $request->title,
                'link'        => $request->link,
                'description' => $request->description,
            ]);
            if ($request->logo) {
                $partner->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('opinions')
                    ->put($partner->id . '/' . $partner->logo, File::get($request->logo));
            }
            $partner->update();
            return redirect(route('opinions.index'))
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.pages.homepage.opinions.edit', [
                'partner' => Opinion::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $partner              = Opinion::find($id);
            $partner->title       = $request->title;
            $partner->link        = $request->link;
            $partner->description = $request->description;
            if ($request->logo) {
                $partner->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('opinions')
                    ->put($partner->id . '/' . $partner->logo, File::get($request->logo));
                if ($request->logo_old) {
                    Storage::delete('public/images/opinions/' . $partner->id . '/' . $request->logo_old);
                }
            }
            $partner->update();
            return redirect(route('opinions.index'))
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $partner = Opinion::find($id);
            Storage::deleteDirectory('public/images/opinions/' . $partner->id . '/');
            $partner->delete();
            return redirect(route('opinions.index'))
                ->with('success', 'Запись успешно удалена');
        }

        /**
         * Sorting
         *
         * @param Request $request
         */
        public function sort(Request $request)
        {
            foreach ($request->items as $key => $value) {
                $item           = Opinion::find($value);
                $item->position = $key;
                $item->update();
            }
        }
    }
