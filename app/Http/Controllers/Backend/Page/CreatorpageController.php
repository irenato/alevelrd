<?php

    namespace App\Http\Controllers\Backend\Page;

    use App\Models\Page\Creatorpage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CreatorpageController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $creatorpage = Creatorpage::first();
            if (!$creatorpage) {
                return redirect('backend/creatorpage/create');
            }
            return redirect('backend/creatorpage/' . $creatorpage->id . '/edit');
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create(Request $request)
        {
            return view('backend.pages.creatorpage.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $creatorpage = Creatorpage::create($request->all());
            return redirect('backend/creatorpage/' . $creatorpage->id . '/edit')->with('success', 'Запись успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.pages.creatorpage.edit', [
                'data' => Creatorpage::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $creatorpage                  = Creatorpage::find($id);
            $creatorpage->seo_title       = $request->seo_title;
            $creatorpage->description     = $request->description;
            $creatorpage->seo_description = $request->seo_description;
            $creatorpage->seo_keywords    = $request->seo_keywords;
            $creatorpage->seo_canonical   = $request->seo_canonical;
            $creatorpage->update();
            return redirect('backend/creatorpage/' . $creatorpage->id . '/edit')->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
