<?php

namespace App\Http\Controllers\Backend\Page;

use App\Helpers\Helpers;
use App\Http\Requests\Pages\HomepageRequest;
use App\Models\Page\Homepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.homepage.page.index', [
            'data' => Homepage::first(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(HomepageRequest $request, $id)
    {
        $homepage = Homepage::find($id);
        if ($request->about_section_thumbnail) {
            $homepage->about_section_thumbnail
                = md5($request->about_section_thumbnail->getClientOriginalName())
                . '.'
                . $request->about_section_thumbnail->getClientOriginalExtension();
            Storage::disk('homepage')
                ->put($homepage->about_section_thumbnail,
                    File::get($request->about_section_thumbnail));
            if ($request->about_section_thumbnail_old) {
                Storage::delete('public/images/homepage/'
                    . $request->about_section_thumbnail);
            }
        }
        $homepage->about_section_title = $request->about_section_title;
        $homepage->about_section_content = $request->about_section_content;
        if ($request->opinion_section_thumbnail) {
            $homepage->opinion_section_thumbnail
                = md5($request->opinion_section_thumbnail->getClientOriginalName())
                . '.'
                . $request->opinion_section_thumbnail->getClientOriginalExtension();
            Storage::disk('homepage')
                ->put($homepage->opinion_section_thumbnail,
                    File::get($request->opinion_section_thumbnail));
            if ($request->opinion_section_thumbnail_old) {
                Storage::delete('public/images/homepage/'
                    . $request->opinion_section_thumbnail_old);
            }
        }
        $homepage->opinion_section_title = $request->opinion_section_title;
        $homepage->about_section_content = $request->about_section_content;
        if ($request->rewards_section_thumbnail) {
            $homepage->rewards_section_thumbnail
                = md5($request->rewards_section_thumbnail->getClientOriginalName())
                . '.'
                . $request->rewards_section_thumbnail->getClientOriginalExtension();
            Storage::disk('homepage')
                ->put($homepage->rewards_section_thumbnail,
                    File::get($request->rewards_section_thumbnail));
            if ($request->rewards_section_thumbnail_old) {
                Storage::delete('public/images/homepage/'
                    . $request->rewards_section_thumbnail_old);
            }
        }
        $homepage->rewards_section_title = $request->rewards_section_title;
        if ($request->discount_section_thumbnail) {
            $homepage->discount_section_thumbnail
                = md5($request->discount_section_thumbnail->getClientOriginalName())
                . '.'
                . $request->discount_section_thumbnail->getClientOriginalExtension();
            Storage::disk('homepage')
                ->put($homepage->discount_section_thumbnail,
                    File::get($request->discount_section_thumbnail));
            if ($request->discount_section_thumbnail) {
                Storage::delete('public/images/homepage/'
                    . $request->discount_section_thumbnail);
            }
        }
        $homepage->discount_section_title = $request->discount_section_title;
        $homepage->seo_description        = $request->seo_description;
        $homepage->seo_keywords           = $request->seo_keywords;
        $homepage->seo_robots             = $request->seo_robots;
        $homepage->seo_canonical          = $request->seo_canonical;
        $homepage->seo_title              = $request->seo_title;
        $homepage->partners_section_title = $request->partners_section_title;
        $homepage->schedule_section_title = $request->schedule_section_title;
        $homepage->teachers_section_title = $request->teachers_section_title;
        $homepage->nearby_courses_title   = $request->nearby_courses_title;
        $homepage->update();
        return redirect(route('homepage.index'))
            ->with('success', 'Запись успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
