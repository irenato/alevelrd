<?php

    namespace App\Http\Controllers\Backend\Seo;

    use App\Http\Requests\Seo\SeoscriptRequest;
    use App\Models\Seo\Seoscript;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class SeoscriptController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return view('backend.seo.seoscripts.index', [
                'data' => Seoscript::latest()->paginate(10),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.seo.seoscripts.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(SeoscriptRequest $request)
        {
            Seoscript::create([
                'key'    => $request->key,
                'value'  => $request->value,
                'active' => isset($request->active),
            ]);
            return redirect('backend/seoscripts')->with('success', 'Запись успешно добавлена.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.seo.seoscripts.edit', [
                'data' => Seoscript::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(SeoscriptRequest $request, $id)
        {
            $seoscript         = Seoscript::find($id);
            $seoscript->key    = $request->key;
            $seoscript->value  = $request->value;
            $seoscript->active = isset($request->active);
            $seoscript->update();
            return redirect('backend/seoscripts')->with('success', 'Запись успешно обновлена.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Seoscript::find($id)->delete();
            return redirect('backend/seoscripts')->with('success', 'Запись успешно удалена.');
        }
    }
