<?php

    namespace App\Http\Controllers\Backend\Creator;

    use App\Http\Requests\Creator\CreatorRequest;
    use App\Models\Creator\Creator;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class CreatorController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return view('backend.creators.index', [
                'creators' => Creator::orderBy('position')->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.creators.create', []);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $links   = [];
            $creator = Creator::create([
                'username'       => $request->username,
                'specialization' => $request->specialization,
                'description'    => $request->description,
            ]);
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $creator->social_links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            if ($request->thumbnail) {
                $creator->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('creators')
                    ->put($creator->id . '/' . $creator->thumbnail, File::get($request->thumbnail));
            }
            $creator->update();
            return redirect('backend/creators/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.creators.edit', [
                'creator' => Creator::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(CreatorRequest $request, $id)
        {
            $links                   = [];
            $creator                 = Creator::find($id);
            $creator->username       = $request->username;
            $creator->specialization = $request->specialization;
            $creator->description    = $request->description;
            if ($request->thumbnail) {
                $creator->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('reviews')
                    ->put($creator->id . '/' . $creator->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/creators/' . $creator->id . '/' . $request->thumbnail_old);
                }
            }
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $creator->social_links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            $creator->update();
            return redirect('backend/creators/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $creator = Creator::find($id);
            Storage::deleteDirectory('public/images/creators/' . $creator->id . '/');
            $creator->delete();
            return redirect('backend/creators/')
                ->with('success', 'Запись успешно удалена');
        }

        /**
         * Sorting
         *
         * @param Request $request
         */
        public function sort(Request $request)
        {
            foreach ($request->items as $key => $value) {
                $item           = Creator::find($value);
                $item->position = $key;
                $item->update();
            }
        }
    }
