<?php

namespace App\Http\Controllers\Backend\ProjectLife;

use App\Http\Controllers\Controller;
use App\Models\Project_life\Project_life_partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class ProcetLifePartnersController extends Controller
{

    public function index()
    {

        return view('backend.projectlife.partners',['partners' => Project_life_partner::all()]);
    }

    public function add()
    {
        return view('backend.projectlife.partners_add');
    }


    public function edit($id)
    {


        return view('backend.projectlife.partners_edit',['partners' => Project_life_partner::find($id)]);

    }

    public function update(Request $request)
    {
        $partner = Project_life_partner::find($request->id);
        if ($request->logo) {
            $name =md5($request->logo).'.'.$request->logo->getClientOriginalExtension();
            $partner->logo = '/storage/images/project_life/partner/'.$name;
            Storage::disk('project_life')
                ->put('/partner/'.$name, File::get($request->logo));
            $oldName = explode('/', $request->logo_old);
            $oldName = end($oldName);
            Storage::disk('project_life')
                ->delete('/partner/'.$oldName);
        }
        $partner->title = $request->title;
        $partner->link = $request->link;
        $partner->type = $request->type;
        $partner->short_text = $request->short_text;
        $partner->text = $request->text;
        $partner->save();

        return redirect(route('partners_edit',['id' => $request->id ]))
            ->with('success', 'Запись успешно обновлена');

    }

    public function create(Request $request)
    {
        $partner = New Project_life_partner();
        if ($request->logo) {
            $name =md5($request->img).'.'.$request->logo->getClientOriginalExtension();
            $partner->logo = '/storage/images/project_life/partner/'.$name;
            Storage::disk('project_life')
                ->put('/partner/'.$name, File::get($request->logo));
        }
        $partner->title = $request->title;
        $partner->link = $request->link;
        $partner->type = $request->type;
        $partner->short_text = $request->short_text;
        $partner->text = $request->text;
        $partner->save();

        return redirect(route('partners'))
            ->with('success', 'Партнёр успешно добавлен');

    }


    public function del(Request $request)
    {
        $partner = Project_life_partner::find($request->id);
        if(!empty($partner->logo)) {
            $oldName1 = explode('/', $partner->logo);
            $oldName1 = end($oldName1);

            Storage::disk('project_life')
                ->delete('/partner/' . $oldName1);
        }
        $partner->delete();
        return redirect(route('partners'))
            ->with('success', 'Запись удалена успешно');
    }
}
