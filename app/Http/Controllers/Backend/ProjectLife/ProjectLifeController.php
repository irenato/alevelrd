<?php

namespace App\Http\Controllers\Backend\ProjectLife;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project_life\Project_lifeRequest;
use App\Models\Project_life\Project_life;
use App\Models\Project_life\Project_life_slider;
use App\Models\Project_life\Project_to_callback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProjectLifeController extends Controller
{
    public function index()
    {

        return view('backend.projectlife.index',['data' => Project_life::first(),'sliders' => Project_life_slider::all(),
            'count_callback' =>count(Project_to_callback::all())]);

    }

    public function update(Project_lifeRequest $request)
    {

        if($request->slider_text1)
        {
            $slider1 = Project_life_slider::find(1);
            $slider1->text = $request->slider_text1;
            if ($request->slider_img1) {
                $name =md5($request->slider_img1).'.'.$request->slider_img1->getClientOriginalExtension();
                $slider1->img = '/storage/images/project_life/main_1_slider/'.$name;
                Storage::disk('project_life')
                    ->put('/main_1_slider/'.$name, File::get($request->slider_img1));
                if ($request->slider_img_old1) {
                    $oldName = explode('/', $request->slider_img_old1);
                    $oldName = end($oldName);
                    Storage::disk('project_life')
                        ->delete('/main_1_slider/' . $oldName);
                }

            }
            $slider1->save();
        }
        if($request->slider_text2)
        {
            $slider2 = Project_life_slider::find(2);
            $slider2->text = $request->slider_text2;
            if ($request->slider_img2) {
                $name =md5($request->slider_img2).'.'.$request->slider_img2->getClientOriginalExtension();
                $slider2->img = '/storage/images/project_life/main_1_slider/'.$name;
                Storage::disk('project_life')
                    ->put('/main_1_slider/'.$name, File::get($request->slider_img2));
                if ($request->slider_img_old2) {
                    $oldName = explode('/', $request->slider_img_old2);
                    $oldName = end($oldName);
                    Storage::disk('project_life')
                        ->delete('/main_1_slider/' . $oldName);
                }
            }
            $slider2->save();
        }
        if($request->slider_text3)
        {
            $slider3 = Project_life_slider::find(3);
            $slider3->text = $request->slider_text3;
            if ($request->slider_img3) {
                $name =md5($request->slider_img3).'.'.$request->slider_img3->getClientOriginalExtension();
                $slider3->img = '/storage/images/project_life/main_1_slider/'.$name;
                Storage::disk('project_life')
                    ->put('/main_1_slider/'.$name, File::get($request->slider_img3));
                if ($request->slider_img_old3) {
                    $oldName = explode('/', $request->slider_img_old3);
                    $oldName = end($oldName);
                    Storage::disk('project_life')
                        ->delete('/main_1_slider/' . $oldName);
                }
            }

            $slider3->save();
        }

        $data = Project_life::first();
        $data->title =$request->title;
        $data->seo_description =$request->seo_description;
        $data->seo_keywords =$request->seo_keywords;
        $data->seo_robots =$request->seo_robots;
        $data->seo_canonical =$request->seo_canonical;
        $data->section_1_h1 =$request->section_1_h1;
        $data->section_1_h2 =$request->section_1_h2;
        $data->section_1_p =$request->section_1_p;
        $data->section_1_p_cl_title =$request->section_1_p_cl_title;
        $data->section_2h1_cl =$request->section_2h1_cl;
        $data->section_2p =$request->section_2p;
        $data->section_4_h1 =$request->section_4_h1;
        $data->section_6_h4_1 =$request->section_6_h4_1;
        $data->section_6_h4_2 =$request->section_6_h4_2;
        $data->section_6_h4_3 =$request->section_6_h4_3;
        $data->section_7_m_h2_cl =$request->section_7_m_h2_cl;
        $data->section_7_m_p =$request->section_7_m_p;
        $data->date =$request->date;
        $data->place =$request->place;
        $data->link =$request->link;
        $data->amount =$request->amount;
        $data->section_7_m_error =$request->section_7_m_error;


        if ($request->section_1_p_img) {
            $name =md5($request->section_1_p_img).'.'.$request->section_1_p_img->getClientOriginalExtension();
            $data->section_1_p_img = '/storage/images/project_life/main_1/'.$name;
            Storage::disk('project_life')
                ->put('/main_1/'.$name, File::get($request->section_1_p_img));
            if ($request->section_1_p_img_old) {
                $oldName = explode('/', $request->section_1_p_img_old);
                $oldName = end($oldName);
                Storage::disk('project_life')
                    ->delete('/main_1/' . $oldName);
            }

        }
        $data->save();


        return redirect(route('project_admin'))
            ->with('success', 'Запись успешно обновлена');

    }
}
