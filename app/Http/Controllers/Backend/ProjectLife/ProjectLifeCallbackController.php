<?php

namespace App\Http\Controllers\Backend\ProjectLife;

use App\Exports\ProjectLifeCallbacksExport;
use App\Http\Controllers\Controller;
use App\Models\Project_life\Project_programm;
use App\Models\Project_life\Project_to_callback;
use App\Models\Project_life\Projectlife_callbacks;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProjectLifeCallbackController extends Controller
{
    public function index(Request $request)
    {
        $callbacks = Projectlife_callbacks::with('workshop');

        if (request('sort') && request('orderby'))
        {
            $callbacks->orderBy(request('sort'),request('orderby'));
        }
        else {
            $callbacks->latest();
        }

        return view('backend.projectlife.callback',['workshops' =>Project_programm::where('workshop','1')->get(),'data' => $callbacks->paginate(10)]);

    }

    public function show(Request $request)
    {
        $message = Projectlife_callbacks::with('workshop')->find($request->id);
        $message->viewed = true;
        $message->update();
        return view('backend.projectlife.callback_show',['message' => $message]);
    }

    public function del($id)
    {
        $message = Projectlife_callbacks::find($id);
        $message->workshop()->detach();
        $message->delete();

        return redirect(route('project_callbacks'))
            ->with('success', 'Запись успешно удалена');
    }

    public function export()
    {
        return Excel::download(new ProjectLifeCallbacksExport(), 'Projectlife-application' . date('Y-m-d') . '.xlsx');

    }
}

