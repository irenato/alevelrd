<?php

namespace App\Http\Controllers\Backend\ProjectLife;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project_life\Project_lifeRequest;
use App\Models\Project_life\Project_life;
use App\Models\Project_life\Project_life_slider;
use App\Models\Project_life\Project_life_speaker;
use App\Models\Project_life\Project_programm;
use App\Models\Project_life\Time;
use App\Models\Project_life\Timeline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProjectProgrammController extends Controller
{


    public function  index()
    {

        return view('backend.projectlife.programm',['data' => Project_programm::with('speakers')->get(),'times' => Time::all()]);

    }

    public function edit($id)
    {
        $teachers_programm = Project_programm::find($id);
        $speakers =$teachers_programm->speakers()->pluck('speaker_id')->toArray();

        return view('backend.projectlife.edit_programm',['data' => Project_programm::find($id),
            'project_speakers'  =>$speakers,
            'speakers' => Project_life_speaker::all(),'times' => Time::all()]);
    }

    public function  update(Request $request,$id)
    {
        $programm = Project_programm::find($id);
        $programm->title = $request->title;
        $programm->time = $request->time;
        $programm->room = $request->room;
        $programm->short_text = $request->short_text;
        $programm->text = $request->text;
        $programm->workshop = $request->workshop;
        $programm->seats = $request->seats;
        $programm->save();
        $programm->speakers()->sync($request->teacher_id);

        return redirect(route('project_programms_edit',['id' => $request->id ]))
            ->with('success', 'Запись успешно обновлена');

    }


    public function del($id)
    {
        $programm = Project_programm::find($id);
        $programm->speakers()->detach();
        $programm->delete();

        return redirect(route('project_programms'))
            ->with('success', 'Запись успешно удалена');
    }


    public function add()
    {
        return view('backend.projectlife.add_programm',['data' => Project_programm::all(),'speakers' => Project_life_speaker::all(),'times' => Time::all()]);
    }

    public function create(Request $request)
    {
        $programm = new Project_programm();
        $programm->title = $request->title;
        $programm->time = $request->time;
        $programm->room = $request->room;
        $programm->short_text = $request->short_text;
        $programm->text = $request->text;
        $programm->save();
        $programm->speakers()->attach($request->teacher_id);

        return redirect(route('project_programms'))
            ->with('success', 'Запись успешно добавлена');

    }

    public function timeline()
    {

        $t = Timeline::with('programm')->get();

        $project = Project_programm::where('workshop',null)->get();
        $workshops = Project_programm::where('workshop',1)->get();

        return view('backend.projectlife.timeline',['data' =>$t,'project' =>$project, 'workshops' => $workshops ,'time' => Time::all()]);

    }

    public function timeline_update(Request $request)
    {

        Timeline::truncate();
        $i = 1;
        foreach ($request->st as $time)
        {
            $time_table = Time::find($i);
            $time_table->time = $time['time'];
            $time_table->save();

            if(isset($time['gray'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'gray';
                $programm = Project_programm::find($time['gray']);
                $programm->time =$time['time'];
                $programm->room ='gray';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();
            }

            if(isset($time['green'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'green';
                $programm = Project_programm::find($time['green']);
                $programm->time =$time['time'];
                $programm->room ='green';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();
            }

            if(isset($time['blue'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'blue';
                $programm = Project_programm::find($time['blue']);
                $programm->time =$time['time'];
                $programm->room ='blue';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();

            }
            if(isset($time['red'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'red';
                $programm = Project_programm::find($time['red']);
                $programm->time =$time['time'];
                $programm->room ='red';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();

            }
            if(isset($time['class'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'class';
                $programm = Project_programm::find($time['class']);
                $programm->time =$time['time'];
                $programm->room ='class';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();
            }

            if(isset($time['all'])) {
                $Newtimeline = New Timeline();
                $Newtimeline->time_id = $time_table['id'];
                $Newtimeline->room = 'all';
                $programm = Project_programm::find($time['all']);
                $programm->time =$time['time'];
                $programm->room ='all';
                $Newtimeline->programm_id =$programm['id'];
                $programm->save();
                $Newtimeline->save();
            }
            $i++;
        }
        return redirect(route('project_timeline'))
            ->with('success', 'Запись успешно обновлена');
    }


}
