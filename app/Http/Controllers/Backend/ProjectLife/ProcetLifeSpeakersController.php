<?php

namespace App\Http\Controllers\Backend\ProjectLife;

use App\Http\Controllers\Controller;
use App\Models\Project_life\Project_life_speaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProcetLifeSpeakersController extends Controller
{

    public function index()
    {


        return view('backend.projectlife.speakers',['speakers' => Project_life_speaker::all()]);

    }

    public function edit($id)
    {

        return view('backend.projectlife.speaker_edit',['speaker' => Project_life_speaker::find($id)]);

    }

    public function update(Request $request)
    {
        $speaker = Project_life_speaker::find($request->id);

        if ($request->img) {
            $name =md5($request->img).'.'.$request->img->getClientOriginalExtension();
            $speaker->img = '/storage/images/project_life/speakers/'.$name;
            Storage::disk('project_life')
                ->put('/speakers/'.$name, File::get($request->img));
            $oldName = explode('/', $request->img_old);
            $oldName = end($oldName);
            Storage::disk('project_life')
                ->delete('/speakers/'.$oldName);
        }
        if ($request->img_hover) {
            $name =md5($request->img_hover).'.'.$request->img_hover->getClientOriginalExtension();
            $speaker->img_hover = '/storage/images/project_life/speakers/'.$name;
            Storage::disk('project_life')
                ->put('/speakers/'.$name, File::get($request->img_hover));
            $oldName = explode('/', $request->img_hover_old);
            $oldName = end($oldName);
            Storage::disk('project_life')
                ->delete('/speakers/'.$oldName);
        }
        $speaker->name = $request->name;
        $speaker->short_text = $request->short_text;
        $speaker->text   = $request->text;
        $speaker->save();

        return redirect(route('speaker_edit',['id' => $request->id ]))
            ->with('success', 'Запись успешно обновлена');

    }

    public function add()
    {
        return view('backend.projectlife.speaker_add');

    }

    public function create(Request $request)
    {

        $speaker = New Project_life_speaker();

        if ($request->img) {
            $name =md5($request->img).'.'.$request->img->getClientOriginalExtension();
            $speaker->img = '/storage/images/project_life/speakers/'.$name;
            Storage::disk('project_life')
                ->put('/speakers/'.$name, File::get($request->img));
        }
        if ($request->img_hover) {
            $name =md5($request->img_hover).'.'.$request->img_hover->getClientOriginalExtension();
            $speaker->img_hover = '/storage/images/project_life/speakers/'.$name;
            Storage::disk('project_life')
                ->put('/speakers/'.$name, File::get($request->img_hover));
        }
        $speaker->name = $request->name;
        $speaker->short_text = $request->short_text;
        $speaker->text   = $request->text;
        $speaker->save();

        return redirect(route('project_speakers'))
            ->with('success', 'Спикер успешно добавлен');

    }

    public function del(Request $request)
    {
        $speaker = Project_life_speaker::find($request->id);


        if(!empty($speaker->img_hover)) {
            $oldName1 = explode('/', $speaker->img_hover);
            $oldName1 = end($oldName1);

            Storage::disk('project_life')
                ->delete('/speakers/' . $oldName1);
        }

        if(!empty($speaker->img)) {
            $oldName2 = explode('/', $speaker->img);
            $oldName2 = end($oldName2);

            Storage::disk('project_life')
                ->delete('/speakers/' . $oldName2);
        }
        $speaker->projects()->detach();
        $speaker->delete();
        return redirect(route('project_speakers'))
            ->with('success', 'Запись удалена успешно');
    }


}
