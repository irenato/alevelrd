<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\SettingRequest;
use App\Models\Setting\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Spatie\ImageOptimizer\OptimizerChain;
use Illuminate\Support\Facades\File;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $settings = Setting::paginate($request->get('limit', 10));
        return view('backend.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SettingRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        $setting              = new Setting();
        $setting->key         = $request->key;
        $setting->value       = $request->value;
        $setting->description = $request->description;
        $setting->save();
        $this->buildCache();
        return redirect('backend/settings/' . $setting->id . '/edit')
            ->with('success', 'Настройка успешно создана.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::where('id', $id)->first();
        return view('backend.settings.edit', ['setting' => $setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SettingRequest|Request $request
     * @param                        $id
     *
     * @return \Illuminate\Http\Response
     * @internal param string $key
     */
    public function update(SettingRequest $request, $id)
    {
        $setting              = $old = Setting::find($id);
        $setting->value       = $request->value;
        $setting->description = $request->description;
        $setting->update();
        $this->buildCache();
        return redirect('backend/settings/' . $id . '/edit')
            ->with('success', 'Настройка успешно обнолена.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Put settings in cache file
     */
    private function buildCache()
    {
        $all = Setting::all()->toArray();
        $res = '<?php' . "\n" . ' return $settings = [' . "\n";
        foreach ($all as $setting) {
            $res .= "\t" . '"' . $setting['key'] . '" => "' . $setting['value']
                . '",' . "\n";
        }
        $res .= '];';
        file_put_contents(storage_path(Setting::CACHE_PATH), $res);
    }

    /**
     * Empty cache file.
     */
    public function setEmptyCache()
    {
        if (file_exists(storage_path(Setting::CACHE_PATH))) {
            unlink(storage_path(Setting::CACHE_PATH));
        }
        $all = Setting::all()->toArray();
        $res = '<?php' . "\n" . 'return $settings = [' . "\n";
        foreach ($all as $setting) {
            $res .= "\t" . '"' . $setting['key'] . '" => "' . $setting['value']
                . '",' . "\n";
        }
        $res .= '];';
        file_put_contents(storage_path(Setting::CACHE_PATH), $res);
        Artisan::call('cache:clear', []);
        File::cleanDirectory(public_path('/assets/images/cache'));
        return redirect('backend/settings')->with('success',
            'Кеш успешно очищен');
    }

    public function optimizeImages(): RedirectResponse
    {
        $exts  = ['jpeg', 'jpg', 'png'];
        $i     = 0;
        $files = Storage::disk('local')->allFiles();
        foreach ($files as $image) {
            $ext = explode('.', $image);
            if (isset($ext[1]) && in_array($ext[1], $exts)) {
                ++$i;
                app(OptimizerChain::class)->optimize(Storage::disk('local')
                    ->path($image));
            }
        };
        return redirect('backend/settings')->with('success',
            'Изображений сжато: ' . $i);
    }

}
