<?php

    namespace App\Http\Controllers\Backend\Student;

    use App\Helpers\Helpers;
    use App\Http\Requests\Student\StudentRequest;
    use App\Models\Course\Course;
    use App\Models\Locations\City;
    use App\Models\Student\Student;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class StudentController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $students = Student::with(['courses_students', 'city']);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $students
                                    ->where('active', $value);
                                break;
                            case 'course_id':
                                $students->whereHas('courses_students', function ($query) use ($value) {
                                    $query->where('course_id', $value);
                                });
                                break;
                            default:
                                $students = $students
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.students.index', [
                'students' => $students->latest()->paginate(10),
                'cities'   => City::orderBy('title')->get(),
                'courses'  => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $courses = Course::orderBy('title')->get();
            if ($courses->count() == 0) {
                return redirect()->back()->with('error', 'Нужно создать хоть один курс.');
            }
            return view('backend.students.create', [
                'cities'  => City::orderBy('title')->get(),
                'courses' => $courses,
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(StudentRequest $request)
        {
            $student = Student::create([
                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'email'      => $request->email,
                'phone'      => $request->phone,
                'city_id'    => $request->city_id,
                'birthdate'  => Carbon::parse($request->birthdate)->format('d.m.Y'),
                'alias'      => Helpers::checkAlias('App\Models\Student\Student', $request->has('alias') && !is_null($request->alias) ?
                    $request->alias : str_slug($request->filrst_name . '-' . $request->last_name)),
                'active'     => isset($request->active),
            ]);
            if ($request->thumbnail) {
                $student->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('students')
                    ->put($student->id . '/' . $student->thumbnail, File::get($request->thumbnail));
                $student->update();
            }
            if ($request->course_id) {
                $student->courses_students()->sync($request->course_id);
            }
            return redirect('backend/students')->with('success', 'Запись успешно добавлена.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $student = Student::find($id);
            return view('backend.students.edit', [
                'student'         => $student,
                'student_courses' => $student->courses_students->pluck('id')->toArray(),
                'courses'         => Course::orderBy('title')->get(),
                'cities'          => City::orderBy('title')->get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(StudentRequest $request, $id)
        {
            $student = Student::find($id);
            $student->first_name = $request->first_name;
            $student->last_name = $request->last_name;
            $student->email = $request->email;
            $student->phone = $request->phone;
            $student->city_id = $request->city_id;
            $student->birthdate = Carbon::parse($request->birthdate)->format('d.m.Y');
            $student->active = isset($request->active);
            $student->alias = Helpers::checkAlias('App\Models\Student\Student', $request->has('alias') && !is_null($request->alias) ?
                $request->alias : str_slug($request->filrst_name . '-' . $request->last_name));
            if ($request->thumbnail) {
                $student->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('students')
                    ->put($student->id . '/' . $student->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/students/' . $student->id . '/' . $request->thumbnail_old);
                }
            }
            $student->update();
            if ($request->course_id) {
                $student->courses_students()->sync($request->course_id);
            }
            return redirect('backend/students')->with('success', 'Данные успешно обновлены.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $student = Student::find($id);
            Storage::deleteDirectory('public/images/students/' . $student->id . '/');
            $student->courses_students()->detach();
            $student->delete();
            return redirect('backend/students/')
                ->with('success', 'Запись успешно удалена');
        }
    }
