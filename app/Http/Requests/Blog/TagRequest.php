<?php

    namespace App\Http\Requests\Blog;

    use Illuminate\Foundation\Http\FormRequest;

    class TagRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'alias' => 'required|string|regex:/^[-a-z0-9]+$/|unique:tags,alias,' . $this->route('tag'),
                'name'  => 'required|string|min:3|unique:tags,name,' . $this->route('tag'),
            ];
        }
    }
