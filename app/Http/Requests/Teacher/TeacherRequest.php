<?php

    namespace App\Http\Requests\Teacher;

    use Illuminate\Foundation\Http\FormRequest;

    class TeacherRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'first_name'     => 'required|min:3',
                'last_name'      => 'required|min:3',
                'specialization' => 'required|min:3',
                'thumbnail'      => 'file|image|mimes:jpeg,jpg,png|max:2000',
                'logo'           => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'first_name'     => 'Фамилия',
                'last_name'      => 'Имя',
                'specialization' => 'Специализация',
                'course_id.*'    => 'Курс',
                'thumbnail'      => 'Изображение',
                'logo'           => 'Лого',
            ];
        }
    }
