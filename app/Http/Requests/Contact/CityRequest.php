<?php

    namespace App\Http\Requests\Contact;

    use Illuminate\Foundation\Http\FormRequest;

    class CityRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'   => 'required|min:3|max:24',
                'address' => 'required|min:3',
                'lat'     => 'required|min:3|numeric',
                'lng'     => 'required|min:3|numeric',
                'email'   => 'required|email',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'title'   => 'Название',
                'address' => 'Адресс',
                'lat'     => 'Широта',
                'lng'     => 'Долгота',
                'email'   => 'Email',
            ];
        }
    }
