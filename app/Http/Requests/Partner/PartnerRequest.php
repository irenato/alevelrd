<?php

    namespace App\Http\Requests\Partner;

    use Illuminate\Foundation\Http\FormRequest;

    class PartnerRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'     => 'required|min:3|unique:partners,title,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id',
                'introtext' => 'required|min:3|max:128',
                'link'      => 'url',
                'logo'      => 'file|image|mimes:jpeg,jpg,png|max:2000',
                'thumbnail' => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'title'     => 'Название',
                'link'      => 'Ссылка',
                'thumbnail' => 'Изображение',
                'logo'      => 'Логотип',
            ];
        }
    }
