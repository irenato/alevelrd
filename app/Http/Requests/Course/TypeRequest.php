<?php

    namespace App\Http\Requests\Course;

    use Illuminate\Foundation\Http\FormRequest;

    class TypeRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'     => 'required|min:3|unique:types,title,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id',
                'thumbnail' => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'title'     => 'Название',
                'thumbnail' => 'Изображение',
            ];
        }
    }
