<?php

    namespace App\Http\Requests\Course;

    use Illuminate\Foundation\Http\FormRequest;

    class CourseRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'        => 'required|min:3',
                'cost'         => 'nullable|numeric',
                'cost_old'     => 'nullable|numeric',
                'discount'     => 'nullable|between:0,99',
                'teacher_id.*' => 'required',
                'cities_ids.*' => 'required',
                'type_id'      => 'required',
                'thumbnail'    => 'file|image|mimes:jpeg,jpg,png|max:2000',
                'logo'         => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'type_id'       => 'Тип',
                'title'         => 'Название',
                'cost'          => 'Стоимость',
                'teacher_id.*'  => 'Учителя',
                'thumbnail'     => 'Изображение',
                'logo'          => 'Логотип',
                'begin'         => 'Дата начала',
                'amount_places' => 'Количество мест',
                'free_places'   => 'Свободных мест',
            ];
        }
    }
