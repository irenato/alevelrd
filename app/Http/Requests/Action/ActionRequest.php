<?php

    namespace App\Http\Requests\Action;

    use Illuminate\Foundation\Http\FormRequest;

    class ActionRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'           => 'required|min:3',
                'discount'        => 'required|numeric',
                'occupied_places' => 'required|numeric',
                'total_places'    => 'required|numeric',
                'end_date'        => 'required|',
                'thumbnail'       => 'file|image|mimes:jpeg,jpg,png|max:2000',
                'background'      => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'title'           => 'Название',
                'discount'        => 'Скидка',
                'occupied_places' => 'Занятых мест',
                'total_places'    => 'Всего мест',
                'thumbnail'       => 'Изображение',
                'background'      => 'Фон',
                'amount_places'   => 'Количество мест',
                'free_places'     => 'Свободных мест',
                'end_date'        => 'Дата окончания',
            ];
        }
    }
