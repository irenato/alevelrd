<?php

    namespace App\Http\Requests\Student;

    use Illuminate\Foundation\Http\FormRequest;

    class StudentRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * @return array
         */
        public function rules()
        {
            return [
                'first_name'  => 'required|min:3',
                'last_name'   => 'required|min:3',
                'city_id'     => 'required|numeric',
                'email'       => 'required|email|unique:students,email,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id,deleted_at,NULL',
                'phone'       => 'required|unique:students,phone,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id,deleted_at,NULL',
                'course_id.*' => 'numeric',
                'thumbnail'   => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'first_name'  => 'Фамилия',
                'last_name'   => 'Имя',
                'city_id'     => 'Город',
                'email'       => 'Email',
                'phone'       => 'Телефон',
                'course_id.*' => 'Курс',
                'thumbnail'   => 'Изображение',
            ];
        }
    }
