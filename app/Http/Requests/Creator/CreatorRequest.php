<?php

    namespace App\Http\Requests\Creator;

    use Illuminate\Foundation\Http\FormRequest;

    class CreatorRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'username'       => 'required|min:3',
                'specialization' => 'required|min:3',
                'description'    => 'required|min:3',
                'thumbnail'      => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'username'       => 'Имя',
                'specialization' => 'Специализация',
                'description'    => 'Описание',
                'thumbnail'      => 'Миниатюра',
            ];
        }
    }
