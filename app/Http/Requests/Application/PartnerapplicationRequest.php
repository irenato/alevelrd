<?php

    namespace App\Http\Requests\Application;

    use Illuminate\Foundation\Http\FormRequest;

    class PartnerapplicationRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'company' => 'required|min:3',
                'email'   => 'nullable|email',
                'phone'   => 'nullable|size:19',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'company' => 'Компания',
                'email'   => 'Email',
                'phone'   => 'Телефон',
            ];
        }
    }
