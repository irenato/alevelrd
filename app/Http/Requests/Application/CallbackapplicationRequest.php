<?php

    namespace App\Http\Requests\Application;

    use Illuminate\Foundation\Http\FormRequest;

    class CallbackapplicationRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'username' => 'required|min:3',
                'phone'    => 'required',
                'message'  => 'required|min:3',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'username' => 'Имя',
                'email'    => 'Телефон',
                'message'  => 'Сообщение',
            ];
        }
    }
