<?php

    namespace App\Http\Requests\Application;

    use Illuminate\Foundation\Http\FormRequest;

    class StapplicationRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'username'    => 'required|min:3',
                'course_id'   => 'required|numeric',
                'email'       => 'required|email',
                'phone'       => 'required'
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'username'    => 'Имя',
                'email'       => 'Email',
                'description' => 'Описание',
                'phone'       => 'Телефон',
                'city_id'     => 'Город',
                'course_id'   => 'Курс',
            ];
        }
    }
