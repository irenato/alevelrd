<?php

    namespace App\Http\Requests\Event;

    use Illuminate\Foundation\Http\FormRequest;

    class EventRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'        => 'required|min:3',
                'cost'         => 'numeric|between:0,99999999.99',
                'teacher_id.*' => 'required',
                'thumbnail'    => 'file|image|mimes:jpeg,jpg,png|max:2000',
                'begin'        => 'date|date_format:d.m.Y',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'title'        => 'Название',
                'cost'         => 'Стоимость',
                'teacher_id.*' => 'Специализация',
                'thumbnail'    => 'Изображение',
                'begin'        => 'Дата начала',
            ];
        }
    }
