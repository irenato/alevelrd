<?php

namespace App\Http\Requests\Pages;

use Illuminate\Foundation\Http\FormRequest;

class HomepageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'about_section_thumbnail'    => 'file|image|mimes:jpeg,jpg,png|max:2000',
            'opinion_section_thumbnail'  => 'file|image|mimes:jpeg,jpg,png|max:2000',
            'rewards_section_thumbnail'  => 'file|image|mimes:jpeg,jpg,png|max:2000',
            'discount_section_thumbnail' => 'file|image|mimes:jpeg,jpg,png|max:2000',
            'nearby_courses_title'       => 'nullable|max:128',
        ];
    }
}
