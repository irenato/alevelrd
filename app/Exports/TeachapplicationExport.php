<?php

    namespace App\Exports;

    use App\Models\Application\Teachapplication;
    use Illuminate\Contracts\View\View;
    use Maatwebsite\Excel\Concerns\FromView;

    class TeachapplicationExport implements FromView
    {
        public function view(): View
        {
            $applications = Teachapplication::with('course', 'city');
            if (request()->all()) {
                foreach (request()->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $applications
                                    ->where('active', $value);
                                break;
                            default:
                                $applications = $applications
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.stapplications.export', [
                'data' => $applications->get(),
            ]);
        }
    }