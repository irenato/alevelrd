<?php

namespace App\Exports;

use App\Models\Blog\Subscriber;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SubscribersExport implements FromView
{
    public function view(): View
    {
        return view('backend.blog.subscribers.export', [
            'data' => Subscriber::latest()->get(),
        ]);
    }
}