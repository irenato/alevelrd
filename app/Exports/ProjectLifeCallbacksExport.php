<?php

namespace App\Exports;

use App\Models\Project_life\Projectlife_callbacks;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProjectLifeCallbacksExport implements FromView
{
    public function view(): View
    {
        $applications = Projectlife_callbacks::with('workshop');
        if (request()->all()) {
            foreach (request()->all() as $key => $value) {
                if (isset($value))
                    switch ($key) {
                        case 'page':
                            break;
                        case 'active':
                            $applications
                                ->where('active', $value);
                            break;
                        default:
                            $applications = $applications
                                ->where($key, 'like', "%" . $value . "%");
                            break;
                    }
            }
        }
        return view('backend.projectlife.export', [
            'data' => $applications->get(),
        ]);
    }
}