<?php

    namespace App\Models\Blog;

    use Illuminate\Database\Eloquent\Model;

    class Tag extends Model
    {
        protected $fillable
            = [
                'name',
                'alias',
                'htmlclass_id'
            ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function htmlClass()
        {
            return $this->BelongsTo(Htmlclass::class, 'htmlclass_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
         */
        public function tags_articles()
        {
            return $this->belongsToMany(Article::class);
        }
    }
