<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Htmlclass extends Model
{
    protected $fillable = [
        'html_class'
    ];
}
