<?php

namespace App\Models\Blog;

use App\Helpers\Helpers;
use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

class Article extends Model
{

    use Translatable;

    protected $fillable
        = [
            'active',
            'alias',
            'thumbnail',
            'background',
            'likes',
            'dislikes',
            'viewed',
            'position',
            'is_outer',
        ];

    public $translatedAttributes
        = [
            'title',
            'content',
            'description',
            'seo_title',
            'seo_keywords',
            'seo_description',
            'seo_robots',
            'seo_canonical',
            'seo_content',
        ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors_articles(): BelongsToMany
    {
        return $this->belongsToMany(Author::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags_articles(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return BelongsToMany
     */
    public function related(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'related_articles',
            'article_id', 'related_id')
            ->whereActive(true)
            ->orderBy('related_id');
    }

    /**
     * @return string
     */
    public function getThumbnailMiniAttribute(): string
    {
        return Helpers::getImageCache(Storage::disk('articles')->url(
            $this->id . '/' . $this->thumbnail), 128, 128);
    }

    /**
     * @return string
     */
    public function getBackgroundMiniAttribute(): string
    {
        return Helpers::getImageCache(Storage::disk('articles')->url(
            $this->id . '/' . $this->background), 128, 128);
    }

    /**
     * @return string
     */
    public function getBackgroundArticleAttribute(): string
    {
        return Helpers::getImageCache(Storage::disk('articles')->url(
            $this->id . '/' . $this->background), 555, 342);
    }

    /**
     * @return string
     */
    public function getBackgroundInnerAttribute(): string
    {
        return Helpers::getImageCache(Storage::disk('articles')->url(
            $this->id . '/' . $this->background), 745, 128);
    }

    /**
     * @return string
     */
    public function getUpdatedAttribute(): string
    {
        $date = Carbon::parse($this->updated_at);
        return $date->format('d-m-Y') . ', ' . $date->format('H:i');
    }
}
