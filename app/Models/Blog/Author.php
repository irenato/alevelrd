<?php

    namespace App\Models\Blog;

    use App\Helpers\Helpers;
    use Dimsav\Translatable\Translatable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Storage;

    class Author extends Model
    {

        use Translatable;

        protected $fillable
            = [
                'active',
                'thumbnail',
                'email',
            ];

        public $translatedAttributes
            = [
                'first_name',
                'last_name',
            ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
         */
        public function authors_articles()
        {
            return $this->belongsToMany(Article::class);
        }

        /**
         * @return string
         */
        public function getFullNameAttribute(){
            return $this->first_name . ' ' . $this->last_name;
        }

        /**
         * @return mixed
         */
        public function getThumbnailMiniAttribute(){
            return Helpers::getImageCache(Storage::disk('authors')->url(  $this->id . '/' . $this->thumbnail  ), 128, 128);
        }
    }
