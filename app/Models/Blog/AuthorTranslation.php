<?php

    namespace App\Models\Blog;

    use Illuminate\Database\Eloquent\Model;

    class AuthorTranslation extends Model
    {
        protected $fillable
            = [
                'first_name',
                'last_name',
            ];
    }
