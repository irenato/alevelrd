<?php

namespace App\Models\Course;

use App\Helpers\Helpers;
use App\Models\Locations\City;
use App\Models\Review\Review;
use App\Models\Student\Student;
use App\Models\Teacher\Teacher;
use App\Models\Test\CourseTestAnswer;
use App\Models\Test\Test;
use App\Models\Test\TestAnswer;
use App\Models\Test\Ttype;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Self_;

class Course extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $appends
        = [
            'days_left',
        ];

    const ACTION_MIN_INTERVAL = 30;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses_teachers()
    {
        return $this->belongsToMany(Teacher::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses_students()
    {
        return $this->belongsToMany(Student::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses_cities()
    {
        return $this->belongsToMany(City::class, 'course_city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses_ttypes()
    {
        return $this->belongsToMany(Ttype::class);
    }

    /**
     * @return mixed
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses_answers()
    {
        return $this->belongsToMany(TestAnswer::class)
            ->withPivot('test_id', 'test_answer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses_tests()
    {
        return $this->belongsToMany(Test::class, 'course_test_answer');
    }

    /**
     * Выбираем переводы по локале
     *
     * @return mixed
     */
    public function test_answers()
    {
        return $this->belongsToMany(TestAnswer::class)->orderBy('test_id');
    }

    /**
     * Выбираем переводы по локале
     *
     * @return mixed
     */
    public function students_reviews()
    {
        return $this->hasMany(Review::class)
            ->where('from_student', Review::STUDENT)->where('active', 1);
    }

    /**
     * Выбираем переводы по локале
     *
     * @return mixed
     */
    public function companies_reviews()
    {
        return $this->hasMany(Review::class)
            ->where('from_student', Review::COMPANY)->where('active', 1);
    }

    /**
     * @return mixed
     */
    public function programs()
    {
        return $this->hasMany(CourseProgram::class)->orderBy('period');
    }

    /**
     * @return mixed
     */
    public function images()
    {
        return $this->hasMany(CourseImage::class)->orderBy('sort');
    }

    /**
     * @return mixed
     */
    public function type()
    {
        return $this->BelongsTo(Type::class)->withTrashed();
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getFilteredDescriptionAttribute()
    {
        return str_replace('&ndash;', '-', strip_tags($this->description));
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getFilteredIntrotextAttribute()
    {
        return strip_tags($this->introtext);
    }


    /**
     * @return string
     */
    public function getSmallImageAttribute()
    {
        return Helpers::getImageCache(Storage::disk('courses')->url($this->id
            . '/' . $this->logo), 80, 80);
    }

    /**
     * @return string
     */
    public function getBigImageAttribute()
    {
        return Helpers::getImageCache(Storage::disk('courses')->url($this->id
            . '/' . $this->thumbnail), 600, 600);
    }

    /**
     * @return mixed
     */
    public function getCostOldAttribute($value)
    {
        $excluded = Helpers::getConfig('excluded_courses');
        if(!$excluded || !in_array($this->id, explode(',', $excluded))) {
            $discount = Helpers::getDiscount();
            return $discount > 0 ? $this->cost : $value;
        }
        return '';
    }

    /**
     * @return mixed
     */
    public function getActionCostAttribute()
    {
        $excluded = Helpers::getConfig('excluded_courses');
        if(!$excluded || !in_array($this->id, explode(',', $excluded))) {
            $discount = Helpers::getDiscount();
            return $discount > 0 ? round($this->cost - ($this->cost * ($discount
                        / 100)), -1) : $this->cost;
        }
        return $this->cost;
    }

    /**
     * @return string
     */
    public function getFullLinkAttribute()
    {
        return route('course', ['course_alias' => $this->alias]);
    }

    /**
     * @return int
     */
    public function getDaysLeftAttribute(): int
    {
        if ($this->discount_end_date
            && $daysLeft
                = Carbon::now()
                ->diffInDays(Carbon::parse($this->discount_end_date)->addDay(),
                    false)
        ) {
            return $daysLeft > 0 ? $daysLeft : 0;
        }
        return 0;
    }

    /**
     * @return float|int
     */
    public function getDiscountPriceAttribute(): float
    {
        if ($this->days_left > 0 && $this->discount > 0) {
            return round($this->cost - ($this->cost * ($this->discount
                        / 100)), -1);
        }
        return 0;
    }
}
