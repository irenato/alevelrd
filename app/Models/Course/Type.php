<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{

    use SoftDeletes;

    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
