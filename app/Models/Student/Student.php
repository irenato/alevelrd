<?php

namespace App\Models\Student;

use App\Models\Course\Course;
use App\Models\Locations\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->BelongsTo(City::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses_students()
    {
        return $this->belongsToMany(Course::class);
    }
}
