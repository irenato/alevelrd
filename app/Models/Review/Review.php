<?php

namespace App\Models\Review;

use App\Models\Course\Course;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    const STUDENT = 1;
    const COMPANY = 0;

    protected $guarded = ['id'];
    /**
     * @return mixed
     */
    public function course()
    {
        return $this->BelongsTo(Course::class)->withTrashed();
    }

}
