<?php

namespace App\Models\Partner;

use App\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Partner extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['smallImage', 'bigImage'];

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getLinksAttribute($value)
    {
        return json_decode($value, true);
    }

    public function urlAttribute(){
        return '<a href="' . $this->link . '">' . $this->title . '</a>';
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getSmallImageAttribute()
    {
        return Storage::disk('partners')->url(  $this->id . '/' . $this->logo  );
    }

    /**
     * @return string
     */
    public function getBigImageAttribute(){
        return Storage::disk('partners')->url(  $this->id . '/' . $this->thumbnail  );
    }
}
