<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

class Sociallink extends Model
{
    protected $guarded = ['id'];
}
