<?php

namespace App\Models\Redirect;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $fillable = ['source', 'target'];
}
