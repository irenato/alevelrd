<?php

namespace App\Models\Application;

use App\Models\Course\Course;
use App\Models\Locations\City;
use Illuminate\Database\Eloquent\Model;

class Teachapplication extends Model
{
    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function course()
    {
        return $this->BelongsTo(Course::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->BelongsTo(City::class);
    }
}
