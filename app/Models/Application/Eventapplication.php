<?php

    namespace App\Models\Application;

    use App\Models\Event\Event;
    use Illuminate\Database\Eloquent\Model;

    class Eventapplication extends Model
    {
        protected $guarded = ['id'];

        /**
         * @return mixed
         */
        public function event()
        {
            return $this->BelongsTo(Event::class)->withTrashed();
        }
    }
