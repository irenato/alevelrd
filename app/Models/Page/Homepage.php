<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

class Homepage extends Model
{
    protected $guarded = ['id'];
}
