<?php

namespace App\Models\Test;

use Illuminate\Database\Eloquent\Model;

class TestContent extends Model
{
    protected $fillable = [
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'seo_canonical',
    ];
}
