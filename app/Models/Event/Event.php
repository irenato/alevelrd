<?php

    namespace App\Models\Event;

    use App\Helpers\Helpers;
    use App\Models\Locations\City;
    use App\Models\Teacher\Teacher;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Illuminate\Support\Facades\Storage;

    class Event extends Model
    {

        use SoftDeletes;

        protected $guarded = ['id'];

        protected $appends = ['smallImage', 'bigImage', 'fullLink', 'filteredDescription', 'filteredIntrotext'];

        /**
         * @return mixed
         */
        public function images()
        {
            return $this->hasMany(EventImage::class)->orderBy('sort');
        }

        /**
         * @return mixed
         */
        public function city()
        {
            return $this->BelongsTo(City::class)->withTrashed();
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function events_teachers()
        {
            return $this->belongsToMany(Teacher::class);
        }

        /**
         * @param $value
         * @return string
         */
        public function getDescriptionAttribute($value)
        {
            return isset($value) ? $value : '';
        }

        /**
         * @param $value
         *
         * @return mixed
         */
        public function getTimelineAttribute($value)
        {
            return json_decode($value, true);
        }

        /**
         * @param $value
         *
         * @return mixed
         */
        public function getFilteredDescriptionAttribute()
        {
            return strip_tags($this->description);

        }

        /**
         * @param $value
         *
         * @return mixed
         */
        public function getFilteredIntrotextAttribute()
        {
            return strip_tags($this->introtext);

        }

        /**
         * @param $value
         *
         * @return mixed
         */
        public function getSmallImageAttribute()
        {
            return Helpers::getImageCache(Storage::disk('events')->url($this->id . '/' . $this->thumbnail), 80, 80);
        }

        /**
         * @return string
         */
        public function getBigImageAttribute()
        {
            return Helpers::getImageCache(Storage::disk('events')->url($this->id . '/' . $this->thumbnail), 600, 600);
        }

        /**
         * @return string
         */
        public function getFullLinkAttribute()
        {
            return $this->is_external ? $this->alias : route('event', ['event_alias' => $this->alias]);
        }
    }
