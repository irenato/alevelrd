<?php

namespace App\Models\Teacher;

use App\Helpers\Helpers;
use App\Models\Course\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Teacher extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $appends = ['smallImage', 'bigImage'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses_teachers()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getLinksAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getSmallImageAttribute()
    {
        return Helpers::getImageCache(Storage::disk('teachers')->url(  $this->id . '/' . $this->thumbnail  ), 80, 80);
    }

    /**
     * @return string
     */
    public function getBigImageAttribute(){
        return Helpers::getImageCache(Storage::disk('teachers')->url(  $this->id . '/' . $this->thumbnail  ), 600, 600);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(){
        return $this->last_name . ' ' . $this->first_name;
    }
}
