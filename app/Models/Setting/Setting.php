<?php

    namespace App\Models\Setting;

    use Illuminate\Database\Eloquent\Model;

    class Setting extends Model
    {
        const CACHE_PATH = 'framework/cache/settings.php';

        protected $guarded = ['id'];

        protected $table = 'system_settings';
    }
