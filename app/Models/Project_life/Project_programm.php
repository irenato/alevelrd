<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project_programm extends Model
{
    protected $table = 'project_life_programm';

    public function speakers()
    {
     return $this->belongsToMany(Project_life_speaker::class, Project_programm_to_speaker::tablename(), 'programm_id', 'speaker_id');
    }

    public function timeline()
    {
        return $this->hasOne(Timeline::class, 'programm_id');
    }

    public function callback()
    {
        return $this->belongsToMany(Projectlife_callbacks::class, Project_to_callback::tablename(), 'programm_id', 'callback_id');
    }




}
