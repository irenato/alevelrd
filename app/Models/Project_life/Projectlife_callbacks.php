<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Projectlife_callbacks extends Model
{

    public function workshop()
    {
        return $this->belongsToMany(Project_programm::class, Project_to_callback::tablename(), 'callback_id', 'programm_id');
    }

}
