<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Project_life_slider extends Model
{
    protected $table = 'project_life_slider';
}
