<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    protected $table = 'timeline';


    public function programm()
    {
        return $this->belongsTo(Project_programm::class);
    }

    public function time()
    {
        return $this->belongsTo(Time::class,'id');
    }

}
