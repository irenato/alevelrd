<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{

  public function timeline()
    {
        return $this->hasMany(Timeline::class,'time');
    }

}
