<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Project_life_speaker extends Model
{
    protected $table = 'project_life_speakers';

    public function projects()
    {
        return $this->belongsToMany(Project_programm::class, Project_programm_to_speaker::tablename(), 'id', 'programm_id');
    }
}
