<?php

namespace App\Models\Creator;

use App\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Creator extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['smallImage', 'bigImage'];
    
    public function getDescriptionAttribute($value){
        return strip_tags($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getSmallImageAttribute()
    {
        return Helpers::getImageCache(Storage::disk('creators')->url(  $this->id . '/' . $this->thumbnail  ), 80, 80);
    }

    /**
     * @return string
     */
    public function getBigImageAttribute(){
        return Helpers::getImageCache(Storage::disk('creators')->url(  $this->id . '/' . $this->thumbnail  ), 800, 800);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getSocialLinksAttribute($value)
    {
        return json_decode($value, true);
    }
}
