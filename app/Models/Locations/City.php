<?php

    namespace App\Models\Locations;

    use App\Models\Course\Course;
    use App\Models\Student\Student;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class City extends Model
    {
        use SoftDeletes;

        const DEFAULT_CITY = 1;

        protected $guarded = ['id'];

        protected $casts
            = [
                'phones'       => 'array',
                'social_links' => 'array',
            ];

        /**
         * @return mixed
         */
        public function country()
        {
            return $this->BelongsTo(Country::class)->withTrashed();
        }

        /**
         * @return mixed
         */
        public function students()
        {
            return $this->hasMany(Student::class)->withTrashed();
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function courses_cities()
        {
            return $this->belongsToMany(Course::class);
        }
    }
