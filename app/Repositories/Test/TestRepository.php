<?php
    /**
     * Created by PhpStorm.
     * User: user
     * Date: 30.10.18
     * Time: 10:51
     */

    namespace App\Repositories\Test;


    use App\Models\Client\Address;
    use App\Models\Client\Client;
    use App\Models\Filters\FilterValue;
    use App\Models\Product\Pgroup;
    use App\Models\Product\Product;
    use App\Models\Product\ProductImage;
    use App\Models\Test\Test;
    use App\Models\Test\TestAnswer;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Storage;
    use Ramsey\Uuid\Uuid;

    class TestRepository
    {
        protected $request;

        /**
         * TestRepository constructor.
         *
         * @param Request $request
         */
        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        /**
         * @return mixed
         */
        public function storeTest()
        {
            $test    = Test::create($this->request->except(['answers', '_token']));
            $answers = [];
            foreach ($this->request->answers as $key => $answer) {
                foreach ($answer as $k => $value) {
                    $answers[$k][$key] = $value;
                }

            }
            foreach ($answers as $key => $answer) {
                $answer['position'] = $key;
                $test->answers()->create($answer);
            }
            if ($this->request->typesIds) {
                $test->tests_ttypes()->sync($this->request->typesIds);
            }
            return $test;
        }

        /**
         * @param $id
         *
         * @return mixed
         */
        /**
         * @return mixed
         */
        public function updateTest($id)
        {
            $test = Test::findOrFail($id);
            $test->update($this->request->except(['answers', '_token']));
            $answers = [];
            $ids     = [];
            foreach ($this->request->answers as $key => $answer) {
                foreach ($answer as $k => $value) {
                    $answers[$k][$key] = $value;
                }
            }
            foreach ($answers as $key => $answer) {
                $answer['position'] = $key;
                if (!isset($answer['id'])) {
                    $answer['test_id'] = $test->id;
                    $answerSingle      = TestAnswer::create($answer);
                    array_push($ids, $answerSingle->id);
                } else {
                    $answerSingle = TestAnswer::findOrFail($answer['id']);
                    $answerSingle->update($answer);
                    array_push($ids, $answerSingle->id);
                }
            }
            if ($ids) {
                TestAnswer::where('test_id', $test->id)->whereNotIn('id', $ids)->delete();
            }
            if ($this->request->typesIds) {
                $test->tests_ttypes()->sync($this->request->typesIds);
            }

            return $test;
        }

    }