<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.10.18
 * Time: 10:51
 */

namespace App\Repositories\Blog;


use App\Http\Requests\Blog\ArticleRequest;
use App\Models\Blog\Article;
use App\Models\Blog\Author;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BlogRepository
{
    protected $request;
    const VIEWED          = 'viewed';
    const LIKED           = 'liked';
    const COOKIE_LIFETIME = 1440;
    const MAIN_RELATIONS
                          = [
            'translations',
            'authors_articles',
            'tags_articles',
            'tags_articles.htmlClass',
        ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param ArticleRequest $request
     *
     * @return Article
     */
    public function viewInner($alias): array
    {
        $article          = Article::with(
            array_merge(
                self::MAIN_RELATIONS, [
                'related',
                'related.translations',
                'related.authors_articles',
                'related.tags_articles',
                'related.tags_articles.htmlClass',
            ]))
            ->whereActive(true)
            ->whereAlias($alias)
            ->firstOrFail();
        $recentlyArticles = Article::with(self::MAIN_RELATIONS)
            ->whereActive(true)
            ->where('id', '!=', $article)
            ->latest()->limit(3)->get();
        if (Cookie::has(self::VIEWED)) {
            $viewed = json_decode(Cookie::get(self::VIEWED), true);
            if (!in_array($article->id, $viewed)) {
                $this->updateViewed($article, $viewed);
            }
        } else {
            $this->updateViewed($article, []);
        }
        return [
            'article'          => $article,
            'recentlyArticles' => $recentlyArticles,
            'ourArticles'      => $this->getOurArticles($article->id),
        ];
    }

    public function likes(): array
    {
        if (Cookie::has(self::LIKED)) {
            $likes = json_decode(Cookie::get(self::LIKED), true);
            if (!isset($likes[$this->request->id])
                || ($likes[$this->request->id] !== $this->request->field)
            ) {
                return $this->changeLiked($likes);
            }
            return [];
        } else {
            return $this->changeLiked();
        }
        return [];
    }

    public function offerArticle(): Article
    {
        $authorName = explode(' ', $this->request->username);
        $author     = Author::whereEmail($this->request->email)->first();
        if (!$author) {
            $author = Author::create([
                'email'          => $this->request->email,
                App::getLocale() => [
                    'first_name' => $authorName[0],
                    'last_name'  => isset($authorName[1]) ? $authorName[1] : '',
                ],
            ]);
        }
        $article = Article::create([
            'alias'          => str_slug($this->request->title) . '-' . rand
                (0, 999),
            App::getLocale() => [
                'title'   => $this->request->get('title'),
                'content' => $this->request->get('content'),
            ],
        ]);
        $this->attachImages($article);
        $article->authors_articles()->attach([$author->id]);
        return $article;
    }

    protected function attachImages(Article $article): bool
    {
        if ($this->request->has('thumbnail')
            || $this->request->has('background')
        ) {
            if ($this->request->has('thumbnail')) {
                $article->thumbnail
                    = md5($this->request->thumbnail->getClientOriginalName())
                    . '.'
                    . $this->request->thumbnail->getClientOriginalExtension();
                File::delete(storage_path('public/images/articles/' .
                    $article->id . '/' . $article->thumbnail));
                Storage::disk('articles')
                    ->put($article->id . '/' . $article->thumbnail,
                        File::get($this->request->thumbnail));
            }
            if ($this->request->background) {
                $article->background
                    = md5($this->request->background->getClientOriginalName())
                    . '.'
                    . $this->request->background->getClientOriginalExtension();
                File::delete(storage_path('public/images/articles/' .
                    $article->id . '/' . $article->background));
                Storage::disk('articles')
                    ->put($article->id . '/' . $article->background,
                        File::get($this->request->background));
            }
            return $article->update();
        }
        return false;
    }

    protected function updateViewed(Article $article, array $viewed): bool
    {
        array_push($viewed, $article->id);
        $article->increment(self::VIEWED);
        Cookie::queue(self::VIEWED, json_encode($viewed),
            self::COOKIE_LIFETIME);
        return $article->update();
    }

    protected function getOurArticles(int $articleId): Collection
    {
        return Article::with(self::MAIN_RELATIONS)
            ->whereActive(true)
            ->whereIsOuter(false)
            ->where('id', '!=', $articleId)
            ->latest()
            ->limit(4)->get();
    }

    protected function changeLiked(array $liked = []): array
    {
        $altField = $this->request->field == 'likes' ? 'dislikes' : 'likes';
        $article  = Article::findOrFail($this->request->id);
        if ($liked && isset($liked[$this->request->id])
            && $liked[$this->request->id] == $altField
        ) {
            if ($article->{$altField} > 0) {
                $article->decrement($altField);
            }
            unset($liked[$this->request->id]);
        }
        $liked[$this->request->id] = $this->request->field;
        Cookie::queue(self::LIKED, json_encode($liked),
            self::COOKIE_LIFETIME);
        $article->increment($this->request->field);
        $article->update();

        return array_merge($article->only('id', 'likes', 'dislikes'));
    }
}
