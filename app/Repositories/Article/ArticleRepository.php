<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.10.18
 * Time: 10:51
 */

namespace App\Repositories\Article;


use App\Http\Requests\Blog\ArticleRequest;
use App\Models\Blog\Article;
use Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ArticleRepository
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param ArticleRequest $request
     *
     * @return Article
     */
    public function store(): Article
    {
        $data = $this->request->all();
        $data['likes'] = $data['likes'] ?? 0;
        $data['dislikes'] = $data['dislikes'] ?? 0;
        $data['position'] = $data['position'] ?? 0;
        $data['viewed'] = $data['viewed'] ?? 0;
        $article = Article::create($data);
        $article->authors_articles()->sync($this->request->authors_articles);
        $article->tags_articles()->sync($this->request->tags_articles);
        $article->related()->sync($this->request->related);
        $this->attachImages($article);

        return $article;
    }

    /**
     * @param $id
     *
     * @return Article
     */
    public function update($id): Article
    {
        $data = $this->request->all();
        $data['likes'] = $data['likes'] ?? 0;
        $data['dislikes'] = $data['dislikes'] ?? 0;
        $data['position'] = $data['position'] ?? 0;
        $data['viewed'] = $data['viewed'] ?? 0;
        $article = Article::findOrFail($id);
        $article->update($data);
        $article->authors_articles()->sync($this->request->authors_articles);
        $article->tags_articles()->sync($this->request->tags_articles);
        $article->related()->sync($this->request->related);
        $this->attachImages($article);

        return $article;
    }

    protected function attachImages(Article $article): bool
    {
        if ($this->request->has('thumbnail')
            || $this->request->has('background')
        ) {
            if ($this->request->has('thumbnail')) {
                $article->thumbnail
                    = md5($this->request->thumbnail->getClientOriginalName())
                    . '.'
                    . $this->request->thumbnail->getClientOriginalExtension();
                File::delete(storage_path('public/images/articles/' .
                    $article->id . '/' . $article->thumbnail));
                Storage::disk('articles')
                    ->put($article->id . '/' . $article->thumbnail,
                        File::get($this->request->thumbnail));
            }
            if ($this->request->background) {
                $article->background
                    = md5($this->request->background->getClientOriginalName())
                    . '.'
                    . $this->request->background->getClientOriginalExtension();
                File::delete(storage_path('public/images/articles/' .
                    $article->id . '/' . $article->background));
                Storage::disk('articles')
                    ->put($article->id . '/' . $article->background,
                        File::get($this->request->background));
            }
            return $article->update();
        }
        return false;
    }

}