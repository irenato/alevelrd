<?php
/**
 * Created by PhpStorm.
 * User: bredevil
 * Date: 11.03.2019
 * Time: 12:37
 */

Route::group(['prefix' => 'backend'], function() {

    Route::group(['middleware' => ['guest']], function() {
        Route::get('/', 'Auth\LoginController@loginForm')->name('login');
        Route::post('/', 'Auth\LoginController@login')->name('auth');
    });
    Route::group(['middleware' => ['guest']], function() {
        Route::get('/login', 'Auth\LoginController@loginForm')->name('login');
        Route::post('/', 'Auth\LoginController@login')->name('auth');
    });

    // admin panel
    Route::group(['middleware' => ['auth']], function() {
        Route::get('/dashboard', 'DashboardController@index');
        Route::get('/logout', 'Auth\LoginController@logout');
        // admin panel
        Route::group(['middleware' => ['auth']], function() {
            Route::get('/', 'DashboardController@index');
            Route::get('/dashboard', 'DashboardController@index')
                ->name('dashboard');
            Route::get('/logout', 'Auth\LoginController@logout');

            // pages
            Route::resource('homepage', 'Backend\Page\HomepageController');
            Route::post('opinions/sort/', 'Backend\Page\OpinionController@sort')
                ->name('opinions.sort');
            Route::resource('opinions', 'Backend\Page\OpinionController');
            Route::resource('creatorpage',
                'Backend\Page\CreatorpageController');

            Route::resource('discounts', 'Backend\Discount\DiscountController');

            // seo
            Route::resource('profile', 'Backend\Profile\ProfileController');

            Route::resource('seoscripts', 'Backend\Seo\SeoscriptController');
            Route::resource('redirects', 'Backend\Seo\RedirectController');
            Route::resource('sitemap', 'Backend\Seo\SitemapController');

            // contacts
            Route::resource('phones', 'Backend\Contact\PhoneController');
            Route::resource('sociallinks',
                'Backend\Contact\SociallinkController');
            Route::resource('cities', 'Backend\Contact\CityController');

            Route::resource('tests', 'Backend\Test\TestController');
            Route::resource('ttypes', 'Backend\Test\TtypeController');
            Route::resource('course-tests',
                'Backend\Test\CourseTestController');
            Route::resource('test-content', 'Backend\Test\TestPageController');

            // courses
            Route::group(['middleware' => ['permission:courses']], function() {
                // types
                Route::resource('types', 'Backend\Course\TypeController');
                // actions
                Route::resource('actions', 'Backend\Course\ActionController');
                // courses gallery
                Route::group(['prefix' => 'courses/image'], function() {
                    Route::post('upload/{id}',
                        'Backend\Course\CourseImageController@uploadImage');
                    Route::post('get/{id}',
                        'Backend\Course\CourseImageController@getImages');
                    Route::post('delete',
                        'Backend\Course\CourseImageController@deleteImage');
                    Route::post('get',
                        'Backend\Course\CourseImageController@getImage');
                    Route::post('set',
                        'Backend\Course\CourseImageController@setImage');
                    Route::post('sort',
                        'Backend\Course\CourseImageController@sortImage');
                });

                // courses
                Route::post('courses/sort/',
                    'Backend\Course\CourseController@sort');
                Route::post('courses/disable-action',
                    'Backend\Course\CourseController@disableAction')
                    ->name('courses.disable-action');
                Route::resource('courses', 'Backend\Course\CourseController');

                // events gallery
                Route::group(['prefix' => 'events/image'], function() {
                    Route::post('upload/{id}',
                        'Backend\Event\EventImageController@uploadImage');
                    Route::post('get/{id}',
                        'Backend\Event\EventImageController@getImages');
                    Route::post('delete',
                        'Backend\Event\EventImageController@deleteImage');
                    Route::post('get',
                        'Backend\Event\EventImageController@getImage');
                    Route::post('set',
                        'Backend\Event\EventImageController@setImage');
                    Route::post('sort',
                        'Backend\Event\EventImageController@sortImage');
                });

                // events
                Route::resource('events', 'Backend\Event\EventController');
            });

            // teachers
            Route::group(['middleware' => ['permission:teachers']], function() {
                Route::post('teachers/sort/',
                    'Backend\Teacher\TeacherController@sort');
                Route::resource('teachers',
                    'Backend\Teacher\TeacherController');
            });

            // settings
            Route::group(['middleware' => ['permission:settings']], function() {

                // settings
                Route::get('settings/empty_cache',
                    'SettingsController@setEmptyCache')->name('clear-cache');
                Route::get('settings/optimize-images',
                    'SettingsController@optimizeImages')
                    ->name('optimize-images');
                Route::resource('settings', 'SettingsController');

                // configs (telegram)
                Route::resource('telegram-configs',
                    'Backend\Config\TelegramconfigController');

                // users
                Route::resource('users', 'UsersController');

                // permissions
                Route::resource('permissions', 'PermissionsController');
                Route::post('permissions/remove/',
                    'PermissionsController@remove');

                // roles
                Route::resource('roles', 'RolesController');
                Route::post('roles/remove/', 'RolesController@remove');

                // creators
                Route::post('creators/sort/',
                    'Backend\Creator\CreatorController@sort');
                Route::resource('creators',
                    'Backend\Creator\CreatorController');
            });

            // students
            Route::group(['middleware' => ['permission:students']], function() {
                Route::resource('students',
                    'Backend\Student\StudentController');
            });

            // partners
            Route::group(['middleware' => ['permission:partners']], function() {
                Route::post('partners/sort/',
                    'Backend\Partner\PartnerController@sort');
                Route::resource('partners',
                    'Backend\Partner\PartnerController');
            });

            // applications
            Route::group(['middleware' => ['permission:applications']],
                function() {
                    // reviews
                    Route::resource('reviews',
                        'Backend\Review\ReviewController');

                    // applications(students)

                    Route::get('stapplications/export/',
                        'Backend\Application\StapplicationController@export')
                        ->name('stapplications.export');
                    Route::get('stapplications/update-all/',
                        'Backend\Application\StapplicationController@updateAll')
                        ->name('stapplications.update-all');
                    Route::resource('stapplications',
                        'Backend\Application\StapplicationController');

                    // applications(teachers)
                    Route::get('teachapplications/export/',
                        'Backend\Application\TeachapplicationController@export')
                        ->name('teachapplications.export');
                    Route::get('teachapplications/update-all/',
                        'Backend\Application\TeachapplicationController@updateAll')
                        ->name('teachapplications.update-all');
                    Route::resource('teachapplications',
                        'Backend\Application\TeachapplicationController');

                    // applications(callback)
                    Route::get('callbackapplications/update-all/',
                        'Backend\Application\CallbackapplicationController@updateAll')
                        ->name('callbackapplications.update-all');
                    Route::resource('callbackapplications',
                        'Backend\Application\CallbackapplicationController');

                    // applications(partners)
                    Route::get('partnerapplications/update-all/',
                        'Backend\Application\PartnerapplicationController@updateAll')
                        ->name('partnerapplications.update-all');
                    Route::resource('partnerapplications',
                        'Backend\Application\PartnerapplicationController');

                    // applications(events)
                    Route::get('eventapplications/export/',
                        'Backend\Application\EventapplicationController@export')
                        ->name('eventapplications.export');
                    Route::get('eventapplications/update-all/',
                        'Backend\Application\EventapplicationController@updateAll')
                        ->name('eventapplications.update-all');
                    Route::resource('eventapplications',
                        'Backend\Application\EventapplicationController');
                });

            // rewards
            Route::group(['middleware' => ['permission:rewards']], function() {

                Route::post('rewards/sort/',
                    'Backend\Reward\RewardController@sort');
                Route::resource('rewards', 'Backend\Reward\RewardController');
            });

            // blog
            Route::group(['prefix' => 'blog', 'as' => 'blog.'], function() {
                Route::resource('html-classes',
                    'Backend\Blog\HtmlClassController');
                Route::resource('tags', 'Backend\Blog\TagController');
                Route::resource('authors', 'Backend\Blog\AuthorController');
                Route::post('articles/sort',
                    'Backend\Blog\ArticleController@sort')
                    ->name('articles.sort');
                Route::resource('articles', 'Backend\Blog\ArticleController');
                Route::resource('blogs-content', 'Backend\Blog\BlogController')
                    ->except(['delete', 'show']);
                Route::group([
                    'prefix' => 'subscribers',
                    'as'     => 'subscribers.',
                ],
                    function() {
                        Route::get('/',
                            'Backend\Blog\SubscriberController@index')
                            ->name('index');
                        Route::get('/export',
                            'Backend\Blog\SubscriberController@export')
                            ->name('export');
                    });
            });

            // promocodes
            Route::group(['middleware' => ['permission:rewards']], function() {
                Route::resource('promocodes',
                    'Backend\Promocode\PromocodeController');
            });
            // hub
            Route::group(['middleware' => ['permission:hub']], function() {
                Route::get('hub-settings', 'Backend\Hub\HubController@index')
                    ->name('admin_hub');
                Route::get('hub-rooms', 'Backend\Hub\HubController@rooms')
                    ->name('hub_rooms');
                Route::get('hub-room-edit/{id}/edit',
                    'Backend\Hub\HubController@room_edit')
                    ->name('hub_room_edit');
                Route::get('hub-room-add/',
                    'Backend\Hub\HubController@add_room_index')
                    ->name('add_room_index');
                Route::get('hub-message/',
                    'Backend\Hub\HubMessageController@index')
                    ->name('hub_message');
                Route::get('hub-message/update-all/',
                    'Backend\Hub\HubMessageController@updateAll')
                    ->name('hub_message_show.update-all');
                Route::get('hub-message/{id}',
                    'Backend\Hub\HubMessageController@show')
                    ->name('hub_message_show');

            });
            Route::post('hub-settings-update',
                'Backend\Hub\HubController@update')->name('admin_hub_update');
            Route::post('hub-room-update/{id}',
                'Backend\Hub\HubController@room_update')
                ->name('admin_hub_room_update');
            Route::post('hub-room-create',
                'Backend\Hub\HubController@room_create')
                ->name('admin_hub_room_create');
            Route::post('hub-room-del/{id}',
                'Backend\Hub\HubController@room_del')
                ->name('admin_hub_room_del');

            // projectLife
            Route::group(['middleware' => ['permission:projectlife']],
                function() {
                    Route::get('project-life',
                        'Backend\ProjectLife\ProjectLifeController@index')
                        ->name('project_admin');
                    Route::post('projcet_life_update',
                        'Backend\ProjectLife\ProjectLifeController@update')
                        ->name('project_life_update');
                    Route::get('project-life-speakers',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@index')
                        ->name('project_speakers');
                    Route::get('speaker/{id}/edit',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@edit')
                        ->name('speaker_edit');
                    Route::post('projcet_speaker_update/{id}',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@update')
                        ->name('project_speaker_update');
                    Route::get('projcet_speaker_add',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@add')
                        ->name('project_speaker_add');
                    Route::post('projcet_speaker_creates',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@create')
                        ->name('project_speaker_create');


                    Route::post('projcet_speaker_del/{id}',
                        'Backend\ProjectLife\ProcetLifeSpeakersController@del')
                        ->name('project_speaker_dell');
                    Route::get('project-programms',
                        'Backend\ProjectLife\ProjectProgrammController@index')
                        ->name('project_programms');
                    Route::get('project-programms/{id}/edit',
                        'Backend\ProjectLife\ProjectProgrammController@edit')
                        ->name('project_programms_edit');
                    Route::get('project-programms/add',
                        'Backend\ProjectLife\ProjectProgrammController@add')
                        ->name('project_programms_add');
                    Route::post('project-programms_update/{id}',
                        'Backend\ProjectLife\ProjectProgrammController@update')
                        ->name('project_programm_update');

                    Route::post('project-programms-del/{id}',
                        'Backend\ProjectLife\ProjectProgrammController@del')
                        ->name('project_programms_del');

                    Route::get('project-programms_timeline',
                        'Backend\ProjectLife\ProjectProgrammController@timeline')
                        ->name('project_timeline');
                    Route::post('projcet_speaker_create/',
                        'Backend\ProjectLife\ProjectProgrammController@create')
                        ->name('project_program_create');
                    Route::post('programms_timeline_update/',
                        'Backend\ProjectLife\ProjectProgrammController@timeline_update')
                        ->name('project_timeline_update');
                    Route::get('project_partners/',
                        'Backend\ProjectLife\ProcetLifePartnersController@index')
                        ->name('partners');
                    Route::get('project_partners_edit/{id}/edit',
                        'Backend\ProjectLife\ProcetLifePartnersController@edit')
                        ->name('partners_edit');
                    Route::get('project_partners_add/',
                        'Backend\ProjectLife\ProcetLifePartnersController@add')
                        ->name('partners_add');
                    Route::post('project_partners_create/',
                        'Backend\ProjectLife\ProcetLifePartnersController@create')
                        ->name('project_partners_create');
                    Route::post('project_partners_update/{id}',
                        'Backend\ProjectLife\ProcetLifePartnersController@update')
                        ->name('project_partners_update');
                    Route::post('projcet_partners_del/{id}',
                        'Backend\ProjectLife\ProcetLifePartnersController@del')
                        ->name('project_partners_dell');


                    Route::get('project_callbacks/',
                        'Backend\ProjectLife\ProjectLifeCallbackController@index')
                        ->name('project_callbacks');
                    Route::get('project_message/{id}',
                        'Backend\ProjectLife\ProjectLifeCallbackController@show')
                        ->name('project_message_show');
                    Route::post('project_message_del/{id}',
                        'Backend\ProjectLife\ProjectLifeCallbackController@del')
                        ->name('project_message_del');
                    Route::get('project_callbacks/export/',
                        'Backend\ProjectLife\ProjectLifeCallbackController@export')
                        ->name('project_callbacks_export');

                });

        });
    });
});