<?php
/**
 * Created by PhpStorm.
 * User: bredevil
 * Date: 11.03.2019
 * Time: 12:37
 */

Route::post('project_register/',
    'Frontend\ProjectLife\ProjectLifeController@register')
    ->name('project_register');

Route::get('/', 'Frontend\Home\HomeController@index')->name('home');
Route::post('teachers', 'Frontend\Home\HomeController@getTeachers')
    ->name('get-teachers');
Route::post('partners', 'Frontend\Home\HomeController@getPartners')
    ->name('get-partners');
Route::post('events', 'Frontend\Home\HomeController@getEvents')
    ->name('get-events');
Route::post('cities', 'Frontend\Home\HomeController@updateCity')
    ->name('update-city');

// life project 3.0
Route::get('/project-life', 'Frontend\ProjectLife\ProjectLifeController@index')
    ->name('project_home');


// course
Route::get('/course/{course_alias}', 'Frontend\Course\CourseController@index')
    ->name('course');

// tests
Route::group(['prefix' => 'tests', 'as' => 'tests.'], function() {
    Route::get('/result/', 'Frontend\Test\TestController@result')
        ->name('result');
    Route::any('/{id?}', 'Frontend\Test\TestController@index')
        ->name('question');
    Route::any('/check/{id?}', 'Frontend\Test\TestController@check')
        ->name('check');
});

// blog
Route::group(['prefix' => 'blog', 'as' => 'blog.'], function() {
    Route::get('/', 'Frontend\Blog\BlogController@index')
        ->name('blog');
    Route::get('/article/{alias}', 'Frontend\Blog\BlogController@inner')
        ->name('article');
    Route::post('/article/likes', 'Frontend\Blog\BlogController@likes')
        ->name('likes');
    Route::post('/subscribe', 'Frontend\Blog\BlogController@subscribe')
        ->name('subscribe');
    Route::post('/offer-article', 'Frontend\Blog\BlogController@offerArticle')
        ->name('offer-article');
});

// team
Route::get('team', 'Frontend\Creatorpage\CreatorpageController@index')
    ->name('team');
Route::post('get-team', 'Frontend\Creatorpage\CreatorpageController@getTeam')
    ->name('get-team');


//Хаб
Route::get('hub', 'Frontend\Hub\HubController@index')->name('hub');
Route::post('send_message', 'Frontend\Hub\HubController@message')
    ->name('send_message');

//
Route::get('hub', 'Frontend\Hub\HubController@index')->name('hub');

// event
Route::get('/event/{event_alias}', 'Frontend\Event\EventController@index')
    ->name('event');

// any page
Route::get('/{alias?}', 'Frontend\Redirect\RedirectController@index')
    ->where('alias', '([A-Za-z0-9\-_:/]+)')->name('page');

// forms
Route::post('add-feedback', 'Frontend\Form\FormController@addFeedback')
    ->name('add-feedback');
Route::post('add-partner', 'Frontend\Form\FormController@addPartner')
    ->name('add-partner');
Route::post('add-teacher', 'Frontend\Form\FormController@addTeacher')
    ->name('add-teacher');
Route::post('add-review', 'Frontend\Form\FormController@addReview')
    ->name('add-review');
Route::post('add-callback', 'Frontend\Form\FormController@addCallback')
    ->name('add-callback');
Route::post('add-event-application',
    'Frontend\Form\FormController@addEventApplication')
    ->name('add-event-application');