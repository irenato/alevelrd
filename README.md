
Installation

1 - Clone repository and configure file .env

2 - Generate unique key for laravel:
php artisan key:generate

3 - Update packages:
composer update

4 - Do migrations & seeds: 
php artisan migrate:fresh --seed

5 - Link to storage: 
php artisan storage:link


